﻿using System;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;
using WorldspaceController;
using Random = UnityEngine.Random;

[Serializable]
public struct CShaderHotspot
{
    public Vector3 Pos;
    public Vector2 Mod;
    public int Active;
}

// ReSharper disable once InconsistentNaming
public class GiC_LeapDataHandler : MonoBehaviour {

#region Variables 

    // Geometry Stuff
    private ComputeShader voronoicomputeShader;
    public Material plantMaterial;
    private Texture noiseTexture;
    private ComputeBuffer voronoiOutputBuffer;
    private ComputeBuffer hotspotPositionBuffer;
    public float NoiseMultiplier = 0.5f;

    private float minowskyMix = 2f;

    private float gridDistance = 1f;
    private int vertexCount = 1024;

    private readonly CShaderHotspot[] shaderHotspots = new CShaderHotspot[21];

    private int worldPosShaderHash;
    private int gridDistanceHash;
    private int minowskyMixHash;
    private int hotspotHash;
    private int noiseTexHash;
    private int noiseMultHash;
    private int bufPointsHash;
    private int outputBufferHash;
    private int plantColorArrayHash;
    private int presetDistributionHash;
    private int fiHash;

    private Mesh baseMesh;
    public Material myMaterial;

    private int csKernel;

    public Color[] PlantColors;

    private Vector3 prevPos = Vector3.zero;

    private int OscThrotle = 5;
    private int OscCounter = 0;

    private float timeDeltaBetweenOscMessages = 0.15f;
    private float acumulatedTimeDelta = 0f;
    //private bool initialized;

    public bool DebugMode;

    // GIC Related
    private ObservedList<LeapHotspot> hotspots  {get {return TheAtom.networkCore.Hotspots; } }

    private readonly Dictionary<int, GameObject> hotspotGameObjects = new Dictionary<int, GameObject>();

    public int NumHotspots { get; private set; }

    public GiC_ProceduralAtom TheAtom { get; private set; }
    public Dictionary<int, SphereTreeGrower> sphereTreeGrowers = new Dictionary<int, SphereTreeGrower>();

    // OSC
    // /g/l/d AtomID, HotspotID, PosX, PosY, Intensity, ModX, ModY, Active 
    private readonly OscMessage hotspotOscMessage = new OscMessage("/g/l/d", 0, 0, 0f, 0f, 0f, 0f, 0f, 0);

#endregion

#region Callbacks
    //private bool isStrokePointsDirty = false;
    private void LeapHotspotArray_Updated(ObservedList<LeapHotspot>.ObserverListChange arg1, int[] arg2)
    {
        // TODO: must be done in another way, because this is also called when network data arrives on any computer!!!

        //Debug.Log("AtomObject.GicPointArrays_Updated: " + arg1.ToString() + " : " + arg2);
        switch (arg1)
        {
            case ObservedList<LeapHotspot>.ObserverListChange.add:
                Debug.Log("LeapHotspot Added Remotely?: " + arg2[0]);
                break;
            case ObservedList<LeapHotspot>.ObserverListChange.removeAll:
                Debug.Log("LeapHotspot removed Remotely?: " + arg2[0]);
                break;
            case ObservedList<LeapHotspot>.ObserverListChange.change:
                Debug.Log("LeapHotspot Changed Remotely?: " + arg2[0]);
                HotspotNetworkModified(arg2[0]);
                // UpdateSingleHotspot(arg2[0]);
                // DispatchAndSetShaderBuffers();
                // SendHotspotDataOsc(arg2[0]);
                break;
        }
        //isStrokePointsDirty = true;
    }
#endregion

#region Unity Internals

    // Update is called once per frame
    private int lastDebugLeapId = -1;

    private int throttleIndex = 0;

    private void Update () {
		if (Input.GetKeyDown(KeyCode.F9)) {
            lastDebugLeapId = AddNewHotspot(new LeapHotspot(Vector3.zero, Vector2.zero, false));
            TheAtom.networkCore.SetGicHotspotsModificationFinished();
        }
        if (Input.GetKeyDown(KeyCode.F10)) {
            if (lastDebugLeapId != -1) {
                ChangeModifierData(lastDebugLeapId, new LeapHotspot(
                    new Vector3(
                        Random.Range(-10f,10f), 0, Random.Range(-10f,10f)),
                        Vector2.zero,
                        true
                ));
                TheAtom.networkCore.SetGicHotspotsModificationFinished();
            }
        }
        if (Input.GetKeyDown(KeyCode.F11)) {
            if (lastDebugLeapId != -1) {
                DeleteHotspot(lastDebugLeapId);
                lastDebugLeapId = -1;
                TheAtom.networkCore.SetGicHotspotsModificationFinished();
            }
        }
        if (Input.GetKeyDown(KeyCode.F12)) {
            for (int i=0; i<20; i++) {
                if (hotspots[i] != null) {
                    Debug.Log("hotspot "+i+" "+hotspots[i].PositionAndIntensity);
                }
            }
        }

        if (ObjectDidMove())
        {
            DispatchAndSetShaderBuffers();
        }

        myMaterial.SetFloat(fiHash, (TheAtom.Core.NormalizedAtomPlaybackTime - 0.5f) * 6.28318530717f );

	    if (!DebugMode) return;



	    UpdateMarkersArray();
	    voronoicomputeShader.SetTexture(csKernel, noiseTexHash, noiseTexture);
        UpdateShaderProperties();
	    DispatchAndSetShaderBuffers();
	}

    private void OnDestroy() {
        Debug.LogError("GiC_LeapDataHandler.OnDestroy: this gets destroyed...");
    }

    private bool ObjectDidMove()
    {
        bool returnValue = (prevPos - transform.position).sqrMagnitude > 0.075f;
        if (returnValue)
        {
            prevPos = transform.position;
        }
        return returnValue;
    }
    #endregion

#region Initialization


    public void InitializeHandler(GiC_ProceduralAtom parentAtom)
    {
        if (!LoadResources()) {
            Debug.LogError(name + " @ " + gameObject.name + " Could not Load Resources, abort");
            return;
        }
        TheAtom = parentAtom;
        for (var i = 0; i < 21; i++) {
            hotspots.Add(new LeapHotspot(Vector3.zero, Vector3.zero, false));
        }

        for (var i = 0; i < 21; i++) {
            hotspotGameObjects.Add(i, null);
        }

        GetShaderHashes();
        CreateGeometry();
        SetColors(GiC_LeapMakeHotspots.Instance.PlantColors);
        SetHeightMap(GiC_LeapMakeHotspots.Instance.TerrainHeight);
        SetAtomAsHotspot();
        UpdateMarkersArray();
        InitializeBuffers();
        DispatchAndSetShaderBuffers();

        //initialized = true;
        // this cannot be used this way, this is called on all clients in the network when data arrives or the data is modified locally
        // Hotspots.Updated += LeapHotspotArray_Updated;
    }

    private void InitializeBuffers()
    {
        voronoiOutputBuffer = new ComputeBuffer(vertexCount, sizeof(float) * 6);
        hotspotPositionBuffer = new ComputeBuffer(shaderHotspots.Length, sizeof(float) * 5 + sizeof(int));

        voronoicomputeShader.SetBuffer(csKernel, outputBufferHash, voronoiOutputBuffer);
        voronoicomputeShader.SetBuffer(csKernel, hotspotHash, hotspotPositionBuffer);
        voronoicomputeShader.SetTexture(csKernel, noiseTexHash, noiseTexture);
        voronoicomputeShader.SetFloat(noiseMultHash, NoiseMultiplier);
        voronoicomputeShader.SetFloat(gridDistanceHash, gridDistance);
        voronoicomputeShader.SetFloat(minowskyMixHash, minowskyMix);
        myMaterial.SetBuffer(bufPointsHash, voronoiOutputBuffer);
    }

    private void UpdateShaderProperties()
    {
        voronoicomputeShader.SetFloat(noiseMultHash, NoiseMultiplier);
        voronoicomputeShader.SetFloat(gridDistanceHash, gridDistance);
        voronoicomputeShader.SetFloat(minowskyMixHash, minowskyMix);
    }

    private void UpdatePlantDistributionModifier(Vector3 pDist)
    {
        Vector4 newpDist = new Vector4(pDist.x, pDist.y, pDist.z, 0f);
        myMaterial.SetVector(presetDistributionHash, newpDist);
    }

    private void GetShaderHashes()
    {
        csKernel = voronoicomputeShader.FindKernel("CSMain");
        worldPosShaderHash = Shader.PropertyToID("_WorldPos");
        hotspotHash = Shader.PropertyToID("HotspotPositions");
        minowskyMixHash = Shader.PropertyToID("minowskyMix");
        gridDistanceHash = Shader.PropertyToID("gridDistance");
        noiseTexHash = Shader.PropertyToID("_NoiseTex");
        noiseMultHash = Shader.PropertyToID("noiseMultiplier");
        bufPointsHash = Shader.PropertyToID("buf_Points");
        outputBufferHash = Shader.PropertyToID("outputBuffer");
        plantColorArrayHash = Shader.PropertyToID("_FlowerColors");
        presetDistributionHash = Shader.PropertyToID("_PlantDistributionModifier");
        fiHash = Shader.PropertyToID("_fi");
    }

    private void CreateGeometry()
    {
        GameObject newGo = new GameObject("Leap_Geometry");
        MeshDraft plane = MeshDraft.Plane(20f, 20f, 32, 32);
        plane.Move(new Vector3(-10f, 0, -10f));
        baseMesh = plane.ToMesh();
        baseMesh.RecalculateBounds();

        Bounds meshBounds = baseMesh.bounds;
        Vector3 meshBoundsSize = meshBounds.size;

        meshBoundsSize.y = 50f;
        meshBoundsSize.x *= 2f;
        meshBoundsSize.z *= 2f;
        meshBounds.size = meshBoundsSize;
        baseMesh.bounds = meshBounds;

        newGo.AddComponent<MeshFilter>().sharedMesh = baseMesh;
        newGo.AddComponent<MeshRenderer>().material = plantMaterial;
        myMaterial = newGo.GetComponent<MeshRenderer>().material;

        newGo.transform.SetParent(TheAtom.transform);
        newGo.transform.localPosition = new Vector3(0f, 0f, 0f);
        newGo.transform.rotation = Quaternion.identity;
        newGo.transform.localScale = Vector3.one;
        newGo.layer = 2;

        TheAtom.LeapGeometry = newGo;
    }

    private bool LoadResources()
    {
        voronoicomputeShader = Resources.Load("LeapProceduralPlants/VoronoiField") as ComputeShader;
        plantMaterial = GiC_LeapMakeHotspots.Instance.GrassMaterial == null ? Resources.Load("LeapProceduralPlants/GiCProceduralPlants") as Material : GiC_LeapMakeHotspots.Instance.GrassMaterial;
        noiseTexture = Resources.Load("LeapProceduralPlants/noise21") as Texture;

        //Debug.Log("vcs: "+voronoicomputeShader+" pm: "+ " nT: "+noiseTexture);

        return (voronoicomputeShader != null) && (plantMaterial != null) && (noiseTexture != null);

    }

#endregion

#region Updates

    public void DispatchAndSetShaderBuffers()
    {
        try { 
            UpdateWorldPos();
            Dispatch();
            myMaterial.SetBuffer(bufPointsHash, voronoiOutputBuffer);
        }
        catch (Exception e) {
            Debug.LogError("Dispatch Exception: " + e);
        }
    }

    private void UpdateWorldPos()
    {
        Vector3 newPos = new Vector3(TheAtom.gameObject.transform.position.x, -1f, TheAtom.gameObject.transform.position.z);
        TheAtom.LeapGeometry.transform.position = newPos;

        UpdateAllHotspotPositions();
        //Debug.Log(newPos + " At No: " + TheAtom.Core.AtomIdentifier);
        UpdateMarkersArray();
        myMaterial.SetVector(worldPosShaderHash, newPos);
    }

    public void UpdateAllHotspotPositions() {
        if (hotspotGameObjects == null)
            return;
        if (sphereTreeGrowers == null)
            return;
        if (hotspots == null)
            return;

        for (int hotspotId = 0; hotspotId < hotspotGameObjects.Count; hotspotId++) {
            if ((hotspotGameObjects.ContainsKey(hotspotId) && (hotspots[hotspotId] != null) && (sphereTreeGrowers.ContainsKey(hotspotId)))) {
                //if (hotspots[hotspotId].PositionAndIntensity == null)
                //    continue;
                //if (hotspots[hotspotId].ModifierData == null)
                //    continue;
                if (hotspotGameObjects[hotspotId] == null)
                    return;
                if (sphereTreeGrowers[hotspotId] == null)
                    return;

                //Get Z value
                Vector3 pos = new Vector3(hotspots[hotspotId].PositionAndIntensity.x, 0, hotspots[hotspotId].PositionAndIntensity.z);
                // conversions in GiC_LeapMakeHotspots.CreateNewHotspotAtPosition, LeapHotspotInteractiable.ApplyTransformation, GiC_LeapDataHandler.UpdateAllHotspotPositions
                Vector3 worldPos = pos + transform.position;
                //Vector3 worldPos = transform.TransformPoint(pos);
                RaycastHit rayInfo;
                Physics.Raycast(new Ray(worldPos+Vector3.up*100, Vector3.down), out rayInfo, 200, 1 << 8);
                hotspotGameObjects[hotspotId].transform.position = rayInfo.point;

                sphereTreeGrowers[hotspotId].SetScale(
                    hotspots[hotspotId].PositionAndIntensity.y, 
                    hotspots[hotspotId].ModifierData.x, 
                    hotspots[hotspotId].ModifierData.y);
            }
        }
    }

    private void Dispatch()
    {
        if (!SystemInfo.supportsComputeShaders)
        {
            Debug.LogError("System does not support Compute Shaders");
        }
        hotspotPositionBuffer.SetData(shaderHotspots);
        voronoicomputeShader.SetBuffer(csKernel, hotspotHash, hotspotPositionBuffer);
        voronoicomputeShader.SetBuffer(csKernel, outputBufferHash, voronoiOutputBuffer);
        if (DebugMode)
        {
            voronoicomputeShader.SetFloat(minowskyMixHash, minowskyMix);
            voronoicomputeShader.SetFloat(gridDistanceHash, gridDistance);
            voronoicomputeShader.SetFloat(noiseMultHash, NoiseMultiplier);
        }

        voronoicomputeShader.Dispatch(csKernel, 4, 4, 1);
    }

    private void UpdateSingleHotspot(int hotspot)
    {
        if (hotspot == 0) {
            //Debug.LogError("hotspot id is 0");
            return;
        }

        // create if not existing?
        if ((hotspots[hotspot].Active) && (hotspotGameObjects[hotspot] == null))
        {
            //Get Z value and create geometry
            RaycastHit rayInfo;
            Physics.Raycast(new Ray(new Vector3(hotspots[hotspot].PositionAndIntensity.x, 100f, hotspots[hotspot].PositionAndIntensity.z), Vector3.down), out rayInfo, 200, 1 << 8);
            hotspotGameObjects[hotspot] = CreateNewHotspotAtPosition(rayInfo.point, hotspot).gameObject;
            //Debug.LogWarning("GiC_LeapDataHandler.UpdateSingleHotspot: create hotspot " + hotspot + " " + hotspotGameObjects[hotspot]);
            
            if (NetworkInteractableObject.HasLocalPlayerAuthority(TheAtom.gameObject) && GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Leap) {
                hotspotGameObjects[hotspot].GetComponent<LeapHotspotInteractable>().StartEditing();
            }
        }
        // destroy is inactive and existing?
        else if ((!hotspots[hotspot].Active) && (hotspotGameObjects[hotspot] != null))
        {
            Destroy(hotspotGameObjects[hotspot]);
            hotspotGameObjects[hotspot] = null;
            if (sphereTreeGrowers.ContainsKey(hotspot))
            {
                sphereTreeGrowers.Remove(hotspot);
            }
        }

        if (sphereTreeGrowers.ContainsKey(hotspot) && sphereTreeGrowers[hotspot] != null) {
            sphereTreeGrowers[hotspot].SetScale(hotspots[hotspot].PositionAndIntensity.y, hotspots[hotspot].ModifierData.x, hotspots[hotspot].ModifierData.y);
        }

        DispatchAndSetShaderBuffers();
    }

    private void SetAtomAsHotspot()
    {
        hotspotGameObjects[0] = TheAtom.LeapGeometry.gameObject;
        hotspots[0].PositionAndIntensity.x = 0f;
        hotspots[0].PositionAndIntensity.z = 0f;

        // Intensity
        hotspots[0].PositionAndIntensity.y = 1f;
        hotspots[0].ModifierData = Vector2.one;

        // Active or not?
        hotspots[0].Active = true;
        Vector3 dist = Vector3.zero;
        switch (TheAtom.Core.InitialSoundPreset)
        {
            case 1:
                dist = new Vector3(1,0,0);
                break;
            case 2:
                dist = new Vector3(1,1,0);
                break;
            case 3:
                dist = new Vector3(1,1,1);
                break;
        }
        UpdatePlantDistributionModifier(dist);
    }

    private void UpdateMarkersArray()
    {
       // Vector4 extends = Vector4.zero;
        for (int i = 0; i < hotspots.Count; i++)
        {
            if (!hotspots[i].Active && shaderHotspots[i].Active == 1)
            {
                shaderHotspots[i].Active = Convert.ToByte(hotspots[i].Active);
            }

            if (!hotspots[i].Active)
                continue;

            // Position
            //shaderHotspots[i].Pos.x = hotspots[i].PositionAndIntensity.x - objectHotspot.x;
            //shaderHotspots[i].Pos.y = hotspots[i].PositionAndIntensity.z - objectHotspot.z;
            shaderHotspots[i].Pos.x = hotspotGameObjects[i].transform.localPosition.x;
            shaderHotspots[i].Pos.y = hotspotGameObjects[i].transform.localPosition.z;

            // Intensity
            shaderHotspots[i].Pos.z = hotspots[i].PositionAndIntensity.y;

            // Active or not?
            shaderHotspots[i].Active = Convert.ToByte(hotspots[i].Active);

            shaderHotspots[i].Mod = hotspots[i].ModifierData;

            //positions[i] = new Vector3(x, y, z);
            //Debug.Log(positions[i]);
            //if (x < extends.x)
            //    extends.x = x - 2f;
            //if (x > extends.y)
            //    extends.y = x + 2f;
            //if (y < extends.z)
            //    extends.z = y - 2f;
            //if (y > extends.w)
            //    extends.w = y + 2f;
            //Debug.Log(shaderHotspots[i].Pos.ToString());
        }
        // this.transform.position = ObjectHotspot;
    }

    public void CheckDistanceToNeighbours()
    {
        Vector3 returnValue = Vector3.zero;

        List<AtomInformationCore> cores = GiC_SoundingObjectsManager.Instance.GetAllAtoms();

        foreach (var core in cores)
        {
            float distance = Vector3.SqrMagnitude(transform.position - core.gameObject.transform.position);
            distance = 1f - (Mathf.Clamp(distance, 0f, 100f)*0.01f);
            switch (core.InitialSoundPreset)
            {
                case 1:
                    returnValue.x += distance;
                    break;
                case 2:
                    returnValue.y += distance;
                    break;
                case 3:
                    returnValue.z += distance;
                    break;
            }
        }
        returnValue /= cores.Count;
        UpdatePlantDistributionModifier(returnValue);
    }

    #endregion

    #region start / stop editing
    public void StartEditing () {
        foreach (KeyValuePair<int, GameObject> kvp in hotspotGameObjects) {
            if (kvp.Value != null && kvp.Key != 0) {
                kvp.Value.GetComponent<LeapHotspotInteractable>().StartEditing();
            }
        }
    }
    public void StopEditing () {
        foreach (KeyValuePair<int, GameObject> kvp in hotspotGameObjects) {
            if (kvp.Value != null && kvp.Key != 0) {
                kvp.Value.GetComponent<LeapHotspotInteractable>().StopEditing();
            }
        }
    }
    #endregion

    #region Hotspot Related

    #region Create Hotspot

    private LeapHotspotInteractable CreateNewHotspotAtPosition(Vector3 position, int hotspotId)
    {
        if (TheAtom == null) {
            Debug.LogError("CreateNewHotspotAtPosition TheAtom is NULL!");
            return null;
        }

        return CreateHotspotGeometry(position, hotspotId);
    }

    private LeapHotspotInteractable CreateHotspotGeometry(Vector3 position, int hotspotId)
    {
        string hotspotName = "HotspotNo_" + hotspotId;
        GameObject newHotspot = Instantiate(GiC_LeapMakeHotspots.Instance.TreeBasePrefab, position, Quaternion.identity, TheAtom.LeapGeometry.transform);
        newHotspot.transform.localScale = Vector3.one;
        newHotspot.name = hotspotName;
        newHotspot.AddComponent<BoxCollider>().isTrigger = true;
        newHotspot.GetComponent<BoxCollider>().center = new Vector3(0,0.125f,0);
        newHotspot.GetComponent<BoxCollider>().size = new Vector3(1,0.25f,1);
        SetColors(GiC_LeapMakeHotspots.Instance.PlantColors);

        SphereTreeGrower grower = newHotspot.AddComponent<SphereTreeGrower>();
        SetSphereTreeBasicValues(grower);
        sphereTreeGrowers.Add(hotspotId, grower);
        grower.Regenerate();
        grower.SetSeed(GiC_LeapMakeHotspots.Instance == null ? Random.Range(0, int.MaxValue) : GiC_LeapMakeHotspots.Instance.TreeSeedPool[TheAtom.Core.InitialSoundPreset - 1][Random.Range(0, 9)]);
        grower.SetScale(hotspots[hotspotId].PositionAndIntensity.y, hotspots[hotspotId].ModifierData.x, hotspots[hotspotId].ModifierData.y);
        grower.UpdateCurves();

        newHotspot.AddComponent<LeapHotspotInteractable>().Initialize(hotspotId, this);

        return newHotspot.GetComponent<LeapHotspotInteractable>();
    }

    private void SetSphereTreeBasicValues(SphereTreeGrower st) {
        st.FallbackPrefab = GiC_LeapMakeHotspots.Instance.TreeFallbackPrefab;
        if (st.FallbackPrefab == null) {
            Debug.LogError("FALLBACK SET ERROR");
        }

        st.updateContinuous = false;
        st.minSize = 0.2f;
        st.maxSize = 3f;
        st.radiusGrowth = 0.7f;
        st.sphereInterval = 3f;
        st.maxSpheresOnBranch = 5f;

        st.spherePrefabId = TheAtom.networkCore.presetNumber;

        st.radiusScale = 3f;
        st.maxBranchingDepth = 2;

        st.minRootBranchRadius = 0.2f;
        st.maxRootBranchRadius = 0.5f;
        st.minRootBranchLength = 1f;
        st.maxRootBranchLength = 1.5f;

        st.minRadiusShrink = 0.5f;
        st.maxRadiusShrink = 0.75f;

        st.minBottomBranchingCount = 1;
        st.minBottomBranchingCount = 4;
        st.minTopBranchingCount = 1;
        st.maxTopBranchingCount = 2;

        st.bottomLengthShrinkY0 = 0.7f;
        st.bottomLengthShrinkY1 = 0.2f;
        st.topLengthShrinkY0 = 1f;
        st.topLengthShrinkY1 = 0.5f;

        st.bottomBranchAngleX0 = 30;
        st.bottomBranchAngleX1 = 90;
        st.topBranchAngleX0 = 15;
        st.topBranchAngleX1 = 45;

        st.minBranchingHeight = 0.1f;
        st.maxBranchingHeight = 1f;

        st.SetSeed(Random.Range(0, int.MaxValue));
    }

    #endregion

    public int AddNewHotspot(LeapHotspot newHotspot)
    {
        for (var i = 1; i < hotspots.Count; i++)
        {
            if (hotspots[i].Active) continue;
            hotspots[i] = newHotspot;
            NumHotspots += 1;

            HotspotModified(i);
            SendHotspotDataOsc(i, true);

            return i;
        }
        return -1;
    }

    public void DeleteHotspot(int hotspotToDelete)
    {
        Debug.Log("LeapDataHandler.DeleteHotspot "+hotspotToDelete );
        if (hotspotToDelete == 0)
            return;

        LeapHotspot hotspot = hotspots[hotspotToDelete];
        hotspot.Active = false;
        shaderHotspots[hotspotToDelete].Active = 0;
        hotspot.PositionAndIntensity.y = 0f;
        hotspot.ModifierData = Vector2.zero;
        hotspots[hotspotToDelete] = hotspot;
        NumHotspots -= 1;

        HotspotModified(hotspotToDelete);
        SendHotspotDataOsc(hotspotToDelete, true);
        TheAtom.networkCore.SetGicHotspotsModificationFinished();
    }

    private void ChangeModifierData(int hotspotToModify, LeapHotspot modifiedHotspot)
    {
        if (hotspotToModify == 0)
            return;
        modifiedHotspot.Active = true;
        hotspots[hotspotToModify] = modifiedHotspot;

        HotspotModified(hotspotToModify);
    }

    public void MoveHotspot(int hotspotToModify, Vector3 position)
    {
        Vector3 newPos = new Vector3(position.x, hotspots[hotspotToModify].PositionAndIntensity.y, position.z);
        LeapHotspot hotspot = new LeapHotspot(newPos, hotspots[hotspotToModify].ModifierData, true);
        hotspots[hotspotToModify] = hotspot;
        HotspotModified(hotspotToModify);
    }

    /// <summary>
    /// change parameters used for tree
    /// </summary>
    public void SetHotspotParameters (int hotspotToModify, Vector3 values) {
        Vector3 newPos = new Vector3(hotspots[hotspotToModify].PositionAndIntensity.x, values.x, hotspots[hotspotToModify].PositionAndIntensity.z);
        Vector2 modifierData = new Vector2(values.y, values.z);
        //Debug.Log(modifierData);
        LeapHotspot hotspot = new LeapHotspot(newPos, modifierData, true);

        hotspots[hotspotToModify] = hotspot;
        HotspotModified(hotspotToModify);
        SendHotspotDataOsc(hotspotToModify);
    }

    public Vector3 GetHotspotParameters (int hotspotId) {
        return new Vector3(
            hotspots[hotspotId].ModifierData.x, 
            hotspots[hotspotId].ModifierData.y,
            hotspots[hotspotId].PositionAndIntensity.y);
    }

    private void HotspotModified (int hotspotId) {
        UpdateSingleHotspot(hotspotId);
        TheAtom.networkCore.SetGicHotspotsModificationFinished();
    }

    public void HotspotNetworkModified (int hotspotId) {
        //Debug.Log("NetworkUpdate HotspotID: "+hotspotId + " Atom ID: "+TheAtom.Core.AtomIdentifier);
        UpdateSingleHotspot(hotspotId);
    }
#endregion

#region Setters

    public void SetColors(Color[] colorArray)
    {
        myMaterial.SetColorArray(plantColorArrayHash, colorArray);
    }

    public void SetHeightMap(Texture2D heightMap)
    {
        myMaterial.SetTexture("_TerrainHeights", heightMap);
    }

#endregion

#region OSC
    public void SendHotspotDataOsc(int hotspotId, bool forceSend = false)
    {
        // Debug.Log("Send Hotspot data "+hotspotId);
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap)
            return;

        if (hotspotId == 0 )
            return;

        acumulatedTimeDelta += Time.unscaledDeltaTime;

        hotspotOscMessage.args[0] = TheAtom.Core.AtomIdentifier;
        hotspotOscMessage.args[1] = hotspotId;
        // make x/y approximately 0 .. 1
        float posX = hotspots[hotspotId].Active ? -hotspots[hotspotId].PositionAndIntensity.x : 0f;
        float posZ = hotspots[hotspotId].Active ? -hotspots[hotspotId].PositionAndIntensity.z : 0f;
        //Debug.Log("LeapData: atomId " + theAtom.Core.AtomIdentifier + " hotspotId " + hotspotId + " posXYZ " + posX + "/" + Hotspots[hotspotId].PositionAndIntensity.y + "/" + posZ + " modXY " + Hotspots[hotspotId].ModifierData.x + "/" + Hotspots[hotspotId].ModifierData.y);
        hotspotOscMessage.args[2] = posX;
        hotspotOscMessage.args[3] = posZ;
        hotspotOscMessage.args[4] = hotspots[hotspotId].PositionAndIntensity.y;
        hotspotOscMessage.args[5] = hotspots[hotspotId].ModifierData.x;
        hotspotOscMessage.args[6] = hotspots[hotspotId].ModifierData.y;
        hotspotOscMessage.args[7] = Convert.ToInt32(hotspots[hotspotId].Active);

        if (acumulatedTimeDelta > timeDeltaBetweenOscMessages|| forceSend)
        {
            GicOscManager.Instance.AddDataToMusicalInformationBundle(hotspotOscMessage);
            //Debug.Log("GiC_LeapDataHandler.SendHotspotDataOsc " + hotspotId + " " + posX + "/" + posZ + " data: " + hotspots[hotspotId].PositionAndIntensity.y);
            acumulatedTimeDelta = 0f;
        }

        if (!hotspots[hotspotId].Active)
        {
            GicOscManager.Instance.AddDataToMusicalInformationBundle(hotspotOscMessage);
        }


        throttleIndex++;
    }

#endregion

#region Deconstructor

    private void Release()
    {
        voronoiOutputBuffer.Release();
        hotspotPositionBuffer.Release();
    }

    private void OnDisable()
    {
        Release();
    }

#endregion

}
