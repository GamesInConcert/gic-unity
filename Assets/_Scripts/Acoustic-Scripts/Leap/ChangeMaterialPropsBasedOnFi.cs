﻿using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Environments;
using JetBrains.Annotations;
using UnityEngine;

public class ChangeMaterialPropsBasedOnFi : MonoBehaviour {

    private int posHash;
    private int fiHash;
    private int colorHash;

    private MaterialPropertyBlock props
    {
        get
        {
            if (_props == null)
                Init();

            return _props;
        }
    }

    private MaterialPropertyBlock _props;
    private GiC_ProceduralAtom theAtom;
    private Renderer myRenderer;
    private Material myMaterial;
    private Vector3 prevPos;
    private Transform myTrans;

    // Use this for initialization
    void Start () {
    }

    private void Init()
    {
        _props = new MaterialPropertyBlock();
        theAtom = GetAtom();
        posHash = Shader.PropertyToID("_myPos");
        fiHash = Shader.PropertyToID("_fiIn");
        colorHash = Shader.PropertyToID("_Color");
        myRenderer = GetComponent<Renderer>();
        myMaterial = myRenderer.material;
        myTrans = transform;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if (theAtom == null)
            return;
        SetValuesBlock();
        //SetValues();
	}

    public void SetColor(Color color)
    {
        props.SetColor(colorHash, color);
    }

    private GiC_ProceduralAtom GetAtom()
    {
        var parent = gameObject.transform.parent;
        if (parent == null)
            return null;
        while (parent != null)
        {
            if (parent.GetComponent<GiC_ProceduralAtom>() != null)
                return parent.GetComponent<GiC_ProceduralAtom>();

                parent = parent.transform.parent;           
        }
        return null;
    }

    private void SetPos()
    {
        props.SetVector(posHash, transform.position - theAtom.transform.position);
        prevPos = transform.position;
    }

    private void SetValuesBlock()
    {
        if (prevPos != myTrans.position)
            SetPos();

        props.SetFloat(fiHash, (theAtom.Core.NormalizedAtomPlaybackTime - 0.5f) * 6.28318530717f);
        gameObject.GetComponent<Renderer>().SetPropertyBlock(props);
    }

    private void SetValues()
    {
        myMaterial.SetVector(posHash, transform.position - theAtom.transform.position);
        myMaterial.SetFloat(fiHash, (theAtom.Core.NormalizedAtomPlaybackTime - 0.5f) * 6.28318530717f);
    }
}
