﻿using UnityEngine;
using UnityEngine.Networking;
using WorldspaceController;

public class GiC_LeapMakeHotspots : MonoBehaviour
{
    #region Singelton
    public static GiC_LeapMakeHotspots Instance { get; private set; }
    #endregion

    #region Variables

    public GameObject[] TreePrefabs;
    public GameObject TreeFallbackPrefab;
    public Material TreePrefabMaterial;
    public GameObject[] InternalTreePrefabs { get; private set; }
    public GicColorScheme colorScheme;
    public Color[] PlantColors {
        get { return colorScheme != null ? colorScheme.colors : new Color[13]; } }
    public int[][] TreeSeedPool = new int[3][];
    public Texture2D TerrainHeight;

    public GameObject TreeBasePrefab;
    public GameObject TreeModifierPrefab;

    public ComputeShader GrassComputeShader;
    public Material GrassMaterial;

    public Material TreeTrunkMaterial;
    public Material HotspotManipulatorMaterial;

    //public ComputeShader voronoiCompute;

    private bool controllerCallbacksAdded = false;
    private WorldspaceInputDevice inputDevice;

    private GiC_ProceduralAtom theAtom { get { return GiC_SoundingObjectsManager.Instance.currentEditableAtom; } }

    private GiC_LeapDataHandler leapDataHandler
    {
        get
        {
            if (theAtom != null)
            {
                if (theAtom.Core != null)
                {
                    if (theAtom.Core.LeapDataHandler != null)
                    {
                        return theAtom.Core.LeapDataHandler;
                    }
                    Debug.LogError("TheAtom.Core.LeapDataHandler is null.. this should not happen");
                }
                else
                {
                    Debug.LogError("TheAtom.Core is null.. this should not happen");
                }
            }
            return null;
        }
    }

    private LeapHotspotInteractable currentHotspot;

    //public InputDeviceData controllerDeviceData { get; set; }
    //private WorldspaceInputDevice myInputdevice { get; set; }
    //private GameObject raySource;

    private bool editingHotspot = false;
    private Vector2 hotspotModifiers = Vector2.zero;

    #endregion

    #region Unity Routines

    private void Awake()
    {
        if (GiC_LeapMakeHotspots.Instance != null && GiC_LeapMakeHotspots.Instance != this)
        {
            Debug.LogError("Only One Instance of GiC_LeapMakeHotspots allowed.. Deleting");
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Initialize()
    {
        ExchangePrefabMaterials(TreePrefabs, TreePrefabMaterial);
        CreateTerrainHeightTexture();
        //voronoiCompute = Resources.Load("LeapProceduralPlants/VoronoiField") as ComputeShader;
        PopulateSeedPool(TreeSeedPool);
    }

    private void OnEnable()
    {
        if (GiCPlaybackManager.Instance != null)
        {
            GiCPlaybackManager.Instance.OnChangeInterface += InitializeInterface;
        }
        if (NetworkManagerModuleManager.Instance != null)
        {
            NetworkManagerModuleManager.Instance.onStartLocalPlayerEvent.AddListener(CreateTerrainHeightTexture);
        }
    }

    private void OnDisable()
    {
        if (GiCPlaybackManager.Instance != null && GiCPlaybackManager.Instance.OnChangeInterface != null)
        {
            GiCPlaybackManager.Instance.OnChangeInterface -= InitializeInterface;
        }
    }

    private void InitializeInterface()
    {
        if ((GiCPlaybackManager.Instance == null))
            return;
        if ((GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Leap) && !controllerCallbacksAdded) {
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance) {
                InitControllerCallback(inputDevice.deviceId);
            }
            if (!controllerCallbacksAdded) {
                WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.AddListener(InitControllerCallback);
            }
        }
        else if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap && controllerCallbacksAdded) {
            WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.RemoveListener(InitControllerCallback);
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance)
            {
                RemoveControllerCallback(inputDevice.deviceId);
            }
        }
        Initialize();
    }

    #region ControllerCallbacks
    private void InitControllerCallback(int controllerId)
    {
        inputDevice = WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);
        Debug.Log("GiC_LeapMakeHotspots.InitControllerCallback: " + controllerId + " " + inputDevice);

        // if this is a right vive controller
        if ((inputDevice.deviceHand == InputDeviceHand.Right) && (inputDevice.deviceType == InputDeviceType.Vive)) {
            inputDevice.inputController.viveTriggerDown.AddListener(OnControllerTriggerClicked);
            inputDevice.inputController.viveTriggerUp.AddListener(OnControllerTriggerUnClicked);
            inputDevice.inputController.viveTouchpadPressed.AddListener(OnXyPadTouched);
            controllerCallbacksAdded = true;
        }

        // left vive
        if ((inputDevice.deviceHand == InputDeviceHand.Left) && (inputDevice.deviceType == InputDeviceType.Vive))
        {}
        // leap hand
        else if ((inputDevice.deviceType == InputDeviceType.Leap)) { 
            inputDevice.inputController.leapIndexFingerDownGestureStartEvent.AddListener(OnControllerTriggerClicked);
            inputDevice.inputController.leapIndexFingerDownGestureStopEvent.AddListener(OnControllerTriggerUnClicked);
            controllerCallbacksAdded = true;
        }
        else if ((inputDevice.deviceType == InputDeviceType.Mouse) || (inputDevice.deviceType == InputDeviceType.Camera)) {
            inputDevice.inputController.mouseButton0Down.AddListener(OnControllerTriggerClicked);
            inputDevice.inputController.mouseButton0Up.AddListener(OnControllerTriggerUnClicked);
            controllerCallbacksAdded = true;
        }
    }

    private void RemoveControllerCallback(int controllerId)
    {
        inputDevice = WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);

        if ((inputDevice.deviceHand == InputDeviceHand.Right) && (inputDevice.deviceType == InputDeviceType.Vive)) {
            inputDevice.inputController.viveTriggerDown.RemoveListener(OnControllerTriggerClicked);
            inputDevice.inputController.viveTriggerUp.RemoveListener(OnControllerTriggerUnClicked);
            inputDevice.inputController.viveTouchpadPressed.RemoveListener(OnXyPadTouched);
            controllerCallbacksAdded = false;
        }
        // left vive
        if ((inputDevice.deviceHand == InputDeviceHand.Left) && (inputDevice.deviceType == InputDeviceType.Vive)) {
        }
        // leap hand
        else if ((inputDevice.deviceType == InputDeviceType.Leap)) { 
            inputDevice.inputController.leapIndexFingerDownGestureStartEvent.RemoveListener(OnControllerTriggerClicked);
            inputDevice.inputController.leapIndexFingerDownGestureStopEvent.RemoveListener(OnControllerTriggerUnClicked);
            controllerCallbacksAdded = false;
        }
        else if ((inputDevice.deviceType == InputDeviceType.Mouse) || (inputDevice.deviceType == InputDeviceType.Camera)) {
            inputDevice.inputController.mouseButton0Down.RemoveListener(OnControllerTriggerClicked);
            inputDevice.inputController.mouseButton0Up.RemoveListener(OnControllerTriggerUnClicked);
            controllerCallbacksAdded = false;
        }
    }
    #endregion


    #endregion

    #region ControllerCallbacks

    private void OnControllerTriggerClicked(InputDeviceData deviceData)
    {
        if (deviceData.uiHit)
            return;
        if (deviceData.hoveredInteractableObjects != null) {
            Debug.Log("GiC_LeapMakeHotspots.OnControllerTriggerClicked: hovered Objects: ");
            foreach (InteractableObject obj in deviceData.hoveredInteractableObjects) {
                //Debug.Log("-    " + obj.name);
            }
        }

        //Debug.Log("GiC_LeapMakeHotspots.OnControllerTriggerClicked: ");
        bool canCreate = false;
        if (theAtom == null)
            return;
        foreach (var iObs in deviceData.hoveredInteractableObjects)
        {
            if (iObs.gameObject.layer == 8)
                canCreate = true;
        }
        if (canCreate)
        {
            Vector3 pos = ReturnWorldSpaceHitPoint(deviceData); // controllerDeviceData);

            //Debug.Log("GiC_LeapMakeHotspots.OnControllerTriggerClicked: create at "+pos);
            CreateNewHotspotAtPosition(pos);
            editingHotspot = true;
        }
        else
        {
            foreach (var obj in deviceData.hoveredInteractableObjects)
            {
                if (obj.gameObject.CompareTag("LeapHotspot"))
                {
                    currentHotspot = obj as LeapHotspotInteractable;
                }
            }
        }
    }

    private void OnControllerTriggerUnClicked(InputDeviceData deviceData)
    {
        currentHotspot = null;
    }

    private void OnXyPadTouched(InputDeviceData deviceData)
    {
        
    }



    #endregion

    #region Create Hotspot

    public void CreateNewHotspotAtPosition(Vector3 position)
    {
        if (TerrainHeight == null)
            CreateTerrainHeightTexture(null);
        // conversions in GiC_LeapMakeHotspots.CreateNewHotspotAtPosition, LeapHotspotInteractiable.ApplyTransformation, GiC_LeapDataHandler.UpdateAllHotspotPositions
        //Vector3 localPosition = leapDataHandler.transform.InverseTransformPoint(position);
        Vector3 localPosition = position - leapDataHandler.transform.position;
        Debug.LogWarning("Gic_LeapMakeHotspots.CreateNewHotspotAtPosition: " + position + " local " + localPosition);

        LeapHotspot newHotspot = new LeapHotspot(localPosition, Vector2.one, true);
        newHotspot.PositionAndIntensity.y = 0.0f;
        int hotspotID = leapDataHandler.AddNewHotspot(newHotspot);

        if (hotspotID < 0)
        {
            // GUI Popup
            Debug.LogError("Already created 20 hotspots, delete one before continuing");
            return;
        }
    }

    #endregion

    #region Utils

    private Vector3 ReturnWorldSpaceHitPoint(InputDeviceData deviceData)
    {
        Vector3 returnVal = deviceData.worldspaceHit;

        returnVal = deviceData.worldspaceHit - deviceData.source;
        returnVal *= 0.95f;
        returnVal += deviceData.source;
        return returnVal;
    }

    private void CreateTerrainHeightTexture(NetworkBehaviour netB = null)
    {
        if (FindObjectOfType<Terrain>() == null)
            return;
        TerrainData myTerrain = FindObjectOfType<Terrain>().terrainData;
        var heights = myTerrain.GetHeights(0, 0, myTerrain.heightmapWidth, myTerrain.heightmapHeight);
        TerrainHeight = new Texture2D(myTerrain.heightmapWidth, myTerrain.heightmapHeight, TextureFormat.RFloat, false);

        Debug.Log(TerrainHeight.height + " " + TerrainHeight.width);

        //TerrainHeight.SetPixels((Color)heights);

        for (int i = 0; i < TerrainHeight.height; i++)
        {
            for (int j = 0; j < TerrainHeight.width; j++)
            {
                TerrainHeight.SetPixel(i, j, new Color(heights[j, i], 0, 0, 0));
            }
        }

        //for (int y = 0; y < TerrainHeight.height; y++)
        //{
        //    for (int x = 0; x < TerrainHeight.width; x++)
        //    {
        //        Debug.Log(x + " " + y);
        //        //TerrainHeight.SetPixel(x, y, new Color(heights[y, x], 0, 0, 0));
        //    }
        //}

        TerrainHeight.Apply();

    }

    private void PopulateSeedPool(int[][] pool)
    {
        int[] seed0 = {24, 25, 26, 27, 28, 29, 31, 32, 33, 34};
        int[] seed1 = {100, 101, 102, 103, 104, 105, 106, 107, 108, 109};
        int[] seed2 = {200, 201, 202, 203, 204, 205, 206, 207, 208, 209};

        pool[0] = seed0;
        pool[1] = seed1;
        pool[2] = seed2;
    }

    private void ExchangePrefabMaterials(GameObject[] prefabs, Material mat)
    {
        if (mat == null || prefabs.Length == 0)
            return;
        for (int i = 0; i < prefabs.Length; i++ )
        {
            GameObject fab = prefabs[i];
            fab.GetComponent<Renderer>().sharedMaterial = mat;
            prefabs[i] = fab;

        }
    }

    #endregion

}
