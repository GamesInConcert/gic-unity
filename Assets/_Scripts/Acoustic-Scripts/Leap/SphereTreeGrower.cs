﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(SphereTreeGrower))]
public class SphereTreeInspector : Editor {
    public override void OnInspectorGUI() {
        SphereTreeGrower myTarget = (SphereTreeGrower)target;

        if (GUILayout.Button("Regenerate")) {
            myTarget.Regenerate();
        }
        if (GUILayout.Button("New Seed")) {
            myTarget.SetSeed(Random.Range(0, int.MaxValue));
        }

       if (GUILayout.Button("Update Curves")) {
            myTarget.UpdateCurves();
        }

        DrawDefaultInspector();
    }
}
#endif

[System.Serializable]
public class TreeBranch {
    // predefined
    public TreeBranch parentBranch;

    public SphereTreeGrower treeParameters;

    // calculated
    // how many child branches are on the parent branch before this one
    public int branchIndex;
    // depth of the branch
    public int branchDepth;
    // what is the position of this branch on the parent branch
    public float branchingHeight;

    // angles of the branch
    public float branchAngle;
    public float branchRotation;

    // width and length of the branch
    public float branchLength;
    public float branchRadius;

    // generated
    public GameObject rootObject;
    public List<TreeBranch> childBranches;
    public List<GameObject> visualObjects;

    public LineRenderer lineRenderer;

    public void InitializeBranch (TreeBranch parent, int _branchIndex, SphereTreeGrower _treeParameters) {
        branchIndex = _branchIndex;
        parentBranch = parent;
        treeParameters = _treeParameters;
        if (parent != null) {
            branchDepth = parent.branchDepth + 1;
        } else {
            branchDepth = 0;
        }

        visualObjects = new List<GameObject>();
        childBranches = new List<TreeBranch>();        

        int branchCount = (int)treeParameters.depthBranching.Evaluate(branchDepth);
        if (branchDepth >= treeParameters.maxBranchingDepth) {
            branchCount = 0;
        }

        // Debug.Log("add branches "+branchCount);
        for (int i=0; i<branchCount; i++) {
            TreeBranch t = new TreeBranch();
            childBranches.Add(t);
            t.InitializeBranch(this, i, treeParameters);
        }
    }

    // internal performance variables
    private GameObject sphereGo;
    private float parentLength;
    private float parentRadius;
    private int parentBranchCount;
    //private int branchDepth;
    private float branchPos;
    private float __branchLength;
    private float __parentLength;
    private float __branchRadius;
    private float __parentRadius;
    private float sphereInterval;
    private int numSpheresOnBranch;
    private float currentPosition;
    private int sphereIndex;
    private float lerp;
    private float sphereScale;
    private float spherePos;

    public void UpdateBranch () {
        parentLength = treeParameters.rootBranchLength;
        parentRadius = treeParameters.rootBranchRadius;
        parentBranchCount = 1;
        //branchDepth = 0;
        Transform parentTransform = treeParameters.transform;

        branchLength = parentLength;
        branchRadius = parentRadius;
        branchRotation = 0;
        branchAngle = 0;
        branchingHeight = 0;
        if (parentBranch != null) {
            parentRadius = parentBranch.branchRadius;
            parentLength = parentBranch.branchLength;
            parentTransform = parentBranch.rootObject.transform;
            parentBranchCount = parentBranch.childBranches.Count;
            //branchDepth = parentBranch.branchDepth + 1;

            branchRadius = parentRadius * treeParameters.radiusShrink;
            branchLength = parentLength * treeParameters.branchLengthShrinkCurve.Evaluate(branchDepth);
            branchingHeight = treeParameters.branchingHeightCurve.Evaluate(branchIndex); // value between 0..1 ( * parentLength for real position)
            branchPos = ((branchingHeight-treeParameters.minBranchingHeight) / ((1f+(1f/(parentBranchCount+1)))-treeParameters.minBranchingHeight));
            
            branchRotation = treeParameters.branchRotationCurve.Evaluate(branchingHeight);
            branchAngle = treeParameters.branchAngleCurve.Evaluate(branchingHeight);
        }

        if (rootObject == null) {
            rootObject = new GameObject("branch-" + branchDepth+"-"+branchIndex);
            rootObject.transform.parent = parentTransform;

            lineRenderer = rootObject.AddComponent<LineRenderer>();
            lineRenderer.material = GiC_LeapMakeHotspots.Instance.TreeTrunkMaterial == null ? new Material(Shader.Find("Particles/Alpha Blended Premultiply")) : GiC_LeapMakeHotspots.Instance.TreeTrunkMaterial;
            lineRenderer.positionCount = 2;
        }

        __branchLength = branchLength * treeParameters.lengthScale;
        __parentLength = parentLength * treeParameters.lengthScale;
        __branchRadius = branchRadius * treeParameters.radiusScale;
        __parentRadius = parentRadius * treeParameters.radiusScale;

        rootObject.transform.localPosition = new Vector3(0, branchingHeight * __parentLength, 0);
        rootObject.transform.localRotation = Quaternion.Euler(branchAngle, branchRotation, 0);

        // update line renderer
        //LineRenderer lr = rootObject.GetComponent<LineRenderer>();
        lineRenderer.startWidth = __parentRadius * 1.5f;
        lineRenderer.endWidth = __branchRadius * 1.5f;
        lineRenderer.startColor = treeParameters.GetColor(branchDepth, 0); // (branchDepth-1, branchingHeight); // oder das hier?
        lineRenderer.endColor = treeParameters.GetColor(branchDepth, 1);
        lineRenderer.SetPosition(0, rootObject.transform.position);
        lineRenderer.SetPosition(1, rootObject.transform.position + rootObject.transform.up * __branchLength);

        // distance between each sphere, increased if we assume large number of spheres on branch
        sphereInterval = treeParameters.sphereInterval;
        numSpheresOnBranch = (int)((__branchLength - (__branchRadius * 1.6f)) / ((__parentRadius + __branchRadius) / 2));
        if (numSpheresOnBranch > treeParameters.maxSpheresOnBranch) {
            sphereInterval *= numSpheresOnBranch / treeParameters.maxSpheresOnBranch;
        }
        
        // start position of the spheres (0 at root)
        currentPosition = 0;
        if (branchDepth != 0) {
            //currentPosition = __branchRadius * 1.6f;
            currentPosition = __branchLength / 2;
        }

        sphereIndex = 0;
        while (currentPosition < __branchLength + __branchRadius * 1.6f) {
            lerp = 1f / __branchLength * Mathf.Min(currentPosition, __branchLength);
            sphereScale = Mathf.Lerp(__parentRadius, __branchRadius, lerp);
            // last object is positioned at directionLength
            spherePos = Mathf.Lerp(0, __branchLength, lerp);

            sphereGo = null;
            // apply values to spehre
            if (sphereIndex >= visualObjects.Count) {
                //  Debug.Log("add item " + sphereIndex);
                sphereGo = GameObject.Instantiate(treeParameters.spherePrefab, rootObject.transform);
                sphereGo.SetActive(true);
                visualObjects.Add(sphereGo);
                // maybe performance is much better like this and it has low influence on visuality
                //treeParameters.ApplyColor(sphereGo, branchDepth, lerp);
            }
            sphereGo = visualObjects[sphereIndex];

            sphereGo.transform.localPosition = new Vector3(0, spherePos, 0);
            sphereGo.transform.localScale = Vector3.one * sphereScale;
            sphereGo.transform.localRotation = Quaternion.identity;

            // is a bit slower, but visuals are worse with the above (no interpolation when scaled up from small)
            treeParameters.ApplyColor(sphereGo, branchDepth, lerp);

            // add sphere radius to position next sphere
            currentPosition = currentPosition + sphereScale * sphereInterval;
            // end
            sphereIndex += 1;
        }

        // remove obsolete GameObjects
        if (visualObjects.Count > sphereIndex) {
            for (int goIndex=visualObjects.Count-1; goIndex > sphereIndex - 1; goIndex--){
                // Debug.Log("remove item " + goIndex);
                GameObject.Destroy(visualObjects[goIndex]);
                visualObjects.RemoveAt(goIndex);
            }
        }

        for (int i=0; i<childBranches.Count; i++) {
            childBranches[i].UpdateBranch();
        }
    }


    public void ResetBranch () {
        if (childBranches != null) {
            for (int branchId = childBranches.Count-1; branchId >= 0; branchId--) {
                childBranches[branchId].ResetBranch();
                childBranches[branchId] = null;
            }
    
            childBranches = null;
        }

        if (visualObjects != null) {
            for (int objId = visualObjects.Count-1; objId >= 0; objId--) {
                GameObject.Destroy(visualObjects[objId]);
            }

            visualObjects = null;
        }

        GameObject.Destroy(rootObject);

        parentBranch = null;
    }
}

public class SphereTreeGrower : MonoBehaviour {
    #region variables
    // @Reto: Ich hab die jetzt auch in das LeapMakeHotspots getan.. sind ja unterschiedlich je nach sound preset
    public GameObject FallbackPrefab;

    public int spherePrefabId = -1;

    public GameObject spherePrefab
    {
        get
        {
            if ((spherePrefabId == -1) || (GiC_LeapMakeHotspots.Instance == null) && (FallbackPrefab != null)) {
                Debug.LogWarning("SphereTreeGrower.spherePrefab: " + spherePrefabId + " " + GiC_LeapMakeHotspots.Instance + " " + FallbackPrefab);
                return FallbackPrefab;
            }
            return GiC_LeapMakeHotspots.Instance.TreePrefabs.Length >= spherePrefabId ? GiC_LeapMakeHotspots.Instance.TreePrefabs[spherePrefabId - 1] : FallbackPrefab;
        }
    }

    [Header("-")]
    public bool updateContinuous;

    //public int CurrentAtomPreset
    //{
    //    get
    //    {
    //        if (GiC_SoundingObjectsManager.instance != null && GiC_SoundingObjectsManager.instance.currentEditableAtom != null)
    //            return GiC_SoundingObjectsManager.instance.currentEditableAtom.Core.InitialSoundPreset;
    //        return -1;
    //    }
    //}

    private MeshRenderer meshRenderer;

    [Header("Growth")]
    public float minSize = 0.2f;
    public float maxSize = 2.0f;
    public float radiusGrowth = 0.7f;

    public float sphereInterval = 1.66f;
    public float maxSpheresOnBranch = 8f;
    
    [Header("Growth Debug")]
    public float lengthScale = 1f;
    public float radiusScale = 1f;
    public float xValue = 1f;
    public float yValue = 1f;

    [Header("Generation")]
    // seed of tree
    public int treeSeed;

    public int maxBranchingDepth = 4;
    
    [Header("Root Radius and Length")]
    // radius of the root
    public float minRootBranchRadius = 0.2f;
    public float maxRootBranchRadius = 0.4f;
    public float minRootBranchLength = 0.6f;
    public float maxRootBranchLength = 1.5f;
    [HideInInspector]
    public float rootBranchRadius;
    [HideInInspector]
    public float rootBranchLength;

    [Header("Branch Radius Shrink")]
    // shrinking of the branch
    public float minRadiusShrink = 0.6f;
    public float maxRadiusShrink = 0.9f;
    [HideInInspector]
    public float radiusShrink;

    [Header("Number of Branches")]
    // how many branches on which recursion
    public float minBottomBranchingCount = 3;
    public float maxBottomBranchingCount = 6;
    public float minTopBranchingCount = 1;
    public float maxTopBranchingCount = 4;
    [HideInInspector]
    public AnimationCurve depthBranching;

    [Header("Rotation around Branches")]
    // how branches rotate with length
    public float minBranchRotation = 360;
    public float maxBranchRotation = 720;
    [HideInInspector]
    public AnimationCurve branchRotationCurve;

    [Header("Length Shrink of Branches")]
    // how branches shrink with recursion
    public float bottomLengthShrinkY0 = 0.5f;
    public float bottomLengthShrinkY1 = 1.0f;
    public float topLengthShrinkY0 = 0.2f;
    public float topLengthShrinkY1 = 0.7f;
    [HideInInspector]
    public AnimationCurve branchLengthShrinkCurve;

    [Header("Angle offset to Parentbranch")]
    // how a branch offsets from parent direction
    public float bottomBranchAngleX0 = 30;
    public float bottomBranchAngleX1 = 90;
    public float topBranchAngleX0 = 15;
    public float topBranchAngleX1 = 45;
    [HideInInspector]
    public AnimationCurve branchAngleCurve;

    [Header("Branching start (0..1)")]
    public float minBranchingHeight = 0.5f;
    public float maxBranchingHeight = 1.0f;
    [HideInInspector]
    public AnimationCurve branchingHeightCurve;

    [Header("Debug")]
    // public GameObject firstSphere;
    // public List<TreeBranch> branches;
    public TreeBranch rootBranch;
    #endregion

    #region Unity functions
    private void OnEnable() {
        props = new MaterialPropertyBlock();
        colorHash = Shader.PropertyToID("_Color");
    } 
    private void Start() {
        Regenerate();
    }

    private Vector3 prevPosition = Vector3.zero;

    private void Update() {
        //if ((prevPosition - transform.position).sqrMagnitude > 0.0001f)
        //{
        //    Vector3 newPos = transform.position;
        //    if (Terrain.activeTerrain) { 
        //        float terrainHeight = Terrain.activeTerrain.SampleHeight(newPos) + Terrain.activeTerrain.transform.position.y;
        //        newPos.y = terrainHeight;
        //    }
        //    transform.position = newPos;
        //    prevPosition = newPos;
        //}

        if (updateContinuous) { 
            UpdateModel();
        }
    }
    #endregion

    #region Public Functions
    public void Regenerate () {
        if (rootBranch != null)
            rootBranch.ResetBranch();

        UpdateCurves();
        InitColor();

        rootBranch = new TreeBranch();
        rootBranch.InitializeBranch(null, 0, this);

        UpdateModel();
    }

    public void SetSeed (int seed) {
        treeSeed = seed;
        Regenerate();
    }

    public void SetScale (float _scale, float _xValue, float _yValue) {
        _scale = Mathf.Clamp01(_scale);
        lengthScale = _scale * maxSize + minSize;
        radiusScale = (1f - radiusGrowth) + _scale * radiusGrowth;
        xValue = Mathf.Clamp01(_xValue);
        yValue = Mathf.Clamp01(_yValue);

        if (!updateContinuous) { 
            UpdateModel();
        }
    }
    #endregion

    #region Initializing Functions of Tree
    public void UpdateCurves () {
        //Debug.Log("SphereTreeGrower.UpdateCurves " + Time.frameCount);
        // init random seed
        Random.InitState(treeSeed);

        // init branching curve (requires regenerate)
        float bottomBranchingCount = Random.Range(minBottomBranchingCount, maxBottomBranchingCount);
        float topBranchingCount = Random.Range(minTopBranchingCount, maxTopBranchingCount);
        depthBranching = new AnimationCurve();
        depthBranching.AddKey(new Keyframe(0, bottomBranchingCount));
        depthBranching.AddKey(new Keyframe(maxBranchingDepth, topBranchingCount));

        // these curves dont need regeneration
        rootBranchLength = Random.Range(minRootBranchLength, maxRootBranchLength);
        rootBranchRadius = Random.Range(minRootBranchRadius, maxRootBranchRadius);

        float bottomLengthShrink = Mathf.Lerp(bottomLengthShrinkY0, bottomLengthShrinkY1, yValue);
        float topLengthShrink = Mathf.Lerp(topLengthShrinkY0, topLengthShrinkY1, yValue);
        branchLengthShrinkCurve = new AnimationCurve();
        branchLengthShrinkCurve.AddKey(new Keyframe(0, bottomLengthShrink));
        branchLengthShrinkCurve.AddKey(new Keyframe(maxBranchingDepth, topLengthShrink));

        float branchRotation = Random.Range(minBranchRotation, maxBranchRotation);
        branchRotationCurve = AnimationCurve.Linear(minBranchingHeight, 0, 1, branchRotation);

        float bottomBranchAngle = Mathf.Lerp(bottomBranchAngleX0, bottomBranchAngleX1, xValue);
        float topBranchAngle = Mathf.Lerp(topBranchAngleX0, topBranchAngleX1, xValue);
        branchAngleCurve = new AnimationCurve();
        branchAngleCurve.AddKey(new Keyframe(minBranchingHeight, bottomBranchAngle, 0, 0));
        branchAngleCurve.AddKey(new Keyframe(1, topBranchAngle, 0, 0));

        branchingHeightCurve = AnimationCurve.Linear(0, maxBranchingHeight, maxBranchingDepth, minBranchingHeight);

        radiusShrink = Random.Range(minRadiusShrink, maxRadiusShrink);
        //Mathf.Clamp(radiusShrink, 0.1f, 0.99f);
    }
    #endregion

    #region Generation of Tree
    /// <summary>
    /// update the meshes that display the tree
    /// </summary>
    private void UpdateModel() {
        sphereInterval = Mathf.Max(sphereInterval, 0.5f);
        lengthScale = Mathf.Max(lengthScale, 0.1f);
        radiusScale = Mathf.Max(radiusScale, 0.1f);

        UpdateCurves();

        rootBranch.UpdateBranch();
    }
    #endregion

    #region Color
    // which colors to use (0..2)
    public int leapColorIndex { get; set; }

    public Gradient colorGradient;

    public void InitColor() {
        leapColorIndex = spherePrefabId == -1 ? Mathf.Clamp(leapColorIndex, 0, 2) : Mathf.Clamp(spherePrefabId - 1, 0, 2);

        GradientColorKey[] gck;
        //gck = new GradientColorKey[4];
        gck = new GradientColorKey[2];
        if (GiC_LeapMakeHotspots.Instance != null) {
            // colors
            // TODO: Retos Original
            //gck[0].color = GiC_LeapMakeHotspots.Instance.PlantColors[0 * 3 + leapColorIndex];
            //gck[0].time = 0.0f;
            //gck[1].color = GiC_LeapMakeHotspots.Instance.PlantColors[1 * 3 + leapColorIndex];
            //gck[1].time = 0.5f;
            //gck[2].color = GiC_LeapMakeHotspots.Instance.PlantColors[2 * 3 + leapColorIndex];
            //gck[2].time = 0.75f;
            //gck[3].color = GiC_LeapMakeHotspots.Instance.PlantColors[3 * 3 + leapColorIndex];
            //gck[3].time = 1f;
            // NOTE: Test
            gck[0].color = GiC_LeapMakeHotspots.Instance.PlantColors[leapColorIndex * 4];
            gck[0].time = 0.0f;
            gck[1].color = GiC_LeapMakeHotspots.Instance.PlantColors[leapColorIndex * 4 + 1];
            gck[1].time = 1.0f;
        }

        colorGradient = new Gradient();
        colorGradient.SetKeys(gck, new GradientAlphaKey[0]);
    }

    // performance values
    private MaterialPropertyBlock props;
    private Color col0;
    private Color col1;
    private float colorDepth;
    private int colId0;
    private int colId1;
    private float colorLerp;
    private int colorHash;

    /// <summary>
    /// calculate color for branch position
    /// </summary>
    public Color GetColor (int recursion, float relPos) {
        // should be a value from 0..1 to be within color range of leap colors
        colorDepth = (recursion + relPos) / (maxBranchingDepth + 1);
        // apply colors to props and apply to meshrenderer
        Color c = colorGradient.Evaluate(colorDepth);
        return c;
    }

    /// <summary>
    /// Apply Color
    /// </summary>
    public void ApplyColor (GameObject go, int recursion, float relPos) {
        Color c = GetColor(recursion, relPos);
        //props.SetColor(colorHash, c);
        //go.GetComponent<MeshRenderer>().SetPropertyBlock(props);
        go.GetComponent<ChangeMaterialPropsBasedOnFi>().SetColor(c);
    }
    #endregion
}
