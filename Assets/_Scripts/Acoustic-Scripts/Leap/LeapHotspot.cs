using System;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class LeapHotspot
{
    public Vector3 PositionAndIntensity;
    public Vector2 ModifierData;
    public bool Active;

    public LeapHotspot(Vector3 positionAndIntensity, Vector2 modifierData, bool active)
    {
        PositionAndIntensity = positionAndIntensity;
        ModifierData = modifierData;
        Active = active;
    }

    public override string ToString()
    {
        return ("PositionXY: " + PositionAndIntensity.x.ToString("G4") + ", " + PositionAndIntensity.z.ToString("G4") + ", Intensity: " + PositionAndIntensity.y.ToString("G4") + ", ModXY: " + ModifierData.x.ToString("G4") + ", " + ModifierData.y.ToString("g4"));
    }

    #region Serialization & Deserialization
    public byte[] GetAsBytes() {
        NetworkWriter networkWriter = new NetworkWriter();
        networkWriter.Write(PositionAndIntensity);
        networkWriter.Write(ModifierData);
        networkWriter.Write(Active);
        return networkWriter.AsArray();
    }

    public static LeapHotspot SetFromBytes(byte[] hotspotBytes  ) {
        NetworkReader networkReader = new NetworkReader(hotspotBytes);
        Vector3 positionAndIntensity = networkReader.ReadVector3();
        Vector2 modifierData = networkReader.ReadVector2();
        bool active = networkReader.ReadBoolean();
        return new LeapHotspot(positionAndIntensity, modifierData, active);
    }
    #endregion
}