﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereTreeGrowth : MonoBehaviour {

    public float vSpeed = 1;
    public float xSpeed = 1;
    public float ySpeed = 1;
    private AnimationCurve sinScale;
	public bool running = true;
    private float t = 0;

    void OnEnable() {
        sinScale = new AnimationCurve();
        sinScale.AddKey(0,0);
        sinScale.AddKey(1,1);
        t = Random.Range(0, 1);
    }
	// Update is called once per frame
	void Update () {
        if (running) {
            t += Time.deltaTime;
            float v = sinScale.Evaluate((Mathf.Sin(t * Mathf.PI * 2 / vSpeed) + 1) / 2) * 0.75f + 0.25f;
            float x = sinScale.Evaluate((Mathf.Sin(t * Mathf.PI * 2 / xSpeed) + 1) / 2);
            float y = sinScale.Evaluate((Mathf.Sin(t * Mathf.PI * 2 / ySpeed) + 1) / 2);
            GetComponent<SphereTreeGrower>().SetScale(v, x, y);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            GetComponent<SphereTreeGrower>().SetSeed(Random.Range(0,int.MaxValue));
        }
        if (Input.GetKeyDown(KeyCode.Return)) {
            running = !running;
        }
    }
}
