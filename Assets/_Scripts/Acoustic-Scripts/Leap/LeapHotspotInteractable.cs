﻿using UnityEngine;
using WorldspaceController;
using Random = UnityEngine.Random;

public class LeapHotspotInteractable : InteractableObject {
    public int HotspotId { get; private set; }
    public Vector2 ModifierData;
    public GiC_LeapDataHandler leapDataHandler;
    private float sizeLerp = 0f;
    private float HotspotScale = 0f;

    private float click1Time = 0;
    private float click2Time = 0;
    private float click3Time = 0;

    private float trippleClickTime = 0.5f;

    // object to modify x/y/z parameter of tree
    private GameObject parameterInteractable;

    public void Initialize(int myID, GiC_LeapDataHandler dataHandler) {
        rotateXByDrag = false;
        rotateZByDrag = false;
        moveYByDrag = false;
        Random.InitState(Mathf.FloorToInt(Time.realtimeSinceStartup));
        HotspotId = myID;
        leapDataHandler = dataHandler;

        AddEvents();
        this.gameObject.tag = "LeapHotspot";

        parameterInteractable = Instantiate(GiC_LeapMakeHotspots.Instance.TreeModifierPrefab, transform);
        parameterInteractable.name = "hotspotController";
        parameterInteractable.AddComponent<LeapHotspotParameterInteractable>();
        LeapHotspotParameterInteractable hotspotParameter = parameterInteractable.GetComponent<LeapHotspotParameterInteractable>();
        hotspotParameter.hotspotInteractable = this;
        hotspotParameter.hotspotId = HotspotId;
        Debug.Log("LeapHotSpotInteractable.Initialize setting hotspotInteractable to " + hotspotParameter.hotspotInteractable);
    }

    public void StartEditing () {

        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap)
            return;

        LeapHotspotParameterInteractable hotspotParameter = parameterInteractable.GetComponent<LeapHotspotParameterInteractable>();
        hotspotParameter.StartEditing();

        // prevent adding the events twice
        RemoveEvents();
        AddEvents();
    }
    public void StopEditing () {
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap)
            return;

        LeapHotspotParameterInteractable hotspotParameter = parameterInteractable.GetComponent<LeapHotspotParameterInteractable>();
        hotspotParameter.StopEditing();
        RemoveEvents();
    }

    public void AddEvents () {
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap)
            return;

        onAttachStartEvent.AddListener(OnAttachStartEvent);
        onAttachStopEvent.AddListener(OnAttachStopEvent);
        disableDefaultApplyTransformEvent = true;
        onApplyTransformOverrideEvent.AddListener(ApplyTransformation);
        onUseStartEvent.AddListener(StartModifying);
    }

    public void RemoveEvents () {
        //DoDetachFromController(null);
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Leap)
            return;

        onAttachStartEvent.RemoveListener(OnAttachStartEvent);
        onAttachStopEvent.RemoveListener(OnAttachStopEvent);
        onApplyTransformOverrideEvent.RemoveListener(ApplyTransformation);
        onUseStartEvent.RemoveListener(StartModifying);
    }

    private void OnDisable() {
        RemoveEvents();
    }

    public Quaternion relativeRotation = Quaternion.identity;
    public Vector3 relativePosition = Vector3.zero;

    private void OnAttachStartEvent(InputDeviceData deviceData) {
        DoAttachToController(deviceData);

        relativePosition = deviceData.inputDevice.controller.transform.InverseTransformPoint(transform.position);
        relativeRotation = Quaternion.Inverse(deviceData.inputDevice.controller.transform.rotation) * transform.rotation;
    }

    public new void ApplyTransformation(InputDeviceData deviceData) {
        Vector3 newPosition = deviceData.inputDevice.controller.transform.TransformPoint(relativePosition);
        Vector3 localPosition = newPosition - leapDataHandler.transform.position;
        leapDataHandler.MoveHotspot(HotspotId, localPosition);
    }

    private void OnAttachStopEvent(InputDeviceData deviceData) {
        leapDataHandler.SendHotspotDataOsc(HotspotId, true);
        DoDetachFromController(deviceData);
    }

    private Vector3 smoothedVelocity = Vector3.zero;
    Vector3 LowPassFilterSmoothedVelocity(Vector3 targetValue, float factor) {
        smoothedVelocity.x = (targetValue.x * factor) + (smoothedVelocity.x * (1.0f - factor));
        smoothedVelocity.y = (targetValue.y * factor) + (smoothedVelocity.y * (1.0f - factor));
        smoothedVelocity.z = (targetValue.z * factor) + (smoothedVelocity.z * (1.0f - factor));
        return smoothedVelocity;
    }

    private void StartModifying(InputDeviceData deviceData) {
        //sizeLerp = 0f;
        smoothedVelocity = Vector3.zero;

        if (Time.unscaledTime - click1Time < trippleClickTime) {
            DeleteHotspot();
        }

        if (Time.unscaledTime - click1Time > trippleClickTime) {
            click1Time = Time.unscaledTime;
        }
    }


    public void ModifyHotspot2(Vector3 values) {
        // clamp value's between 0..1
        values = new Vector3(Mathf.Clamp01(values.x), Mathf.Clamp01(values.y), Mathf.Clamp01(values.z));

        leapDataHandler.SetHotspotParameters(HotspotId, values);
    }

    private void DeleteHotspot() {
        leapDataHandler.DeleteHotspot(HotspotId);
    }
}