﻿using UnityEngine;
using WorldspaceController;

public class LeapHotspotParameterInteractable : InteractableObject
{

    public LeapHotspotInteractable hotspotInteractable;
    public Vector3 value;
    public int hotspotId;

    private bool valuesDirty = true;

    private void OnEnable () {
        StartEditing();
    }

    void OnDisable () {
        StopEditing();
    }

    private bool editActive = false;

    // enable the colliders etc. to allow editing
    public void StartEditing() {
        if (editActive == false) { 
            editActive = true;
            // make sure everything is reset
            //Debug.Log("LeapHotspotParameterInteractable.StartEditing: " + hotspotId + "");
            //onAttachRepeatEvent.AddListener(OnAttachRepeatEvent);
            onAttachStartEvent.AddListener(OnAttachStartEvent);
            onAttachStopEvent.AddListener(OnAttachStopEvent);
            disableDefaultApplyTransformEvent = true;
            onApplyTransformOverrideEvent.AddListener(ApplyTransformation);

            gameObject.AddComponent<SphereCollider>();
            gameObject.transform.localScale = Vector3.one;

            MeshRenderer r = gameObject.GetComponentInChildren<MeshRenderer>();
            if (r != null) {
                r.enabled = true;
            }

            // read values from hotspot and apply to this
            GetHotspotValues();
            valuesDirty = true;
        }
    }
    // disable the colliders etc. to disallow editing
    public void StopEditing() {
        if (editActive == true) {
            editActive = false;
            Debug.Log("LeapHotspotParameterInteractable.StopEditing: " + hotspotId + "");
            //onAttachRepeatEvent.RemoveListener(OnAttachRepeatEvent);
            onAttachStartEvent.RemoveListener(OnAttachStartEvent);
            onAttachStopEvent.RemoveListener(OnAttachStopEvent);
            onApplyTransformOverrideEvent.RemoveListener(ApplyTransformation);

            Destroy(gameObject.GetComponent<SphereCollider>());

            MeshRenderer r = gameObject.GetComponentInChildren<MeshRenderer>();
            if (r != null) {
                r.enabled = false;
            }
        }
    }

    public Quaternion relativeRotation = Quaternion.identity;
    public Vector3 relativePosition = Vector3.zero;

    public void ApplyTransformation(InputDeviceData deviceData) {
        Vector3 newPosition = deviceData.inputDevice.controller.transform.TransformPoint(relativePosition);

        gameObject.transform.position = newPosition;

        if (deviceData.inputDevice.deviceType == InputDeviceType.Mouse) {
            //Debug.Log("mouse "+ deviceData.inputValueX+" "+ deviceData.inputValueY); 
            //Quaternion newRotation = deviceData.inputDevice.controller.transform.rotation * relativeRotation;
            //newRotation = Quaternion.Euler(0, 0, deviceData.inputValueX + deviceData.inputValueY);
            gameObject.transform.localRotation *= Quaternion.Euler(0, 0, deviceData.inputValueX + deviceData.inputValueY); ;
        } else {
            Quaternion newRotation = deviceData.inputDevice.controller.transform.rotation * relativeRotation;
            gameObject.transform.localRotation = newRotation;
        }

        SetHotspotValues();
        GetHotspotValues();
    }

    //private void OnAttachRepeatEvent(InputDeviceData deviceData)
    //{
    //    Vector3 curPos = deviceData.inputDevice.grabAttachementPoint.transform.position;
    //    Vector3 deltaPos = curPos - prevPos;
    //    prevPos = curPos;

    //    Quaternion curRot = deviceData.inputDevice.grabAttachementPoint.transform.rotation;
    //    Quaternion deltaRot = Quaternion.Inverse(prevRot) * curRot;
    //    prevRot = curRot;

    //    transform.position += deltaPos;
    //    transform.rotation *= deltaRot;

    //    // dont apply and send if no update
    //    if ((deltaPos == Vector3.zero) && (deltaRot == Quaternion.identity)) return;

    //    SetHotspotValues();
    //    GetHotspotValues();
    //}

    //private Vector3 prevPos = Vector3.zero;
    //private Quaternion prevRot = Quaternion.identity;

    private void OnAttachStartEvent(InputDeviceData deviceData)
    {
        //prevPos = deviceData.inputDevice.grabAttachementPoint.transform.position;
        //prevRot = deviceData.inputDevice.grabAttachementPoint.transform.rotation;
        DoAttachToController(deviceData);

        relativePosition = deviceData.inputDevice.controller.transform.InverseTransformPoint(transform.position);
        relativeRotation = Quaternion.Inverse(deviceData.inputDevice.controller.transform.rotation) * transform.rotation;
    }

    private void OnAttachStopEvent(InputDeviceData deviceData)
    {
        DoDetachFromController(deviceData);
    }

    //private void LateUpdate()
    //{
    //    if (!valuesDirty) return;
    //    valuesDirty = false;
    //    SetHotspotValues();
    //    GetHotspotValues();
    //}

    // scale of offsets
    float posXScale = 1.5f;
    float posYScale = 1.5f;
    //Vector3 scale = new Vector3(1.5f, 1.5f, 1f);
    float posYOffset = 0.25f;


    // set values to the hotspot values
    private void SetHotspotValues () {
        float localPosX = Mathf.Clamp01(1 - transform.localPosition.x / posXScale);
        float localPosY = Mathf.Clamp01((transform.localPosition.y - posYOffset) / posYScale);
        float localZRot = 1 - transform.localEulerAngles.z / 90f;

        //Debug.Log("localZRot 1 " + localZRot);
        if (localZRot < -2) {
            localZRot = 1;
        }
        else if (localZRot < -1) { // < -90degrees
            localZRot = 0;
        }
        else if (localZRot < 0) { // -90..0 degrees
            localZRot = 0;
        }
        else if (localZRot > 2) { // 180..270 degrees
            localZRot = 0;
        }
        else if (localZRot > 1) { // 90..180 degrees
            localZRot = 1;
        }
        //Debug.Log("localZRot 2 " + localZRot);

        if (hotspotInteractable != null) {
            // x is left right, y is up down, z is rotation
            // 1st value is size of tree, 2nd value is gravity for tree-branches, 3th value is branch length
            Vector3 values = new Vector3(localPosY, localZRot, localPosX);
            hotspotInteractable.leapDataHandler.SetHotspotParameters(hotspotId, values);
        }
        else {
            Debug.LogError("LeapHotspotParameterInteractable: hotspotInteratable is null!");
        }
    }
    // read values from the hotspot and move modifier accordingly
    private void GetHotspotValues() {
        if (hotspotInteractable && hotspotInteractable.leapDataHandler) { 
            Vector3 values = hotspotInteractable.leapDataHandler.GetHotspotParameters(hotspotId);

            float localPosX = (1 - values.y) * posXScale;
            float localPosY = (values.z * posYScale) + posYOffset;
            float localRotZ = (1 - values.x) * 90;

            // convert back to position
            transform.localPosition = new Vector3(localPosX, localPosY, 0);
            transform.localEulerAngles = new Vector3(0, 0, localRotZ);
        }
    }
}
