﻿using System.Collections.Generic;
using UnityEngine;

public class GiC_DrawingDataHandler : MonoBehaviour
{
    // General Necessary Data
    private GiC_ProceduralAtom theAtom;

    //private float playbackSpeed { get { return 1f/theAtom.Core.atomPlaybackSpeedInSeconds; } }

    //private int bpm = 40;
    //private int numBeats = 16;
    //private bool clickEnabled = false;

    // Drawing Related Information
    // private readonly List<GicStroke> strokes = new List<GicStroke>();
    public ObservedList<GicStroke> Strokes { get { return theAtom.networkCore.Strokes; } }
    public int CurrentStrokeIndex { get { return Mathf.Clamp(Strokes.Count, 0, 10000000); } }

    // TODO: Die Strokes Hier müssen eigentlich auch übers netzwerk
    public List<GicStroke> NonPermanentStrokes = new List<GicStroke>();
    //private int npStrokeIndex = 0;

    //Necessary?     
    //private float actualStrokeTime = 0.0f;
    private float currentPlaybackAngle;
    //private int currentStrokeIndex = 0;
    //private GicStroke previousStroke;

    private GameObject drawingParent;
    public GameObject DrawingParent
    {
        get { return drawingParent ?? CreateDrawingParent(0.5f); }
    }

    // Playback Related
    private Vector3 outPos = Vector3.zero;
    private Vector3 pastOutPos = Vector3.zero;
    private Color outColor = Color.black;
    //private Color pastOutColor = Color.black;
    private float outScale;
    private int outMat;
    private float outTime;
    private float previousPlaybackAngle;

    public int OscThrottle = 2;
    public float TimeDeltaBetweenOscMessages = 0.05f;
    private float acumulatedTimeDelta = 0f;
    private int throttleIndex;
    //private bool sendToMaxPlaybackWait = false;
    //private bool sendToMaxPlaybackNow = false;

    //private GameObject play-headPrefab;

    //private readonly List<GicStroke> activeStrokes = new List<GicStroke>();
    //public List<GicStroke> ActiveStrokes { get { return activeStrokes; } }

    //private readonly List<object> oscMessage = new List<object>();
    //float[] oscArray = new float[10];

    //private GameObject scanner;

    //public GiC_DrawingDataHandler()
    //{
    //    //CurrentStrokeIndex = 0;
    //}

    public void InitializeHandler(GiC_ProceduralAtom parentAtom)
    {
        theAtom = parentAtom;
        //scanner = theAtom.Scanner;
        CreateDrawingParent(0.5f);
    }

    private GameObject CreateDrawingParent(float scale)
    {
        drawingParent = new GameObject("DrawingParent_" + theAtom.Core.AtomIdentifier);
        drawingParent.transform.SetParent(theAtom.transform);
        drawingParent.transform.localPosition = Vector3.zero;
        drawingParent.transform.localRotation = Quaternion.identity;
        drawingParent.transform.localScale = Vector3.one * scale;
        return drawingParent;
    }
	
	// Update is called once per frame
	void Update () {
	    if (theAtom == null)
            return;
	    if (theAtom.PlaybackState != GiC_PlaybackStates.Play)
            return;

	    //AdvancePlayhead();
        UpdatePlayhead();

        //TODO: Now the "Playheads also dissapear after recording has finished .. shall they continue?
	    if (GiC_SoundingObjectsManager.Instance.currentEditableAtom == theAtom || theAtom.Core.SendToMaxPlaybackWait || theAtom.Core.SendToMaxPlaybackNow)
	        PlayDrawing();
	}

    private void UpdatePlayhead()
    {
        previousPlaybackAngle = currentPlaybackAngle;
        currentPlaybackAngle = theAtom.Core.NormalizedAtomPlaybackTime * 360f;
    }

    private void PlayDrawing()
    {
        float playbackSpeed = theAtom.Core.AtomPlaybackAdvanceMultiplier;
        // start elements at end of rotation
        if (previousPlaybackAngle > currentPlaybackAngle)
        {
            List<GicStroke> results0 =
                Strokes.FindAll(item => item.StartingAngle < 360f && item.StartingAngle > previousPlaybackAngle);
            List<GicStroke> results1 =
                Strokes.FindAll(item => item.StartingAngle <= currentPlaybackAngle && item.StartingAngle > 0f);

            foreach (var s in results0)
            {
                s.StartPlayback();
            }

            foreach (var s in results1)
            {
                s.StartPlayback();
            }
        }
        else
        {
            List<GicStroke> results =
                Strokes.FindAll(item => item.StartingAngle <= currentPlaybackAngle && item.StartingAngle > previousPlaybackAngle);

            foreach (var s in results)
            {
                s.StartPlayback();
            }
        }

        acumulatedTimeDelta += Time.unscaledDeltaTime;
        bool sendNow = false;
        if (acumulatedTimeDelta > TimeDeltaBetweenOscMessages)
        {
            sendNow = true;
            acumulatedTimeDelta = 0f;
        }


        foreach (var stroke in Strokes)
        {
            if (!stroke.Playback || !(stroke.TotalStrokeLength > 0.25f)) continue;

            stroke.GetValuesAtTime(out outPos, out outScale, out outColor, out outMat, out outTime);
            stroke.AdvanceAngularPlayhead(playbackSpeed);
            stroke.MoveGoPlayhead(outPos);
            stroke.ChangePlayheadColor(outColor);
            outPos = outPos - theAtom.transform.position;
            outPos.z = stroke.FarMode ? outPos.z : outPos.z * 2f;

            if (float.IsNaN(outPos.x + outPos.y + outPos.z))
            {
                outPos = Vector3.zero;
            }

            if (float.IsNaN(outScale))
            {
                outScale = 0f;
            }

            if (float.IsNaN(outColor.a + outColor.r + outColor.g + outColor.b))
            {
                outColor = Color.black;
            }

            if (float.IsNaN(outMat))
            {
                outMat = 0;
            }

            //float delta = Vector3.Magnitude(outPos - stroke.pastOutPos);
            float delta = outTime - stroke.pastOutTime;

            //Debug.Log(outPos + " " + stroke.pastOutPos);

            delta = stroke.FarMode ? delta : delta * 2.0f;

            if (!stroke.Playback)
            {
                delta = 0f;
                outScale = 0f;
            }

            stroke.pastOutPos = outPos;
            stroke.pastOutTime = outTime;

            if (!stroke.Playback)
                continue;

            if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Paint)
                continue;

            //if (throttleIndex % OscThrottle != 0)
            //    continue;
            if (sendNow)
            {
                HSBColor hsbOut = HSBColor.FromColor(outColor);
                stroke.OscMessagePos.args[0] = theAtom.Core.AtomIdentifier;
                stroke.OscMessagePos.args[1] = stroke.StrokeId;
                stroke.OscMessagePos.args[2] = outPos.x;
                stroke.OscMessagePos.args[3] = outPos.y;
                stroke.OscMessagePos.args[4] = outPos.z;
                stroke.OscMessagePos.args[5] = delta;
                stroke.OscMessagePos.args[6] = hsbOut.b;
                stroke.OscMessagePos.args[7] = outScale;
                stroke.OscMessagePos.args[8] = hsbOut.h;

                GicOscManager.Instance.AddDataToMusicalInformationBundle(stroke.OscMessagePos);
            }
            //messageIndex++;
        }
        //throttleIndex++;
    }

    public void ResyncPlayback()
    {
        currentPlaybackAngle = 0f;
        previousPlaybackAngle = 0f;
        foreach (var stroke in Strokes)
        {
            stroke.StopPlayback();
        }
    }

    public void DeleteDrawing()
    {
        //GiC_PlaybackManager.Instance.TogglePlayback(false);
        foreach (var stroke in Strokes)
        {
            stroke.DeleteStroke();
        }
        Strokes.Clear();
        theAtom.networkCore.SetGicStrokeModificationFinished();
        float dpScale = drawingParent.transform.localScale.x;
        Destroy(drawingParent);
        drawingParent = CreateDrawingParent(dpScale);
        //CurrentStrokeIndex = 0;

        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Paint)
            return;
        GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/Paint/ma", theAtom.Core.AtomIdentifier, 1));
    }


    //private void HotspotModified(int hotspotId) {
    //    //UpdateSingleHotspot(hotspotId);
    //    theAtom.networkCore.SetGicHotspotsModificationFinished();
    //}

    //public void HotspotNetworkModified(int hotspotId) {
    //    //Debug.Log("NetworkUpdate HotspotID: "+hotspotId + " Atom ID: "+TheAtom.Core.AtomIdentifier);
    //    //UpdateSingleHotspot(hotspotId);
    //}
}
