﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GiCCreateShadeColorTexture : MonoBehaviour
{

    public RawImage rawImage;
    public Texture2D tex;
    public Color MainColorA;
    public Color MainColorB;

    private bool initialized = false;
    // Use this for initialization
    void Start ()
	{

    }

    private void Initialize()
    {
        if (GiC_SoundingObjectsManager.Instance == null)
            return;
        tex = CreateTexture();
        GetComponent<UiColorSelector>().colorTex = tex;
        UpdateTexture();
        rawImage = GetComponent<RawImage>();
        GiC_SoundingObjectsManager.Instance.OnEditableAtomchange += DoUpdate;
        DoUpdate(GiC_SoundingObjectsManager.Instance.currentEditableAtom);
        initialized = true;
    }

    private void OnEnable()
    {
        if (!initialized)
            Initialize();
    }

    private void OnDisable()
    {
        //GiC_SoundingObjectsManager.Instance.OnEditableAtomchange -= DoUpdate;
    }

	// Update is called once per frame
	void Update () {
	    //if (Input.GetKeyDown(KeyCode.C))
	    //{
	    //    DoUpdate(0);
	    //}
	}

    public void DoUpdate(GiC_ProceduralAtom atom)
    {
        if (atom == null)
            return;
        if (tex == null)
            tex = CreateTexture();
        if (rawImage.texture != tex)
            rawImage.texture = tex;

        GetAndSetColors(atom.Core.InitialSoundPreset-1);
        UpdateTexture();
    }

    public void GetAndSetColors(int setting = 0)
    {
        MainColorA = GiC_LeapMakeHotspots.Instance.PlantColors[setting * 4];
        MainColorA.a = 1f;
        MainColorB = GiC_LeapMakeHotspots.Instance.PlantColors[setting * 4+2];
        MainColorB.a = 1f;
    }

    private Texture2D CreateTexture()
    {
        return new Texture2D(256, 256, TextureFormat.ARGB32, false);
    }

    private void UpdateTexture()
    {
        float lerp = 1.0f / tex.width;
        float halfLerp = 1.0f / (tex.width * 0.5f);
        float doubleLerp = lerp*2.0f;
        HSBColor colorA = HSBColor.FromColor(MainColorA);
        HSBColor colorB = HSBColor.FromColor(MainColorB);

        for (int i = 0; i < tex.width; i++)
        {
            //Color lerpedColor = Color.Lerp(MainColorA, MainColorB, i * lerp);
            //HSBColor lerpHsbColor = HSBColor.Lerp(colorA, colorB, i*lerp);
            HSBColor lerpHsbColor = i >= 128 ? colorA : colorB;
            for (int j = 0; j < tex.height; j++)
            {
                lerpHsbColor.b = Mathf.Clamp01(j * halfLerp);
                lerpHsbColor.s = 1.0f - Mathf.Clamp01(-1.0f + (j * doubleLerp));

                tex.SetPixel(i,j, HSBColor.ToColor(lerpHsbColor));
            }
        }
        tex.Apply(false);
    }

}
