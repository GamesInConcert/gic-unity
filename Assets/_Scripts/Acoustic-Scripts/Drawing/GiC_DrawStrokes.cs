﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WorldspaceController;

public class GicStrokeAngleComparer : IComparer<GicStroke>
{
    public int Compare(GicStroke x, GicStroke y)
    {
        if (y != null && (x != null && x.StartingAngle > y.StartingAngle)) return 1;
        if (y != null && (x != null && x.StartingAngle < y.StartingAngle)) return -1;
        return 0;
    }
}
// ReSharper disable once InconsistentNaming
public class GiC_DrawStrokes : MonoBehaviour
{

    //Debuging
    public GameObject DebugObject;

    private GiC_ProceduralAtom theAtom
    {
        get
        {
            // ReSharper disable once ConstantNullCoalescingCondition
            return GiC_SoundingObjectsManager.Instance.currentEditableAtom ?? null;
            //Debug.LogError("GiC_SoundingObjectsManager.instance.currentEditableAtom is null.. this should not happen");
        }
    }

    private GiC_DrawingDataHandler theDrawing
    {
        get
        {
            if (theAtom == null) return null;
            if (theAtom.Core != null)
            {
                if (theAtom.Core.DrawingDataHandler != null) { 
                    return theAtom.Core.DrawingDataHandler;
                }
                Debug.LogError("TheAtom.Core.DrawingDataHandler is null.. this should not happen");
            } else {
                Debug.LogError("TheAtom.Core is null.. this should not happen");
            }
            return null;
        }
    }

    private int currentStrokeIndex
    {
        get { return theDrawing.CurrentStrokeIndex-1; }
    }

    // UI and Input Related
    public GameObject UsedController { get; set; }
    public SteamVR_Controller.Device SteamViveController { get; set; }
    private InputDeviceData controllerDeviceData { get; set; }
    //private LaserPointer gicLaserPointer { get; set; }
    private GameObject brushTip { get; set; }
    private WorldspaceInputDevice myInputdevice { get; set; }
    private Image reticleImage;
    private UiColorSelector uiColorSelector;

    // Visible in Inspector
    public GameObject EditRoomAnchor;

    public GameObject DrawingParticleEffect { get { return GiCPlaybackManager.Instance.PlayheadPrefab; } }
    private GameObject drawingParticleEffect;
    private ParticleSystem[] drawingParticleSystem;
    private Light[] PlayheadLight;
    private Vector3 drawingParticleEffectPosLerp;

    // Stroke Variables
    private GameObject actualStroke { get; set; }

    // Non Permanent Stroke related
    public List<GicStroke> NonPermanentStrokes = new List<GicStroke>();
    private int npStrokeIndex;

    // Brush Related Values
    private Color brushColor = Color.red;
    //private Color BrushColorNonPermanent = Color.blue;
    public Material[] BrushMaterials;
    public int SelectedBrushMaterial;
    public float BrushScale = 0.5f;
    private float internalBrushScale { get { return Mathf.Clamp01(BrushScale + ((viveTriggerAxis * (2f*BrushScale) - BrushScale))); } }

    public bool FarMode = true;
    public int VertexSlowdownModulo = 5;

    //private int loopMode { get; set; }

    // Functional Variables
    private bool drawing { get; set; }
    private bool permanent = true;
    private float actualStrokeTime;

    private bool viveTriggerPressed = false;
    private float viveTriggerAxis = 1f;

    private float startingAngle = 0f;
    private float endAngle = 0f;
    private Vector3 prevPos = Vector3.zero;
    private float prevTime = 0f;

    Vector3 pastOutPos = Vector3.zero;

    private float gotoRandomScale;
    private float randomScale;

    private int vertexSlowDown;

    private MeshCollider atomCollider;

    //Singleton
    public static GiC_DrawStrokes Instance { get; private set; }

    // OSC related
    //private float[] oscArray = new float[10];
    private OscMessage oscMessage;

    private bool controllerCallbacksInitialized = false;

    // OnEnable and Disable
    #region OnEnable OnDisable

    private void OnEnable()
    {
        permanent = false;
        FarMode = true;
        BrushScale = 0.7f;

        // singelton
        if (Instance != null && Instance != this)
        {
            Debug.LogError("Only One Instance of GIC_PlaybackManager allowed.. Deleting");
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

        if (GiCPlaybackManager.Instance != null)
        {
            GiCPlaybackManager.Instance.OnChangeInterface += InitializeInterface;
        }

    }

    private void InitializeInterface()
    {
        if ((GiCPlaybackManager.Instance == null))
            return;
        if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Paint && !controllerCallbacksInitialized)
        {

            Debug.Log("GiC_DrawStrokes.OnEnable: initialize devices");
            WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.AddListener(InitControllerCallback);
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance)
            {
                InitControllerCallback(inputDevice.deviceId);
            }

            oscMessage = new OscMessage("/g/p/sp", 0, 0, 0f, 0f, 0f, 0f, 0f, 0f, 0f);
            if (drawingParticleEffect == null && DrawingParticleEffect != null)
            {
                drawingParticleEffect = Instantiate(DrawingParticleEffect);
                drawingParticleSystem = drawingParticleEffect.GetComponentsInChildren<ParticleSystem>();
                if (drawingParticleSystem.Length > 0)
                {
                    foreach (var psys in drawingParticleSystem)
                    {
                        psys.Stop();
                    }
                }
                PlayheadLight = drawingParticleEffect.GetComponentsInChildren<Light>();
                if (PlayheadLight.Length > 0)
                {
                    foreach (var light in PlayheadLight)
                    {
                        light.enabled = false;
                    }
                }
            }
        } else if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Paint && controllerCallbacksInitialized)
        {
            RemoveControllerListeners();
        }
    }

    private void OnDisable()
    {
        if (GiCPlaybackManager.Instance != null && GiCPlaybackManager.Instance.OnChangeInterface != null)
        {
            GiCPlaybackManager.Instance.OnChangeInterface -= InitializeInterface;
        }
        RemoveControllerListeners();
    }
    #endregion

    // Controller Listener Management
    #region Controller Listener Management
    private void InitControllerCallback(int controllerId) {
        WorldspaceInputDevice inputDevice = WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);

        // if this is a right vive controller
        if ((inputDevice.deviceHand == InputDeviceHand.Right) && (inputDevice.deviceType == InputDeviceType.Vive)) {
            Debug.Log("GiC_DrawStrokes.InitControllerCallback: add right vive " + controllerId);
            AddListenersR(inputDevice);
        }
        if ((inputDevice.deviceHand == InputDeviceHand.Left) && (inputDevice.deviceType == InputDeviceType.Vive)) {
            Debug.Log("GiC_DrawStrokes.InitControllerCallback: add left vive " + controllerId);
            AddListenersL(inputDevice);
        }
        if (inputDevice.deviceType == InputDeviceType.Mouse && PlayerSettingsController.Instance.mouseControllerEnabled) {
            Debug.Log("GiC_DrawStrokes.InitControllerCallback: add mouse " + controllerId);
            AddListenersMouse(inputDevice);
        }
        if (inputDevice.deviceType == InputDeviceType.Camera && PlayerSettingsController.Instance.gazeControllerEnabled) {
            Debug.Log("GiC_DrawStrokes.InitControllerCallback: add camera " + controllerId);
            AddListenersMouse(inputDevice);
        }

    }

    private void RemoveControllerListeners()
    {
        if (myInputdevice != null)
        {
            switch (myInputdevice.deviceType)
            {
                case InputDeviceType.Vive:
                    //myInputdevice.inputController.viveTriggerDown.RemoveListener(OnControllerTriggerClicked);
                    //myInputdevice.inputController.viveTriggerUp.RemoveListener(OnControllerTriggerUnClicked);
                    myInputdevice.inputController.viveTriggerTouched.RemoveListener(OnControllerTriggerTouched);
                    myInputdevice.inputController.viveTriggerTouchUp.RemoveListener(OnControllerTriggerTouchUp);
                    myInputdevice.inputController.viveTouchpadPressed.RemoveListener(OnXyPadTouched);
                    break;
                case InputDeviceType.Mouse:
                    myInputdevice.inputController.mouseButton0Down.RemoveListener(OnControllerTriggerClicked);
                    myInputdevice.inputController.mouseButton0Up.RemoveListener(OnControllerTriggerUnClicked);
                    break;
                case InputDeviceType.Camera:
                    myInputdevice.inputController.mouseButton0Down.RemoveListener(OnControllerTriggerClicked);
                    myInputdevice.inputController.mouseButton0Up.RemoveListener(OnControllerTriggerUnClicked);
                    break;
            }
        }
        else
        {
            Debug.LogError("GiC_DrawStrokes.OnDisable: myInputdevice undefined");
        }
    }

    private void AddListenersL(WorldspaceInputDevice inputDevice)
    {
        inputDevice.inputController.viveTouchpadPressed.AddListener(OnXyPadTouched);
    }

    private void AddListenersR(WorldspaceInputDevice inputDevice)
    {
        if (!inputDevice.controller)
        {
            Debug.LogError("GiC_DRawStrokes.InitControllerCallback: controller undefined.. returning ");
            return;
        }
        if (inputDevice.deviceType == InputDeviceType.Mouse)
        {
            brushTip = inputDevice.controller.Find("RaySource").gameObject;
        }
        else
        {
            if (inputDevice.controller.Find("BrushTip") == null)
            {
                brushTip = inputDevice.controller.Find("RaySource").gameObject;
            }
            else
            {
                brushTip = inputDevice.controller.Find("BrushTip").gameObject;
            }

            if (brushTip == null)
            {
                Debug.LogWarning("GiC_DRawStrokes.InitControllerCallback: BrushTip not found on " + inputDevice.controller);
            }
        }       
        controllerDeviceData = inputDevice.inputController.inputDevice.deviceData;
        //inputDevice.inputController.viveTriggerDown.AddListener(OnControllerTriggerClicked);
        //inputDevice.inputController.viveTriggerUp.AddListener(OnControllerTriggerUnClicked);
        inputDevice.inputController.viveTriggerTouched.AddListener(OnControllerTriggerTouched);
        inputDevice.inputController.viveTriggerTouchUp.AddListener(OnControllerTriggerTouchUp);
        myInputdevice = inputDevice;
    }

    private void AddListenersMouse(WorldspaceInputDevice inputDevice)
    {
        if (!inputDevice.controller)
        {
            Debug.LogError("GiC_DRawStrokes.InitControllerCallback: controller undefined ");
            return;
        }

        brushTip = inputDevice.controller.Find("RaySource").gameObject;

        controllerDeviceData = inputDevice.inputController.inputDevice.deviceData;
        inputDevice.inputController.mouseButton0Down.AddListener(OnControllerTriggerClicked);
        inputDevice.inputController.mouseButton0Up.AddListener(OnControllerTriggerUnClicked);
        myInputdevice = inputDevice;
    }

    #endregion

    // Controller Events
    #region Controller Events

    public void OnXyPadTouched(InputDeviceData deviceData)
    {
        HSBColor color = HSBColor.FromColor(brushColor);
        color.h = Mathf.Clamp01((deviceData.inputValueX + 1f) * 0.5f);
        color.b = Mathf.Clamp01((deviceData.inputValueY + 1f) * 0.5f);
        color.s = 1f;

        brushColor = HSBColor.ToColor(color);

        if (reticleImage == null || uiColorSelector == null)
        {
            reticleImage = GameObject.Find("PaintColorSelector").GetComponent<Image>();
            uiColorSelector = FindObjectOfType<UiColorSelector>();
            return;
        }
        uiColorSelector.colorSetCallback.Invoke(HSBColor.ToColor(color));
    }

    public void OnControllerTriggerClicked(InputDeviceData deviceData)
    {
        if (theDrawing && deviceData.hoveredInteractableObjects.Count == 0 && deviceData.grabbedInteractableObject == null) {
            OnControllerTriggerClicked(deviceData.uiHit);
        }
    }

    public void OnControllerTriggerTouched(InputDeviceData deviceData)
    {
        if (!viveTriggerPressed)
        {
            viveTriggerPressed = true;
            OnControllerTriggerClicked(deviceData);
            Debug.Log("Vive Controller Click Down");
        }
        viveTriggerAxis = Mathf.Lerp(viveTriggerAxis, deviceData.inputValueX, Time.unscaledDeltaTime * 5f);
    }

    public void OnControllerTriggerTouchUp(InputDeviceData deviceData)
    {
        if (viveTriggerPressed)
        {
            viveTriggerPressed = false;
            OnControllerTriggerUnClicked(deviceData);
            Debug.Log("Vive Controller Unclick");
        }
    }

    public void OnControllerTriggerClicked(bool uiHit = false)
    {
        if (uiHit) {
            return;
        }
        //Debug.Log("GiC_DrawStrokes.AddStrokeVive: draw 2" + theDrawing);

        //currentAngularDistance = 0f;
        if (theDrawing != null)
        {
            prevTime = Time.time;
            atomCollider = theAtom.gameObject.GetComponent<MeshCollider>();
            if (atomCollider != null) { 
                atomCollider.enabled = false;
            }
        if (true)
            {
                AddPermanentStrokeVive(FarMode);
            } else
            {
                AddNonPermanentStrokeVive(FarMode);
                //prevTime = Time.time;
            }
            drawing = true;
            //if (DrawingParticleEffect != null)
            //    DrawingParticleEffect.SetActive(true);
            if (drawingParticleSystem.Length > 0)
            {
                foreach (var psys in drawingParticleSystem)
                {
                    psys.Play();
                }
            }
            if (PlayheadLight.Length > 0)
            {
                foreach (var light in PlayheadLight)
                {
                    light.enabled = true;
                }
            }

        }
        else
        {
            Debug.LogError("The Drawing is null.. Cannot Draw");
        }
    }

    public void OnControllerTriggerUnClicked(InputDeviceData deviceData)
    {
        if (theDrawing) {
            OnControllerTriggerUnClicked();
        }
    }

    public void OnControllerTriggerUnClicked()
    {
        //drawing = false;
        if (!drawing)
            return;

        if (permanent)
        {
            if (theDrawing.Strokes[currentStrokeIndex].StrokePoints.Count < 6)
            {
                drawing = false;
                EndNonPermanentStrokeOsc();
                theDrawing.Strokes.RemoveAt(currentStrokeIndex);
                Destroy(actualStroke);
                actualStrokeTime = 0.0f;

            }
            else
            {
                drawing = false;
                EndNonPermanentStrokeOsc();
                theDrawing.Strokes[currentStrokeIndex].EndingAngle = endAngle;
                theDrawing.Strokes[currentStrokeIndex].FinishStroke(endAngle < startingAngle);
                actualStrokeTime = 0.0f;
                //if (currentStrokeIndex == 0)
                //{
                //    GiC_PlaybackManager.Instance.TogglePlayback(true);
                //}
                theAtom.networkCore.SetGicStrokeModificationFinished();
            }
        }
        else
        {
            drawing = false;
            EndNonPermanentStrokeOsc();
            theAtom.networkCore.SetStrokeArrayIndexesDirty(new int[] { currentStrokeIndex });
            theDrawing.Strokes.RemoveAt(currentStrokeIndex);
            Destroy(actualStroke);
            actualStrokeTime = 0.0f;
            theAtom.networkCore.SetGicStrokeModificationFinished();
        }
        //else
        //{
        //    if (NonPermanentStrokes[npStrokeIndex].StrokePoints.Count < 3)
        //    {
        //        drawing = false;
        //        EndNonPermanentStrokeOsc();
        //        NonPermanentStrokes.RemoveAt(npStrokeIndex);
        //        Destroy(actualStroke);
        //        actualStrokeTime = 0.0f;
        //    }
        //    else
        //    {
        //        drawing = false;
        //        NonPermanentStrokes[npStrokeIndex].FinishStroke();
        //        EndNonPermanentStrokeOsc();
        //        DestroyStroke(npStrokeIndex);
        //        actualStrokeTime = 0.0f;
        //    }
        //}
        if (atomCollider != null)
        {
            atomCollider.enabled = true;
            atomCollider = null;
        }
        actualStrokeTime = 0.0f;
        //if (DrawingParticleEffect != null)
        //    DrawingParticleEffect.SetActive(false);
        if (drawingParticleSystem.Length > 0)
        {
            foreach (var psys in drawingParticleSystem)
            {
                psys.Stop();
            }
        }
        if (PlayheadLight.Length > 0)
        {
            foreach (var light in PlayheadLight)
            {
                light.enabled = false;
            }
        }
    }
    #endregion

    // Unity Specific (Update etc..)
    #region Unity Specific

    // Update is called once per frame
    private void Update () {
        //if (drawing)
        //{
        //    DrawStrokeVive(permanent, FarMode);
        //   }
        // TODO: Tryout simon adding live non permanent stroke
        if (drawing)
        {
            DrawStrokeVive(true, FarMode);
        }

    }
    #endregion

    // Creating A Stroke
    #region Create Strokes Main Functions

    private void AddPermanentStrokeVive(bool farMode)
    {
        //Debug.Log(theDrawing);
        if (!theDrawing)
            return;

        //GameObject newStroke = CreateNewStrokeGameObject(drawingParentOverride);
        // TODO: Change Time.UnscaledTime to some internal Clock for synchronization?

        // Create new Stroke  
        GicStroke newGicStroke = new GicStroke(Time.unscaledTime, theDrawing.DrawingParent, SelectedBrushMaterial, currentStrokeIndex+1, farMode);
        actualStroke = newGicStroke.GameObjectBelongingToThisStroke;
        theDrawing.Strokes.Add(newGicStroke);

        // TODO: Is this still needed?
        //newStroke.GetComponent<GiCMonoStroke>().Init(newGicStroke, currentStrokeIndex, TheDrawing);

        // Vector3 position = farMode ? ReturnBrushTipHitPoint(brushTip.transform.position, brushTip.transform.forward) : brushTip.transform.position + brushTip.transform.forward * (brushTip.transform.localScale.z * 0.25f);
        Vector3 position = farMode ? ReturnBrushTipHitPoint(controllerDeviceData) : controllerDeviceData.source + (controllerDeviceData.direction * 1f);
        //position = theAtom.transform.InverseTransformPoint(position);
        Quaternion rotation = farMode ? Quaternion.identity : brushTip.transform.rotation;

        //Vector3 V1 = new Vector3(theAtom.transform.up.x, 0f, theAtom.transform.up.z);
        Vector3 v1 = EditRoomAnchor.transform.forward;
        Vector3 v2 = EditRoomAnchor.transform.position - position;
        v2 = new Vector3(v2.x, 0f, v2.z);

        float theAngle = AngleSigned(v1, v2, Vector3.up);

        if (theAngle < 0)
        {
            theAngle += 360f;
        }
        newGicStroke.StartingAngle = theAngle;

        startingAngle = theAngle;
        endAngle = theAngle;

        // Test
        theAngle -= newGicStroke.StartingAngle;
        //theAngle = theAngle < 0f ? 0f : theAngle;

        float internalScale = farMode ? internalBrushScale : internalBrushScale * 0.1f;

        // TheAngle = 0f --> Test
        theDrawing.Strokes[currentStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, 0f);
        //theDrawing.Strokes[currentStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, theAngle);
        drawingParticleEffectPosLerp = position;

        prevPos = position;
        //Debug.Log("StartDrawing Angle "+theAngle);
    }

    private void AddNonPermanentStrokeVive(bool farMode)
    {
        npStrokeIndex = NonPermanentStrokes.Count;

        GicStroke newGicStroke = new GicStroke(Time.unscaledTime, theDrawing.DrawingParent, SelectedBrushMaterial, npStrokeIndex, farMode, true);
        NonPermanentStrokes.Add(newGicStroke);
        actualStroke = newGicStroke.GameObjectBelongingToThisStroke;

        // Calculate BrushTipValues
        Vector3 position = farMode ? ReturnBrushTipHitPoint(controllerDeviceData) : controllerDeviceData.source + (controllerDeviceData.direction * 1f);
        Quaternion rotation = farMode ? Quaternion.identity : brushTip.transform.rotation;

        Vector3 v1 = EditRoomAnchor.transform.forward;
        Vector3 v2 = EditRoomAnchor.transform.position - position;
        v2 = new Vector3(v2.x, 0f, v2.z);

        float theAngle = AngleSigned(v1, v2, Vector3.up);

        if (theAngle < 0)
        {
            theAngle += 360f;
        }

        newGicStroke.StartingAngle = theAngle;

        // Test
        theAngle -= newGicStroke.StartingAngle;
        //theAngle = theAngle < 0f ? 0f : theAngle;

        float internalScale = farMode ? internalBrushScale : internalBrushScale * 0.1f;

        NonPermanentStrokes[npStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, theAngle);
        NonPermanentStrokes[npStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, theAngle);

        drawingParticleEffectPosLerp = position;

        prevPos = position;
    }

    private void DrawStrokeVive(bool isPermanentStroke, bool farMode = true)
    {
        Quaternion rot = Quaternion.identity;
        //Vector3 hitpoint = ReturnBrushTipHitPoint(brushTip.transform.position, brushTip.transform.forward);

        //Transform hacktrans = this.transform;
        //Vector3 position = farMode ? ReturnBrushTipHitPoint(brushTip.transform.position, brushTip.transform.forward) : brushTip.transform.position + brushTip.transform.forward * (brushTip.transform.localScale.z * 0.25f);
        Vector3 position = farMode ? ReturnBrushTipHitPoint(controllerDeviceData) : controllerDeviceData.source + (controllerDeviceData.direction * 1f);
        Quaternion rotation = farMode ? rot : brushTip.transform.rotation;

        //Vector3 up = farMode ? transform.up : brushTip.transform.up;
        //Vector3 right = farMode ? transform.right : brushTip.transform.right;
        //Vector3 forward = farMode ? transform.forward : brushTip.transform.forward;

        Vector3 up = Vector3.up;
        Vector3 right = Vector3.right;
        Vector3 forward = Vector3.forward;

        if (vertexSlowDown == 0 && ApproximatelyEqual(prevPos, position, farMode ? 0.05f : 0.01f))
        {
            gotoRandomScale = Mathf.Lerp(gotoRandomScale, randomScale, Time.unscaledDeltaTime * 0.1f);
            if (Mathf.Abs(gotoRandomScale - randomScale) < 0.00001f)
            {
                randomScale = Random.Range(-0.002f, 0.002f);
                //Debug.Log(randomScale);
            }

            float internalScale = farMode ? (internalBrushScale + randomScale) : (internalBrushScale + randomScale) * 0.1f;
            //internalScale = farMode ? internalScale : internalScale * 0.25f;

            //Vector3 V1 = new Vector3(theAtom.transform.up.x, 0.0f, theAtom.transform.up.z);
            Vector3 v1 = EditRoomAnchor.transform.forward;
            Vector3 v2 = EditRoomAnchor.transform.position - position;
            Vector3 v3 = EditRoomAnchor.transform.position - prevPos;
            v2 = new Vector3(v2.x, 0f, v2.z);
            v3 = new Vector3(v3.x, 0f, v3.z);

            //Vector3 V1 = Vector3.up;
            //Vector3 V2 = Vector3.forward;
            //Vector3 V3 = Vector3.right;

            //float theAngle = AngleSigned(v1, v2, Vector3.up);

            //if (theAngle < 0)
            //{
            //    theAngle += 360f;
            //}

            //endAngle = theAngle;
            //Debug.Log("Angle1 "+theAngle);
            // Test
            //theAngle -= isPermanentStroke ? theDrawing.Strokes[currentStrokeIndex].StartingAngle : NonPermanentStrokes[npStrokeIndex].StartingAngle;
            //theAngle = theAngle < 0f ? 0f : theAngle;

            //Debug.Log("Angle2 " + theAngle);
            
            float deltaAngle = AngleSigned(v3, v2, Vector3.up);

            endAngle += deltaAngle;

            if (drawingParticleEffect != null)
            {
                drawingParticleEffectPosLerp = Vector3.Lerp(drawingParticleEffectPosLerp, position, Time.unscaledDeltaTime * 15f);
                drawingParticleEffect.transform.position = drawingParticleEffectPosLerp;
                drawingParticleEffect.transform.rotation = rotation;
            }

            if (deltaAngle > 0f)
            {
                if (isPermanentStroke)
                    theDrawing.Strokes[currentStrokeIndex].TotalAngularDistance += deltaAngle;
                else
                    NonPermanentStrokes[npStrokeIndex].TotalAngularDistance += deltaAngle;
            }

            if (isPermanentStroke)
            {
                theDrawing.Strokes[currentStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, deltaAngle, up, right, forward);
                Vector3 pos = position - theAtom.Core.transform.position;
                SendStrokeOsc(pos, internalScale, brushColor, SelectedBrushMaterial, false, farMode);
            }
            else
            {
                NonPermanentStrokes[npStrokeIndex].AddPoint(position, rotation, internalScale, brushColor, actualStrokeTime, deltaAngle, up, right, forward);
                Vector3 pos = position - theAtom.Core.transform.position;
                SendStrokeOsc(pos, internalScale, brushColor, SelectedBrushMaterial, false, farMode);
            }

            prevPos = position;


            // TESTING RETO, ADDING LIVE TRANSMISSION OF STROKES
            theAtom.networkCore.SetStrokeArrayIndexesDirty(new int[] { currentStrokeIndex });
            theAtom.networkCore.SetGicStrokeModificationFinished();


        }
        vertexSlowDown = (vertexSlowDown + 1) % VertexSlowdownModulo;
        actualStrokeTime += Time.time - prevTime;
        prevTime = Time.time;
    }
    #endregion

    // Helper Functions
    #region Helper Functions
    private float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    private void SendStrokeOsc(Vector3 outPos, float outScale, Color outColor, int outMat, bool endReached, bool farMode)
    {
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Paint)
            return;
        //pastOutPos = Vector3.zero;
        if (float.IsNaN(outPos.x + outPos.y + outPos.z))
        {
            outPos = Vector3.zero;
        }

        outPos.z = farMode ? outPos.z : outPos.z * 2f;

        if (float.IsNaN(outScale))
        {
            outScale = 0f;
        }

        if (float.IsNaN(outColor.a + outColor.r + outColor.g + outColor.b))
        {
            outColor = Color.black;
        }

        //if (float.IsNaN(outMat))
        //{
        //}

        float delta = Time.time - prevTime;
        prevTime = Time.time;

        //float delta = Vector3.Magnitude(outPos - pastOutPos);

        //delta = farMode ? delta : delta * 2f;

        if (endReached)
        {
            delta = 0f;
            outScale = 0f;
        }

        pastOutPos = outPos;

        HSBColor hsbOut = HSBColor.FromColor(outColor);

        //if (pastOutPos != outPos)
        //{
            
        //}

        oscMessage.args[0] = theAtom.Core.AtomIdentifier;
        oscMessage.args[1] = theDrawing.CurrentStrokeIndex + npStrokeIndex;
        oscMessage.args[2] = outPos.x;
        oscMessage.args[3] = outPos.y;
        oscMessage.args[4] = outPos.z;
        oscMessage.args[5] = delta;
        oscMessage.args[6] = hsbOut.b;
        oscMessage.args[7] = outScale;
        oscMessage.args[8] = hsbOut.h;
        //stroke.oscMessagePos.args[6] = hsbOut.s;
        //stroke.oscMessagePos.args[7] = hsbOut.b;
        //stroke.oscMessagePos.args[8] = outMat;
        GicOscManager.Instance.AddDataToMusicalInformationBundle(oscMessage);

    }

    private void EndNonPermanentStrokeOsc()
    {
        if (GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.Paint)
            return;

        oscMessage.args[5] = 0;
        oscMessage.args[7] = 0;
        GicOscManager.Instance.AddDataToMusicalInformationBundle(oscMessage);
    }

    private void DestroyStroke(int index)
    {
        // Todo: Stroke soll sich von hinten her auflösen..
        // Todo: Mit Textur machen? 
        Destroy(NonPermanentStrokes[index].GameObjectBelongingToThisStroke, 1f);
        NonPermanentStrokes.RemoveAt(index);
    }

    private Vector3 ReturnBrushTipHitPoint()
    {
        Vector3 returnVal = Vector3.zero;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            returnVal = hitInfo.point - ray.origin;
            returnVal *= 0.95f;
            returnVal += ray.origin;
        }
        return returnVal;
    }

    private Vector3 ReturnBrushTipHitPoint(Vector3 position, Vector3 forward)
    {
        Vector3 returnVal = position;

        Ray ray = new Ray(position, forward);

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            returnVal = hitInfo.point - ray.origin;
            returnVal *= 0.95f;
            returnVal += ray.origin;
        }
        return returnVal;
    }

    private Vector3 ReturnBrushTipHitPoint(InputDeviceData deviceData)
    {
        var returnVal = deviceData.worldspaceHit - deviceData.source;
        returnVal *= 0.95f;
        returnVal += deviceData.source;
        return returnVal;
    }

    private bool ApproximatelyEqual(Vector3 from, Vector3 to, float maxDifference, bool precisionMode = false)
    {
        Vector3 delta = to - from;
        if (precisionMode)
        {
            return delta.sqrMagnitude > (maxDifference * maxDifference);
        }

        if (Mathf.Abs(delta.x) > maxDifference || Mathf.Abs(delta.y) > maxDifference || Mathf.Abs(delta.z) > maxDifference)
        {
            return true;
        }
        return false;
    }
    #endregion

    // UI Inputs
    #region UiInputs
    public void SetColor(Color newColor)
    {
        // use the same color for both paint modes now
        brushColor = newColor;

        //if (gicLaserPointer != null)
        //{
        //    gicLaserPointer.ChangeHitColor(newColor);
        //}
    }

    public void ToggleFarMode()
    {
        FarMode = !FarMode;
        ToggleFarMode(FarMode);
    }

    public void EnableFarMode(bool mode)
    {
        if (!mode)
            return;
        FarMode = true;
        ToggleFarMode(FarMode);
    }

    public void DisableFarMode(bool mode)
      {
        if (!mode)
            return;
        FarMode = false;
        ToggleFarMode(FarMode);
    }

    public void ToggleFarMode(bool newFarMode)
    {       
        //if (GiCLaserPointer == null) return;
        if (FarMode) {
            // switch all vive controllers to display ray allways
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance) {
                if (inputDevice.deviceType == InputDeviceType.Vive) {
                    inputDevice.showUiRaySetting = UiCastVisualizeSetting.Allways;
                }
            }
        }
        else {
            // switch all vive controllers to display ray only when ui is hit
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance) {
                if (inputDevice.deviceType == InputDeviceType.Vive) {
                    inputDevice.showUiRaySetting = UiCastVisualizeSetting.UiHit;
                }
            }
            //GiCLaserPointer.DisableLaserPointer(GiCLaserPointer.laserPointerEmitter);
        }
    }

    public void SetBrushMaterialOne(bool newMaterial)
    {
        if (newMaterial)
        {
            SelectedBrushMaterial = 0;
        }
    }

    public void SetBrushMaterialTwo(bool newMaterial)
    {
        if (newMaterial)
        {
            SelectedBrushMaterial = 1;
        }
    }

    public void SetBrushMaterialThree(bool newMaterial)
    {
        if (newMaterial)
        {
            SelectedBrushMaterial = 2;
        }
    }

    public void SetBrushMaterialFour(bool newMaterial)
    {
        if (newMaterial)
        {
            SelectedBrushMaterial = 3;
        }
    }

    public void SetStrokeScale(float newScale)
    {
        BrushScale = newScale;
    }

    public void EnablePermanentMode(bool isPermanent)
    {
        if (isPermanent)
            permanent = true;
    }

    public void EnableNonPermanentMode(bool isNonPermanent)
    {
        if (isNonPermanent)
            permanent = false;
    }
    #endregion

}
