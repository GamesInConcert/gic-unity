﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// ReSharper disable MemberCanBePrivate.Global

public class GicStroke {

    // Mesh Information
    private List<Vector3> vertecies = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();
    private List<Color32> colors = new List<Color32>();
    //private List<Color32> playbackColors = new List<Color32>();
    private List<Vector2> uvs = new List<Vector2>();
    private List<int> indicies = new List<int>();
    public Mesh StrokeMesh { get; set; }
    public int MaterialId;
    public bool FarMode;

    private int vertexCount;

    //private MeshCollider collider;

    // General Stroke Information
    private ObservedList<GicPoint> strokePoints = new ObservedList<GicPoint>();
    public float TotalStrokeLength { get; set; }
    //public bool strokeComplete = false;
    public float StartingTime { get; set; }
    public float StartingAngle { get; set; }
    public float EndingAngle { get; set; }
    //public float StartingTimeNormalized { get; set; }

    public GameObject Playhead;

    private Vector3 playheadLerp;
    //private ParticleSystem.MainModule particlesMainModule;
    //private ParticleSystem pSys;

    // Change to InformationCore
    public GameObject GameObjectBelongingToThisStroke { get; set; }
    public AtomInformationCore AtomCore { get; set; }

    public ObservedList<GicPoint> StrokePoints
    {
        get { return strokePoints; }
        set { strokePoints = value; }
    }

    private int vertexIndex;
    private float uvPosition;
    private float uvAddtoPosition;
    private float deltaBa;
    private Vector3 dir;

    public float MinAngle = 400f;
    public float MaxAngle;

    public float TotalAngularDistance { get; set; }

    private float totalDeg;

    // playback information
    private float internalPlayheadTime;
    private float arcPos;
    private int foundTimeIndex = 2;
    private bool reversePlayback = false;

    public bool Playback { get; set; }
    //private bool iPlayback;

    public int StrokeId { get; set; }

    public OscMessage OscMessagePos { get; set; }
    public OscMessage OscMessageColor { get; set; }
    public OscMessage OscMessageScale { get; set; }
    public OscMessage OscMessageMat { get; set; }

    // internal privates
    private Vector3 prevPos= Vector3.zero;
    private Quaternion prevRot = Quaternion.identity;
    private float prevAngle = 0f;

    // Number of substeps between each gic point als teiler
    private int substepsPerPoint = 4;

    private int playheadIndex;

    public Vector3 pastOutPos { get; set; }
    public float pastOutTime { get; set; }

    private bool finished = false;
    //private Color pastOutColor;
    //private int pastOutMat;
    //private float pastOutScale;

    //private const float Pi = 3.1415926535897932384626433832795f, TwoPi = 6.283185307179586476925286766559f;

    #region Constructors
    // Constructor 1
    public GicStroke(Vector3 position, Quaternion rotation, float scale, Color color, float time, float startingtime, float angle)
    {
        vertexIndex = 0;
        AddPoint(position, rotation, scale, color, time, angle);
        StartingTime = startingtime;
        StrokeMesh = new Mesh();

        // must be in every constructor
        strokePoints.Updated += GicPointArrays_Updated;
    }

    // Constructor 2
    public GicStroke(float startingtime, GameObject drawingParent, int materialId, int strokeId, bool farMode, bool nonPermanent = false) {
//        Debug.Log("GicStroke.GicStroke: " + _startingtime + " " + _DrawingParent + " " + _materialID + " " + _strokeID +
//                  " " + nonPermanent);
        MaterialId = materialId;
        StrokeId = strokeId;
        vertexIndex = 0;
        uvPosition = 0;
        StartingTime = startingtime;
        FarMode = farMode;
        GameObjectBelongingToThisStroke = CreateNewStrokeGameObject(strokeId, materialId, drawingParent.transform, nonPermanent);
        //Debug.Log("DrawingParent: "+_DrawingParent.transform.position);
        //Debug.Log("GO_Stroke: "+gameObjectBelongingToThisStroke.transform.localPosition);
        AtomCore = GameObjectBelongingToThisStroke.transform.GetComponentInParent<AtomInformationCore>();
        //Debug.Log(AtomCore.AtomIdentifier);
        StrokeMesh = new Mesh();
        StrokeMesh.name = "StrokeMesh_" + strokeId;
        GameObjectBelongingToThisStroke.GetComponent<MeshFilter>().mesh = StrokeMesh;
        //collider = GameObjectBelongingToThisStroke.GetComponent<MeshCollider>();
        OscMessagePos = new OscMessage("/g/p/sp", 0, 0, 0f, 0f, 0f, 0f, 0f, 0f, 0f);

        // must be in every constructor
        strokePoints.Updated += GicPointArrays_Updated;
    }
    #endregion

    #region Callbacks
    //private bool isStrokePointsDirty = false;
    private void GicPointArrays_Updated(ObservedList<GicPoint>.ObserverListChange arg1, int[] arg2) {
        //Debug.Log("AtomObject.GicPointArrays_Updated: " + arg1.ToString() + " : " + arg2);
        switch (arg1) {
            case ObservedList<GicPoint>.ObserverListChange.add:
                break;
            case ObservedList<GicPoint>.ObserverListChange.removeAll:
                break;
        }
        //isStrokePointsDirty = true;
    }
    #endregion

    #region GicFunctions

    // The main Functions for Stroke Creation
    #region Create Stroke

    private GameObject CreateNewStrokeGameObject(int currentStrokeIndex, int materialIndex, Transform drawingParent, bool nonPermanent = false)
    {
//        Debug.Log("GicStroke.CreateNewStrokeGameObject: " + currentStrokeIndex + " " + MaterialIndex + " " +
//                  ((drawingParent != null) ? drawingParent.name : "null") + " " + nonPermanent);
        // First Create a new GameObject For our Stroke
        GameObject newStroke = new GameObject((nonPermanent ? "NpStroke_" : "Stroke_") + currentStrokeIndex, typeof(MeshRenderer), typeof(MeshFilter));

        newStroke.transform.parent = drawingParent;

        // Set base values
        newStroke.transform.localPosition = Vector3.zero;
        newStroke.transform.localRotation = Quaternion.identity;
        newStroke.transform.localScale = Vector3.one;
        newStroke.layer = 2;

        if (GiC_DrawStrokes.Instance != null) {
            if (GiC_DrawStrokes.Instance.BrushMaterials != null) {
//                Debug.Log("GicStroke.CreateNewStrokeGameObject: 2 " + GiC_DrawStrokes.Instance.brushMaterials.Length +
//                          " " + MaterialIndex);
                if ((0 <= materialIndex) && (materialIndex < GiC_DrawStrokes.Instance.BrushMaterials.Length)) {
                    newStroke.GetComponent<MeshRenderer>().sharedMaterial =
                        GiC_DrawStrokes.Instance.BrushMaterials[materialIndex];
                }
                else {
                    Debug.LogWarning(
                        "GicStroke.CreateNewStrokeGameObject: material ERROR!!! using replacement standard shader!!!");
                    newStroke.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Specular"));
                }
            }
            else {
                Debug.LogWarning("GicStroke.CreateNewStrokeGameObject: GiC_DrawStrokes.Instance.brushMaterials is null");
            }
        }
        else {
            Debug.LogWarning("GicStroke.CreateNewStrokeGameObject: GiC_DrawStrokes.Instance is null");
        }
        return newStroke;
    }

    public void AddPoint(Vector3 position, Quaternion rotation, float scale, Color color, float time, float angle)
    {
        AddPoint(position, rotation, scale, color, time, angle, Vector3.up, Vector3.right, Vector3.forward);
    }

    public void AddPoint(GicPoint newPoint)
    {
        AddPoint(newPoint.Position, newPoint.Rotation, newPoint.Scale, newPoint.Color, newPoint.Time, newPoint.Angle);
    }

    // TODO: up, right, forward können weg
    // For Every GiC Point we create subveretices and add them to the already existing mesh
    public void AddPoint(Vector3 position, Quaternion rotation, float scale, Color color, float time, float angle, Vector3 up, Vector3 right, Vector3 forward) {
        GicPoint newPoint = new GicPoint(position, rotation, scale, color, time, angle, up, right, forward);

        if (newPoint == null)
        {
            Debug.LogError("ERROR Point is Null ");
        }

        if (strokePoints.Count < 3)
        {
            strokePoints.Add(newPoint);
            prevPos = newPoint.Position;
            return;
        }

        //if (strokePoints.Count == 3)
        //{
        //    internalPlayheadTime = strokePoints[2].Time+0.1f;
        //    //Debug.Log(internalPlayheadTime);
        //}

        strokePoints.Add(newPoint);

        CreateVertexDataFromPoint(newPoint);

    }

    private void CreateVertexDataFromPoint(GicPoint point)
    {
        deltaBa = Vector3.Magnitude(prevPos - point.Position);
        TotalStrokeLength += deltaBa;

        //float steps = Mathf.Clamp(0.8f / ((Mathf.Abs(deltaBa) * 20f) + float.Epsilon), 0.2f, 5f);
        //steps = 1f / steps;

        //float currentAngle = point.Angle;
        //if (currentAngle - prevAngle < -180f)
        //{
        //    currentAngle -= 360f;
        //}

        //if (MinAngle > currentAngle)
        //{
        //    MinAngle = currentAngle;
        //    //minIndex = strokePoints.Count - 1;
        //}
        //else if (MaxAngle < currentAngle)
        //{
        //    MaxAngle = currentAngle;
        //    //maxIndex = strokePoints.Count - 1;
        //}

        //prevAngle = point.Angle;

        totalDeg += point.Angle;

        if (MinAngle > totalDeg)
        {
            MinAngle = totalDeg;
            //minIndex = strokePoints.Count - 1;
        }
        else if (MaxAngle < totalDeg)
        {
            MaxAngle = totalDeg;
            //maxIndex = strokePoints.Count - 1;
        }

        // Use Spline interpolation to create a curve between the previous and actual point (Add two points)
        //for (float i = 0.1f; i <= 0.9f; i += 0.4f)
        float part = 1f / substepsPerPoint;
        float end = 1 - part;
        //int debugCount = 0;

        for (float i = 0f; i <= end; i += part)
        {
            float time = i;
            //debugCount++;
            // Get position and rotation from spline interpolator
            Vector3 currPos = GetHermiteAtTime(time, strokePoints.Count - 1);
            Quaternion curRot = GetRotationAtTime(time, strokePoints.Count - 1);
            Vector3 newdir = prevPos - currPos;
            currPos = Vector3.Lerp(prevPos, currPos, 0.5f);
            curRot = Quaternion.LerpUnclamped(prevRot, curRot, 0.5f);
            dir = Vector3.Lerp(dir, newdir, 0.5f);

            // TODO: Hier stimmt noch was nicht ... Die rotationsnormale sollte der vektor zwischen punkt und kamera sein?
            // Calculate the rotation of the "line" of vertecies by taking the previeous point - actual point and rotating that by 90°
            //Vector3 rotationNormal = currPos - Camera.main.transform.position;
            float angleBetweenRight = AngleSigned(Camera.main.transform.right, dir, Camera.main.transform.forward);
            //float angleBetweenRight = Vector3.Angle(Camera.main.transform.right, dir);
             
            //Debug.Log(angleBetweenRight);
            //float angleBetweenRight = AngleSigned(Vector3.right, dir, Vector3.forward);
            newdir = Quaternion.Euler(0f, 0f, angleBetweenRight) * Vector3.up;
            //newdir = Vector3.right;

            // HACK: Test
            //float angleBetweenRight = AngleSigned(Vector3.right, dir, (Camera.main.transform.position - currPos).normalized);
            //newdir = Quaternion.Euler(0f, 0f, angleBetweenRight) * Vector3.up;

            // Calculate interpolated color and add them to the list (Two times as there are two vertecies)
            Color32 newColor = Color.Lerp(strokePoints[strokePoints.Count - 1].Color, point.Color, time);
            colors.Add(newColor);
            colors.Add(newColor);

            // Calculate interpolated scale as to know where to put the vertecies
            float newScale = Mathf.Lerp(strokePoints[strokePoints.Count - 2].Scale, point.Scale, time);

            // Calculate and add the Vertices to the Vertices array (to create a mesh later)
            // Take the center (GiCPoint) Move it in the desired direction (rotation) using the newScale as distance.
            //currPos = gameObjectBelongingToThisStroke.transform.InverseTransformPoint(currPos);
            Vector3 localPos = GameObjectBelongingToThisStroke.transform.InverseTransformPoint(currPos);
            vertecies.Add(localPos + (curRot * newdir) * newScale);
            vertecies.Add(localPos + (curRot * -newdir) * newScale);
            //Debug.Log(Vertecies[Vertecies.Count-1]);

            // Calculate and add the Texture Coordinates to the uv array (to create a mesh later)
            Vector4 uvS = CalculateUVs(time);
            uvs.Add(new Vector2(uvS.x, uvS.y));
            uvs.Add(new Vector2(uvS.z, uvS.w));

            // Calculate and add the Triangle indices and add them to their List
            // First Triangle
            indicies.Add(vertexIndex);
            indicies.Add(vertexIndex + 1);
            indicies.Add(vertexIndex + 3);

            // Second Triangle
            indicies.Add(vertexIndex + 2);
            indicies.Add(vertexIndex + 0);
            indicies.Add(vertexIndex + 3);

            // Calculate Normals New Version
            Vector3 pointNormal = Camera.main.transform.position - currPos;
            normals.Add(pointNormal);
            normals.Add(pointNormal);

            // Calculate Normals Old Version
            //if (vertexIndex > 3)
            //{
            //    Vector3 BA = Vertecies[vertexIndex -1] - Vertecies[vertexIndex];
            //    Vector3 CA = Vertecies[vertexIndex -2] - Vertecies[vertexIndex];
            //    Vector3 newNormal = Vector3.Cross(BA, CA);
            //    Normals.Add(newNormal);
            //    Normals.Add(newNormal);
            //    if (vertexIndex == 4)
            //    {
            //        Normals.Add(newNormal);
            //        Normals.Add(newNormal);
            //        Normals.Add(newNormal);
            //        Normals.Add(newNormal);
            //    }

            //    //BA = Vertecies[vertexIndex - 2] - Vertecies[vertexIndex - 3];
            //    //CA = Vertecies[vertexIndex] - Vertecies[vertexIndex - 3];
            //    //newNormal = Vector3.Cross(BA, CA);
            //    //Normals.Add(newNormal);
            //    //if (vertexIndex == 4)
            //    //{
            //    //    Normals.Add(newNormal);
            //    //    Normals.Add(newNormal);

            //    //}
            //}


            prevPos = currPos;
            prevRot = curRot;
            // advance the vertex count by 2
            vertexIndex += 2;
        }
        if (indicies.Count > 6)
            UpdateMesh();
    }

    public void FinishStroke(bool revPlayback = false)
    {
        reversePlayback = revPlayback;
        //Debug.Log("revPB = "+reversePlayback);
        if (reversePlayback)
        {
            float temp = StartingAngle;
            StartingAngle = EndingAngle;
            EndingAngle = temp;
        }
        finished = true;
        //Debug.Log("Stroke angles:" + MinAngle + " " + MaxAngle + " " + StartingAngle);
    }

    #endregion

    // Helper functions for stroke creation
    # region Stroke Helpers
    public Vector3 GetHermiteInternal(Vector3[] transforms, float t)
    {
        float t2 = t * t;
        float t3 = t2 * t;

        Vector3 p0 = transforms[0];
        Vector3 p1 = transforms[1];
        Vector3 p2 = transforms[2];
        Vector3 p3 = p2;

        float tension = 0.5f;   // 0.5 equivalent to catmull-rom

        Vector3 ten1 = tension * (p2 - p0);
        Vector3 ten2 = tension * (p3 - p1);

        float blend1 = 2 * t3 - 3 * t2 + 1;
        float blend2 = -2 * t3 + 3 * t2;
        float blend3 = t3 - 2 * t2 + t;
        float blend4 = t3 - t2;

        return blend1 * p1 + blend2 * p2 + blend3 * ten1 + blend4 * ten2;
    }

    Quaternion GetSquad(Quaternion[] rotations, float t)
    {
        Quaternion q0 = rotations[0];
        Quaternion q1 = rotations[1];
        Quaternion q2 = rotations[2];
        Quaternion q3 = q2;

        Quaternion t1 = GiC_MathUtils.GetSquadIntermediate(q0, q1, q2);
        Quaternion t2 = GiC_MathUtils.GetSquadIntermediate(q1, q2, q3);

        return GiC_MathUtils.GetQuatSquad(t, q1, q2, t1, t2);
    }

    public Vector3 GetHermiteAtTime(float timeParam, int index)
    {
        if (index > strokePoints.Count)
        {
            return strokePoints[strokePoints.Count - 1].Position;
        }
        Vector3[] transforms = {strokePoints[index - 2].Position, strokePoints[index - 1].Position, strokePoints[index].Position };

        if (timeParam >= 1)
            return transforms[2];

        float param = GiC_MathUtils.Ease(timeParam, 0f, 1f);

        //Debug.Log(param);

        return GetHermiteInternal(transforms, param);
    }

    public Quaternion GetRotationAtTime(float timeParam, int index)
    {
        if (index > strokePoints.Count)
        {
            return strokePoints[strokePoints.Count - 1].Rotation;
        }
        Quaternion[] rotations = { strokePoints[index - 2].Rotation, strokePoints[index - 1].Rotation, strokePoints[index].Rotation };

        if (timeParam >= 1)
            return rotations[2];

        float param = GiC_MathUtils.Ease(timeParam, 0f, 1f);

        return GetSquad(rotations, param);
    }

    public Vector4 CalculateUVs(float time)
    {

        float part = (deltaBa*time)*0.1f;
        if (uvPosition > 0.7f)
        {
            uvAddtoPosition = -part;
        } else if (uvPosition < 0.3f)
        {
            uvAddtoPosition = part;
        }

        uvPosition += uvAddtoPosition;

        return new Vector4(uvPosition, 0, uvPosition, 1);
    }

    public int[] GetIndexArray()
    {

        var triangles = new int[(vertecies.Count / 2 - 1) * 2 * 3];
        for (int i = 0; i < triangles.Length / 6; i++)
        {
            triangles[i * 6 + 0] = i * 2;
            triangles[i * 6 + 1] = i * 2 + 1;
            triangles[i * 6 + 2] = i * 2 + 2;

            triangles[i * 6 + 3] = i * 2 + 2;
            triangles[i * 6 + 4] = i * 2 + 1;
            triangles[i * 6 + 5] = i * 2 + 3;
        }

        return triangles;
    }

    public void UpdateMesh()
    {
        //collider.sharedMesh = null;
        StrokeMesh.Clear();
        StrokeMesh.vertices = vertecies.ToArray();
        StrokeMesh.uv = uvs.ToArray();
        //mesh.triangles = Indicies.ToArray();
        StrokeMesh.triangles = GetIndexArray();
        StrokeMesh.colors32 = colors.ToArray();
        StrokeMesh.normals = normals.ToArray();
        //strokeMesh.RecalculateNormals();
        //mesh.RecalculateBounds();
        //collider.sharedMesh = mesh;
    }

    /// <summary>
    /// Determine the signed angle between two vectors, with normal 'n'
    /// as the rotation axis.
    /// </summary>
    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    #endregion

    // Playback Related
    #region Playback Related

    public void GetValuesAtTime(out Vector3 position, out float scale, out Color color, out int material, out float time)
    {
        if (reversePlayback)
        {
            GetValuesAtTimeReversed(out position, out scale, out color, out material, out time);
            return;
        }
        GetValsAtTime(out position, out scale, out color, out material, out time);
    }


    public void GetValsAtTime(out Vector3 position, out float scale, out Color color, out int material, out float time)
    {

        // if internalPlayheadTime outside the time of the third stroke point and the last stroke point
        // return the values of the second stroke point...

        //Debug.Log("1 -> " + internalPlayheadTime + " " + strokePoints[2].Time);
        if (internalPlayheadTime < strokePoints[2].Time)
        {
            position = strokePoints[2].Position;
            pastOutPos = position - AtomCore.transform.position;          
            scale = strokePoints[2].Scale;
            color = strokePoints[2].Color;
            material = MaterialId;
            time = strokePoints[2].Time;
            pastOutTime = time;
            return;
        }

        //Debug.Log("2 -> " + internalPlayheadTime + " " + strokePoints[strokePoints.Count - 1].Time);
        if (internalPlayheadTime > strokePoints[strokePoints.Count-1].Time)
        {
            position = strokePoints[strokePoints.Count - 1].Position;
            scale = strokePoints[strokePoints.Count - 1].Scale;
            color = strokePoints[strokePoints.Count - 1].Color;
            material = MaterialId;
            time = strokePoints[strokePoints.Count - 1].Time;
            return;
        }


        // Watch Out
        while ((internalPlayheadTime > strokePoints[foundTimeIndex].Time) && (foundTimeIndex < strokePoints.Count - 1))
        {           
            playheadIndex ++;
            foundTimeIndex++;                      
        }
        // TODO: Check if correct, if foundTimeIndex is larger then (strokePoints.Count - 1) it continues?
        //DisplayPlayhead(playheadIndex);
        GicPoint actualPoint = strokePoints[foundTimeIndex];
        GicPoint pastPoint = strokePoints[foundTimeIndex - 1];

        if (internalPlayheadTime < pastPoint.Time)
        {
            position = pastPoint.Position;
            scale = pastPoint.Scale;
            color = pastPoint.Color;
            material = MaterialId;
            time = pastPoint.Time;
            return;
        }

        //Debug.Log("4 -> " +internalPlayheadTime + " " + actualPoint.Time);
        if (internalPlayheadTime > actualPoint.Time)
        {
            position = actualPoint.Position;
            scale = actualPoint.Scale;
            color = actualPoint.Color;
            time = actualPoint.Time;
            material = MaterialId;
            return;
        }
        
        float timeparam = Mathf.Clamp01((internalPlayheadTime - pastPoint.Time) / (actualPoint.Time - pastPoint.Time));
        position = GetHermiteAtTime(timeparam, foundTimeIndex);
        if (float.IsNaN(position.x))
            Debug.Log("ERROR");
        scale = Mathf.Lerp(pastPoint.Scale, actualPoint.Scale, timeparam);
        color = Color.Lerp(pastPoint.Color, actualPoint.Color, timeparam);
        time = Mathf.Lerp(pastPoint.Time, actualPoint.Time, timeparam);
        material = MaterialId;
    }

    public void GetValuesAtTimeReversed(out Vector3 position, out float scale, out Color color, out int material, out float time)
    {

        // if internalPlayheadTime outside the time of the third stroke point and the last stroke point
        // return the values of the second stroke point...

        //Debug.Log("Rev");

        //Debug.Log("1 -> " + internalPlayheadTime + " " + strokePoints[2].Time);
        if (internalPlayheadTime < strokePoints[2].Time)
        {
            position = strokePoints[2].Position;
            pastOutPos = position;
            scale = strokePoints[2].Scale;
            color = strokePoints[2].Color;
            material = MaterialId;
            time = strokePoints[2].Time;
            return;
        }

        //Debug.Log("2 -> " + internalPlayheadTime + " " + strokePoints[strokePoints.Count - 1].Time);
        if (internalPlayheadTime > strokePoints[strokePoints.Count - 1].Time)
        {
            position = strokePoints[strokePoints.Count - 1].Position;
            scale = strokePoints[strokePoints.Count - 1].Scale;
            color = strokePoints[strokePoints.Count - 1].Color;
            material = MaterialId;
            time = strokePoints[strokePoints.Count - 1].Time;
            return;
        }


        // Watch Out
        while ((internalPlayheadTime < strokePoints[foundTimeIndex].Time) && (foundTimeIndex > 2))
        {
            playheadIndex--;
            foundTimeIndex--;
        }

        //Debug.Log("phT: " + internalPlayheadTime + "ftI: " + foundTimeIndex );
        // TODO: Check if correct, if foundTimeIndex is larger then (strokePoints.Count - 1) it continues?
        //DisplayPlayhead(playheadIndex);
        GicPoint actualPoint = strokePoints[foundTimeIndex];
        GicPoint pastPoint = strokePoints[foundTimeIndex + 1];

        if (internalPlayheadTime > pastPoint.Time)
        {
            position = pastPoint.Position;
            scale = pastPoint.Scale;
            color = pastPoint.Color;
            material = MaterialId;
            time = pastPoint.Time;
            return;
        }

        //Debug.Log("4 -> " +internalPlayheadTime + " " + actualPoint.Time);
        if (internalPlayheadTime < actualPoint.Time)
        {
            position = actualPoint.Position;
            scale = actualPoint.Scale;
            color = actualPoint.Color;
            time = actualPoint.Time;
            material = MaterialId;
            return;
        }

        float timeparam = Mathf.Clamp01((internalPlayheadTime - pastPoint.Time) / (actualPoint.Time - pastPoint.Time));
        //Debug.LogWarning(timeparam);
        position = GetHermiteAtTime(timeparam, foundTimeIndex);
        scale = Mathf.Lerp(pastPoint.Scale, actualPoint.Scale, timeparam);
        color = Color.Lerp(pastPoint.Color, actualPoint.Color, timeparam);
        time = Mathf.Lerp(pastPoint.Time, actualPoint.Time, timeparam);
        //Debug.Log(time);
        material = MaterialId;
    }

    private void DisplayPlayhead(int index)
    {
        int numSteps = 1;
        Color32[] colors32 = StrokeMesh.colors32;
        int substeps = colors32.Length / (strokePoints.Count - 3);

        //index -= 3;
        if (index < 3)
        {
            index = 0;
        }
        if (index > strokePoints.Count - 3)
        {
            index = strokePoints.Count - 3;
        }

        //muss weiterlaufen nach stop. Berechnung: lerp = 1/steps*i

        int start = ((index - (numSteps+1)) * substeps);

        //Clip und multiply
        start = start < 0 ? 0 : start;

        int end = index*substeps;
        end = end > colors32.Length ? colors32.Length - 1 : end;

        //Debug.Log(start + " " + Mathf.Abs(start-end) + " " + index + " " + strokePoints.Count);

            for (int i = start; i < end; i += 2)
            {
                int imult = ((i - start) - substeps);
                imult = imult < 0 ? 0 : imult; 
                //Debug.Log(imult);
                float lerp = (1f / substeps) * (imult);
                //lerp = i == 0 ? 0f : lerp;
                //lerp = lerp > 1f ? 1f : lerp;
                colors32[i] = Color32.Lerp(colors[i], Color.white, lerp);
                colors32[i + 1] = Color32.Lerp(colors[i+1], Color.white, lerp);
            }
        //}
        StrokeMesh.colors32 = colors32;
    }

    public void MoveGoPlayhead(Vector3 position)
    {
        if (float.IsNaN(position.x) || float.IsNaN(position.y) || float.IsNaN(position.z))
        {
            Debug.LogError("MoveGoPlayHead position contains NaN " + position);
            return;
        }  
        if (Playhead == null) return;
        playheadLerp = Vector3.Lerp(playheadLerp, position, Time.unscaledDeltaTime * 20f);
        Playhead.transform.position = playheadLerp;
    }

    private void ResetGoPlayhead()
    {
        ParticleSystem[] ps = Playhead.GetComponentsInChildren<ParticleSystem>();
        Light[] light = Playhead.GetComponentsInChildren<Light>();
        foreach (var p in ps)
        {
            p.Stop();
        }

        foreach (var l in light)
        {
            l.enabled = false;
        }
        //emi.enabled = false;
        playheadLerp = strokePoints[2].Position;
        Playhead.transform.position = playheadLerp;
        foreach (var p in ps)
        {
            ParticleSystem.EmissionModule emi = p.emission;
            emi.enabled = true;
            p.Play();
        }
        foreach (var l in light)
        {
            l.enabled = true;
        }
    }

    public void ChangePlayheadColor(Color newcolor)
    {
        //if (playhead == null || arcPos < 1f) return;
        //if (playhead != null && pSys == null)
        //{
        //    pSys = playhead.GetComponent<ParticleSystem>();
        //    var em = pSys.emission;
        //    em.enabled = true;
        //    particlesMainModule = playhead.GetComponent<ParticleSystem>().main;
        //}       
        //particlesMainModule.startColor = newcolor;
    } 

    public void AdvanceAngularPlayhead(float speed)
    {
        //Debug.Log(collider.bounds.size);
        if (!Playback) return;
        //float arcLength = Mathf.Abs(AngleSigned(collider.bounds.min, collider.bounds.max, gameObjectBelongingToThisStroke.transform.parent.parent.forward));
        //float ma = MaxAngle;
        //if (MinAngle > MaxAngle)
        //{
        //    ma = MaxAngle + 360f;
        //}


        float arcLength = Mathf.Abs(MinAngle - MaxAngle);

        //Debug.Log("Min: "+MinAngle+ " Max: " + MaxAngle + " AL: " + arcLength + " TD: "+ TotalAngularDistance);

        //if (Mathf.Abs(TotalAngularDistance - arcLength) > 180)
        //{
        //    arcLength = TotalAngularDistance;
        //}

        //TODO: Remove this hack

        if (arcPos > arcLength+1)
        {
            //    Debug.Log("Stop "+arcPos + " " + arcLength);
            StopPlayback();
            return;
        }

        float advancement = strokePoints[strokePoints.Count - 1].Time * (arcPos / arcLength);

        internalPlayheadTime = reversePlayback ? strokePoints[strokePoints.Count - 1].Time - advancement : advancement;

        //Debug.Log("Running "+internalPlayheadTime + " " + arcPos + " " + arcLength + " " + strokePoints[strokePoints.Count - 1].Time);

        arcPos += 360f * (Time.unscaledDeltaTime * speed);
        //if (internalPlayheadTime >= stroke[stroke.Count - 1].time)
        //{
        //    internalPlayheadTime = stroke[2].time;
        //    foundTimeIndex = 2;
        //}
    }

    public void DeleteStroke()
    {
        if (Playhead != null)
        {
            GameObject.Destroy(Playhead);
        }
    }

    // if we overwrite a stroke from the network we use this function to delete the stroke
    public void RemoveStroke () {
        AtomCore = null;
        GameObject.Destroy(GameObjectBelongingToThisStroke);
        strokePoints.Updated -= GicPointArrays_Updated;
        StrokeMesh = null;
        OscMessagePos = null;
    }

    //void AdvanceTimeBasedPlayhead(float speed)
    //{
    //    internalPlayheadTime += Time.unscaledDeltaTime * speed;
    //    if (internalPlayheadTime >= strokePoints[strokePoints.Count - 1].Time)
    //        StopPlayback();
    //}

    public void StartPlayback()
    {
        if (strokePoints.Count >= 6 && finished)
        {
            if (GiCPlaybackManager.Instance.PlayheadPrefab != null && Playhead == null)
            {
                Playhead = GameObject.Instantiate(GiCPlaybackManager.Instance.PlayheadPrefab);             
            }
            arcPos = 0f;
            internalPlayheadTime = reversePlayback ? strokePoints[strokePoints.Count-1].Time : strokePoints[2].Time;
            foundTimeIndex = reversePlayback ? strokePoints.Count - 2 : 2;
            playheadIndex = foundTimeIndex;
            ResetGoPlayhead();
            Playback = true;
            //iPlayback = true;
        }
    }

    public OscMessage StopPlayback()
    {
        //iPlayback = false;
        Playback = false;
        StrokeMesh.colors32 = colors.ToArray();
        if (Playhead != null)
        {
            GameObject.Destroy(Playhead);
            //playhead.GetComponent<ParticleSystem>().Stop();
            //var em = playhead.GetComponent<ParticleSystem>().emission;
            //em.enabled = false;
        }
        //TODO: ADD CALLBACK TO STOP PLAYBACK ON OLAVS SIDE
        Vector3 outPos;
        Color outColor;
        float outScale;
        int outMat;
        float outTime;
        GetValuesAtTime(out outPos, out outScale, out outColor, out outMat, out outTime);
        outPos = outPos - AtomCore.transform.position;
        HSBColor hsbOut = HSBColor.FromColor(outColor);
        OscMessagePos.args[0] = AtomCore.AtomIdentifier;
        OscMessagePos.args[1] = StrokeId;
        OscMessagePos.args[2] = outPos.x;
        OscMessagePos.args[3] = outPos.y;
        OscMessagePos.args[4] = outPos.z;
        OscMessagePos.args[5] = 0f;
        OscMessagePos.args[6] = hsbOut.b;
        OscMessagePos.args[7] = 0f;
        OscMessagePos.args[8] = outMat;

        if (GicOscManager.Instance != null)
            GicOscManager.Instance.AddDataToMusicalInformationBundle(OscMessagePos);

        //stroke.oscMessagePos.args[6] = hsbOut.s;
        //stroke.oscMessagePos.args[7] = hsbOut.b;
        //stroke.oscMessagePos.args[8] = outMat;
        return OscMessagePos;
    }

    public void Resync()
    {
        StopPlayback();
        StartPlayback();
    }

    #endregion

    #endregion

    #region Possibly Obsolete
    public Vector3 GetLocalHermiteAtTime(float timeParam, int index)
    {
        if (index > strokePoints.Count)
        {
            return strokePoints[strokePoints.Count - 1].Position;
        }
        Vector3[] transforms = { strokePoints[index - 2].Position, strokePoints[index - 1].Position, strokePoints[index].Position };

        if (timeParam >= 1)
            return transforms[2];

        float param = GiC_MathUtils.Ease(timeParam, 0f, 1f);

        return GetHermiteInternal(transforms, param);
    }

    public Vector2[] CreateUvArray()
    {

        Vector2[] uvs = new Vector2[vertexCount];
        int index = 0;
        float distance = 0f;
        float uvPos = 0f;
        Vector3 prevPos = strokePoints[0].Position;
        bool forward = true;
        float uvAdvance = 0.05f;
        foreach (GicPoint p in strokePoints)
        {
            float deltaLength = Vector3.SqrMagnitude(prevPos - p.Position);

            if (uvPos < 0.2f)
            {
                uvAdvance = 0.05f;
            }
            else if (uvPos > 0.8f)
            {
                uvAdvance = -0.05f;
            }

            uvPos += uvAdvance / deltaLength;
            uvPos = Mathf.Clamp01(uvPos);

            uvs[index] = new Vector2(1, uvPos);
            uvs[index + 1] = new Vector2(uvPos, 0);
            index += 2;
        }
        return uvs;
    }

    public int[] GetIndexArray(int vertexCount)
    {
        int[] triangles = new int[vertecies.Count <= 4 ? 6 : ((vertecies.Count - 2) * 3)];

        int index = 0;
        for (int i = 0; i < triangles.Length / 6; i++)
        {
            triangles[i * 6] = index;
            triangles[i * 6 + 1] = index + 1;
            triangles[i * 6 + 2] = index + 3;

            triangles[i * 6 + 3] = index + 2;
            triangles[i * 6 + 4] = index + 0;
            triangles[i * 6 + 5] = index + 3;

            index += 2;

        }

        return triangles;
    }

    public Vector3[] GetGicPointsPositionsArray()
    {
        var positions = strokePoints.Select(item => item.Position).ToArray();
        return positions;
    }

    public Vector3[] GetVertexArray()
    {
        return vertecies.ToArray();
    }

    public Vector2[] GetUvArray()
    {
        return uvs.ToArray();
    }

    public void AddSectionVertices(Vector3 vertex1, Vector3 vertex2)
    {
        vertecies.Add(vertex1);
        vertecies.Add(vertex2);
    }

    public Vector3[] CreateVertexArray()
    {
        Vector3[] vertices = new Vector3[strokePoints.Count * 2];
        vertexCount = vertices.Length;
        int index = 0;
        foreach (GicPoint p in strokePoints)
        {
            vertices[index + 1] = p.Position + (p.Rotation * Vector3.up) * p.Scale;
            vertices[index] = p.Position + (p.Rotation * Vector3.down) * p.Scale;
            index += 2;
        }
        return vertices;
    }

    #endregion


    #region Serialization & Deserialization
    public byte[] GetAsBytes() {
        NetworkWriter networkWriter = new NetworkWriter();
        networkWriter.Write(strokePoints.Count);
        networkWriter.Write(StartingTime);
        networkWriter.Write(MaterialId);
        networkWriter.Write(StrokeId);
        networkWriter.Write(FarMode);
        networkWriter.Write(StartingAngle);
        for (int i = 0; i < strokePoints.Count; i++) {
            strokePoints[i].AddBytesToNetworkWriter(networkWriter);
        }
        return networkWriter.AsArray();
    }

    public static GicStroke SetFromBytes(byte[] strokeBytes, GameObject drawingParent) {
        NetworkReader networkReader = new NetworkReader(strokeBytes);
        int count = networkReader.ReadInt32();
        float startingTime = networkReader.ReadSingle();
        int materialId = networkReader.ReadInt32();
        int strokeId = networkReader.ReadInt32();
        bool farMode = networkReader.ReadBoolean();
        float startingAngle = networkReader.ReadSingle();
        GicStroke gicStroke = new GicStroke(startingTime, drawingParent, materialId, strokeId, farMode);
        gicStroke.StartingAngle = startingAngle;
        //ObservedList<GicPoint> _strokePoints = new ObservedList<GicPoint>();
//        Debug.Log("- - GicStroke.ReadFromNetworkReader: add " + count + " points to " + _gicStroke.gameObjectBelongingToThisStroke.name +" (parent) "+DrawingParent);
        for (int i = 0; i < count; i++) {
            GicPoint newPointFromNetwork = GicPoint.ReadFromNetworkReader(networkReader);
            if (newPointFromNetwork == null) {
                Debug.LogError("ERROR: GicPoint received via network is null.. this must not happen. return");
            }
            gicStroke.AddPoint(newPointFromNetwork);
        }
//        Debug.Log("- - GicStroke.ReadFromNetworkReader: return add " + _gicStroke);
        return gicStroke;
    }
    #endregion

    #region Debug
    public override string ToString() {
        StringBuilder sb = new StringBuilder();
        sb.Append("GicPointArray size = ");
        sb.Append(strokePoints.Count);
        sb.Append("\n");
        for (int i = 0; i < strokePoints.Count; i++) {
            sb.Append(strokePoints[i]);
        }
        return sb.ToString();
    }
    #endregion
}
