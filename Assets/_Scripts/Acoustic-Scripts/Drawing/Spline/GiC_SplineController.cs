using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eOrientationMode { NODE = 0, TANGENT }

[AddComponentMenu("Splines/Spline Controller")]
[RequireComponent(typeof(GiC_SplineInterpolator))]
public class GiC_SplineController : MonoBehaviour
{
	public GameObject SplineRoot;
	public float Duration = 10;
	public eOrientationMode OrientationMode = eOrientationMode.NODE;
	public eWrapMode WrapMode = eWrapMode.ONCE;
	public bool AutoStart = true;
	public bool AutoClose = true;
	public bool HideOnExecute = true;


	GiC_SplineInterpolator mGiCSplineInterp;
	Transform[] mTransforms;

    private Transform[] trans;
    private Transform[] mytrans;


    void OnDrawGizmos()
	{
		trans = GetTransforms();
        mytrans = trans;
		if (trans == null || trans.Length < 2)
			return;

		GiC_SplineInterpolator interp = GetComponent(typeof(GiC_SplineInterpolator)) as GiC_SplineInterpolator;
		SetupSplineInterpolator(interp, trans);
		interp.StartInterpolation(null, false, WrapMode);


		Vector3 prevPos = trans[0].position;
		for (int c = 1; c <= 100; c++)
		{
			float currTime = c * Duration / 100;
			Vector3 currPos = interp.GetHermiteAtTime(currTime);
			float mag = (currPos-prevPos).magnitude * 2;
			Gizmos.color = new Color(mag, 0, 0, 1);
			Gizmos.DrawLine(prevPos, currPos);
			prevPos = currPos;
		}
	}

    void CreateMyMesh()
    {
        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        Transform[] trans = mTransforms;

        if (trans == null || trans.Length < 2)
        {
            Debug.Log("Transforms not found"+ trans.Length.ToString());
            return;
        }
            

        GiC_SplineInterpolator interp = GetComponent(typeof(GiC_SplineInterpolator)) as GiC_SplineInterpolator;
        SetupSplineInterpolator(interp, trans);
        interp.StartInterpolation(null, false, WrapMode);

        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> myUV = new List<Vector2>();
        Vector3 prevPos = trans[0].position;
        for (int c = 1; c <= 100; c++)
        {
            float currTime = c * Duration / 100;
            Vector3 currPos = interp.GetHermiteAtTime(currTime);
            Quaternion curRot = interp.GetRotationAtTime(currTime);
            Vector3 Vertex1 = currPos + (curRot*Vector3.up)*0.8f;
            Vector3 Vertex2 = currPos + (curRot * Vector3.down) * 0.8f;
            float mag = (currPos - prevPos).magnitude * 2;
            vertices.Add(Vertex1);

            Debug.Log(Vertex1.ToString());

            vertices.Add(Vertex2);
            prevPos = currPos;
        }

        int vCount = vertices.Count;

        for (int i = 0; i < vCount; i += 2)
        {
            myUV.Add(new Vector2(1, (float)i/(float)vCount));
            myUV.Add(new Vector2((float)i / (float)vCount, 0));
        }

        var triangles = new int[(vCount / 2 - 1) * 2 * 3];
        for (int i = 0; i < triangles.Length / 6; i++)
        {
            triangles[i * 6 + 0] = i * 2;
            triangles[i * 6 + 1] = i * 2 + 1;
            triangles[i * 6 + 2] = i * 2 + 2;

            triangles[i * 6 + 3] = i * 2 + 2;
            triangles[i * 6 + 4] = i * 2 + 1;
            triangles[i * 6 + 5] = i * 2 + 3;
        }

        mesh.vertices = vertices.ToArray();
        mesh.uv = myUV.ToArray();
        mesh.triangles = triangles;

    }


	void Start()
	{
		mGiCSplineInterp = GetComponent(typeof(GiC_SplineInterpolator)) as GiC_SplineInterpolator;

		mTransforms = GetTransforms();

		if (HideOnExecute)
			DisableTransforms();

		if (AutoStart)
			FollowSpline();
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            CreateMyMesh();
        }
    }

	void SetupSplineInterpolator(GiC_SplineInterpolator interp, Transform[] trans)
	{
		interp.Reset();

		float step = (AutoClose) ? Duration / trans.Length :
			Duration / (trans.Length - 1);

		int c;
		for (c = 0; c < trans.Length; c++)
		{
			if (OrientationMode == eOrientationMode.NODE)
			{
				interp.AddPoint(trans[c].position, trans[c].rotation, step * c, new Vector2(0, 1));
			}
			else if (OrientationMode == eOrientationMode.TANGENT)
			{
				Quaternion rot;
				if (c != trans.Length - 1)
					rot = Quaternion.LookRotation(trans[c + 1].position - trans[c].position, trans[c].up);
				else if (AutoClose)
					rot = Quaternion.LookRotation(trans[0].position - trans[c].position, trans[c].up);
				else
					rot = trans[c].rotation;

				interp.AddPoint(trans[c].position, rot, step * c, new Vector2(0, 1));
			}
		}

		if (AutoClose)
			interp.SetAutoCloseMode(step * c);
	}


	/// <summary>
	/// Returns children transforms, sorted by name.
	/// </summary>
	Transform[] GetTransforms()
	{
		if (SplineRoot != null)
		{
			List<Component> components = new List<Component>(SplineRoot.GetComponentsInChildren(typeof(Transform)));
			List<Transform> transforms = components.ConvertAll(c => (Transform)c);

			transforms.Remove(SplineRoot.transform);
			transforms.Sort(delegate(Transform a, Transform b)
			{
				return a.name.CompareTo(b.name);
			});
			return transforms.ToArray();
		}

		return null;
	}

	/// <summary>
	/// Disables the spline objects, we don't need them outside design-time.
	/// </summary>
	void DisableTransforms()
	{
		if (SplineRoot != null)
		{
			SplineRoot.SetActiveRecursively(false);
		}
	}


	/// <summary>
	/// Starts the interpolation
	/// </summary>
	void FollowSpline()
	{
		if (mTransforms.Length > 0)
		{
			SetupSplineInterpolator(mGiCSplineInterp, mTransforms);
			mGiCSplineInterp.StartInterpolation(null, true, WrapMode);
		}
	}
}