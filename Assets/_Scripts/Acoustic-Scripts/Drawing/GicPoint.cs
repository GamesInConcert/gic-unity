﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;


public class GicPoint {
    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }
    public float Scale { get; set; }
    public Color Color { get; set; }
    public float Time { get; set; }
    public Vector2 EaseIo { get; set; }

    public float Angle { get; set; }

    public Vector3 Up { get; set; }
    public Vector3 Right { get; set; }
    public Vector3 Forward { get; set; }

    public GicPoint(Vector3 _position, Quaternion _rotation, float _scale, Color _color, float _time, float _angle) : this()
    {
        Position = _position;
        Rotation = _rotation;
        Scale = _scale;
        Color = _color;
        Time = _time;
        EaseIo = new Vector2(0f, 1f);
        Up = Vector3.up;
        Right = Vector3.right;
        Forward = Vector3.forward;
        Angle = _angle;
    }

    public GicPoint(Vector3 _position, Quaternion _rotation, float _scale, Color _color, float _time, float _angle, Vector3 _up, Vector3 _right, Vector3 _forward) : this()
    {
        Position = _position;
        Rotation = _rotation;
        Scale = _scale;
        Color = _color;
        Time = _time;
        Angle = _angle;
        EaseIo = new Vector2(0f, 1f);
        Up = _up;
        Right = _right;
        Forward = _forward;
    }

    public GicPoint(Vector3 _position, Quaternion _rotation, float _scale, Color _color, float _time, Vector2 _easeio, float _angle, Vector3 _up, Vector3 _right, Vector3 _forward) {
        Position = _position;
        Rotation = _rotation;
        Scale = _scale;
        Color = _color;
        Time = _time;
        EaseIo = _easeio;
        Angle = _angle;
        Up = _up;
        Right = _right;
        Forward = _forward;
    }

    public GicPoint()
    {
        Position = Vector3.zero;
        Rotation = Quaternion.identity;
        Scale = 1f;
        Color = Color.black;
        Time = 0f;
        EaseIo = new Vector2(0f, 1f);
        Angle = 0f;
        Up = Vector3.up;
        Right = Vector3.right;
        Forward = Vector3.forward;
    }

    public void AddBytesToNetworkWriter(NetworkWriter networkWriter) {
        networkWriter.Write(Position);
        networkWriter.Write(Rotation);
        networkWriter.Write(Scale);
        networkWriter.Write(Color);
        networkWriter.Write(Time);
        networkWriter.Write(EaseIo);
        networkWriter.Write(Angle);
        networkWriter.Write(Up);
        networkWriter.Write(Right);
        networkWriter.Write(Forward);
    }
    public static GicPoint ReadFromNetworkReader(NetworkReader networkReader) {
        GicPoint gicPoint = new GicPoint(
            networkReader.ReadVector3(),
            networkReader.ReadQuaternion(),
            networkReader.ReadSingle(),
            networkReader.ReadColor(),
            networkReader.ReadSingle(),
            networkReader.ReadVector2(),
            networkReader.ReadSingle(),
            networkReader.ReadVector3(),
            networkReader.ReadVector3(),
            networkReader.ReadVector3()
        );
        return gicPoint;
        /*position = networkReader.ReadVector3();
        rotation = networkReader.ReadQuaternion();
        scale = networkReader.ReadSingle();
        color = networkReader.ReadColor();
        time = networkReader.ReadSingle();
        EaseIO = networkReader.ReadVector2();
        angle = networkReader.ReadSingle();
        up = networkReader.ReadVector3();
        right = networkReader.ReadVector3();
        forward = networkReader.ReadVector3();*/
    }

    public override string ToString () {
        StringBuilder sb = new StringBuilder();
        sb.Append(Position).Append(", ");
        sb.Append(Rotation).Append(", ");
        sb.Append(Scale).Append(", ");
        sb.Append(Color).Append(", ");
        sb.Append(Time).Append(", ");
        sb.Append(EaseIo).Append(", ");
        sb.Append(Angle).Append(", ");
        sb.Append(Up).Append(", ");
        sb.Append(Right).Append(", ");
        sb.Append(Forward).Append(";\n");
        return sb.ToString();
    }
}
