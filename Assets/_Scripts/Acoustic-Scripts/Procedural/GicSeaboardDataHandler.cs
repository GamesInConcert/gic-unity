﻿using UnityEngine;

public enum LoopMode
{
    RealTime,
    Loop,
    LoopWithMetronome
}

public struct SeaboardData
{
    public int Pitch;
    public int Velocity;
    public int Aftertouch;

    public SeaboardData(int pitchIn = 0, int velocityIn = 0, int aftertouchIn = 0)
    {
        Pitch = pitchIn;
        Velocity = velocityIn;
        Aftertouch = aftertouchIn;
    }

    public void SetAll(int pitchIn, int velocityIn, int aftertouchIn)
    {
        Pitch = pitchIn;
        Velocity = velocityIn;
        Aftertouch = aftertouchIn;
    }
}

public class GicSeaboardDataHandler : MonoBehaviour {
    public int StartPitch = 32;
    public int KeysRange = 64;
    public bool UseAftertouch;
    public bool UseGlide;

    private GiC_ProceduralAtom theAtom;

    //private readonly List<SeaboardData> seabordData = new List<SeaboardData>();
    private readonly SeaboardData[] velocities = new SeaboardData[127];
    private float glideValue;
    private int slideValue;

    private GameObject seaboardGeometry;

    private Texture2D baseTextureRealTime;
    private int dispTexOffsetUid;
    private Material myMaterial;

    private float[] oldcolorsArrayRealTime;
    private float[] internalVelocitiesArray;

    //private Color32[] colorsArrayLoop;
    private Color32[] colorsArrayRealTime;

    private Vector2 seaboardXyPadLerp = Vector2.zero;
    private Vector2 seaboardVector2XyIn = Vector2.zero;
    private Vector2 prevSeaboardVector2XyIn = Vector2.zero;

    private float slideValueLerp;

    private Gic_ProceduralAtomParameters visParam;

    private bool initialized;

    // Use this for initialization
    private void Start () {
        dispTexOffsetUid = Shader.PropertyToID("_DisplaceTex_1_ST");

     //   seabordData.Clear();
	    //for (int i = 0; i <= 127; i++)
     //   {
     //       seabordData.Add(new SeaboardData(i));
     //   }

        InitializeTextures();
    }

    private void InitializeTextures()
    {
        baseTextureRealTime = new Texture2D(1, KeysRange, TextureFormat.ARGB32, false, true);
        baseTextureRealTime.wrapMode = TextureWrapMode.Repeat;

        colorsArrayRealTime = new Color32[KeysRange];
        oldcolorsArrayRealTime = new float[KeysRange];
        internalVelocitiesArray = new float[KeysRange];

        myMaterial = seaboardGeometry.GetComponent<Renderer>().material;
        myMaterial.SetTexture("_DisplaceTex_1", baseTextureRealTime);
        myMaterial.SetFloat("_DisplaceStrength", 5.88f);

        baseTextureRealTime.SetPixels32(colorsArrayRealTime);
        baseTextureRealTime.Apply();
    }

    public void InitializeHandler(GiC_ProceduralAtom parentProceduralAtomComponent)
    {
        theAtom = parentProceduralAtomComponent;
        seaboardGeometry = parentProceduralAtomComponent.SeaboardGeometry;
        visParam = new Gic_ProceduralAtomParameters(theAtom.Core.ProceduralAtomParameters.visualParameters);
        initialized = true;
    }
	
	// Update is called once per frame
    private void Update () {
	    if (initialized)
	    {
	        UpdateDisplacementTexture();
	    }
	    seaboardXyPadLerp = Vector2.Lerp(prevSeaboardVector2XyIn, seaboardVector2XyIn, Time.unscaledDeltaTime);
	}

    private void UpdateDisplacementTexture()
    {
        for (int i = StartPitch; i < StartPitch + KeysRange; i++)
        {
            int internalI = i - StartPitch;

            float vel = velocities[i].Velocity * 2;
            if (UseAftertouch)
            {
                vel = Mathf.Clamp(vel * (velocities[i].Aftertouch / 127f + 1f), 0f, 255f);
            }
            vel = Mathf.Lerp(oldcolorsArrayRealTime[internalI], vel, Time.unscaledDeltaTime * 5);
            oldcolorsArrayRealTime[internalI] = vel;
            internalVelocitiesArray[internalI] = vel;
        }

        float[] convolvedArray = GiC_MathUtils.Convolve(internalVelocitiesArray);

        int xi = 0;

        if (UseGlide)
        {
            slideValueLerp = Mathf.Lerp(slideValueLerp, slideValue / 255f - 0.25f, Time.unscaledDeltaTime * 3f);
            //xi += slideValue;
            //generativeObjectMaterial.SetTextureOffset("_DisplaceTex_1", new Vector2(0,slideValue/127f-0.5f));
            myMaterial.SetVector(dispTexOffsetUid, new Vector4(1, 1, slideValueLerp, slideValueLerp));
        }

        foreach (var val in convolvedArray)
        {
            byte value = (byte)Mathf.FloorToInt(val);
            colorsArrayRealTime[xi] = new Color32(value, value, value, value);
            xi++;
            xi %= convolvedArray.Length;
        }
        baseTextureRealTime.SetPixels32(colorsArrayRealTime);
        baseTextureRealTime.Apply();
    }

    private void UpdateSeaboardGeometry()
    {
        visParam = Gic_ProceduralAtomParameters.QLerp(theAtom.Core.ProceduralAtomParameters.visualParameters,
            theAtom.Core.ProceduralAtomParameters.visualParametersModX,
            theAtom.Core.ProceduralAtomParameters.visualParametersModY, seaboardXyPadLerp);
        //visParam.PerlinNoiseXCoordinate = theAtom.Core.proceduralAtomParameters.visualParameters.PerlinNoiseXCoordinate * seaboardXyPadLerp.x;
        //visParam.PerlinNoiseYCoordinateMultiplier = theAtom.Core.proceduralAtomParameters.visualParameters.PerlinNoiseYCoordinateMultiplier * seaboardXyPadLerp.y;
        var mesh = GiC_ProceduralObjects.GenerativeSphericalObject(visParam).ToMesh();
        mesh.RecalculateNormals();
        theAtom.SeaboardGeometry.GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public void SetSeaboardGlideValue(float newGlideValue)
    {
        glideValue = newGlideValue;
    }

    public void SetSeaboardSlideValue(int newSlideValue)
    {
        slideValue = newSlideValue;
    }

    public void SetSeaboardDataPiVeRelVe (int pitch, int velocity, int aftertouch)
    {
        if (velocity < 1)
        {
            aftertouch = 0;
        }
        velocities[pitch].SetAll(pitch, velocity, aftertouch);
    }

    public void SetSeaboardDataAfterTouch(int pitch, int aftertouch)
    {
        velocities[pitch].Aftertouch = aftertouch;
    }

    public void SetSeaboardDataXyPad(float x, float y)
    {
        prevSeaboardVector2XyIn = seaboardVector2XyIn;
        seaboardVector2XyIn.x = x*0.5f;
        seaboardVector2XyIn.y = y*0.5f;
        UpdateSeaboardGeometry();
    }

    public void SetSeaboardDataOnOff(int state)
    {

    }

}
