﻿using UnityEngine;
using System.Collections;
using VR = UnityEngine.VR;

public class EnableVR : MonoBehaviour {

    public bool VREnabled = false;

    void Awake()
    {
        UnityEngine.XR.XRSettings.enabled = VREnabled;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
