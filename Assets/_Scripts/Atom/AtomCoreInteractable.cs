﻿using System.Collections;
using UnityEngine;
using WorldspaceController;


// Funktionalitäten:
// Control Authority
// Move
// Store
// Delete
// Set Marker (for others)

public class AtomCoreInteractable : MonoBehaviour
{
    private NetworkInteractableObject interactableObject;

    private AtomUiController uiController { get; set; }
    private GiC_ProceduralAtom theAtom;

    private bool editing {
        get {
            return NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject);
        }
    }

    private bool requestEditing = false;
    private bool openMenu { get; set; }
    private GiC_AtomCoreInteractionsEnum currentInteraction = GiC_AtomCoreInteractionsEnum.Nothing;

    private Color defaultColor;
    private Color currentColor;

    private Renderer myRenderer;
    private int shaderColorPropertyId;

    private Vector3 currentDrawingScale = Vector3.one;
    private float currentAtomScale = 1f;

    public float atomHeightAboveTerrain = 1.5f;

    private bool AtomIsSmall = false;
    private bool AtomIsScaling = false;
    private bool playerIsOutsideEditArea = true;

    private bool firstEdit = true;

    private IEnumerator scaleDrIEnumerator;
    private IEnumerator scaleAtIEnumerator;


    #region Initialize and Destroy

        // TODO: Hier müssen wir uns überlegen wo und wann man das menu aufmacht.. On Start authority macht sinn.. aber wann dropt man die authority? Wenn das menu zugeht, geht nicht da man ja vllt malen will etc..

    public void Initialize(GiC_ProceduralAtom atom)
    {
        // setup to receive callbacks of the object itself
        // controller callbacks must be done by the WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent callback

        openMenu = false;
        requestEditing = false;

        interactableObject = GetComponent<NetworkInteractableObject>();

        AddListeners();

        theAtom = atom;

        shaderColorPropertyId = Shader.PropertyToID("_Color");
        myRenderer = theAtom.SeaboardGeometry.gameObject.GetComponent<Renderer>();
        defaultColor = myRenderer.material.GetColor(shaderColorPropertyId);
        currentColor = defaultColor;

        if (!GiCUiManager.Instance.DisableMenu && GiCUiManager.Instance.atomMenuItems.Length > 0)
        {
            uiController = gameObject.AddComponent<AtomUiController>();
            uiController.Initialize(this);
        }
        else
        {
            requestEditing = true;
            //ToggleEditMode();
        }
    }

    private void OnDisable()
    {
        RemoveListeners();
    }

    private void AddListeners()
    {
        if (interactableObject == null) return;
        interactableObject.onAttachRepeatEvent.AddListener(GrabRepeatEvent);
        interactableObject.onAttachStartEvent.AddListener(OnAttachStartEvent);
        interactableObject.onAttachStopEvent.AddListener(OnAttachStopEvent);
        interactableObject.onHoverEnterEvent.AddListener(OnHoverEnterEvent);
        interactableObject.onHoverExitEvent.AddListener(OnHoverExitEvent);

        interactableObject.onUseStartEvent.AddListener(OnUseStart);

        // dont attach to contorller, insted use own transform funtion
        interactableObject.disableDefaultApplyTransformEvent = true;
        interactableObject.onApplyTransformOverrideEvent.AddListener(ApplyTransformation);

        interactableObject.onStartAuthorityCallback.AddListener(OnStartAuthority);
        interactableObject.onStopAuthorityCallback.AddListener(OnStopAuthority);
    }

    private void RemoveListeners()
    {
        if (interactableObject == null) return;
        interactableObject.onAttachRepeatEvent.RemoveListener(GrabRepeatEvent);
        interactableObject.onAttachStartEvent.RemoveListener(OnAttachStartEvent);
        interactableObject.onAttachStopEvent.RemoveListener(OnAttachStopEvent);
        interactableObject.onHoverEnterEvent.RemoveListener(OnHoverEnterEvent);
        interactableObject.onHoverExitEvent.RemoveListener(OnHoverExitEvent);

        interactableObject.onUseStartEvent.RemoveListener(OnUseStart);

        // dont attach to contorller, insted use own transform funtion
        interactableObject.disableDefaultApplyTransformEvent = true;
        interactableObject.onApplyTransformOverrideEvent.RemoveListener(ApplyTransformation);

        interactableObject.onStartAuthorityCallback.RemoveListener(OnStartAuthority);
        interactableObject.onStopAuthorityCallback.RemoveListener(OnStopAuthority);
    }

    #endregion

    #region Unity Internals

    private void Update()
    {

        int pdSwitch = PlayerDistanceToObjectSwitch(30f, 5f);

        if (pdSwitch > 0 && currentAtomScale < 2f && !AtomIsScaling)
        {
            ScaleAtom(2f);
            //StopCoroutine("ScaleAtomIenumerator");
            //StartCoroutine(ScaleAtomIenumerator(2f));
        }

        if (pdSwitch < 0 && currentAtomScale > 1f && !AtomIsScaling)
        {
            ScaleAtom(1f);
            //StopCoroutine("ScaleAtomIenumerator");
            //StartCoroutine(ScaleAtomIenumerator(1f));
        }
    }

    #endregion

    #region Direct Interactions

    private void OnUseStart(InputDeviceData deviceData)
    {
        if (uiController == null)
        {
            ToggleEditMode();
        }
        else
        {
            ToggleMenu();
        }
    }

    public void ToggleMenu()
    {
        firstEdit = false;
        if (!uiController.menuIsOpen)
        {
            openMenu = true;
            StartCoroutine(GiC_SoundingObjectsManager.Instance.ManageAuthorityCoroutine(theAtom));
        }
        else
        {
            StartCoroutine(GiC_SoundingObjectsManager.Instance.ManageAuthorityCoroutine(null));
        }
    }

    public void ToggleEditMode()
    {
        //firstEdit = false;
        if (theAtom.Core.SendToMaxPlaybackNow)
        {
            Debug.Log("Still Sending Data to olav. Wait please");
            return;
        }

        if (!editing)
        {
            requestEditing = true;
            //StartCoroutine(GiC_SoundingObjectsManager.instance.ManageAuthorityCoroutine(theAtom));
            GiC_SoundingObjectsManager.Instance.ManageAuthority(theAtom);
        }
        else
        {
            //StartCoroutine(GiC_SoundingObjectsManager.instance.ManageAuthorityCoroutine(null));
            GiC_SoundingObjectsManager.Instance.ManageAuthority(null);
        }
    }

    #endregion

    #region Edit Mode Handling

    public void StartEditMode()
    {
        //if (editing)
        //{
        //    Debug.LogError("Started Edit Mode with editing=true");
        //    return;
        //}
        //StartCoroutine(GetAuthorityAndStartEditMode());

        theAtom.Core.DisarmRecordPlayback();

        ScaleDrawing(1f);
        //editing = true;
        theAtom.SetEditMode(editing);
        SetEditModeColor(editing);
        GicEditRoomManager.Instance.OpenEditRoom(theAtom.transform.position);
    }

    public void StopEditMode()
    {
        //if (!editing)
        //{
        //    Debug.LogError("AtomCoreInteractable.StopEditmode: "+editing);
        //    return;
        //}


        //editing = false;
        theAtom.SetEditMode(editing);
        SetEditModeColor(editing);
        ScaleDrawing(new Vector3(0.15f, 0.3f, 0.15f));
        GicEditRoomManager.Instance.CloseEditRoom();
    }

    //private IEnumerator GetAuthorityAndStartEditMode()
    //{
    //    yield return StartCoroutine(GiC_SoundingObjectsManager.instance.ManageAuthority(theAtom));
    //    StartCoroutine(ScaleDrawing(1f));
    //    editing = true;
    //    theAtom.SetEditMode(editing);
    //}

    #endregion

    #region Authority Handling

    // Event Listeners
    public void OnStartAuthority()
    {
        //Debug.Log("AtomCoreInteractable.OnStartAuthority:" +
        //    " localPlayerAuth: " + NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject) +
        //    " hasAuth: " + theAtom.networkCore.hasAuthority +
        //    " client " + theAtom.networkCore.isClient +
        //    " server " + theAtom.networkCore.isServer +
        //    " atomId " + theAtom.Core.AtomIdentifier);
      
        // if dont have authority we should not execute
        if (!NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject))
        {
            //theAtom.EditModeIndicator.SetActive(true);
            Debug.LogError("AtomCoreInteractable.OnStartAuthority: ERROR: " + theAtom.networkCore.hasAuthority);
            return;
        }

        //Debug.LogWarning("AtomcoreInteractable: On Start authority");
        GiC_SoundingObjectsManager.Instance.RegisterAtom(theAtom, requestEditing);

        // If we want to open a menu
        if (openMenu && uiController != null)
        {
            uiController.OpenMenu();
            openMenu = false;
        }

        // If we do not have a menu but want to start editing
        if (requestEditing)
        {
            StartEditMode();
            requestEditing = false;
        }

    }

    private void OnStopAuthority()
    {
        //Debug.Log("AtomCoreInteractable.OnStopAuthority:" +
        //    " localPlayerAuth: " + NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject) +
        //    " hasAuth: " + theAtom.networkCore.hasAuthority +
        //    " client " + theAtom.networkCore.isClient +
        //    " server " + theAtom.networkCore.isServer + 
        //    " atomId " + theAtom.Core.AtomIdentifier);



        // if still have authority we should not execute
        if (NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject)) {
            Debug.LogError("AtomCoreInteractable.OnStopAuthority: ERROR: " + theAtom.networkCore.hasAuthority);
            //theAtom.EditModeIndicator.SetActive(false);
            return;
        }

        //if (!NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject)) return;
        //Debug.LogWarning("OnStopAuthority Atom: " + theAtom.Core.AtomIdentifier);
        if (uiController != null)
            uiController.CloseMenu();

        // Das darf natürlich nur passieren wenn currentlyeditableatom == this.atom
        if ((GiC_SoundingObjectsManager.Instance.currentEditableAtom != null) && (theAtom.Core.AtomIdentifier == GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.AtomIdentifier)) {
            GiC_DrawStrokes.Instance.OnControllerTriggerUnClicked();
            GiC_SoundingObjectsManager.Instance.UnregisterAtom(theAtom);
            StopEditMode();
        }
        else {
            Debug.LogError("AtomCoreInteractable.OnStopAuthority: current Atom not equal to object that sends StopAuthority " + GiC_SoundingObjectsManager.Instance.currentEditableAtom);
        }
    }

    #endregion

    #region Helpers

    private void SetEditModeColor(bool isEditing)
    {
        if (myRenderer == null) return;
        currentColor = isEditing ? new Color(0.7f, 0.1f, 0f, 1f) : defaultColor;
        myRenderer.material.SetColor(shaderColorPropertyId, currentColor);
    }

    private void ScaleDrawing(float factor, float timeToScale = 1f)
    {
        if (scaleDrIEnumerator != null)
        {
            StopCoroutine(scaleDrIEnumerator);
        }
        scaleDrIEnumerator = ScaleDrawingIenumerator(new Vector3(factor, factor, factor), timeToScale);
        StartCoroutine(scaleDrIEnumerator);
    }

    private void ScaleDrawing(Vector3 factor, float timeToScale = 1f)
    {
        if (scaleDrIEnumerator != null)
        {
            StopCoroutine(scaleDrIEnumerator);
        }
        scaleDrIEnumerator = ScaleDrawingIenumerator(factor, timeToScale);
        StartCoroutine(scaleDrIEnumerator);
    }
    //private void ScaleDrawing
    private IEnumerator ScaleDrawingIenumerator(Vector3 factor, float timeToScale = 1f)
    {
        float mix = 0f;
        Vector3 newScale = Vector3.zero;
        while (mix < 1.01f)
        {
            mix += Time.unscaledDeltaTime * (1 / timeToScale);
            newScale = Vector3.Lerp(currentDrawingScale, factor, Mathf.Clamp01(mix));
            currentDrawingScale = newScale;
            theAtom.Core.DrawingDataHandler.DrawingParent.transform.localScale = Vector3.Scale(Vector3.one, newScale);
            yield return null;
        }
        currentDrawingScale = newScale;
    }

    private void ScaleAtom(float factor, float timeToScale = 1f)
    {
        if (scaleAtIEnumerator != null)
        {
            StopCoroutine(scaleAtIEnumerator);
        }
        scaleAtIEnumerator = ScaleAtomIenumerator(factor, timeToScale);
        StartCoroutine(scaleAtIEnumerator);
    }

    private IEnumerator ScaleAtomIenumerator(float factor, float timeToScale = 1f)
    {
        float mix = 0f;
        float newScale = 0f;
        AtomIsScaling = true;
        while (mix < 1.01f)
        {
            mix += Time.unscaledDeltaTime * (1 / timeToScale);
            newScale = Mathf.SmoothStep(currentAtomScale, factor, Mathf.Clamp01(mix));
            currentAtomScale = newScale;
            theAtom.SeaboardGeometry.transform.localScale = Vector3.one * newScale;
            yield return null;
        }
        AtomIsScaling = false;
        currentAtomScale = newScale;
    }

    private int PlayerDistanceToObjectSwitch(float switchingDistance = 80f, float hysteresis = 5f)
    {
        int returnValue = 0;
        float distanceToAtom = Vector3.SqrMagnitude(theAtom.gameObject.transform.position - NetworkPlayerController.LocalInstance.transform.position);
        //Debug.Log(distanceToAtom.ToString());
        if (distanceToAtom > switchingDistance + hysteresis)
            returnValue = 1;
        else if (distanceToAtom < switchingDistance - hysteresis)
            returnValue = -1;

        return returnValue;
    }

    #endregion

    #region Hover and Grab Event Callbacks

    public Quaternion relativeRotation = Quaternion.identity;
    public Vector3 relativePosition = Vector3.zero;
    private void OnAttachStartEvent(InputDeviceData deviceData)
    {
        //StartCoroutine(ScaleAtomIenumerator(0.5f, 0.1f));
        interactableObject.DoAttachToController(deviceData);

        relativePosition = deviceData.inputDevice.controller.transform.InverseTransformPoint(transform.position);
        relativeRotation = Quaternion.Inverse(deviceData.inputDevice.controller.transform.rotation) * transform.rotation;
    }

    //private void OnAttachStopEvent(InputDeviceData deviceData)
    //{
    //    StartCoroutine(ScaleAtom(1f));
    //    interactableObject.DoDetachFromController(deviceData);
    //}

    public void ApplyTransformation(InputDeviceData deviceData)
    {
        // if another player takes the atom out of our hands we loose local player authority
        if (NetworkInteractableObject.HasLocalPlayerAuthority(this.gameObject)) {
            Vector3 newPosition = deviceData.inputDevice.controller.transform.TransformPoint(relativePosition);
            Quaternion newRotation = deviceData.inputDevice.controller.transform.rotation * relativeRotation;

            SetPosition(newPosition);
            //float terrainHeight = Terrain.activeTerrain.SampleHeight(newPosition);
            //newPosition.y = terrainHeight + Terrain.activeTerrain.transform.position.y + atomHeightAboveTerrain;

            //gameObject.transform.position = newPosition;
        }
    }

    public void SetPosition (Vector3 newPosition) {
        float terrainHeight = Terrain.activeTerrain.SampleHeight(newPosition);
        newPosition.y = terrainHeight + Terrain.activeTerrain.transform.position.y + atomHeightAboveTerrain;

        gameObject.transform.position = newPosition;
    }

    //ApplyTransformation does the movement now
    //private void OnAttachStopEvent(InputDeviceData deviceData)
    //{
    //    StartCoroutine(ScaleAtom(1f));
    //    interactableObject.DoDetachFromController(deviceData);
    //}

    private void OnAttachStopEvent(InputDeviceData deviceData)
    {
        //StartCoroutine(ScaleAtomIenumerator(1f));

        //foreach (var core in GiC_SoundingObjectsManager.instance.GetAllAtoms())
        //{
        //    if (core.LeapDataHandler != null) {
        //        core.LeapDataHandler.CheckDistanceToNeighbours();
        //    }
        //}

        interactableObject.DoDetachFromController(deviceData);
    }

    private void GrabRepeatEvent(InputDeviceData deviceData)
    {
        if (GicOscManager.Instance == null)
            return;
        if (theAtom == null)
            return;
        if (theAtom.Core == null)
            return;
        GicOscManager.Instance.SendAtomPosition(theAtom.Core.AtomIdentifier, theAtom.Core.gameObject.transform.position);

        if (theAtom.LeapGeometry == null) return;
        //theAtom.Core.LeapDataHandler.CheckDistanceToNeighbours();
        //theAtom.Core.LeapDataHandler.DispatchAndSetShaderBuffers();
    }

    private void OnHoverEnterEvent(InputDeviceData deviceData)
    {
        if (myRenderer)
        {
            myRenderer.material.SetColor(shaderColorPropertyId, currentColor*Color.grey);
        }
    }

    private void OnHoverExitEvent(InputDeviceData deviceData)
    {
        if (myRenderer)
        {
            myRenderer.material.SetColor(shaderColorPropertyId, currentColor);
        }
    }

    #endregion

}
