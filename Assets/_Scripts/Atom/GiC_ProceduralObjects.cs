﻿using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

public static class GiC_ProceduralObjects {

// Mesh Draft Scripts
   public static MeshDraft GenerativeSphericalObject(Gic_ProceduralAtomParameters parameters, float perlinMult = 2f)
   {
       int longitudeSegments = parameters.longitudeSegments;
       int latitudeSegments = parameters.latitudeSegments;
       float MinimumRadius = parameters.MinimumRadius;
       float Mult1 = parameters.Mult1;


        var draft = new MeshDraft { name = "GenerativeSphericalObject" };

        float longitudeSegmentAngle = Mathf.PI * 2 / longitudeSegments;
        float latitudeSegmentAngle = Mathf.PI / latitudeSegments;

        float[] longituteAngles = new float[longitudeSegments];
        float longAnglesStart = -longitudeSegmentAngle;

        for (int i = 0; i < longituteAngles.Length - 1; i++)
        {
            float random = Random.Range(0f, Mathf.PI * (1f / longitudeSegments));
            random = Mathf.PerlinNoise(parameters.PerlinNoiseXCoordinate, i) * (Mathf.PI * (1f / longitudeSegments));
            //float random = 0f;
            longituteAngles[i] = longAnglesStart + random;
            longituteAngles[i + 1] = longAnglesStart - random;
            longAnglesStart -= longitudeSegmentAngle;
        }

        int sameness = 0;
        float random1 = 0;

        float currentLatitude = -Mathf.PI / 2;
        for (var ring = 0; ring <= latitudeSegments; ring++)
        {
            var currentLongitude = 0f;
            if (sameness == 0)
            {
                //random1 = 1 + (Random.Range(-1f, 1.0f)*Mult2);
                float signy = 1f;
                if (sameness == 1)
                    signy *= -1f;
                random1 = Mathf.PerlinNoise(parameters.PerlinNoiseXCoordinate, ((float)ring * parameters.PerlinNoiseYCoordinateMultiplier) / (float)latitudeSegments) * parameters.BaseMultiplier1;
                random1 = random1 * 2f - 1f;
                random1 += 1*signy;
            }

            sameness = (sameness + 1) % (int)parameters.SubstepsModulo;

            for (int i = 0; i < longitudeSegments; i++)
            {
                Vector3 point = Vector3.zero;
                //Color col = parameters.colorA;
                float radius = 0f;
                switch (parameters.Form)
                {
                    case Formen.asses:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(currentLongitude))), MinimumRadius, 100f);
                        //point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.vases:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(currentLatitude))), MinimumRadius, 100f);
                        // point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.random:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(Mathf.PerlinNoise(random1, (float)i / (float)longitudeSegments)))) * Mult1, MinimumRadius, 100f);
                        // point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.BatMask:
                        radius = random1 * (Mathf.Abs(Mathf.Sin(currentLongitude) * Mathf.Cos(currentLatitude)));
                        // radius = Mathf.Clamp(radius, Add3, 100f);
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Sin(currentLongitude) * Mathf.Cos(currentLatitude))), currentLongitude, currentLatitude);
                        break;
                    case Formen.IrokeseFish:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Cos(currentLatitude) * Mult1)));
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Cos(currentLatitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.Spaceships:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLatitude) * Mult1)));
                        //point = PTUtils.PointOnSphereOld(Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLatitude) * Mult1))), Add3, 100f), currentLongitude, currentLatitude);
                        break;
                    case Formen.SpaceFlowers:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLongitude) * Mult1)));
                        //point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLongitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.Spores:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mathf.Atan(currentLongitude) * Mult1)));
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mathf.Atan(currentLongitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.PlayWithMult:
                        radius = random1 *
                                 (Mathf.Abs(
                                     Mathf.Cos(currentLongitude * Mult1 +
                                               Mathf.PerlinNoise(currentLongitude, currentLatitude))));
                        //  point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mult1 + Mathf.PerlinNoise(currentLongitude, currentLatitude)))), currentLongitude, currentLatitude);
                        break;
                    default:
                        radius = random1;
                        // point = PTUtils.PointOnSphereOld(random1, currentLongitude, currentLatitude);
                        break;
                }
                float colMult = Mathf.Clamp(radius / parameters.BaseMultiplier1, 0f, 1f);
                radius = Mathf.Clamp(radius, MinimumRadius, 100f);
                point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);


                // Debug.Log(colMult);

                draft.colors.Add(Color.Lerp(parameters.colorA, parameters.colorB, colMult));
                draft.vertices.Add(point);
                draft.normals.Add(point.normalized);
                draft.uv.Add(new Vector2((float)i / longitudeSegments, (float)ring / latitudeSegments));
                currentLongitude = longituteAngles[i];
            }
            currentLatitude += latitudeSegmentAngle;
        }

        int i0, i1, i2, i3;
        for (int ring = 0; ring < latitudeSegments; ring++)
        {
            for (int i = 0; i < longitudeSegments - 1; i++)
            {
                i0 = ring * longitudeSegments + i;
                i1 = (ring + 1) * longitudeSegments + i;
                i2 = ring * longitudeSegments + i + 1;
                i3 = (ring + 1) * longitudeSegments + i + 1;
                draft.triangles.AddRange(new[] { i0, i1, i2 });
                draft.triangles.AddRange(new[] { i2, i1, i3 });
            }

            i0 = (ring + 1) * longitudeSegments - 1;
            i1 = (ring + 2) * longitudeSegments - 1;
            i2 = ring * longitudeSegments;
            i3 = (ring + 1) * longitudeSegments;
            draft.triangles.AddRange(new[] { i0, i1, i2 });
            draft.triangles.AddRange(new[] { i2, i1, i3 });
        }
        draft.Scale(parameters.MeshScaleFactor);
        return draft;
    }

    public static MeshDraft GenerativeSphericalObjectTriStrip(Gic_ProceduralAtomParameters parameters, float perlinMult = 2f)
    {
        int longitudeSegments = parameters.longitudeSegments;
        int latitudeSegments = parameters.latitudeSegments;
        float MinimumRadius = parameters.MinimumRadius;
        float Mult1 = parameters.Mult1;


        var draft = new MeshDraft { name = "GenerativeSphericalObject" };
        List<Vector3> verts = new List<Vector3>();

        float longitudeSegmentAngle = Mathf.PI * 2 / longitudeSegments;
        float latitudeSegmentAngle = Mathf.PI / latitudeSegments;

        float[] longituteAngles = new float[longitudeSegments];
        float longAnglesStart = -longitudeSegmentAngle;

        for (int i = 0; i < longituteAngles.Length - 1; i++)
        {
            float random = Random.Range(0f, Mathf.PI * (1f / longitudeSegments));
            random = Mathf.PerlinNoise(parameters.PerlinNoiseXCoordinate, i) * (Mathf.PI * (1f / longitudeSegments));
            //float random = 0f;
            longituteAngles[i] = longAnglesStart + random;
            longituteAngles[i + 1] = longAnglesStart - random;
            longAnglesStart -= longitudeSegmentAngle;
        }

        int sameness = 0;
        float random1 = 0;

        float currentLatitude = -Mathf.PI / 2;
        for (var ring = 0; ring <= latitudeSegments; ring++)
        {
            var currentLongitude = 0f;
            if (sameness == 0)
            {
                //random1 = 1 + (Random.Range(-1f, 1.0f)*Mult2);
                float signy = 1f;
                if (sameness == 1)
                    signy *= -1f;
                random1 = Mathf.PerlinNoise(parameters.PerlinNoiseXCoordinate, ((float)ring * parameters.PerlinNoiseYCoordinateMultiplier) / (float)latitudeSegments) * parameters.BaseMultiplier1;
                random1 = random1 * 2f - 1f;
                random1 += 1 * signy;
            }

            sameness = (sameness + 1) % (int)parameters.SubstepsModulo;

            for (int i = 0; i < longitudeSegments; i++)
            {
                Vector3 point = Vector3.zero;
                //Color col = parameters.colorA;
                float radius = 0f;
                switch (parameters.Form)
                {
                    case Formen.asses:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(currentLongitude))), MinimumRadius, 100f);
                        //point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.vases:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(currentLatitude))), MinimumRadius, 100f);
                        // point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.random:
                        radius = Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Sin(Mathf.PerlinNoise(random1, (float)i / (float)longitudeSegments)))) * Mult1, MinimumRadius, 100f);
                        // point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);
                        break;
                    case Formen.BatMask:
                        radius = random1 * (Mathf.Abs(Mathf.Sin(currentLongitude) * Mathf.Cos(currentLatitude)));
                        // radius = Mathf.Clamp(radius, Add3, 100f);
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Sin(currentLongitude) * Mathf.Cos(currentLatitude))), currentLongitude, currentLatitude);
                        break;
                    case Formen.IrokeseFish:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Cos(currentLatitude) * Mult1)));
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Cos(currentLatitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.Spaceships:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLatitude) * Mult1)));
                        //point = PTUtils.PointOnSphereOld(Mathf.Clamp(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLatitude) * Mult1))), Add3, 100f), currentLongitude, currentLatitude);
                        break;
                    case Formen.SpaceFlowers:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLongitude) * Mult1)));
                        //point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude) + (Mathf.Atan(currentLongitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.Spores:
                        radius = random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mathf.Atan(currentLongitude) * Mult1)));
                        // point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mathf.Atan(currentLongitude) * Mult1))), currentLongitude, currentLatitude);
                        break;
                    case Formen.PlayWithMult:
                        radius = random1 *
                                 (Mathf.Abs(
                                     Mathf.Cos(currentLongitude * Mult1 +
                                               Mathf.PerlinNoise(currentLongitude, currentLatitude))));
                        //  point = PTUtils.PointOnSphereOld(random1 * (Mathf.Abs(Mathf.Cos(currentLongitude * Mult1 + Mathf.PerlinNoise(currentLongitude, currentLatitude)))), currentLongitude, currentLatitude);
                        break;
                    default:
                        radius = random1;
                        // point = PTUtils.PointOnSphereOld(random1, currentLongitude, currentLatitude);
                        break;
                }
                float colMult = Mathf.Clamp(radius / parameters.BaseMultiplier1, 0f, 1f);
                radius = Mathf.Clamp(radius, MinimumRadius, 100f);
                point = PTUtils.PointOnSphereOld(radius, currentLongitude, currentLatitude);


                // Debug.Log(colMult);
                verts.Add(point);
                draft.colors.Add(Color.Lerp(parameters.colorA, parameters.colorB, colMult));
                draft.vertices.Add(point);
                draft.normals.Add(point.normalized);
                draft.uv.Add(new Vector2((float)i / longitudeSegments, (float)ring / latitudeSegments));
                currentLongitude = longituteAngles[i];
            }
            currentLatitude += latitudeSegmentAngle;
        }

        draft = MeshDraft.TriangleStrip(verts);

        //int i0, i1, i2, i3;
        //for (int ring = 0; ring < latitudeSegments; ring++)
        //{
        //    for (int i = 0; i < longitudeSegments - 1; i++)
        //    {
        //        i0 = ring * longitudeSegments + i;
        //        i1 = (ring + 1) * longitudeSegments + i;
        //        i2 = ring * longitudeSegments + i + 1;
        //        i3 = (ring + 1) * longitudeSegments + i + 1;
        //        draft.triangles.AddRange(new[] { i0, i1, i2 });
        //        draft.triangles.AddRange(new[] { i2, i1, i3 });
        //    }

        //    i0 = (ring + 1) * longitudeSegments - 1;
        //    i1 = (ring + 2) * longitudeSegments - 1;
        //    i2 = ring * longitudeSegments;
        //    i3 = (ring + 1) * longitudeSegments;
        //    draft.triangles.AddRange(new[] { i0, i1, i2 });
        //    draft.triangles.AddRange(new[] { i2, i1, i3 });
        //}
        draft.Scale(parameters.MeshScaleFactor);
        return draft;
    }

    public static MeshDraft GenerativeBandOfQuads(Vector3 newPoint, Vector2 width, Vector2 height)
    {
        var bandOfQuads = new MeshDraft { name = "BandOfQuads" };

        Vector3 v0 = Vector3.zero;
        Vector3 v1 = Vector3.zero;
        Vector3 v2 = Vector3.zero;
        Vector3 v3 = Vector3.zero;

        var upperRing = new List<Vector3>();
        var lowerRing = new List<Vector3>();

        int vertexListLength = bandOfQuads.vertices.Count;

        v0 = bandOfQuads.vertices[vertexListLength - 1];
        v0 = new Vector3(0f, 0f, 0f);
        v1 = new Vector3(0f + width.x, 0f, 0f);
        v2 = new Vector3(0f + width.x, 1f, 0f);
        v3 = new Vector3(0f, 1f, 0f);

        var bandPart = MeshDraft.Quad(v0, v1, v2, v3);
        var flippedBandPart = MeshDraft.Quad(v3, v2, v1, v0);
        flippedBandPart.Move(flippedBandPart.normals[1] * height.y);
        bandOfQuads.Add(bandPart);
        bandOfQuads.Add(flippedBandPart);

        //var draft = new MeshDraft { name = "Sphere" };

        upperRing.Add(Random.insideUnitSphere);

        upperRing.Sort();
        var flipped = MeshDraft.TriangleStrip(upperRing);
        flipped.FlipFaces();
        bandOfQuads.Add(MeshDraft.TriangleStrip(upperRing));
        bandOfQuads.Add(flipped);

        bandOfQuads.Add(MeshDraft.FlatBand(lowerRing, upperRing));
        return bandOfQuads;
    }

    public static MeshDraft GenerativeTree(int segments, float radius, float height, Color meshColor)
    {
        var tree = new MeshDraft { name = "GenerativeTree" };

        float segmentAngle = 360f / segments;
        float currentAngle = 0;

        var lowerRing = new List<Vector3>(segments);
        var upperRing = new List<Vector3>(segments);
        for (var i = 0; i < segments; i++)
        {
            var point = PTUtils.PointOnCircle3XZ(radius, currentAngle);
            lowerRing.Add(point - Vector3.up * height / 2);
            upperRing.Add(point + Vector3.up * height / 2);
            currentAngle -= segmentAngle;
        }

        var draft = MeshDraft.TriangleFan(lowerRing);
        draft.Add(MeshDraft.Band(lowerRing, upperRing));
        upperRing.Reverse();
        draft.Add(MeshDraft.TriangleFan(upperRing));
        draft.Paint(meshColor);
        draft.name = "Cylinder";
        return draft;
    }
}
