﻿using System;
using ProceduralToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class GiC_ProceduralAtom : MonoBehaviour
{
    #region Variables  

    private SoundPreset parameters;

    // TODO: Simon hier nummer definieren für erstellen des Atoms
    public int templateNumber { get { return 1; } }

    private AtomInformationCore informationCore;
    public AtomInformationCore Core { get { return informationCore;} }

    public NetworkInformationCore networkCore { get { return Core.NetworkedData; } }

    private GameObject theEditModeIndicator;
    
    public GameObject EditModeIndicator { get { return theEditModeIndicator; } }

    public GameObject SeaboardGeometry { get; private set; }
    public GameObject DrawingGeometry { get; private set; }
    public GameObject LeapGeometry { get; set; }

    public Mesh EditRoomColliderMesh;

    public GiC_BagPackController BagPackController { get; private set; }

    private bool initialized = false;

    public bool EditModeEnabled { get; private set; }

    public void SetEditMode(bool editing)
    {
        // detect if changed
        if (editing != EditModeEnabled) { 
            // apply
            EditModeEnabled = editing;

            // execute stuff depending on edit mode change
            if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Leap)
            {
                if (EditModeEnabled)
                {
                    Core.LeapDataHandler.StartEditing();
                }
                else
                {
                    Core.LeapDataHandler.StopEditing();
                }
            }
        }
    }

    // PlaybackRelated

    public GiC_PlaybackStates PlaybackState
    {
        get { return Core.CurrentPlaybackState; }
    }

    //public delegate void TogglePlayback(GiC_PlaybackStates state);
    //public TogglePlayback onTogglePlayback;

    //public Action<GiC_PlaybackStates> OnTogglePlayback;

    //public Action OnLoopHasRestarted;

    #endregion

    #region Unity Methods

    // On enable and Awake must not contain any grand schemes as this script will be initialized using its own initialize routine
    void OnEnable () {
		
	}

    void Start()
    {
    }

	// Update is called once per frame
	void Update () {
	    if (PlaybackState != GiC_PlaybackStates.Play)
            return;
        Core.AdvancePlayhead();
	}

    #endregion

    #region Playback

    // Es gibt kein "stop" Bei erestellen des Atoms startet der Loop und spielt unendlich ab. Looplaenge = abspielgeschwindigkeit + zeichengenauigkeit.
    public void StartPlayback (){
        informationCore.SetPlaybackState(GiC_PlaybackStates.Play);
    }

    // Intern??
    public void StopPlayback()
    {
        Core.SetPlaybackState(GiC_PlaybackStates.Stop);
    }

    // Intern ??
    public void PausePlayback()
    {
        informationCore.SetPlaybackState(GiC_PlaybackStates.Pause);
    }

    public void Resync()
    {
        if (PlaybackState == GiC_PlaybackStates.Play)
        {
            Core.DrawingDataHandler.ResyncPlayback();
        }
    }

    #endregion

    #region Initialize

    private void CreateObjectGeometry(Gic_ProceduralAtomParameters geometryParameters)
    {
        SeaboardGeometry = new GameObject("SeaboardGeometry");
        SeaboardGeometry.transform.SetParent(this.gameObject.transform);
        SeaboardGeometry.transform.localPosition = Vector3.zero;
        SeaboardGeometry.transform.localScale = Vector3.one;
        SeaboardGeometry.transform.localRotation = Quaternion.identity;

        // Create the Object Geometry
        var meshDraft = GiC_ProceduralObjects.GenerativeSphericalObject(parameters.visualParameters);
        var mesh = meshDraft.ToMesh();
        //meshDraft.Scale(15f);
        meshDraft.Rotate(Quaternion.Euler(90, 0, 0));
        meshDraft.Expand(0.4f, 0f);
        MeshDraft EditRoomMesh = new MeshDraft() { name = "EditRoomMesh" };
        EditRoomMesh.Add(meshDraft);
        meshDraft.FlipFaces();
        meshDraft.Scale(0.95f);
        EditRoomMesh.Add(meshDraft);
        EditRoomMesh.Scale(25f);
        //var MeshDraft2 = 
        EditRoomColliderMesh = EditRoomMesh.ToMesh();
        //var mesh = GiC_ProceduralObjects.GenerativeSphericalObject(parameters.visualParameters).ToMesh();

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        SeaboardGeometry.AddComponent<MeshRenderer>().sharedMaterial = parameters.visualParameters.myMaterial;
        SeaboardGeometry.AddComponent<MeshFilter>().sharedMesh = mesh;
        // is handled by drag & drop automatism
        //Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        //if (rigidbody == null) {
        //    rigidbody = this.gameObject.AddComponent<Rigidbody>()
        //}
        //rigidbody.isKinematic = true;
        MeshCollider coll = this.gameObject.AddComponent<MeshCollider>();
        coll.sharedMesh = mesh;
    }

    // Add destroy
    private void CreateEditModeIndicator()
    {
        theEditModeIndicator = Instantiate(Resources.Load("General/EditModeIndicator") as GameObject);
        theEditModeIndicator.transform.SetParent(this.transform);
        theEditModeIndicator.transform.localPosition = new Vector3(0,3,0);
        theEditModeIndicator.transform.localEulerAngles = new Vector3(0,0,0);
        theEditModeIndicator.transform.localScale = new Vector3(1,1,1);
        theEditModeIndicator.SetActive(false);
    }

    public void SetEditingPlayer (GiC_Player player) {
        Color itemcolor = Color.white;

        switch (player) {
            case GiC_Player.Seaboard:
                theEditModeIndicator.SetActive(true);
                itemcolor = Color.magenta;
                break;
            case GiC_Player.Leap:
                theEditModeIndicator.SetActive(true);
                itemcolor = Color.yellow;
                break;
            case GiC_Player.Paint:
                theEditModeIndicator.SetActive(true);
                itemcolor = Color.green;
                break;
            case GiC_Player.GJ:
                theEditModeIndicator.SetActive(false);
                break;
            case GiC_Player.Undefined:
                theEditModeIndicator.SetActive(false);
                break;
            default:
                theEditModeIndicator.SetActive(true);
                itemcolor = Color.red;
                break;
        }

        theEditModeIndicator.GetComponent<Renderer>().material.SetColor("_Color", itemcolor);
    }

    // TODO: Simon Reto
    // wird auf dem server ausgeführt, keine client parameter oder einstellungen sind vorhanden (backpack geschlossen, variablen nicht initialisiert?)
    // visuelles wird nicht angezeigt
    public void Create (int soundPresetNumber = 0) {
        //Debug.Log("ProcAtom:CmdCreate soundpresetNo: " + soundPresetNumber);
        GiC_BagPackController bpController = Object.FindObjectsOfType<GiC_BagPackController>()[0];
        //SoundPreset paramIn = new SoundPreset(soundPresetNumber);
        SoundPreset paramIn = new SoundPreset(bpController.soundPresets[soundPresetNumber-1]);
        Create(paramIn, soundPresetNumber, bpController, false);
    }

    // TODO: Simon Reto
    // wird auf dem server ausgeführt, keine client parameter oder einstellungen sind vorhanden (backpack geschlossen, variablen nicht initialisiert?)
    // GiC_BagBackController ist nicht verfügbar
    public void Create (SoundPreset paramIn, int initialPresetNo, GiC_BagPackController bpController = null, bool addPnPScript = false)
    {
        BagPackController = bpController;
        parameters = paramIn != null ? new SoundPreset(paramIn) : new SoundPreset();

        // Create the Geometry
        CreateObjectGeometry(parameters.visualParameters);

        // Add Blank InformationCore
        informationCore = gameObject.GetComponent<AtomInformationCore>();
        if (informationCore == null) {
            //Debug.Log("InformationCore not found, adding one");
            informationCore = this.gameObject.AddComponent<AtomInformationCore>();
            informationCore.enabled = true;
        } else {
            Debug.Log("using already existing InformationCore");
        }

        // Feed initial Data to the Core
        informationCore.ProceduralAtomParameters = new SoundPreset(parameters);
        informationCore.InitialSoundPreset = initialPresetNo;

        // Add Pick And Place Functionality to move the Atom out of the BagPack and register it with the Server upon doing so
        if (addPnPScript)
        {
            SoundingObjectPickAndPlace pnp = this.gameObject.AddComponent<SoundingObjectPickAndPlace>();
            if (bpController != null)
            {
                pnp.Initialize(this);
            }
            else
            {
                Debug.LogWarning(gameObject.name + ": No BagPackController detected.. Is this Valid?");
            }
        }
    }


    // muss vom NetworkInformationCore generiert / aufgerufen werden
    public void Initialize()
    {

        // Add "Scanner" Child Object
        CreateEditModeIndicator();

        if (informationCore == null)
        {
            parameters = parameters ?? new SoundPreset();  
            Debug.Log("proceduralAtom.init: assign atominfocore");
            //Add Blank InformationCore
            informationCore = this.gameObject.GetComponent<AtomInformationCore>();
            Core.ProceduralAtomParameters = new SoundPreset(parameters);
            Core.InitialSoundPreset = parameters.presetIndex;
        }

        if (informationCore == null) {
            Debug.Log("missing informationCore!");
            initialized = false;
            return;
        }

        // Add GiC_SeaboardDataHandler
        Core.SeaboardDataHandler = this.gameObject.AddComponent<GicSeaboardDataHandler>();
        Core.SeaboardDataHandler.InitializeHandler(this);

        // Add GiC_DrawingDataHandler
        Core.DrawingDataHandler = this.gameObject.AddComponent<GiC_DrawingDataHandler>();
        Core.DrawingDataHandler.InitializeHandler(this);

        // Add GiC_LeapDataHandler
        Core.LeapDataHandler = this.gameObject.AddComponent<GiC_LeapDataHandler>();

        // Add Network InformationCore
        Core.NetworkedData = gameObject.GetComponent<NetworkInformationCore>();
        //if (Core.networkedData == null)
        //    this.gameObject.AddComponent<NetworkInformationCore>();
        Core.LeapDataHandler.InitializeHandler(this);

        // Add Interaction Interlinks
        Core.AtomCoreInteractable = this.gameObject.AddComponent<AtomCoreInteractable>();
        Core.AtomCoreInteractable.Initialize(this);

        Core.Initialize();

        initialized = true;
    }
    #endregion

}
