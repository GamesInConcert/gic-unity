﻿using System;
using UnityEngine;
using WorldspaceController;


//This Class holds all Variables and Information bound to this Atom
//Using this information it should be possible to reliably recreate an Atom.
public class AtomInformationCore : MonoBehaviour {
    
    // all data that needs transmission is in the parent class

    // the GameObject bound to the information
    //public GameObject theAtom { get { return this.gameObject; } }

    /// <summary>
    /// Die zeichnung an sich
    /// </summary>
    //public GameObject theDrawing;

    /// <summary>
    /// Alles was zur generation der jeweiligen heightmap gehört
    /// </summary>
    //public GameObject theLeap;

    /// <summary>
    /// The generativeObject creation parameters class 
    /// </summary>
    public SoundPreset ProceduralAtomParameters { get; set; }

    public GicSeaboardDataHandler SeaboardDataHandler { get; set; }

    public GiC_DrawingDataHandler DrawingDataHandler { get; set; }

    public GiC_LeapDataHandler LeapDataHandler { get; set; }

    public NetworkInformationCore NetworkedData { get; set; }

    public GiC_ProceduralAtom ProceduralAtomComponent;

    public AtomCoreInteractable AtomCoreInteractable { get; set; }

    public GameObject EditModeIndicator { get; set; }

    public int ObjectNumber;
    public int AtomIdentifier { get { return ObjectNumber; } }

    public int InitialSoundPreset { get; set; }

    private bool atomIsAlive;

    // Playback Related

    public GiC_PlaybackStates CurrentPlaybackState = GiC_PlaybackStates.Stop;

    public LoopMode LoopMode { get; private set; }

    //public float AtomPlaybackTime { get; private set; }
    public float NormalizedAtomPlaybackTime { get; private set; }

    // Wie lange dauert es das loop einmal abzuspielen
    public float AtomPlaybackSpeedInSeconds { get; private set; }
    public float AtomPlaybackAdvanceMultiplier { get; private set; }

    public bool ClickEnabled { get; private set; }

    public int Bpm { get { return NetworkedData.AtomBpm; } private set { NetworkedData.AtomBpm = value; } }
    public int NumBeats { get { return NetworkedData.AtomNumBeats; } private set { NetworkedData.AtomNumBeats = value; } }

    public bool SendToMaxPlaybackWait { get; private set; }
    public bool SendToMaxPlaybackNow { get; private set; }

    public Action<GiC_PlaybackStates> OnTogglePlayback;

    public Action OnLoopHasRestarted;

    public void Initialize()
    {
        ObjectNumber = (int)GetComponent<NetworkInformationCore>().netId.Value;
        transform.SetParent(GameObject.Find("VisibleAtoms").transform);
        GiC_SoundingObjectsManager.Instance.RegisterNewAtom(this);

        // Give it a new Name, Layer and Tag
        gameObject.name = "Atom_" + ObjectNumber;
        gameObject.layer = 13;
        gameObject.tag = "Atom";

        //InitOsc();
        InitVariables();

        SetAtomLive();
    }


    // TODO: OSC: Add possible "has Authority" Filter to prevent multiple atom creation on Startup
    public void InitOsc()
    {
        if (!GicOscManager.Instance)
            return;
        if (GicOscManager.Instance.OscBundleAtomMetaInformation == null)
            return;

        if (!NetworkInteractableObject.HasLocalPlayerAuthority(gameObject))
        {
            Debug.Log("AtomInformationCore.InitOsc Has NOT local player authority: " + Time.frameCount);
            return;
        }

        Debug.Log("AtomInformationCore.InitOsc Has local player authority: " + Time.frameCount);

        // Tell Max there is a new Atom
        GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/n/"+GiCPlaybackManager.Instance.ThisPlayerInterface, AtomIdentifier, ProceduralAtomParameters.presetIndex));

        //And Where
        GicOscManager.Instance.SendAtomPosition(AtomIdentifier, gameObject.transform.position);
    }

    public void InitVariables()
    {
        Bpm = 120;
        NumBeats = 8;
        LoopMode = LoopMode.RealTime;
        ClickEnabled = false;
        SetBpm(Bpm);
        SendToMaxPlaybackWait = false;
        SendToMaxPlaybackNow = false;
        CurrentPlaybackState = GiC_PlaybackStates.Play;
    }

    public void SetPlaybackState(GiC_PlaybackStates newState)
    {
        CurrentPlaybackState = newState;
        if (OnTogglePlayback != null)
            OnTogglePlayback(CurrentPlaybackState);
    }

    // The current manipulating user

    // Total length of the loop
    // total granularity of the loop (time / Amount of information)

    // The Drawing information
    // Container of all strokes bound to this Atom
    // Thus all strokes
    // General drawing info such as:
    // Total length
    // Total Time
    // Granularity (amount of strokes)
    // Average length of a stroke
    // Average color of a stroke
    // Average width of a stroke
    // Total Spread of strokes (area wise)
    // Average distance of strokes (wie sehr verklumpt)

    //public GiC_DrawingMetadata DrawingData;

    // The Leap information
    // Heightmap ??? Also alle changes um es wiederherstellen zu können
    // Total amount of changes to the environment
    // granularity (amount of changes / time?)
    // Average steepness (steilheit der kurven)
    // Total spread of changes (area wise)
    // Average distance of changes (wie sehr verklumpt)
    // Tonality clusterdness (wie viele änderungen auf welcher tonhöhe?) 

    //public GiC_LeapMotionMetadata LeapData;

    // The Seaboard information
    // Irgendwie die midi information .. Die textur?? 
    // Total amount of inputs (anzahl an gespielten noten / akkorden innerhalb der zeit)
    // clusterdness (spread der noten)
    // ratio einzelnoten / akkorde
    // Average velocity (midi wise)

    //public GiC_SeaboardMetadata SeaboardData;

    #region Playback Related

    public void AdvancePlayhead()
    {
        NormalizedAtomPlaybackTime += Time.unscaledDeltaTime * AtomPlaybackAdvanceMultiplier;
        if (!(NormalizedAtomPlaybackTime > 1f))
            return;

        if (OnLoopHasRestarted != null)
        {
            OnLoopHasRestarted();
        }
        BeginFromZeroTrigger();
        NormalizedAtomPlaybackTime = NormalizedAtomPlaybackTime % 1f;

        if (NetworkSettingsController.Instance.networkState == NetworkSettingsController.NETWORK_STATE.HOST) {
            //Debug.Log("Send LoopSync for " + AtomIdentifier);
            GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/ls", AtomIdentifier, 0));
        }
    }

    public void ToggleClick(bool clickIsOn)
    {
        ClickEnabled = clickIsOn;
        SendMetroOsc();
    }

    public LoopMode SetLoopMode(int newMode)
    {

        LoopMode = (LoopMode)newMode;
        SendLoopModeOsc();
        return LoopMode;
    }

    public void SetBpm(int newBpm)
    {
        Bpm = newBpm == Bpm ? Bpm : newBpm;
        NetworkedData.CmdBpmChanged(Bpm);
        SendMetroOsc();
        SetPlaybackValues();
    }

    public void SetNumBeats(int newNumBeats)
    {
        NumBeats = newNumBeats == NumBeats ? NumBeats : newNumBeats;
        NetworkedData.CmdNumBeatsChanged(NumBeats);
        SendMetroOsc();
        SetPlaybackValues();
    }

    public void SetPlaybackValues()
    {
        AtomPlaybackSpeedInSeconds = (60f / Bpm) * NumBeats;
        AtomPlaybackAdvanceMultiplier = 1f / AtomPlaybackSpeedInSeconds;
    }

    public void ArmRecordPlayback(bool record = true)
    {
        if (record && !SendToMaxPlaybackWait)
        {
            Debug.Log("Atom Nr: "+AtomIdentifier+" is going To Record");
            SendToMaxPlaybackWait = true;
            SendRecordOsc();
        }
    }

    public void DisarmRecordPlayback()
    {
        if (SendToMaxPlaybackWait && !SendToMaxPlaybackNow)
        {
            Debug.Log("Disarming Record on Atom: "+AtomIdentifier);
            SendToMaxPlaybackWait = false;
        }
    }

    private void BeginFromZeroTrigger()
    {
        if (SendToMaxPlaybackWait && !SendToMaxPlaybackNow)
        {
            Debug.Log("Recording Data To Olav on Atom: " + AtomIdentifier);
            SendToMaxPlaybackNow = true;         
        }
        else if (SendToMaxPlaybackNow)
        {          
            SendToMaxPlaybackWait = false;
            SendToMaxPlaybackNow = false;
            if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Seaboard)
            {
                if (GiC_SoundingObjectsManager.Instance.currentEditableAtom.EditModeEnabled)
                GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.AtomCoreInteractable.ToggleEditMode();
            }
        }
    }

    public void ResyncPlayback()
    {
        //BeginFromZeroTrigger();
        if (!SendToMaxPlaybackNow && !SendToMaxPlaybackWait)
        {
            NormalizedAtomPlaybackTime = 0f;
            DrawingDataHandler.ResyncPlayback();
            SendToMaxPlaybackNow = false;
            SendToMaxPlaybackWait = false;
        }
    }

    #endregion

    #region OSC

    private void SendLoopModeOsc()
    {
        if (NetworkInteractableObject.HasLocalPlayerAuthority(gameObject))
        GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/" + GiCPlaybackManager.Instance.ThisPlayerInterface + "/lm", AtomIdentifier, (int)LoopMode), GicOscManager.Instance.UseMulticastBroadcast);
    }

    private void SendMetroOsc()
    {
        if (NetworkInteractableObject.HasLocalPlayerAuthority(gameObject))
        GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/m", AtomIdentifier, Bpm, NumBeats, ClickEnabled));
    }

    private void SendRecordOsc()
    {
        GicOscManager.Instance.OscOut.Send(new OscMessage("/g/" + GiCPlaybackManager.Instance.ThisPlayerInterface + "/r", AtomIdentifier, 1));
    }
    #endregion

    public bool SetAtomLive()
    {
        atomIsAlive = true;
        return atomIsAlive;
    }

    public bool DeleteAtom()
    {
        atomIsAlive = false;
        return atomIsAlive;
    }

    public bool AtomIsAlive()
    {
        return atomIsAlive;
    }
}
