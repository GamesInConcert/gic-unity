﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Is Part of the Atom and Creates the Menu with the desired items on startup.
public class AtomUiController : MonoBehaviour
{
    public AtomCoreInteractable thisCoreInteractable { get; private set; }
    private GameObject menuItemsParent;
    private List<GiCMenuItem> menuItems = new List<GiCMenuItem>();
    private GameObject mainCamera;
    public bool menuIsOpen { get; private set; }

    private GiC_ProceduralAtom theAtom;

	void Start ()
	{

    }
	
	void Update () {
        if (menuIsOpen)
        {
            menuItemsParent.transform.LookAt(mainCamera.transform);
        }
    }

    public void OpenMenu()
    {
        menuIsOpen = true;
        menuItemsParent.transform.LookAt(mainCamera.transform);
        menuItemsParent.SetActive(menuIsOpen);
    }

    public void CloseMenu()
    {
        menuIsOpen = false;
        foreach (var item in menuItems)
        {
            item.CloseSubmenu();
        }
        menuItemsParent.SetActive(menuIsOpen);

    }

    public void Initialize(AtomCoreInteractable coreInteractable)
    {
        thisCoreInteractable = coreInteractable;
        theAtom = GetComponent<GiC_ProceduralAtom>();
        if (coreInteractable == null)
        {
            return;
        }

        mainCamera = GameObject.FindWithTag("MainCamera");
        menuItemsParent = new GameObject("MenuParent");
        menuItemsParent.transform.SetParent(this.gameObject.transform);
        menuItemsParent.transform.localPosition = Vector3.zero;
        CreateMenuWheel(menuItemsParent.transform);
        menuItemsParent.SetActive(menuIsOpen);
    }

    private void CreateMenuWheel(Transform parent)
    {
        float radius = Vector3.Max(Vector3.zero, GetComponent<MeshCollider>().bounds.extents).x * 3f;
        float angle = (Mathf.PI * 2f) / GiCUiManager.Instance.atomMenuItems.Length;
        for (int i = 0; i < GiCUiManager.Instance.atomMenuItems.Length; i++)
        {
            GameObject newMenuItem = null;
            switch (GiCUiManager.Instance.atomMenuItems[i].TypeOfElement)
            {
                case AtomUiElement.Default:
                    newMenuItem = Instantiate(GiCUiManager.Instance.atomMenuItemPrefab);
                    newMenuItem.name = "MenuItem_" + GiCUiManager.Instance.atomMenuItems[i].menuItemText;
                    break;
                case AtomUiElement.Button:
                    newMenuItem = Instantiate(GiCUiManager.Instance.ButtonPrefab);
                    break;
                case AtomUiElement.Toggle:
                    newMenuItem = Instantiate(GiCUiManager.Instance.TogglePrefab);
                    break;
                case AtomUiElement.Slider:
                    newMenuItem = Instantiate(GiCUiManager.Instance.SliderPrefab);
                    break;
                default:
                    newMenuItem = Instantiate(GiCUiManager.Instance.atomMenuItemPrefab);
                    newMenuItem.name = "MenuItem_" + GiCUiManager.Instance.atomMenuItems[i].menuItemText;
                    break;
            }
            if (newMenuItem == null) continue;
            float xPos = Mathf.Cos(angle * i);
            float yPos = Mathf.Sin(angle * i);
            newMenuItem.transform.SetParent(parent);
            Vector3 pos = new Vector3(xPos, yPos, 0f) * radius;
            newMenuItem.transform.localPosition = pos;
            newMenuItem.transform.localRotation = Quaternion.identity;

            GiCMenuItem mi = newMenuItem.GetComponent<GiCMenuItem>();
            if (mi != null)
            {
                mi.Initialize(this, GiCUiManager.Instance.atomMenuItems[i]);
                menuItems.Add(newMenuItem.GetComponent<GiCMenuItem>());
            }
        }
    }

    private void CreateSubmenu(Transform parent)
    {

    }
}
