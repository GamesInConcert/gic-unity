﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using WorldspaceController;

[Serializable]
public class MyObjectEvent : UnityEvent<GameObject, WorldspaceInputDevice, object> { }

[Serializable]
public class GiCMenuItem : InteractableObject {
    public AtomUiController UiController { get; private set; }
    private MyObjectEvent onTrigger;

    private Color unusedColor;

    private Renderer myRenderer;
    private int shaderColorPropertyId = 0;

    private GiCMenuItemSettings mySettings;
    private GameObject submenuGO;

    private bool isTrigger = false;

    public void Awake () {
        base.Awake();
        moveXByDrag = false; moveYByDrag = false; moveZByDrag = false; rotateXByDrag = false; rotateYByDrag = false; rotateZByDrag = false;

    }


// Event Listeners
    private void OnUseStartEvent(InputDeviceData deviceData)
    {
        onTrigger.Invoke(UiController.gameObject, deviceData.inputDevice, null);
        UiController.CloseMenu();
    }

    private void OnUseStopEvent(InputDeviceData deviceData)
    {

    }

    private void OnHoverEnterEvent(InputDeviceData deviceData) {
        if (myRenderer) {
            myRenderer.material.SetColor(shaderColorPropertyId, Color.green);
        }
        OpenSubmenu();
    }

    public void OnHoverRepeatEvent(InputDeviceData deviceData) {
    }

    private void OnHoverExitEvent(InputDeviceData deviceData) {
        if (myRenderer) {
            myRenderer.material.SetColor(shaderColorPropertyId, unusedColor);         
        }
    }

// Public Functions
    public void OpenSubmenu()
    {
        if (submenuGO != null)
        {
            submenuGO.transform.LookAt(Camera.main.transform);
            submenuGO.SetActive(true);
        }
    }

    public void CloseSubmenu()
    {
        if (submenuGO != null)
        {
            submenuGO.SetActive(false);
        }
    }

// Initialization and Procedural Creation
    public void Initialize(AtomUiController parentUiController, GiCMenuItemSettings settings)
    {
        mySettings = settings;
        UiController = parentUiController;
        onTrigger = settings.onActivate;

        switch (settings.TypeOfElement)
        {
            case AtomUiElement.Default:
                shaderColorPropertyId = Shader.PropertyToID("_Color");
                myRenderer = this.GetComponent<Renderer>();
                unusedColor = myRenderer.material.GetColor(shaderColorPropertyId);
                transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = settings.menuItemText;

                this.onUseStartEvent.AddListener(OnUseStartEvent);
                this.onHoverEnterEvent.AddListener(OnHoverEnterEvent);
                this.onHoverExitEvent.AddListener(OnHoverExitEvent);
                break;
            case AtomUiElement.Button:
                GetComponent<Button>().onClick.AddListener(OnButtonClickedAction);
                GetComponentInChildren<Text>().text = settings.menuItemText;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);
                break;
            case AtomUiElement.Toggle:
                GetComponent<Toggle>().onValueChanged.AddListener(OnToggleChangedAction);
                GetComponent<UiToggleGraphicSwitch>().graphicOn = settings.toggleOnImage;
                GetComponent<UiToggleGraphicSwitch>().graphicOff = settings.toggleOffImage;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);
                break;
            case AtomUiElement.Slider:
                GetComponent<Slider>().onValueChanged.AddListener(OnSliderValueChangedAction);
                GetComponent<Slider>().minValue = settings.sliderMin;
                GetComponent<Slider>().maxValue = settings.sliderMax;
                GetComponent<Slider>().wholeNumbers = settings.sliderIsInt;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);
                break;
            default:
                break;
        }

        if (settings.submenu.Count > 0)
        {
            CreateSubmenu(settings);
        }
    }

    private void InitButton()
    {
        
    }

    private void InitSlider()
    {
        
    }

    private void InitToggle()
    {
        
    }

    private void OnSliderValueChangedAction(float newValue)
    {
        onTrigger.Invoke(UiController.gameObject, null, newValue);
    }

    private void OnButtonClickedAction()
    {
        onTrigger.Invoke(UiController.gameObject, null, null);
    }

    private void OnToggleChangedAction(bool newValue)
    {
        onTrigger.Invoke(UiController.gameObject, null, newValue);
    }

    public void InitializeSubmenu(AtomUiController parentUiController, GiCSubMenuItemSettings settings)
    {
        onTrigger = settings.onActivate;
        UiController = parentUiController;
        gameObject.name = "MenuItem_" + settings.menuItemText;
        switch (settings.TypeOfElement)
        {
            case AtomUiElement.Default:
                shaderColorPropertyId = Shader.PropertyToID("_Color");
                myRenderer = this.GetComponent<Renderer>();
                unusedColor = myRenderer.material.GetColor(shaderColorPropertyId);
                transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = settings.menuItemText;
                transform.localScale = Vector3.one * 0.5f;
                this.onUseStartEvent.AddListener(OnUseStartEvent);
                this.onHoverEnterEvent.AddListener(OnHoverEnterEvent);
                this.onHoverExitEvent.AddListener(OnHoverExitEvent);
                break;
            case AtomUiElement.Button:
                GetComponent<Button>().onClick.AddListener(OnButtonClickedAction);
                GetComponentInChildren<Text>().text = settings.menuItemText;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);
                break;
            case AtomUiElement.Toggle:
                GetComponent<Toggle>().onValueChanged.AddListener(OnToggleChangedAction);
                GetComponent<UiToggleGraphicSwitch>().graphicOn = settings.toggleOnImage;
                GetComponent<UiToggleGraphicSwitch>().graphicOff = settings.toggleOffImage;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);
                break;
            case AtomUiElement.Slider:
                GetComponent<Slider>().onValueChanged.AddListener(OnSliderValueChangedAction);
                GetComponent<Slider>().minValue = settings.sliderMin;
                GetComponent<Slider>().maxValue = settings.sliderMax;
                GetComponent<Slider>().wholeNumbers = settings.sliderIsInt;
                GetComponent<Gic_Ui_ValueToLabel>().append = settings.menuItemText;
                transform.localScale = Vector3.one * 0.01f;
                transform.localRotation = Quaternion.Euler(0f, 180, 0f);

                break;
            default:
                break;
        }
    }

    void CreateSubmenu(GiCMenuItemSettings settings)
    {

        float radius = Vector3.Max(Vector3.zero, GetComponent<Collider>().bounds.size).x * 6f;
        float angle = ((Mathf.PI) / settings.submenu.Count);
        submenuGO = new GameObject("Submenu");
        submenuGO.transform.SetParent(this.transform);
        submenuGO.transform.localPosition = Vector3.zero;
        submenuGO.transform.localRotation = Quaternion.identity;
        submenuGO.transform.localScale = Vector3.one;

        for (int i = 0; i < settings.submenu.Count; i++)
        {
            GameObject newMenuItem = null;

            switch (settings.submenu[i].TypeOfElement)
            {
                case AtomUiElement.Default:
                    newMenuItem = Instantiate(GiCUiManager.Instance.atomMenuItemPrefab);
                    newMenuItem.name = "MenuItem_" + settings.submenu[i].menuItemText;
                    break;
                case AtomUiElement.Button:
                    newMenuItem = Instantiate(GiCUiManager.Instance.ButtonPrefab);
                    break;
                case AtomUiElement.Toggle:
                    newMenuItem = Instantiate(GiCUiManager.Instance.TogglePrefab);
                    break;
                case AtomUiElement.Slider:
                    newMenuItem = Instantiate(GiCUiManager.Instance.SliderPrefab);
                    break;
                default:
                    newMenuItem = Instantiate(GiCUiManager.Instance.atomMenuItemPrefab);
                    newMenuItem.name = "MenuItem_" + settings.submenu[i].menuItemText;
                    break;
            }
            if (newMenuItem == null) continue;
            float xPos = Mathf.Sin(angle * i);
            float yPos = Mathf.Cos(angle * i);
            newMenuItem.transform.SetParent(submenuGO.transform);
            Vector3 pos = new Vector3(xPos, yPos, 0f) * radius;
            newMenuItem.transform.localPosition = pos;
            newMenuItem.transform.localRotation = Quaternion.identity;
            

            GiCMenuItem mi = newMenuItem.GetComponent<GiCMenuItem>();
            if (mi != null)
                mi.InitializeSubmenu(UiController, settings.submenu[i]);
        }

        submenuGO.SetActive(false);
    }

}
