﻿using System;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GiCMenuItemSettings
{
    public AtomUiElement TypeOfElement = AtomUiElement.Button;
    public string menuItemText;
    public Image toggleOnImage;
    public Image toggleOffImage;
    public float sliderMin = 0;
    public float sliderMax = 1;
    public bool sliderIsInt = false;
    public MyObjectEvent onActivate;
    public List<GiCSubMenuItemSettings> submenu = new List<GiCSubMenuItemSettings>();
}