﻿public enum GiC_AtomCoreInteractionsEnum {

    Nothing,
    Edit,
    Move,
    Store,
    Delete,
    SetMarker

}
