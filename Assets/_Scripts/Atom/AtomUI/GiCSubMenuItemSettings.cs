﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GiCSubMenuItemSettings {

    public AtomUiElement TypeOfElement = AtomUiElement.Button;
    public string menuItemText;
    public Image toggleOnImage;
    public Image toggleOffImage;
    public float sliderMin = 0;
    public float sliderMax = 1;
    public bool sliderIsInt = false;
    public MyObjectEvent onActivate;
}
