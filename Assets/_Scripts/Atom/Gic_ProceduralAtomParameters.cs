﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public enum Formen
{
    asses, vases, random, BatMask, IrokeseFish, Spaceships, SpaceFlowers, Spores, PlayWithMult,
}

[Serializable]
public class Gic_ProceduralAtomParameters {

    public int longitudeSegments = 10;
    public int latitudeSegments = 10;
    public Formen Form = Formen.asses;

    public float Mult1 = 1f;
    public float BaseMultiplier1 = 1f;
    public float MeshScaleFactor = 1f;
    public float SubstepsModulo = 1f;
    public float PerlinNoiseXCoordinate = 0f;
    public float PerlinNoiseYCoordinateMultiplier = 1f;
    public float MinimumRadius = 0.2f;

    public Color colorA = Color.white;
    public Color colorB = Color.white;

    public Material myMaterial;

    public Gic_ProceduralAtomParameters()
    {
        longitudeSegments = 10;
        latitudeSegments = 32;
        Form = Formen.IrokeseFish;

        Mult1 = 0.62f;
        BaseMultiplier1 = 0.5f;
        MeshScaleFactor = 1f;
        SubstepsModulo = 4f;
        PerlinNoiseXCoordinate = 1f;
        PerlinNoiseYCoordinateMultiplier = 1f;
        MinimumRadius = 0f;

        colorA = new Color(0.9f, 0.1f, 0.1f, 1f);
        colorB = new Color(0.5f, 0.05f, 0.05f, 1f);
    }

    public Gic_ProceduralAtomParameters(Gic_ProceduralAtomParameters paramIn)
    {
        longitudeSegments = paramIn.longitudeSegments;
        latitudeSegments = paramIn.latitudeSegments;
        Form = paramIn.Form;

        Mult1 = paramIn.Mult1;
        BaseMultiplier1 = paramIn.BaseMultiplier1;
        MeshScaleFactor = paramIn.MeshScaleFactor;
        SubstepsModulo = paramIn.SubstepsModulo;
        PerlinNoiseXCoordinate = paramIn.PerlinNoiseXCoordinate;
        PerlinNoiseYCoordinateMultiplier = paramIn.PerlinNoiseYCoordinateMultiplier;
        MinimumRadius = paramIn.MinimumRadius;

        colorA = paramIn.colorA;
        colorB = paramIn.colorB;

        myMaterial = paramIn.myMaterial;
    }

    public void SetAllParameters(Gic_ProceduralAtomParameters paramIn)
    {
        longitudeSegments = paramIn.longitudeSegments;
        latitudeSegments = paramIn.latitudeSegments;
        Form = paramIn.Form;

        Mult1 = paramIn.Mult1;
        BaseMultiplier1 = paramIn.BaseMultiplier1;
        MeshScaleFactor = paramIn.MeshScaleFactor;
        SubstepsModulo = paramIn.SubstepsModulo;
        PerlinNoiseXCoordinate = paramIn.PerlinNoiseXCoordinate;
        PerlinNoiseYCoordinateMultiplier = paramIn.PerlinNoiseYCoordinateMultiplier;
        MinimumRadius = paramIn.MinimumRadius;

        colorA = paramIn.colorA;
        colorB = paramIn.colorB;

        myMaterial = paramIn.myMaterial;
    }

    public static Gic_ProceduralAtomParameters Lerp(Gic_ProceduralAtomParameters paramsA, Gic_ProceduralAtomParameters paramsB,
        float factor)
    {
        Gic_ProceduralAtomParameters outValue = new Gic_ProceduralAtomParameters();
        factor = Mathf.Clamp01(factor);
        outValue.BaseMultiplier1 = Mathf.Lerp(paramsA.BaseMultiplier1, paramsB.BaseMultiplier1, factor);
        outValue.Form = (Formen)Mathf.Lerp((float) paramsA.Form, (float) paramsB.Form, factor);
        outValue.MeshScaleFactor = Mathf.Lerp(paramsA.MeshScaleFactor, paramsB.MeshScaleFactor, factor);
        outValue.MinimumRadius = Mathf.Lerp(paramsA.MinimumRadius, paramsB.MinimumRadius, factor);
        outValue.Mult1 = Mathf.Lerp(paramsA.Mult1, paramsB.Mult1, factor);
        outValue.PerlinNoiseXCoordinate = Mathf.Lerp(paramsA.PerlinNoiseXCoordinate, paramsB.PerlinNoiseXCoordinate, factor);
        outValue.PerlinNoiseYCoordinateMultiplier = Mathf.Lerp(paramsA.PerlinNoiseYCoordinateMultiplier, paramsB.PerlinNoiseYCoordinateMultiplier, factor);
        outValue.SubstepsModulo = Mathf.Lerp(paramsA.SubstepsModulo, paramsB.SubstepsModulo, factor);
        outValue.colorA = Color.Lerp(paramsA.colorA, paramsB.colorA, factor);
        outValue.colorB = Color.Lerp(paramsA.colorB, paramsB.colorB, factor);
        outValue.latitudeSegments = (int)Mathf.Lerp(paramsA.latitudeSegments, paramsB.latitudeSegments, factor);
        outValue.longitudeSegments = (int)Mathf.Lerp(paramsA.longitudeSegments, paramsB.longitudeSegments, factor);
        outValue.myMaterial = factor <= 0.5f ? paramsA.myMaterial : paramsB.myMaterial;
        return outValue;
    }

    /// <summary>
    /// Quadratic Lerp Between three presets (X/Y pad)
    /// </summary>
    /// <param name="paramsA"> At Position 0,0</param>
    /// <param name="paramsB"> At Position 0,1</param>
    /// <param name="paramsC"> At Position 1,0</param>
    /// <param name="factor">Returns lerp(lerp(a,b,factor.x), c, factor.y)</param>
    /// <returns></returns>
    public static Gic_ProceduralAtomParameters QLerp(Gic_ProceduralAtomParameters paramsA, Gic_ProceduralAtomParameters paramsB, Gic_ProceduralAtomParameters paramsC,
        Vector2 factor)
    {

        return Lerp(Lerp(paramsA, paramsB, factor.x), Lerp(paramsC, Lerp(paramsB, paramsC, 0.5f), factor.x), factor.y);
    }

}
