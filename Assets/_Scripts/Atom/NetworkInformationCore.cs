﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using WorldspaceController;

public class NetworkInformationCore : NetworkInteractableObject {

    private readonly ObservedList<GicStroke> strokes = new ObservedList<GicStroke>();
    public ObservedList<GicStroke> Strokes { get { return strokes; } }

    public readonly ObservedList<LeapHotspot> hotspots = new ObservedList<LeapHotspot>();
    public ObservedList<LeapHotspot> Hotspots { get { return hotspots; } }

    public NetworkTransmitter networkTransmitter;

    public GiC_ProceduralAtom theAtom { get; private set; }

    [SyncVar]
    public int presetNumber;

    [SyncVar(hook = "EditingPlayerChanged")]
    public GiC_Player editingPlayer = GiC_Player.Undefined;

    // to change this value call the CmdInterpolationXChange function!
    [SyncVar(hook = "InterpolationXchanged")]
    public int interpolationX;
    // to change this value call the CmdInterpolationYChange function!
    [SyncVar(hook = "InterpolationYchanged")]
    public int interpolationY;

    [SyncVar(hook = "BpmChanged")] public int AtomBpm;
    [SyncVar(hook = "NumBeatsChanged")] public int AtomNumBeats;



    #region SyncVar Transmission
    /// <summary>
    /// set the interpolation value on the server, it then gets replicated to all clients if the value is defined as [SyncVar]
    /// </summary>
    [Command]
    public void CmdInterpolationXChange(int value) {
        interpolationX = value;
    }

    /// <summary>
    /// callback der auf allen client ausgeführt wird, wenn der wert verändert wird
    /// </summary>
    [Client]
    public void InterpolationXchanged(int value) {
        interpolationX = value;
        // führe visuelle anpassungen aus (SeaboardDataHandler)
    }

    /// <summary>
    /// set the interpolation value on the server, it then gets replicated to all clients if the value is defined as [SyncVar]
    /// </summary>
    [Command]
    public void CmdInterpolationYChange(int value) {
        interpolationX = value;
    }

    /// <summary>
    /// callback der auf allen client ausgeführt wird, wenn der wert verändert wird
    /// </summary>
    [Client]
    public void InterpolationYchanged(int value) {
        interpolationX = value;
        // führe visuelle anpassungen aus (SeaboardDataHandler)
    }

    /// <summary>
    /// set the interpolation value on the server, it then gets replicated to all clients if the value is defined as [SyncVar]
    /// </summary>
    [Command]
    public void CmdBpmChanged(int value)
    {
        AtomBpm = value;
    }

    /// <summary>
    /// callback der auf allen client ausgeführt wird, wenn der wert verändert wird
    /// </summary>
    [Client]
    public void BpmChanged(int value)
    {
        AtomBpm = value;
        //Debug.LogWarning("BPM Received: " + AtomBpm);
        theAtom.Core.SetPlaybackValues();
    }

    /// <summary>
    /// set the interpolation value on the server, it then gets replicated to all clients if the value is defined as [SyncVar]
    /// </summary>
    [Command]
    public void CmdNumBeatsChanged(int value)
    {
        AtomNumBeats = value;
    }

    /// <summary>
    /// callback der auf allen client ausgeführt wird, wenn der wert verändert wird
    /// </summary>
    [Client]
    public void NumBeatsChanged(int value)
    {
        AtomNumBeats = value;
        //Debug.LogWarning("NumBeats Received: " + AtomNumBeats);
        theAtom.Core.SetPlaybackValues();
        // führe visuelle anpassungen aus (SeaboardDataHandler)
    }

    [Command]
    public void CmdEditingPlayerChanged(GiC_Player player) {
       //Debug.Log("NetworkInformationCore.CmdEditingPlayerChanged: player: " + player + " editingPlayer: " + editingPlayer);
       editingPlayer = player;
    }

    [Client]
    public void EditingPlayerChanged(GiC_Player player) {
        //Debug.Log("NetworkInformationCore.EditingPlayerChanged: player: " + player + " editingPlayer: " + editingPlayer);

        editingPlayer = player;

        theAtom.SetEditingPlayer(player);
    }
    #endregion

    #region INIT
    // wird auf dem server ausgeführt, wenn das objekt erstellt wird, kann nur den soundpresetId empfangen
    // hier koennen werte nach presets gesetzt werden, nur daten die netzwerktransparent sind werden vom client empfangen (oder was fix in unity definiert wurde)
    // keine AddComponents oder aehnlich
    [Server]
    public void Initialize(int soundPresetId) {
        // ...
        presetNumber = soundPresetId;

        // set random seed based on time
        UnityEngine.Random.InitState(((int)System.DateTime.Now.Ticks));
        serverNetworkPackageRandomBase = UnityEngine.Random.Range(0, int.MaxValue);
        clientNetworkPackageRandomBase = UnityEngine.Random.Range(0, int.MaxValue);

        
        // create funktion aufrufen, die netzwerkparameter als template kopiert
    }
    #endregion

    #region CLIENT
    // CLIENT (hat nicht die selben parameter wie der client, nur variablen mit [SyncVar] sind vorhanden)
    // das objekt wurde auf dem client erstellt
    public void OnStartClientCommand() {
        // erstelle visuelle elemente (gic_proceduralAtom)
        // ...
        Debug.Log("NIC: OnStartClient");
        theAtom = gameObject.AddComponent<GiC_ProceduralAtom>();
        theAtom.Create(presetNumber);
        theAtom.Initialize();
        // setup seaboard receiving und visual aktualsierung
    }

    // der lokale client hat die authoritaet ueber das objekt bekommen, wir muessen nun die osc daten selbst versenden
    //[Client]
    //public void OnStartClientAuthority() {
    //    Debug.LogError("OnStartClientAuthority");

    //}

    // der lokale client hat die authoritaet ueber das objekt verloren, wir stoppen das versenden der osc daten
    //[Client]
    //public void OnStopClientAuthority() {
    //    // ...
    //    // destroy osc and stop sending
    //    // moeglicherweise auch DisableEditing();
    //    editingPlayer = GiC_Player.Undefined;
    //}

    // funktion wird aufgerufen wenn der benutzer das objekt auswaehlt und editiert
    //[Client]
    //public void EnableEditing() {
    //    // aktiviere edit dome, setze dieses atom als aktives element
    //    // ...


    //    // so aehnlich ...
    //    // GiC_SoundingObjectsManager.instance.SetAtomToEdit(GiC_SoundingObjectsManager.instance.GetAtomByIdentifier((int)netId));
    //}

    // funktion wird aufgerufen wenn der benutzer das objekt auswaehlt und editiert
    //[Client]
    //public void DisableEditing() {
    //    // deaktiviere edit dome, entferne dieses atom vom editieren
    //    // ...
    //    //theAtom.Core.AtomCoreInteractable.StopEditMode();
    //}
    #endregion

    #region SERVER
    // SERVER (hat nicht die selben parameter wie der client, nur variablen mit [SyncVar] sind vorhanden)
    // 
    [Server]
    public void OnStartServerCommand() {
        // ...
    }

    // der server hat die authoritaet ueber das objekt bekommen, wir muessen nun die osc daten "selbst" versenden
    //[Server]
    //public void OnStartServerAuthority() {
    //    // ...
    //    // init osc and start sending/receiving (allenfalls wenn olav nicht loopen kann)
    //    Debug.LogError("OnStartServerAuthority");

    //}

    // der server hat die authoritaet ueber das objekt verloren, das versenden der daten wird abgestellt
    //[Server]
    //public void OnStopServerAuthority() {
    //    // ...
    //    // destroy osc and stop sending/receiving
    //}
    #endregion

    #region UnityClasses
    protected override void Awake() {
        base.Awake();

        networkTransmitter = GetComponent<NetworkTransmitter>();
        strokes.Updated += GicStrokeArrays_Updated;
        hotspots.Updated += GicHotspotArrays_Updated;
    }

    float packageSendDelayer = 0;
    protected override void Update() {
        base.Update();

        packageSendDelayer -= Time.deltaTime;

        if ((((gicStrokeArrayIndexesDirty.Count > 0) && (gicStrokeArrayModificationFinished)) ||
            ((gicHotspotsArrayIndexesDirty.Count > 0) && (gicHotspotsArrayModificationFinished))) && 
            (packageSendDelayer < 0)) {
            packageSendDelayer = 0.2f;

            //Debug.Log("AtomObject.Update: " +
            //    " dirty strokes: " + PrintInts(gicStrokeArrayIndexesDirty) +
            //    " strokes mod fin: " + gicStrokeArrayModificationFinished +
            //    " dirty hotspots: " + PrintInts(gicHotspotsArrayIndexesDirty) +
            //    " hotspots mod fin: " + gicHotspotsArrayModificationFinished +
            //    " isServer: " + isServer + 
            //    " isClient: " + isClient);

            // isGicPointArraysDirty = false;
            // a host
            if (isServer && isClient) {
                DistributeAtomData(new List<NetworkConnection>());
            }
            // a dedicated server
            else if (isServer) {
                DistributeAtomData(new List<NetworkConnection>());
            }
            // a client which has local player authority of the object
            else if (isClient && NetworkInteractableObject.HasLocalPlayerAuthority(gameObject)) {
                UploadAtomData();
            }
            // a client which has no authority over the object -> clear dirty tag
            else {
                //Debug.Log("NetworkInformationCore.Update: no authority over data, clearing gicStrokeArrayIndexesDirty & gicHotspotsArrayIndexesDirty");
                gicStrokeArrayIndexesDirty = new List<int>();
                gicHotspotsArrayIndexesDirty = new List<int>();
            }
            gicStrokeArrayModificationFinished = false;
            gicHotspotsArrayModificationFinished = false;

        }
    }
    #endregion

    #region Unity Networking Classes
    public override void OnStartAuthority() {
        base.OnStartAuthority();
        Debug.Log("NetworkInformationCore.OnStartAuthority: clientAuthority " + HasLocalPlayerAuthority(this.gameObject) + " serverAuthority " + HasServerAuthority(this.gameObject));

        if (HasServerAuthority(this.gameObject)) {
            editingPlayer = GiC_Player.Undefined;
        }
        else if (HasLocalPlayerAuthority(this.gameObject)) {
            editingPlayer = GiCPlaybackManager.Instance.ThisPlayerInterface;
        }
        if (!isServer) {
            CmdEditingPlayerChanged(editingPlayer);
        }

        // authority given to player
        //if (HasLocalPlayerAuthority(this.gameObject)) { 
        //    editingPlayer = GiCPlaybackManager.Instance.ThisPlayerInterface;
        //}
        //// authority given to server
        //else {
        //    editingPlayer = GiC_Player.Undefined;
        //}

        //if (HasLocalPlayerAuthority()) {
        //    // send changes to server
        //    // create a sample array of points
        //    /*GicPointArray gicTempPointArray = new GicPointArray();
        //    gicTempPointArray.gicPoints.Add(new GicPoint(Vector3.zero, Quaternion.identity, 0, Color.red, 0, 0, Vector3.up, Vector3.right, Vector3.forward));
        //    gicTempPointArray.gicPoints.Add(new GicPoint(Vector3.one, Quaternion.identity, 0, Color.red, 0, 0, Vector3.up, Vector3.right, Vector3.forward));
        //    gicTempPointArray.gicPoints.Add(new GicPoint(Vector3.one * 2, Quaternion.identity, 0, Color.red, 0, 0, Vector3.up, Vector3.right, Vector3.forward));
        //    // add sample array to pointArrays
        //    strokes.Add(gicTempPointArray);
        //    strokes.Add(gicTempPointArray);*/
        //}
    }

    public override void OnStopAuthority() {
        base.OnStopAuthority();
        Debug.Log("NetworkInformationCore.OnStopAuthority: clientAuthority " + HasLocalPlayerAuthority(this.gameObject) + " serverAuthority " + HasServerAuthority(this.gameObject));

        if (isServer) { 
            if (HasServerAuthority(this.gameObject)) {
                editingPlayer = GiC_Player.Undefined;
            }
            else if (HasLocalPlayerAuthority(this.gameObject)) {
                editingPlayer = GiCPlaybackManager.Instance.ThisPlayerInterface;
            }
        }

        //HasServerAuthority(this.gameObject);

        //// player on host released authority
        //if (isClient && isServer && !HasLocalPlayerAuthority(this.gameObject)) {
        //    Debug.Log("player on host released authority");
        //}
        //if (HasLocalPlayerAuthority(this.gameObject)) {
        //}
    }

    public override void OnStartClient() {
        base.OnStartClient();
        OnStartClientCommand();
    }

    public override void OnStartServer() {
        base.OnStartServer();
        OnStartServerCommand();
    }
    #endregion

    #region Gic Stroke Modification Callback & Modification
    private List<int> gicStrokeArrayIndexesDirty = new List<int>();
    private string PrintInts(List<int> list) {
        return string.Join(", ", list.Select(i => i.ToString()).ToArray());
    }
    private string PrintInts(int[] array) {
        return PrintInts(array.ToList());
    }
    public void SetStrokeArrayIndexesDirty(int[] items) {
        foreach (int i in items) {
            //Debug.Log("SetStrokeArrayIndexesDirty: add " + i);
            
            // only add if not already in list
            if (gicStrokeArrayIndexesDirty.IndexOf(i) < 0) {
                gicStrokeArrayIndexesDirty.Add(i);
            }
        }
        gicStrokeArrayIndexesDirty.Sort();
        // isGicPointArraysDirty = true;
    }

    private void GicStrokeArrays_Updated(ObservedList<GicStroke>.ObserverListChange arg1, int[] arg2) {
        switch (arg1) {
            case ObservedList<GicStroke>.ObserverListChange.add:
                SetStrokeArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicPointArrays_Updated: Added Stroke at Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicPointArrayIndexesDirty: " + PrintInts(gicPointArrayIndexesDirty));
                break;
            case ObservedList<GicStroke>.ObserverListChange.clear:
                SetStrokeArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicPointArrays_Updated: Cleared Strokes from Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicPointArrayIndexesDirty: " + PrintInts(gicPointArrayIndexesDirty));
                break;
            case ObservedList<GicStroke>.ObserverListChange.change:
                SetStrokeArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicPointArrays_Updated: Changed Strokes from Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicPointArrayIndexesDirty: " + PrintInts(gicPointArrayIndexesDirty));
                break;
            default:
                Debug.LogError("NetworkInformationCore.GicPointArrays_Updated: Unhandled Stroke change " + arg1 + " of list from Atom: " + theAtom.Core.AtomIdentifier + " on item " + arg2);
                break;
        }
    }

    private bool gicStrokeArrayModificationFinished = false;
    public void SetGicStrokeModificationFinished() {
        gicStrokeArrayModificationFinished = true;
    }
    #endregion

    #region Gic Hotspots Modification Callback & Modification 
    private List<int> gicHotspotsArrayIndexesDirty = new List<int>();
    private void SetHotspotsArrayIndexesDirty(int[] items) {
        foreach (int i in items) {
            //Debug.Log("SetHotspotsArrayIndexesDirty: add " + i);
            if (gicHotspotsArrayIndexesDirty.IndexOf(i) < 0) {
                gicHotspotsArrayIndexesDirty.Add(i);
            }
        }
        gicHotspotsArrayIndexesDirty.Sort();
    }

    private void GicHotspotArrays_Updated(ObservedList<LeapHotspot>.ObserverListChange arg1, int[] arg2) {
        switch (arg1) {
            case ObservedList<LeapHotspot>.ObserverListChange.add:
                SetHotspotsArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicHotspotsArrays_Updated: Added Hotspot at Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicHotspotsArrayIndexesDirty: " + PrintInts(gicHotspotsArrayIndexesDirty));
                break;
            case ObservedList<LeapHotspot>.ObserverListChange.clear:
                SetHotspotsArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicHotspotsArrays_Updated: Cleared Hotspot from Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicHotspotsArrayIndexesDirty: " + PrintInts(gicHotspotsArrayIndexesDirty));
                break;
            case ObservedList<LeapHotspot>.ObserverListChange.change:
                SetHotspotsArrayIndexesDirty(arg2);
                //Debug.Log("NetworkInformationCore.GicHotspotsArrays_Updated: Changed Hotspot from Atom: " + theAtom.Core.AtomIdentifier + " arg2: " + PrintInts(arg2) + " gicHotspotsArrayIndexesDirty: " + PrintInts(gicHotspotsArrayIndexesDirty));
                break;
            default:
                //Debug.LogError("NetworkInformationCore.GicHotspotsArrays_Updated: Unhandled Hotspot change " + arg1 + " of list from Atom: " + theAtom.Core.AtomIdentifier + " on item " + arg2);
                break;
        }
    }

    private bool gicHotspotsArrayModificationFinished = false;
    public void SetGicHotspotsModificationFinished() {
        gicHotspotsArrayModificationFinished = true;
    }
    #endregion

    #region stroke Handling (destroying)
    private void DestroyAllStrokes() {
        Debug.Log("NetworkInformationCore.DestroyAllStrokes");
        // clear all strokes
        for (int i = strokes.Count - 1; i >= 0; i--) {
            Destroy(strokes[i].GameObjectBelongingToThisStroke);
        }
        strokes.Clear();
    }

    private void DestroyAllHotspots () {
        Debug.Log("NetworkInformationCore.DestroyAllHotspots: doesnt do anything atm");
        for (int i = strokes.Count - 1; i >= 0; i--) {
            hotspots[i].Active = false;
        }
    }

    [Command]
    public void CmdResendData() {
        // Clear all Stroke Data and send again
        gicStrokeArrayIndexesDirty = new List<int>();
        gicStrokeArrayIndexesDirty.Add(-1);
        for (int i = 0; i < strokes.Count; i++) {
            gicStrokeArrayIndexesDirty.Add(i);
        }

        // Clear all Leap Data and send again
        gicHotspotsArrayIndexesDirty = new List<int>();
        gicHotspotsArrayIndexesDirty.Add(-1);
        for (int i = 0; i < hotspots.Count; i++) {
            gicHotspotsArrayIndexesDirty.Add(i);
        }

        //Debug.Log("NetworkInformationCore.ResendDataCmd: strokes " + PrintInts(gicStrokeArrayIndexesDirty) + " hotspots " + PrintInts(gicHotspotsArrayIndexesDirty));
        NetworkIdentity networkIdentity = gameObject.GetComponent<NetworkIdentity>();
        List<NetworkConnection> excludeConnections = new List<NetworkConnection> { }; // connectionToClient does only work on player, connectionToServer also doesnt work right here
        // exclude connection to object owner
        //if (networkIdentity != null)
        //    excludeConnections.Add(networkIdentity.clientAuthorityOwner);
        // exclude connection to client (of host)
        if (NetworkPlayerController.LocalInstance.connectionToClient != null)
            excludeConnections.Add(NetworkPlayerController.LocalInstance.connectionToClient);
        DistributeAtomData(excludeConnections);
    }
    #endregion

    #region Serialize & Deserialize gicPointArrays
    enum NetworkDataType {
        Stroke,
        Hotspot
    }

    private void SerializeAtom(List<int> serializeStrokes, List<int> serializeHotspots, out byte[] atomDataToSend, out int size) {
        //Debug.Log("NetworkInformationCore.SerializeAtom: Strokes: " + PrintInts(serializeStrokes) + " Count: " + serializeStrokes.Count + " Hotspots: "+PrintInts(serializeHotspots) + " Count: " + serializeHotspots.Count);
        int networkSize = serializeStrokes.Count + serializeHotspots.Count;

        NetworkWriter networkWriter = new NetworkWriter();

        if ((GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Paint) || NetworkPlayerController.LocalInstance.isServer) { 
            // package index is the index of the sent data
            for (int packageIndex = 0; packageIndex < serializeStrokes.Count; packageIndex++) {
                // get index of stroke to serialize
                int strokeIndex = serializeStrokes[packageIndex];

                // add package if within valid range
                if ((strokeIndex < strokes.Count) && (strokeIndex >= -1)) {
                    //Debug.Log("NetworkInformationCore.SerializeAtom: adding stroke " + strokeIndex);
                    // add type of data
                    networkWriter.Write((int)NetworkDataType.Stroke);
                    // add index of hotspot
                    networkWriter.Write(strokeIndex);
                    // dont add data if we send clear (-1)
                    if (strokeIndex != -1) {
                        byte[] strokeBytes = strokes[strokeIndex].GetAsBytes();
                        networkWriter.Write(strokeBytes.Length);
                        networkWriter.Write(strokeBytes, strokeBytes.Length);
                        //Debug.Log("NetworkInformationCore.SerializeAtom: frame "+Time.frameCount+" strokeIndex " + strokeIndex + " packed to send with bytes " + strokeBytes.Length);
                    }
                    else {
                        Debug.Log("NetworkInformationCore.SerializeAtom: send stroke list clear");
                    }
                }
                else {
                    //Debug.Log("NetworkInformationCore.SerializeAtom: frame " + Time.frameCount + " strokeIndex " + strokeIndex + " destroyed, sending 0");
                    networkWriter.Write((int)NetworkDataType.Stroke);
                    networkWriter.Write(strokeIndex);
                    networkWriter.Write(0);
                }
            }
        }

        if ((GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Leap) || NetworkPlayerController.LocalInstance.isServer) {
            for (int packageIndex = 0; packageIndex < serializeHotspots.Count; packageIndex++) {
                // get index of hotspot to serialize
                int hotspotIndex = serializeHotspots[packageIndex];

                // add package if within valid range
                if ((hotspotIndex < hotspots.Count) && (hotspotIndex >= -1)) {
                    // add type of data
                    networkWriter.Write((int)NetworkDataType.Hotspot);
                    // add index of hotspot
                    networkWriter.Write(hotspotIndex);
                    // dont add data if we send clear (-1)
                    if (hotspotIndex != -1) {
                        byte[] hotspotBytes = hotspots[hotspotIndex].GetAsBytes();
                        networkWriter.Write(hotspotBytes.Length);
                        networkWriter.Write(hotspotBytes, hotspotBytes.Length);
                    }
                    else {
                        //Debug.Log("NetworkInformationCore.SerializeAtom: send hotspot clear");
                    }
                }
                else {
                    Debug.LogError("NetworkInformationCore.SerializeAtom: hotspotIndex " + hotspotIndex + " outside of hotspots array " + hotspots.Count);
                }
            }
        }

        atomDataToSend = networkWriter.ToArray();
        size = atomDataToSend.Length;
    }

    private void DeserializeAtom(byte[] atomBytes, bool isServerReceiving) {
        //Debug.Log("NetworkInformationCore.DeserializeAtom: deserializing bytes (" + atomBytes.Length + ") strokes into " + theAtom.Core.DrawingDataHandler.DrawingParent + " has Authority " + hasAuthority);
        //for (int packageIndex = 0; packageIndex < atomBytes.Length; packageIndex++) {
        //byte[] strokeBytes = atomBytes[packageIndex];
        //            Debug.Log("- NetworkInformationCore.DeserializeAtom: "+theAtom.Core.AtomIdentifier+" Deserialize Stroke:" + strokeIndex);
        if (!isServerReceiving && isServer && isClient) {
            Debug.Log("NetworkInformationCore.DeserializeAtom: discard, we are receiving as client on a host "+Time.frameCount);
            return;
        }

        if (hasAuthority) {
            //Debug.Log("NetworkInformationCore.DeserializeAtom: dont do it "+hasAuthority);
            return;
        }

        List<int> deserializedStrokes = new List<int>();
        List<int> deserializedHotspots = new List<int>();


        List<int> handledStrokes = new List<int>();

        //Debug.Log("NetworkInformationCore.DeserializeAtom: handling new package deserializement in frame " + Time.frameCount);

        NetworkReader networkReader = new NetworkReader(atomBytes); // strokeBytes);
        networkReader.SeekZero();

        while (networkReader.Position < networkReader.Length) {
            NetworkDataType dataType = (NetworkDataType)networkReader.ReadInt32();
            switch (dataType) {
                case NetworkDataType.Stroke:
                    int strokeIndex = networkReader.ReadInt32();

                    // OBSOLETE, DOES NOT WORK
                    bool hasAlreadyBeenHandled = false;
                    if (handledStrokes.Contains(strokeIndex)) { 
                        handledStrokes.Add(strokeIndex);
                        hasAlreadyBeenHandled = true;
                    }

                    deserializedStrokes.Add(strokeIndex);
                    //Debug.LogError("NetworkInformationCore.DeserializeAtom: frame " + Time.frameCount + " handling strokeIndex: " + strokeIndex);
                    //Debug.Log("NetworkInformationCore.DeserializeAtom: strokeIndex: " + strokeIndex);
                    if (strokeIndex == -1) {
                        DestroyAllStrokes();
                    } else {
                        int strokeSize = networkReader.ReadInt32();
                        // if size is 0 we remove the stroke
                        if (strokeSize != 0) {
                            if (strokeSize > (networkReader.Length - networkReader.Position)) { 
                                Debug.LogWarning("NetworkInformationCore.DeserializeAtom: is the error occuring now? : pos" + networkReader.Position + " len " + networkReader.Length + " readsize" + strokeSize);
                            }
                            byte[] strokeBytes = networkReader.ReadBytes(strokeSize);
                            GicStroke stroke = GicStroke.SetFromBytes(strokeBytes, theAtom.Core.DrawingDataHandler.DrawingParent);
                            if (stroke == null) {
                                Debug.LogError("ERROR This Must not happen. Stroke From Network reader is null");
                            }
                            if (strokeIndex >= strokes.Count) {
                                //Debug.Log("NetworkInformationCore.DeserializeAtom: ADD strokeIndex: " + strokeIndex + " stroke: " + stroke.StrokePoints.Count);
                                if (hasAlreadyBeenHandled) {
                                    Debug.Log("NetworkInformationCore.DeserializeAtom: frame " + Time.frameCount + " SKIP strokeIndex: " + strokeIndex + " size " + strokeSize + " has already been handled");
                                }
                                else {
                                    strokes.Add(stroke);
                                    //Debug.Log("NetworkInformationCore.DeserializeAtom: frame " + Time.frameCount + " ADD strokeIndex: " + strokeIndex + " size " + strokeSize);
                                }
                            }
                            else {
                                //Debug.Log("NetworkInformationCore.DeserializeAtom: REPLACE strokeIndex: " + strokeIndex + " stroke: " + stroke.StrokePoints.Count);
                                // delete old stroke
                                //Debug.Log("NetworkInformationCore.DeserializeAtom: frame " + Time.frameCount + " REPLACE strokeIndex: " + strokeIndex + " size " + strokeSize);
                                strokes[strokeIndex].RemoveStroke();
                                strokes[strokeIndex] = stroke;
                            }
                        }
                        else {
                            //Debug.Log("NetworkInformationCore.DeserializeAtom: frame " + Time.frameCount + " REMOVE strokeIndex: " + strokeIndex);
                            if (strokeIndex < strokes.Count) { 
                                strokes[strokeIndex].RemoveStroke();
                                strokes[strokeIndex] = new GicStroke(0, theAtom.Core.DrawingDataHandler.DrawingParent, 0, 0, false);
                                strokes.RemoveAt(strokeIndex);
                            } else {
                                Debug.LogError("NetworkInformationCore.DeserializeAtom: strokeIndex: " + strokeIndex + " not in strokesList " + strokes.Count);
                            }
                        }
                        //theAtom.Core.DrawingDataHandler.HotspotNetworkModified(hotspotIndex);
                        //theAtom.networkCore.SetGicHotspotsModificationFinished();
                        //SetGicStrokeModificationFinished();
                    }
                    break;
                
                case NetworkDataType.Hotspot:
                    int hotspotIndex = networkReader.ReadInt32();
                    deserializedHotspots.Add(hotspotIndex);
                    //Debug.Log("NetworkInformationCore.DeserializeAtom: hotspotIndex: " + hotspotIndex);
                    if (hotspotIndex == -1) {
                        DestroyAllHotspots();
                    } else {
                        int hotspotSize = networkReader.ReadInt32();
                        byte[] hotspotBytes = networkReader.ReadBytes(hotspotSize);
                        LeapHotspot hotspot = LeapHotspot.SetFromBytes(hotspotBytes);
                        // if (hotspotIndex >= hotspots.Count) {
                        //     Debug.Log("NetworkInformationCore.DeserializeAtom: ADD hotspotIndex: " + hotspotIndex + " hotspot: " + hotspots.Count);
                        //     hotspots.Add(hotspot);
                        // }
                        // else {
                        //Debug.Log("NetworkInformationCore.DeserializeAtom: set hotspotIndex: " + hotspotIndex + " hotspot: " + hotspots.Count);
                        // new hotspot
                        if (hotspots[hotspotIndex] == null) {
                            Debug.LogError("NetworkInformationCore.DeserializeAtom: new hotspot, does this happen?");
                            hotspots[hotspotIndex] = hotspot;
                        } else {
                            hotspots[hotspotIndex] = hotspot;
                        }
                        theAtom.Core.LeapDataHandler.HotspotNetworkModified(hotspotIndex);
                        //SetGicHotspotsModificationFinished();
                    }
                    break;
            }
        }
        //Debug.Log("AtomInformationCore.DeserializeAtom: strokes (" + PrintInts(deserializedStrokes) + ") hotspots (" + PrintInts(deserializedHotspots) + ") hasAuthority " + hasAuthority + " localAuthority "+ HasLocalPlayerAuthority(gameObject));
        //}

        //Debug.Log("NetworkInformationCore.DeserializeAtom: Strokes: " + PrintInts(gicStrokeArrayIndexesDirty) + " gicPointArrayIndexesDirty.Count " + gicStrokeArrayIndexesDirty.Count);
        //Debug.Log("NetworkInformationCore.DeserializeAtom: deserializing " + atomBytes.Length + " strokes into " + theAtom.Core.DrawingDataHandler.DrawingParent + " successfully finished");
    }
    #endregion

    #region CLIENT to SERVER sending

    //for data transmission
    private int totalServerReceivedArrayBytes = 0;
    private int totalServerExpectedArrayBytes = 0;
    private int totalServerExpectedArrays = 0;
    private byte[][] serverDataReceiveCache;

    private int clientNetworkPackageRandomBase = 0;
    private int clientNetworkPackageCounter = 0;
    [Client]
    private void UploadAtomData() {
        //Debug.Log("NetworkInformationCore.UploadAtomData: " + Time.frameCount + " Atom " + theAtom.Core.AtomIdentifier);

        byte[] atomDataToSend;
        int size;
        SerializeAtom(gicStrokeArrayIndexesDirty, gicHotspotsArrayIndexesDirty, out atomDataToSend, out size);
        if (size > 20000)
        {
            Debug.LogWarning("Atom Size very big: "+size);
            GiC_DrawStrokes.Instance.OnControllerTriggerUnClicked();
            GiC_DrawStrokes.Instance.OnControllerTriggerClicked(false);
            return;
        }
        gicStrokeArrayIndexesDirty = new List<int>();
        gicHotspotsArrayIndexesDirty = new List<int>();

        
        CmdOnServerReceiveAtom(atomDataToSend);
    }

    [Command(channel = 3)]
    private void CmdOnServerReceiveAtom(byte[] strokeBytes) {
        // store the stroke data into the cache
        //Debug.Log("NetworkInformationCore.OnServerReceiveAtom: strokeBytes.Length " + strokeBytes.Length + " serverDataReceiveCache " + (serverDataReceiveCache != null ? serverDataReceiveCache.Length.ToString() : "null"));

        DeserializeAtom(strokeBytes, true);

        // distribute the data to the clients
        // exclude the client which send the data and the local client
        NetworkIdentity networkIdentity = gameObject.GetComponent<NetworkIdentity>();
        List<NetworkConnection> excludeConnections = new List<NetworkConnection> {  }; // connectionToClient does only work on player, connectionToServer also doesnt work right here
        // exclude connection to object owner
        if (networkIdentity != null)
            excludeConnections.Add(networkIdentity.clientAuthorityOwner);
        // exclude connection to client (of host)
        if (NetworkPlayerController.LocalInstance.connectionToClient != null)
            excludeConnections.Add(NetworkPlayerController.LocalInstance.connectionToClient);

        DistributeAtomData(excludeConnections);
    }
    #endregion

    #region SERVER to CLIENT sending
    //for data transmission
    private int totalClientReceivedArrayBytes = 0;
    private int totalClientExpectedArrayBytes = 0;
    private int totalClientExpectedArrays = 0;
    private byte[][] clientDataReceiveCache;


    private int serverNetworkPackageRandomBase = 0;
    private int serverNetworkPackageCounter = 0;
    [Server]
    private void DistributeAtomData(List<NetworkConnection> excludeConnections) {
        if (excludeConnections == null) {
            Debug.LogError("invalid exclude list " + excludeConnections);
            return;
        }

        //StartCoroutine(DistributeAtomDataCoroutine());
        byte[] atomDataToSend;
        int size;
        SerializeAtom(gicStrokeArrayIndexesDirty, gicHotspotsArrayIndexesDirty, out atomDataToSend, out size);
        gicStrokeArrayIndexesDirty = new List<int>();
        gicHotspotsArrayIndexesDirty = new List<int>();

        //int networkPackageId = serverNetworkPackageRandomBase + ++serverNetworkPackageCounter; //UnityEngine.Random.Range(0, int.MaxValue);
        //networkTransmitter.SendBytesToClients(networkPackageId, atomDataToSend);
        RpcOnClientReceiveAtom(atomDataToSend);

        foreach (NetworkConnection excludeNetConn in excludeConnections) {
            Debug.Log("- exclude connections " + excludeNetConn + " " + (excludeNetConn != null ? excludeNetConn.connectionId.ToString() : "null"));
        }

        foreach (NetworkConnection netConn in NetworkServer.connections) {
            //Debug.Log("considering send to connection: " + 
            //    netConn.address + " " + 
            //    netConn.connectionId + " " + 
            //    netConn.hostId + " " + 
            //    netConn.isReady + " " + 
            //    netConn.lastMessageTime + " " + 
            //    netConn.logNetworkMessages + " " +
            //    netConn.isConnected + " " + 
            //    netConn.clientOwnedObjects + " " +
            //    netConn.lastError + " " + 
            //    netConn.playerControllers);

#if false
            // send to all cients
            if (netConn != null) { 
                TargetOnClientReceiveAtom(netConn, atomDataToSend);
            }
#else 
            // selective data transfer
            if (excludeConnections.Contains(netConn)) {
                // skip
                //Debug.Log("skipped");
            }
            else {
                TargetOnClientReceiveAtom(netConn, atomDataToSend);
                //Debug.Log("sent");
            }
#endif
        }
    }
    
    [ClientRpc(channel = 3)]
    private void RpcOnClientReceiveAtom(byte[] strokeBytes) {
        //Debug.Log("NetworkInformationCore.OnClientFragmentCompletelyReceivedStroke: strokeIndex " + strokeIndex + " strokeBytes.Count: " + strokeBytes.Length);
        
        // dont overwrite data if the have the authority over the object (would cause loop)
        if (NetworkInteractableObject.HasLocalPlayerAuthority(gameObject)) {
            //Debug.Log("NetworkInformationCore.OnClientFragmentCompletelyReceivedStroke: discarding Atom modifications (has authority over object) AtomId: " + theAtom.Core.AtomIdentifier + " strokeIndex " + strokeIndex);
            return;
        }
        // this is the host, dont overwrite data again (we already received them as server)
        if (isServer) {
            //Debug.Log("NetworkInformationCore.OnClientFragmentCompletelyReceivedStroke: discarding Atom modifications (this is a server & client) AtomId: " + theAtom.Core.AtomIdentifier + " strokeIndex " + strokeIndex);
            return;
        }

        DeserializeAtom(strokeBytes, false);
    }

    [TargetRpc(channel = 3)]
    private void TargetOnClientReceiveAtom(NetworkConnection netConn, byte[] strokeBytes) {
        //Debug.Log("NetworkInformationCore.OnClientFragmentCompletelyReceivedStroke: strokeIndex " + strokeIndex + " strokeBytes.Count: " + strokeBytes.Length);

        // dont overwrite data if the have the authority over the object (would cause loop)
        if (NetworkInteractableObject.HasLocalPlayerAuthority(gameObject) || isServer) {
            Debug.LogWarning("NetworkInformationCore.OnClientFragmentCompletelyReceivedStroke: Discarding Atom modifications " +
                " hasAuthority: "+ NetworkInteractableObject.HasLocalPlayerAuthority(gameObject) +
                " isServer: " + isServer +
                " AtomId: " + theAtom.Core.AtomIdentifier + 
                " netConn: " + netConn);
            return;
        }

        DeserializeAtom(strokeBytes, false);
    }
#endregion

#region Debug
    public override string ToString() {
        StringBuilder sb = new StringBuilder();
        sb.Append("GicPointArray size = ");
        sb.Append(strokes.Count);
        sb.Append("\n");
        for (int i = 0; i < strokes.Count; i++) {
            sb.Append(strokes[i].ToString());
        }
        return sb.ToString();
    }
#endregion
}
