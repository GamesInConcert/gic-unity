﻿using System;
using UnityEngine;
//using UnityEngine.VR.WSA.Persistence;

struct VoronoiDataOut
{
    public Vector3 pos;
    public float Voronoi;
}

struct VoronoiDataIn
{
    public Vector3 HotspotPosition;
}


public class ComputeShaderVoronoiToGeometry : MonoBehaviour
{

    public ComputeShader VoronoicomputeShader;
    public ComputeBuffer VoronoiOutputBuffer;
    public ComputeBuffer HotspotPositionBuffer;
    public Shader PlantShader;
    public Material PlantMaterial;
    public Texture NoiseTexture;
    public float NoiseMultiplier;

    public bool doMeshBased = false;
    [Range(1f,2f)]
    public float minowskyMix = 1f;

    public float Area = 5f;
    public GameObject ObjectHotspotMarker;
    public GameObject[] markers;
    private Vector3[] positions = new Vector3[10];
    public CShaderHotspot[] shaderHotspots = new CShaderHotspot[21];

    public int xGridSteps = 64;
    public int yGridSteps = 64;
    public float xGridSize = 20f;
    public float yGridSize = 20f;
    public float gridDistance = 0.5f;
    public int VertexCount = 16;

    private int worldPosShaderHash;
    private int gridDistanceHash;
    private int minowskyMixHash;
    private int hotspotHash;
    private int noiseTexHash;
    private int noiseMultHash;
    private int bufPointsHash;
    private int outputBufferHash;

    private Mesh baseMesh;

    private int CSKernel; 

    private void InitializeBuffers()
    {
        VoronoiOutputBuffer = new ComputeBuffer(VertexCount, sizeof(float)*4);
        HotspotPositionBuffer = new ComputeBuffer(shaderHotspots.Length, sizeof(float) * 3 + sizeof(int));

        VoronoicomputeShader.SetBuffer(CSKernel, outputBufferHash, VoronoiOutputBuffer);
        VoronoicomputeShader.SetTexture(CSKernel, noiseTexHash, NoiseTexture);
        VoronoicomputeShader.SetFloat(noiseMultHash, 1.0f);
        PlantMaterial.SetBuffer(bufPointsHash, VoronoiOutputBuffer);
    }

    public void Dispatch()
    {
        if (!SystemInfo.supportsComputeShaders)
        {
            Debug.LogError("System does not support Compute Shaders");
        }
        HotspotPositionBuffer.SetData(shaderHotspots);
        VoronoicomputeShader.SetBuffer(CSKernel, hotspotHash, HotspotPositionBuffer);
        VoronoicomputeShader.SetFloat(minowskyMixHash, minowskyMix);

        VoronoicomputeShader.SetFloat(gridDistanceHash, gridDistance);
        VoronoicomputeShader.SetFloat(noiseMultHash, NoiseMultiplier);

        // has to be removed from here
        VoronoicomputeShader.SetTexture(CSKernel, noiseTexHash, NoiseTexture);
        VoronoicomputeShader.SetBuffer(CSKernel, outputBufferHash, VoronoiOutputBuffer);
        VoronoicomputeShader.Dispatch(CSKernel, 4,4,1);
    }

    void Release()
    {
        VoronoiOutputBuffer.Release();
        HotspotPositionBuffer.Release();
    }

	// Use this for initialization
	void Start()
	{
	    //PlantMaterial = new Material(PlantShader);
        CSKernel = VoronoicomputeShader.FindKernel("CSMain");
	    worldPosShaderHash = Shader.PropertyToID("_WorldPos");
	    hotspotHash = Shader.PropertyToID("HotspotPositions");
	    minowskyMixHash = Shader.PropertyToID("minowskyMix");
	    gridDistanceHash = Shader.PropertyToID("gridDistance");
	    noiseTexHash = Shader.PropertyToID("_NoiseTex");
	    noiseMultHash = Shader.PropertyToID("noiseMultiplier");
	    bufPointsHash = Shader.PropertyToID("buf_Points");
	    outputBufferHash = Shader.PropertyToID("outputBuffer");
        InitializeBuffers();

        //UpdateMarkersArray();
        //Dispatch();
        //SetBuffers();
	}

    void SetBuffers()
    {
        PlantMaterial.SetBuffer(bufPointsHash, VoronoiOutputBuffer);
    }

    void OnRenderObject()
    {

        //Debug.Log("Test");
        //material.SetBuffer("bPoints", computeBuffer);
        //material.SetFloat("_Size", 1f);
        //material.SetFloat("_Height", 2f);
        //PlantMaterial.SetVector(worldPosShaderHash, this.transform.position);
        //PlantMaterial.SetPass(1);
        //Graphics.DrawMeshNow(baseMesh, Matrix4x4.identity, 0);
        //Debug.Log(Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 137.5f, 0), Vector3.one));
        //PlantMaterial.SetPass(0);
        //Graphics.DrawProcedural(MeshTopology.Points, VertexCount);
        //Graphics.DrawProcedural(MeshTopology.Points, VertexCount);
        //Graphics.DrawProcedural(MeshTopology.Points, VertexCount);
        if (!doMeshBased)
        {
            UpdateMarkersArray();
            Dispatch();
            PlantMaterial.SetBuffer(bufPointsHash, VoronoiOutputBuffer);
            Graphics.DrawMesh(baseMesh, Matrix4x4.identity, PlantMaterial, 0);
        }


    }

    // Update is called once per frame
    void Update () {
        if (!doMeshBased) return;
        UpdateMarkersArray();
        Dispatch();
        PlantMaterial.SetVector(worldPosShaderHash, this.transform.position);
        PlantMaterial.SetBuffer(bufPointsHash, VoronoiOutputBuffer);
    }

    void UpdateMarkersArray()
    {
        Vector3 ObjectHotspot = ObjectHotspotMarker.transform.position;
        //positions = new Vector3[markers.Length];
        Vector4 extends = Vector4.zero;
        for (int i = 0; i < shaderHotspots.Length; i++)
        {
            if (i > markers.Length-1)
            {
                shaderHotspots[i].Pos = Vector3.zero;
                shaderHotspots[i].Active = Convert.ToInt16(false);
                continue;
            }
            float x = markers[i].transform.position.x - ObjectHotspot.x;
            float y = markers[i].transform.position.z - ObjectHotspot.z;
            float z = markers[i].transform.position.y - ObjectHotspot.y;
            shaderHotspots[i].Pos = new Vector3(x, y, z);
            shaderHotspots[i].Active = Convert.ToInt16(true);
            //Debug.Log(positions[i]);
            if (x < extends.x)
                extends.x = x - Area;
            if (x > extends.y)
                extends.y = x + Area;
            if (y < extends.z)
                extends.z = y - Area;
            if (y > extends.w)
                extends.w = y + Area;
        }
           // this.transform.position = ObjectHotspot;
    }

    void OnDisable()
    {
        Release();
    }
}
