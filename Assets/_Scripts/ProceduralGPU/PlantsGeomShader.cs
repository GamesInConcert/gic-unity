﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct geomData
{
    public Vector3 pos;
    public Vector4 col;
}

public class PlantsGeomShader : MonoBehaviour
{

    public Material material;
    public Shader myGeomShader;

    public bool useComputeBuffer = false;

    private geomData[] data;

    private ComputeBuffer computeBuffer;

    void Start()
    {
        data = new geomData[2];
        data[0].pos = Vector3.zero;
        data[0].col = Color.blue;
        data[1].pos = Vector3.one;
        data[1].col = Color.red;

        computeBuffer = new ComputeBuffer(data.Length, sizeof(float)*7);
        computeBuffer.SetData(data);
    }

    void OnRenderObject()
    {
        //Debug.Log("Test");
        material.SetBuffer("bPoints", computeBuffer);
        //material.SetFloat("_Size", 1f);
        //material.SetFloat("_Height", 2f);
        material.SetVector("_WorldPos", this.transform.position);
        Debug.LogError(computeBuffer.count);
        Graphics.DrawProcedural(MeshTopology.Points, computeBuffer.count);
    }

    void OnDisable()
    {
        OnDestroy();
    }

    void OnDestroy()
    {
        computeBuffer.Release();
    }

    void OnShutdown()
    {
        OnDestroy();
    }

}

