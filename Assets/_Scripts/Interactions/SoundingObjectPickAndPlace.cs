﻿using UnityEngine;
using WorldspaceController;

public class SoundingObjectPickAndPlace : InteractableObject
{
    private GiC_ProceduralAtom theAtom;
    private AtomInformationCore core { get { return theAtom.Core; } }

    private Color originalColor;

    private MeshRenderer targetRenderer;

    private GiC_BagPackController bpController { get { return theAtom.BagPackController; } }
    private bool inProximityToStorageContainer;
    //private int storageSlot = -1;
    private GameObject possibleStorageContainer;

    private int shaderColorPropertyId;

    private void Awake()
    {
        onAttachStartEvent.AddListener(OnAttachStartEvent);
        onAttachStopEvent.AddListener(OnAttachStopEvent);

        onDragRepeatEvent.AddListener(OnDragRepeatEvent);
        onDragExitEvent.AddListener(OnDragExitEvent);
        // object is hovered

        onHoverEnterEvent.AddListener(OnHoverEnterEvent);
        // onHoverRepeatEvent.AddListener(OnHoverRepeatEvent);

        onHoverExitEvent.AddListener(OnHoverExitEvent);

        // WorldspaceController.WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.AddListener(InitControllerCallback);
    }

    private void OnDisable()
    {
        onAttachStartEvent.RemoveListener(OnAttachStartEvent);
        onAttachStopEvent.RemoveListener(OnAttachStopEvent);

        onDragRepeatEvent.RemoveListener(OnDragRepeatEvent);
        onDragExitEvent.RemoveListener(OnDragExitEvent);
        // object is hovered
        onHoverEnterEvent.RemoveListener(OnHoverEnterEvent);
        onHoverExitEvent.RemoveListener(OnHoverExitEvent);
    }

    private void OnAttachStartEvent(InputDeviceData deviceData) {
        DoAttachToController(deviceData);
    }

    private void OnAttachStopEvent(InputDeviceData deviceData) {
        DoDetachFromController(deviceData);
    }

    public void OnHoverEnterEvent(InputDeviceData deviceData)
    {
        targetRenderer.material.SetColor(shaderColorPropertyId, Color.red);
        if (!GiCPlaybackManager.Instance)
            return;
        if (core.ProceduralAtomParameters == null)
            return;
        GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/p/" + GiCPlaybackManager.Instance.ThisPlayerInterface, (core.ProceduralAtomParameters.presetIndex)), GicOscManager.Instance.UseMulticastBroadcast);
    }

    public void OnHoverRepeatEvent(InputDeviceData deviceData)
    {
        // if this object is touched, but not grabbed by something
        if (!GicOscManager.Instance)
        {
            Debug.LogError("GicOscManager.Instance undefined");
            return;
        }

        if (GicOscManager.Instance.OscBundleAtomMetaInformation == null)
        {
            return;
        }

        if (core == null)
        {
            Debug.LogError("Core undefined");
        }
    }

    public void OnHoverExitEvent(InputDeviceData deviceData)
    {
        targetRenderer.material.SetColor(shaderColorPropertyId, originalColor);
    }

    public void OnDragRepeatEvent(InputDeviceData deviceData)
    {
        MoveObject(deviceData);
    }

    public void Initialize(GiC_ProceduralAtom atom)
    {
        theAtom = atom;
        shaderColorPropertyId = Shader.PropertyToID("_Color");
        //strokes = GameObject.FindObjectOfType<GicCreateStrokes>();
        targetRenderer = theAtom.SeaboardGeometry.GetComponent<MeshRenderer>();
        originalColor = targetRenderer.material.GetColor(shaderColorPropertyId);
    }

    //protected override void StyleChangeEvent()
    //{
    //    // change visuals depending on this.grabbedByDevice (null or not) and or this.hoveringDevices (count)
    //    if (grabbedByDevice != null) {
    //        // is grabbed
    //    } else if (hoveringDevices.Count > 0) {
    //        // is hovered by some device
    //    }
    //}

    public void OnDragExitEvent(InputDeviceData deviceData) //GameObject usingObject)
    {
        GiC_SoundingObjectsManager.Instance.ManageAuthority(null);
        InstantiateCompleteAtom(theAtom.Core.InitialSoundPreset);

        //firstMovementAfterCreation = false;
        bpController.CloseBagPack();
        Destroy(this);
    }

    // Use this for initialization of a complete new Atom at "this" place
    private void InstantiateCompleteAtom(int soundPresetNumber)
    {
        NetworkPlayerController.LocalInstance.CmdCreateAtom(gameObject.transform.position, gameObject.transform.rotation, soundPresetNumber, GiCPlaybackManager.Instance.ThisPlayerInterface);
    }

    private void MoveObject(InputDeviceData deviceData) //GameObject objectToMove)
    {
        if (inProximityToStorageContainer)
        {
            // grab point is where the object is usually when grabbed
            Vector3 newPosition = deviceData.inputDevice.grabAttachementPoint.transform.position;

            float distanceToCenter = Vector3.Distance(possibleStorageContainer.transform.position, newPosition);
            if (distanceToCenter < possibleStorageContainer.GetComponent<BoxCollider>().size.x + 0.2f)
            {
                transform.position = possibleStorageContainer.transform.position;
            }
            else
            {
                inProximityToStorageContainer = false;
                //storageSlot = -1;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BagPackStorageContainer"))
        {
            inProximityToStorageContainer = true;
            possibleStorageContainer = other.gameObject;
            //storageSlot = other.GetComponent<GiC_StoreObjectInContainer>().storageSlot;
        }
    }
}
