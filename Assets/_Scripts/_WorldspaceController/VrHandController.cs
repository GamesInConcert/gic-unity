﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using WorldspaceController;

public enum FINGERS {
    thumb,
    index,
    middle,
    ring,
    pinki
}

public enum HANDEDNESS { Left, Right };

[System.Serializable]
public class HandFingers {
    public FINGERS finger;
    public Transform head;
    public Quaternion headRot;
    public Transform mid;
    public Quaternion midRot;
    public Transform root;
    public Quaternion rootRot;

    /// <summary>
    /// search the given transform by name to define head, mid & root objects
    /// </summary>
    /// <param name="handRoot"></param>
    public void ApplyFromTransform (Transform handRoot) {
        string searchStringBase = finger.ToString().ToLower();
        foreach (Transform t in handRoot.GetComponentsInChildren<Transform>(true)) {
            string value = t.name.ToLower();
            // skip _end
            if (value.Contains("_end")) {
                continue;
            }
            if (value.StartsWith(searchStringBase + ".001")) {
                root = t;
            }
            if (value.StartsWith(searchStringBase + ".002")) {
                mid = t;
            }
            if (value.StartsWith(searchStringBase + ".003")) {
                head = t;
            }
            //Debug.Log("checking " + value + " for " + searchStringBase);
        }

        if ((root != null) && (mid != null) && (head != null)) {
            headRot = head.transform.localRotation;
            midRot = mid.transform.localRotation;
            rootRot = root.transform.localRotation;
        }
        else {
            Debug.LogError("Something is missing " + root + " " + mid + " " + head + " root " + handRoot.name + " search " + searchStringBase);
        }
    } 

    public void ApplyRotation (float angle) {
        if ((root != null) && (mid != null) && (head != null)) {
            head.transform.localRotation = headRot * Quaternion.Euler(0, 0, angle);
            mid.transform.localRotation = midRot * Quaternion.Euler(0, 0, angle);
            if (finger != FINGERS.thumb) {
                root.transform.localRotation = rootRot * Quaternion.Euler(0, 0, angle);
            }
        }
    }
}

public enum ACTION {
    Thumb,
    Point,
    Grab
}

public class VrHandController : NetworkBehaviour {
    public Transform handArmature;
    public HandFingers[] handFingers = new HandFingers[5];
    public WorldspaceController.InputDeviceHand handedness;

    NetworkPlayerController networkPlayerController = null;

    public bool thumbPressed;
    public bool pointPressed;
    public bool grabPressed;

    [SyncVar(hook = "ThumbAngle")]
    private float thumbAngle;
    [SyncVar(hook = "PointAngle")]
    private float pointAngle;
    [SyncVar(hook = "GrabAngle")]
    private float grabAngle;

    public override void OnStartClient() {
        //Debug.LogWarning("VrHandController.OnStartClient: NetworkPlayerController" + networkPlayerController.isLocalPlayer + " NetworkIdentity " + GetComponent<NetworkIdentity>().isLocalPlayer + " parent " + (transform.parent != null ? transform.parent.name : "null"));
        networkPlayerController = GetComponent<NetworkPlayerController>();
        base.OnStartClient();
        InitHandModel();
    }

    public override void OnStartLocalPlayer() {
        //Debug.LogWarning("VrHandController.OnStartLocalPlayer: NetworkPlayerController" + networkPlayerController.isLocalPlayer + " NetworkIdentity " + GetComponent<NetworkIdentity>().isLocalPlayer + " parent " + (transform.parent!=null?transform.parent.name:"null"));
        base.OnStartLocalPlayer();
        // will be true anyway...
        if (networkPlayerController.isLocalPlayer) {
            InitController();
        }
    }

    private void InitHandModel() {
        handFingers[0] = new HandFingers() { finger = FINGERS.thumb };
        handFingers[1] = new HandFingers() { finger = FINGERS.index };
        handFingers[2] = new HandFingers() { finger = FINGERS.middle };
        handFingers[3] = new HandFingers() { finger = FINGERS.ring };
        handFingers[4] = new HandFingers() { finger = FINGERS.pinki };
        foreach (HandFingers hf in handFingers) {
            hf.ApplyFromTransform(handArmature);
        }
    }

    private void InitController() {
        WorldspaceController.WorldspaceInputDeviceManager.Instance.AddCallForInputDevice(InitControllerCallback);
    }

    /// <summary>
    /// update angle on server
    /// </summary>
    [Command]
    public void CmdThumbAngle(float value) { thumbAngle = value; }
    /// <summary>
    /// update angle on clients
    /// </summary>
    [Client]
    public void ThumbAngle(float value) { thumbAngle = value; }
    /// <summary>
    /// update angle on server
    /// </summary>
    [Command]
    public void CmdPointAngle(float value) { pointAngle = value; }
    /// <summary>
    /// update angle on clients
    /// </summary>
    [Client]
    public void PointAngle(float value) { pointAngle = value; }
    /// <summary>
    /// update angle on server
    /// </summary>
    [Command]
    public void CmdGrabAngle(float value) { grabAngle = value; }
    /// <summary>
    /// update angle on clients
    /// </summary>
    [Client]
    public void GrabAngle(float value) { grabAngle = value; }
    /// <summary>
    /// 
    /// </summary>
    public void UpdateAngles() {
        if (networkPlayerController.isLocalPlayer) {
            // send data to server
            CmdThumbAngle(thumbAngle);
            CmdPointAngle(pointAngle);
            CmdGrabAngle(grabAngle);
        }
    }

    void Update() {
        if (networkPlayerController == null)
            return;

        if (networkPlayerController.isLocalPlayer) {
            // update angles from input
            UpdateInput();
            UpdateAngles();
            ApplyAngles();
        }
        else {
            ApplyAngles();
        }
    }

    WorldspaceController.WorldspaceInputDevice listenedInputDevice;
    private void InitControllerCallback(int controllerId) {
        WorldspaceController.WorldspaceInputDevice _inputDevice = WorldspaceController.WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);
        // if this is a vive controller
        if (_inputDevice.deviceType == WorldspaceController.InputDeviceType.Vive) {
            if (_inputDevice.deviceHand == handedness) {
                // thumb
                _inputDevice.inputController.viveTouchpadDown.AddListener(ViveTouchpadDown);
                _inputDevice.inputController.viveTouchpadUp.AddListener(ViveTouchpadUp);
                // index
                _inputDevice.inputController.viveGripDown.AddListener(ViveGripDown);
                _inputDevice.inputController.viveGripUp.AddListener(ViveGripUp);
                // middle, ring, pinky
                _inputDevice.inputController.viveTriggerDown.AddListener(ViveTriggerDown);
                _inputDevice.inputController.viveTriggerUp.AddListener(ViveTriggerUp);

                listenedInputDevice = _inputDevice;

                WorldspaceController.WorldspaceInputDeviceManager.Instance.RemoveCallForInputDevice(InitControllerCallback);
            }
        }
        if (_inputDevice.deviceType == WorldspaceController.InputDeviceType.Mouse) {
            if ((_inputDevice.deviceHand == InputDeviceHand.Undefined) && (handedness == InputDeviceHand.Right)) {
                // thumb
                _inputDevice.inputController.mouseButton0Down.AddListener(ViveTouchpadDown);
                _inputDevice.inputController.mouseButton0Up.AddListener(ViveTouchpadUp);
                // index
                _inputDevice.inputController.mouseButton2Down.AddListener(ViveTriggerDown);
                _inputDevice.inputController.mouseButton2Up.AddListener(ViveTriggerUp);
                // middle, ring, pinky
                _inputDevice.inputController.mouseButton1Down.AddListener(ViveGripDown);
                _inputDevice.inputController.mouseButton1Up.AddListener(ViveGripUp);

                listenedInputDevice = _inputDevice;

                WorldspaceController.WorldspaceInputDeviceManager.Instance.RemoveCallForInputDevice(InitControllerCallback);
            }
        }
    }

    private void RemoveControllerCallbacks () {
        listenedInputDevice.inputController.viveGripDown.AddListener(ViveGripDown);
        listenedInputDevice.inputController.viveGripUp.AddListener(ViveGripUp);
        listenedInputDevice.inputController.viveTouchpadDown.AddListener(ViveTouchpadDown);
        listenedInputDevice.inputController.viveTouchpadUp.AddListener(ViveTouchpadUp);
        listenedInputDevice.inputController.viveTriggerDown.AddListener(ViveTriggerDown);
        listenedInputDevice.inputController.viveTriggerUp.AddListener(ViveTriggerUp);
    }

    private void ViveGripDown(InputDeviceData deviceData) {
        SetGrabState(true);
    }

    private void ViveGripUp(InputDeviceData deviceData) {
        SetGrabState(false);
    }

    private void ViveTouchpadDown(InputDeviceData deviceData) {
        SetThumbState(true);
    }

    private void ViveTouchpadUp(InputDeviceData deviceData) {
        SetThumbState(false);
    }

    private void ViveTriggerDown(InputDeviceData deviceData) {
        SetPointState(true);
    }

    private void ViveTriggerUp(InputDeviceData deviceData) {
        SetPointState(false);
    }

    public void SetThumbState(bool state) {
        thumbPressed = state;
    }
    public void SetPointState(bool state) {
        pointPressed = state;
    }
    public void SetGrabState(bool state) {
        grabPressed = state;
    }

    // Update is called once per frame
    void UpdateInput() {
        if (thumbPressed) {
            thumbAngle = Mathf.MoveTowards(thumbAngle, 1, Time.deltaTime * 5);
        }
        else {
            thumbAngle = Mathf.MoveTowards(thumbAngle, 0, Time.deltaTime * 5);
        }
        if (pointPressed) {
            pointAngle = Mathf.MoveTowards(pointAngle, 1, Time.deltaTime * 5);
        }
        else {
            pointAngle = Mathf.MoveTowards(pointAngle, 0, Time.deltaTime * 5);
        }
        if (grabPressed) {
            grabAngle = Mathf.MoveTowards(grabAngle, 1, Time.deltaTime * 5);
        }
        else {
            grabAngle = Mathf.MoveTowards(grabAngle, 0, Time.deltaTime * 5);
        }
    }

    void ApplyAngles () {
        float angle = 60;
        if (handedness == InputDeviceHand.Right) {
            angle = -60;
        }
        handFingers[0].ApplyRotation(thumbAngle * angle);
        handFingers[1].ApplyRotation(pointAngle * angle);
        handFingers[2].ApplyRotation(grabAngle * angle);
        handFingers[3].ApplyRotation(grabAngle * angle);
        handFingers[4].ApplyRotation(grabAngle * angle);
    }
}
