﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace WorldspaceController {

    [System.Serializable]
    public class GiC_Symbol {
        // group of symbols
        [SerializeField]
        public int symbolId;
        // image of symbol
        [SerializeField]
        public Sprite symbol;
    }

    public class CommentObject : NetworkInteractableObject {

        private Transform commentKeyboardPosition;
        //private GameObject commentKeyboardInstance;
        //public GameObject commentKeyboardPrefab;
        private GameObject symbolKeyboardInstance;
        public GameObject symbolKeyboardPrefab;
        private Transform commentOutputPosition;
        //private GameObject commentOutputInstance;
        //public GameObject commentOutputPrefab;
        //public TextMeshProUGUI outputTextMeshPro;

        public Image majorSymbol;
        public Image minorSymbol;

        [SyncVar]
        private string textComment;

        [SyncVar(hook = "SymbolChanged")]
        private int selectedSymbol;
        [Command]
        public void CmdSymbolChanged(int value) {
            if (value == -1) {
                NetworkServer.Destroy(gameObject);
            }

            Debug.Log("CmdSymbolChanged " + value + " " + selectedSymbol);
            selectedSymbol = value;
        }
        [Client]
        public void SymbolChanged(int value) {
            if (value == -1) {
                Network.Destroy(gameObject);
            }

            Debug.Log("SymbolChanged " + value + " " + selectedSymbol);
            selectedSymbol = value;
            UpdateSymbol();
        }

        [SerializeField]
        List<GiC_Symbol> symbols;

        private void Start() {
            Create();
            //onStartAuthorityCallback.AddListener(OpenEntryDialogue);
            //onStopAuthorityCallback.AddListener(CloseEntryDialogue);
        }

        public void Create() {
            commentOutputPosition = transform.Find("CommentOutputPosition");
            commentKeyboardPosition = transform.Find("CommentKeyboardPosition");

            //if (outputTextMeshPro == null) {
            //    outputTextMeshPro = commentOutputPosition.GetComponentInChildren<TextMeshProUGUI>();
            //}
            //if (outputTextMeshPro == null) {
            //    GameObject commentOutputInstance = new GameObject("CommentOutputObject", typeof(TextMeshProUGUI));
            //    outputTextMeshPro = commentOutputInstance.GetComponent<TextMeshProUGUI>();
            //    outputTextMeshPro.transform.SetParent(commentOutputPosition);
            //}
        }

        public override void OnStartClient() {
            // must update synced values myself
            UpdateSymbol();
        }

        public override void OnStartAuthority () {
            if (HasLocalPlayerAuthority(gameObject)) {
                OpenEntryDialogue();
            }
        }
        public override void OnStopAuthority () {
            CloseEntryDialogue();
        }

        protected override void Update () {
            base.Update();

            if (NetworkPlayerController.LocalInstance) {
                commentOutputPosition.LookAt(NetworkPlayerController.LocalInstance.transform.position);
                //commentKeyboardPosition.LookAt(NetworkPlayerController.LocalInstance.transform.position);
            }
        }
 
        public void Destroy() {
            Destroy(gameObject);
        }

        //private bool isEntryDialogueOpen = false;
        //public void ToggleEntryDialogue(InputDeviceData deviceData) {
        //    isEntryDialogueOpen = !isEntryDialogueOpen;
        //    if (isEntryDialogueOpen) {

        //        OpenEntryDialogue();
        //    }
        //    else {
        //        CloseEntryDialogue();
        //    }
        //}

        public void OpenEntryDialogue (InputDeviceData deviceData) {
            OpenEntryDialogue();
        }

        public void OpenEntryDialogue () {
            commentKeyboardPosition.transform.position = transform.position + Vector3.down;
            //if (commentKeyboardInstance == null) {
            //    commentKeyboardInstance = Instantiate(commentKeyboardPrefab, commentKeyboardPosition); // , Vector3.zero, Quaternion.identity
            //    commentKeyboardInstance.transform.localPosition = Vector3.zero;
            //    commentKeyboardInstance.transform.localRotation = Quaternion.identity;

            //    UiKeyboard uiKeyboard = commentKeyboardInstance.GetComponentInChildren<UiKeyboard>();
            //    uiKeyboard.textChangeEvent.AddListener(UpdateComment);
            //}
            if (symbolKeyboardInstance == null) {
                symbolKeyboardInstance = Instantiate(symbolKeyboardPrefab, commentKeyboardPosition);
                symbolKeyboardInstance.transform.localPosition = Vector3.zero;
                symbolKeyboardInstance.transform.localRotation = Quaternion.Euler(0,180,0);
                symbolKeyboardInstance.SetActive(true);

                SymbolKeyboard symbolKeyboard = symbolKeyboardInstance.GetComponentInChildren<SymbolKeyboard>(true);
                symbolKeyboard.symbolChangeEvent.AddListener(SymbolSelected);
            }

            commentKeyboardPosition.LookAt(NetworkPlayerController.LocalInstance.transform.position);
            //symbolKeyboardInstance.transform.rotation = Quaternion.LookRotation(NetworkPlayerController.LocalInstance.transform.position);
            //symbolKeyboardInstance.transform.rotation = Quaternion.LookRotation(NetworkPlayerController.LocalInstance.transform.position);
        }

        public void CloseEntryDialogue (InputDeviceData deviceData) {
            CloseEntryDialogue();
        }

        public void CloseEntryDialogue () {
            //if (commentKeyboardInstance != null) {
            //    UiKeyboard uiKeyboard = commentKeyboardInstance.GetComponentInChildren<UiKeyboard>();
            //    uiKeyboard.textChangeEvent.RemoveAllListeners();

            //    Destroy(commentKeyboardInstance);
            //}
            if (symbolKeyboardInstance != null) {
                SymbolKeyboard symbolKeyboard = symbolKeyboardInstance.GetComponentInChildren<SymbolKeyboard>();
                symbolKeyboard.symbolChangeEvent.RemoveListener(SymbolSelected);

                Destroy(symbolKeyboardInstance);
            }
        }

        //public void UpdateComment (string comment) {
        //    outputTextMeshPro.text = comment;
        //}

        /// <summary>
        /// called locally on input
        /// </summary>
        /// <param name="symbolId"></param>
        public void SymbolSelected (int symbolId) {
            //selectedSymbol = symbolId;
            CmdSymbolChanged(symbolId);
            //UpdateSymbol();
        }

        public void UpdateSymbol () {
            Sprite majorSprite = GetSymbol(selectedSymbol);
            Sprite minorSprite = null;

            if ((selectedSymbol % 10) == 0) {
                minorSprite = GetSymbol(selectedSymbol + 1);
            }
            else {
                minorSprite = GetSymbol(selectedSymbol - 1);
            }

            Debug.Log("UpdateSymbol: " + selectedSymbol + " major " + ((majorSprite!=null)?majorSprite.name:"null") + " minor " + ((minorSprite!=null)?minorSprite.name:"null"));
            majorSymbol.sprite = majorSprite;
            minorSymbol.sprite = minorSprite;
        }

        public Sprite GetSymbol (int symbolId) {
            for (int i = 0; i < symbols.Count; i++) {
                if (symbols[i].symbolId == symbolId) {
                    return symbols[i].symbol;
                }
            }
            return null;
        }
    }
}