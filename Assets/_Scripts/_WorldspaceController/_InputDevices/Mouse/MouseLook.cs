﻿using UnityEngine;
using System.Collections;
using WorldspaceController;

public class MouseLook : MonoBehaviour {

    private MouseLookHandler mouseLook;

    public Transform camera;

	// Use this for initialization
	void Start () {
        StartMouse ();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateMouse ();
	}

    void StartMouse () {
        mouseLook = new MouseLookHandler ();
        mouseLook.Init (Camera.main.transform, Camera.main.transform);
    }

    void UpdateMouse () {
        if (WorldspaceController.MouseController.Instance.MouseControlsCamera() ) {
            // non vr player input here
            float x = Input.GetAxis ("Horizontal") * Time.deltaTime * 3.0f; //Camera.main.transform.right
            float z = Input.GetAxis ("Vertical") * Time.deltaTime * 3.0f; // Camera.main.transform.forward *

            transform.Translate (x, 0, z, Camera.main.transform);

            //float y = Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime * 30.0f;
            //GetComponent<PlayerPhysics>().playerHeight += y;

            mouseLook.LookRotation (Camera.main.transform, Camera.main.transform);
        }
    }
}
