﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

namespace WorldspaceController {
    using System;
    using System.Reflection;
#if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine.Events;

    [CustomEditor(typeof(MouseController))]
    public class MouseControllerInspector : InputControllerInspector {
        public override void OnInspectorGUI() {
            MouseController myTarget = (MouseController)target;
            //myTarget.mouseController = myTarget;

            base.OnInspectorGUI();
        }
    }
    #endif


    public class MouseController : InputController {
        #region Singleton
        private static MouseController instance = null;
        public static MouseController Instance {
            get {
                if (instance == null) {
                    instance = (MouseController)FindObjectOfType(typeof(MouseController));
                }
                return instance;
            }
        }
        #endregion

        //public bool mouseCanLock = true;
        public bool mouseCameraControlRequiresLock = true;

        void Awake() {
            base.Awake();
        }

        #region SETUP
        internal override bool SetupController () {
            if (PlayerSettingsController.Instance.mouseControllerEnabled) {
                Debug.Log("MouseController.SetupController: start mouse");
                Transform controller = PlayerSettingsController.Instance.headTransform;
                Transform uiRaySource = FindChildNamed(controller, uiRaySourceTransformName);
                Transform physicsRaySource = FindChildNamed(controller, physicsRaySourceTransformName);
                Transform sphereCastPoint = FindChildNamed(controller, sphereCastObjectTransformName);
                Transform grabAttachementPoint = FindChildNamed(controller, grabAttachementPointTransformName);
                inputDevice = new WorldspaceInputDevice() {
                    controller = controller,
                    uiRaySource = uiRaySource,
                    physicsRaySource = physicsRaySource,
                    sphereCastPoint = sphereCastPoint,
                    grabAttachementPoint = grabAttachementPoint,

                    uiActivationMethod = ActivationMethod.ExternalInput,
                    deviceType = InputDeviceType.Mouse,
                    inputController = this,
                    physicsCastType = physicsCastTypeDefault,

                    showUiRaySetting = showUiRaySettingDefault,
                    showPhysicsRaySetting = showPhysicsRaySettingDefault,
                    uiCastActive = uiCastActiveDefault,
                    physicsCastActive = physicsCastActiveDefault,
                    uiPointerOffsetMode = PointerOffsetMode.screenCenter,

                    physicRayColors = WorldspaceInputDeviceManager.Instance.defaultPhysicsRayGradient,
                    physicsRayMaterial = WorldspaceInputDeviceManager.Instance.defaultPhysicsRayMaterial,
                    uiRayColors = WorldspaceInputDeviceManager.Instance.defaultUiRayGradient,
                    uiRayMaterial = WorldspaceInputDeviceManager.Instance.defaultUiRayMaterial
                };
                WorldspaceInputDeviceManager.Instance.AddDevice(inputDevice);
                inputDevice.deviceActive = true;
            }
            return true;
        }

        internal override void SetupCallbacks (WorldspaceInputDevice inputDevice) {
            // default callbacks are now set in inputController
        }

        internal override void SetupNetworkController (NetworkBehaviour netBehaviour) {}
        #endregion

        void Update() {
            base.Update();
        }


        public bool MouseControlsCamera () {
            // is mouse locked? 
            return ((MouseLock.Instance.GetLocked() && mouseCameraControlRequiresLock) || !mouseCameraControlRequiresLock);
        }
    }
}