﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldspaceController
{
    using System;
    using System.Reflection;
    using UnityEngine.Events;
    using UnityEngine.Networking;

#if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(CameraController))]
    public class CameraControllerInspector : InputControllerInspector
    {
        public override void OnInspectorGUI()
        {
            CameraController myTarget = (CameraController)target;
            //myTarget.mouseController = myTarget;

            base.OnInspectorGUI();
        }
    }
#endif



    public class CameraController : InputController
    {
        #region Singleton
        private static CameraController instance = null;
        public static CameraController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (CameraController)FindObjectOfType(typeof(CameraController));
                }
                return instance;
            }
        }
        #endregion

        public ActivationMethod cameraUiActivationMethod;
        //public KeyCode cameraUiInteractionKey = KeyCode.F2;

        void Awake()
        {
            base.Awake();
            InitializeFade();
        }

        void Update()
        {
            base.Update();

            //if (Input.GetKeyDown(cameraUiInteractionKey)) {
            //    inputDevice.externalPressed = true;
            //}
            //if (Input.GetKeyUp(cameraUiInteractionKey)) {
            //    inputDevice.externalReleased = true;
            //}
        }

        void LateUpdate () {
            //GetComponentInParent<PlayerPhysics>().vrControlledHeight = false;
            //GetComponentInParent<PlayerPhysics>().playerHeight = 1.5f;
        }


        #region SETUP
        internal override bool SetupController()
        {
            // other controllers do the init in activation classes (LeapInputController & ViveControllerRayEnabler)
            if (PlayerSettingsController.Instance.gazeControllerEnabled) {
                Debug.Log("CameraController.SetupController: start gaze");
                Transform controller = PlayerSettingsController.Instance.headTransform;
                Transform uiRaySource = FindChildNamed(controller.parent, uiRaySourceTransformName);
                Transform physicsRaySource = FindChildNamed(controller.parent, physicsRaySourceTransformName);
                Transform sphereCastPoint = FindChildNamed(controller.parent, sphereCastObjectTransformName);
                Transform grabAttachementPoint = FindChildNamed(controller.parent, grabAttachementPointTransformName);

                if (uiRaySource == null) {
                    Debug.LogError("MouseController.SetupController: uiRaySource is null!");
                }
                if (physicsRaySource == null) {
                    Debug.LogError("MouseController.SetupController: physicsRaySource is null!");
                }
                if (grabAttachementPoint == null)
                {
                    Debug.LogError("MouseController.SetupController: grabAttachementPoint is null!");
                }

                //Debug.Log("PlayerSettingsController.CreatePlayerCamera: add gaze input controller");
                inputDevice = new WorldspaceInputDevice()
                {
                    controller = controller,
                    uiRaySource = uiRaySource,
                    physicsRaySource = physicsRaySource,
                    sphereCastPoint = sphereCastPoint,
                    grabAttachementPoint = grabAttachementPoint,

                    uiActivationMethod = cameraUiActivationMethod,
                    deviceType = InputDeviceType.Camera,
                    inputController = this,
                    physicsCastType = physicsCastTypeDefault,

                    showUiRaySetting = showUiRaySettingDefault,
                    showPhysicsRaySetting = showPhysicsRaySettingDefault,
                    uiCastActive = uiCastActiveDefault,
                    physicsCastActive = physicsCastActiveDefault,

                    physicRayColors = WorldspaceInputDeviceManager.Instance.defaultPhysicsRayGradient,
                    uiRayColors = WorldspaceInputDeviceManager.Instance.defaultUiRayGradient
                };
                WorldspaceInputDeviceManager.Instance.AddDevice(inputDevice);
                inputDevice.deviceActive = true;
                //} else {

            }
            return true;
        }

        internal override void SetupCallbacks(WorldspaceInputDevice inputDevice)
        {
            // default callbacks are now set in inputController
        }

        internal override void SetupNetworkController(NetworkBehaviour netBehaviour) { }
        #endregion

        #region Fade Out Visual stuff
        //SteamVR_Fade steamVrFade;
        internal void InitializeFade ()
        {
        }
        internal void FadeToColor (float _fadeDuration, Color color)
        {
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
                SteamVR_Fade.Start(Color.clear, 0f);
                fadeCoroutine = null;
            }
            fadeCoroutine = FadeCoroutine(_fadeDuration, color);
            StartCoroutine(fadeCoroutine);
        }
        IEnumerator fadeCoroutine = null;
        internal IEnumerator FadeCoroutine(float _fadeDuration, Color color)
        {
            float halfDuration = _fadeDuration / 2;
            // fade in
            // set start color
            SteamVR_Fade.Start(Color.clear, 0f);
            // set and start fade to
            SteamVR_Fade.Start(Color.black, halfDuration);
            yield return new WaitForSeconds(halfDuration);
            // fade out
            // set start color
            SteamVR_Fade.Start(color, 0);
            // set and start fade to
            SteamVR_Fade.Start(Color.clear, halfDuration);
        }
        
        #endregion
    }
}
