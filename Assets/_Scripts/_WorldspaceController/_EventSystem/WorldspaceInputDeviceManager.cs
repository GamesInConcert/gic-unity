﻿using System.Collections.Generic;
using UnityEngine.VR;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;

namespace WorldspaceController {

    [System.Serializable]
    public class UnityIntEvent : UnityEvent<int> { }

    /// <summary>
    /// Keeps track of all WorldspaceInputDevices
    /// </summary>
    public class WorldspaceInputDeviceManager : MonoBehaviour, IEnumerable<WorldspaceInputDevice> {
        #region Singleton
        private static WorldspaceInputDeviceManager instance = null;
        public static WorldspaceInputDeviceManager Instance {
            get {
                if (instance == null) {
                    instance = ((WorldspaceInputDeviceManager)FindObjectOfType(typeof(WorldspaceInputDeviceManager)));
                }
                return instance;
            }
        }
        #endregion

        #region Callbacks
        // stores newly added device id's
        private List<int> newInputDevicesInitialized = new List<int>();
        // initiates callbacks to devices
        public UnityIntEvent newInputDeviceInitializedEvent = new UnityIntEvent();

        public void AddCallForInputDevice (UnityAction<int> call) {
            WorldspaceController.WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.AddListener(call);
            foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance) {
                call(inputDevice.deviceId);
            }
        }
        public void RemoveCallForInputDevice (UnityAction<int> call) {
            WorldspaceController.WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.RemoveListener(call);
        }
        #endregion

        [Header("add a sprite for the reticle here")]
        public Sprite defaultReticleSprite;
        public Gradient defaultUiRayGradient = new Gradient();
        public Material defaultUiRayMaterial;
        public Gradient defaultPhysicsRayGradient = new Gradient();
        public Material defaultPhysicsRayMaterial;

        [Header("call UpdateReticles after adding a entry @ runtime")]
        [SerializeField]
        private List<WorldspaceInputDevice> inputDevices;
        //private bool newInputDeviceConnected = false;

        void Update() {
            if (!WorldspaceInputModule.Instance.IsInitialized()) {
                Debug.LogWarning("WorldspaceInputDeviceManager.Update: WorldspaceInputModule not yet initialized!");
                return;
            }

            for (int i = 0; i < inputDevices.Count; i++) {
                if (!inputDevices[i].isInitialized) {
                    Debug.Log("WorldspaceInputDeviceManager.Update: found not initialized device, doing it now");
                    int deviceId = inputDevices[i].Initialize();
                    newInputDevicesInitialized.Add(deviceId);
                }
            }

            if (newInputDevicesInitialized.Count > 0) {
                foreach (int deviceId in newInputDevicesInitialized) {
                    newInputDeviceInitializedEvent.Invoke(deviceId);
                    Debug.Log(newInputDevicesInitialized.Count);
                }
                newInputDevicesInitialized = new List<int>();
            }

            // debug function, because of rare vive controllers sending events from wrong controller
            if (Input.GetKeyDown(KeyCode.F8)) {
                List<WorldspaceInputDevice> viveControllers = new List<WorldspaceInputDevice>();
                viveControllers.AddRange(new List<WorldspaceInputDevice>(GetInputDevicesOfType(InputDeviceType.Vive, InputDeviceHand.Left, true)));
                viveControllers.AddRange(new List<WorldspaceInputDevice>(GetInputDevicesOfType(InputDeviceType.Vive, InputDeviceHand.Right, true)));
                viveControllers.AddRange(new List<WorldspaceInputDevice>(GetInputDevicesOfType(InputDeviceType.Vive, InputDeviceHand.Undefined, true)));
                Debug.LogWarning("WorldspaceInputDeviceManager.Update: resetting "+ viveControllers.Count + " vive controllers");
                for (int i=0; i<viveControllers.Count; i++) {
                    (viveControllers[i].inputController as ViveInputController).DeviceIdChangeEvent(true);
                }
            }
        }

        public int AddDevice(WorldspaceInputDevice inputDevice) {
            inputDevices.Add(inputDevice);
            int deviceId = inputDevice.Initialize();
            newInputDevicesInitialized.Add(deviceId);
            return deviceId;
        }

        public int GetDeviceCount () {
            return inputDevices.Count;
        }
        public WorldspaceInputDevice GetDevice (int index) {
            return inputDevices[index];
        }

        /// <summary>
        /// enumarator for this class to iterate over the WorldspaceInputDevice's
        /// </summary>
        /// <returns></returns>
        public IEnumerator<WorldspaceInputDevice> GetEnumerator() {
            for (int i = 0; i < WorldspaceInputDeviceManager.Instance.inputDevices.Count; i++) {
                yield return WorldspaceInputDeviceManager.Instance.inputDevices[i];
            }
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }


        /// <summary>
        /// generates and returns a new device id
        /// </summary>
        private int deviceIdCounter = -1;
        internal int GetDeviceId() {
            deviceIdCounter += 1;
            return deviceIdCounter;
        }

        /// <summary>
        /// search all input devices for a type of device
        /// </summary>
        public WorldspaceInputDevice[] GetInputDevicesOfType(InputDeviceType _deviceType, InputDeviceHand _deviceHand, bool returnInactive = true) {
            //Debug.Log ("WorldspaceInputModule.GetInputDevicesOfType: " + _deviceType + " " + _deviceHand + " " + returnInactive);
            List<WorldspaceInputDevice> foundDevices = new List<WorldspaceInputDevice>();

            for (int i = 0; i < WorldspaceInputDeviceManager.Instance.inputDevices.Count; i++) {
                //Debug.Log("- WorldspaceInputModule.GetInputDevicesOfType: comparing with " + inputDevices[i].deviceType + " " + inputDevices[i].deviceHand + " " + inputDevices[i].deviceActive);
                if ((WorldspaceInputDeviceManager.Instance.inputDevices[i].deviceType == _deviceType) && (WorldspaceInputDeviceManager.Instance.inputDevices[i].deviceHand == _deviceHand)) {
                    if (returnInactive) {
                        foundDevices.Add(WorldspaceInputDeviceManager.Instance.inputDevices[i]);
                    } else {
                        if (WorldspaceInputDeviceManager.Instance.inputDevices[i].deviceActive) {
                            foundDevices.Add(WorldspaceInputDeviceManager.Instance.inputDevices[i]);
                        }
                    }
                }
            }

            return foundDevices.ToArray();
        }

        /// <summary>
        /// return the device with the given identifier
        /// </summary>
        public WorldspaceInputDevice GetInputDeviceFromId(int deviceId) {
            for (int i = 0; i < WorldspaceInputDeviceManager.Instance.inputDevices.Count; i++) {
                if (deviceId == WorldspaceInputDeviceManager.Instance.inputDevices[i].deviceId) {
                    return WorldspaceInputDeviceManager.Instance.inputDevices[i];
                }
            }
            Debug.Log("WorldspaceInputDeviceManager.GetInputDeviceFromId: no input device with id " + deviceId + " in " + WorldspaceInputDeviceManager.Instance.inputDevices.Count);
            return null;
        }
    }
}