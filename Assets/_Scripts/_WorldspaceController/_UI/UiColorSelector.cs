﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[System.Serializable]
public class UnityColorEvent : UnityEvent<Color> { }
[System.Serializable]
public class UnityUiPosEvent : UnityEvent<Vector2> { }


public class UiColorSelector : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {
    public Texture2D colorTex;
    RectTransform rectTransform;
    Canvas canvas;
    public Image selectedColor;

    public UnityColorEvent colorSetCallback;
    public UnityColorEvent colorSetCallbackUpdateLoop;
    public UnityUiPosEvent posSetCallback;

    private bool doUpdate = false;
    private PointerEventData myped;

    //private RectTransform rectTransform;

    void Awake () {
        RawImage image = GetComponent<RawImage>();
        colorTex = image.texture as Texture2D;
        rectTransform = GetComponent<RectTransform>();
        canvas = GetComponentInParent<Canvas>();
    }

    public void OnPointerClick(PointerEventData ped) {
        //Debug.Log("UiColorSelector.OnPointerClick");

        Vector2 localCursor;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, ped.position, ped.pressEventCamera, out localCursor))
            return;

        //Vector2 pixelUV = new Vector2(
        //    Mathf.InverseLerp(rectTransform.rect.min.x, rectTransform.rect.max.x, localCursor.x),
        //    Mathf.InverseLerp(rectTransform.rect.min.y, rectTransform.rect.max.y, localCursor.y));
        //Color chosenColor = colorTex.GetPixel((int)(pixelUV.x * colorTex.width), (int)(pixelUV.y * colorTex.height));

        selectedColor.color = GetColor(localCursor);
        if (colorSetCallback != null) {
            colorSetCallback.Invoke(selectedColor.color);
        }

        if (posSetCallback != null)
        {
            posSetCallback.Invoke(localCursor);
        }
    }

    private Color GetColor(Vector2 localCursor)
    {

        Vector2 pixelUV = new Vector2(
            Mathf.InverseLerp(rectTransform.rect.min.x, rectTransform.rect.max.x, localCursor.x),
            Mathf.InverseLerp(rectTransform.rect.min.y, rectTransform.rect.max.y, localCursor.y));
        Color chosenColor = colorTex.GetPixel((int)(pixelUV.x * colorTex.width), (int)(pixelUV.y * colorTex.height));

        return chosenColor;
    }
    
    private void Update()
    {
        if (!doUpdate)
            return;

        Vector2 localCursor;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, myped.position, myped.enterEventCamera, out localCursor))
            return;
        selectedColor.color = GetColor(localCursor);
        colorSetCallbackUpdateLoop.Invoke(selectedColor.color);
    }

    public void OnPointerDown(PointerEventData eventData) {
        //Debug.Log("UiColorSelector.OnPointerDown");
    }

    public void OnPointerUp(PointerEventData eventData) {
        //Debug.Log("UiColorSelector.OnPointerUp");
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        doUpdate = true;
        myped = eventData;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        doUpdate = false;
    }
}