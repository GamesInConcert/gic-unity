﻿using UnityEngine;
using UnityEngine.UI;

public class UiColorToPos : MonoBehaviour
{
    public RawImage parentImage;
    private Image myImage;

    private void Start()
    {
        myImage = GetComponent<Image>();
    }

    public void SetColorToXyPos(Color colorIn)
    {
        HSBColor hsbCol = HSBColor.FromColor(colorIn);
        myImage.rectTransform.localPosition = new Vector3((Mathf.Abs(1f-hsbCol.h) * 2 * parentImage.rectTransform.rect.x) - parentImage.rectTransform.rect.x, (Mathf.Abs(1f-hsbCol.b) * 2f * parentImage.rectTransform.rect.y) - parentImage.rectTransform.rect.y, 0);
    }

    public void SetImageToXyPos(Vector2 pos)
    {
        myImage.rectTransform.localPosition = new Vector3(pos.x, pos.y, 0f);
    }

}
