﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateUiLoopIndicator : MonoBehaviour
{
    public Color Normal = Color.white;
    public Color Arm = Color.yellow;
    public Color Record = Color.red;
    private Image myImage;
    private bool oneShot;

	// Use this for initialization
	void Start ()
	{
	    myImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {

	    if (oneShot)
	    {
	        myImage.color = Normal;
	        oneShot = false;
	    }


	    if (GiC_SoundingObjectsManager.Instance.currentEditableAtom == null || GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.CurrentPlaybackState != GiC_PlaybackStates.Play)
            return;

	    oneShot = true;
	    myImage.fillAmount = GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.NormalizedAtomPlaybackTime;
	    if (GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.SendToMaxPlaybackNow)
	    {
	        myImage.color = Record;
	    }
	    else if (GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.SendToMaxPlaybackWait)
	    {
	        myImage.color = Arm;
	    }
	    //else
	    //{
	    //    myImage.color = Normal;
	    //}
	}
}
