﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using WorldspaceController;

public class UiButtonPressHandler : InteractableObject {

    public NetworkInteractableObject thisInteractableObject;

    public void OnEnable() {
        base.onUseStartEvent.AddListener(OnPointerDown);
        base.onUseStopEvent.AddListener(OnPointerUp);
    }

    public void OnDisable() {
        base.onUseStartEvent.RemoveListener(OnPointerDown);
        base.onUseStopEvent.RemoveListener(OnPointerUp);
    }

    private void OnPointerDown(InputDeviceData arg0) {
        thisInteractableObject.DoAttachToController(arg0);
    }

    private void OnPointerUp(InputDeviceData arg0) {
        thisInteractableObject.DoDetachFromController(arg0);
    }
}
