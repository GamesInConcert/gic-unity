﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ConfigMessage : MessageBase {
    public string instrumentDeviceName;

    public override void Deserialize(NetworkReader reader) {
        instrumentDeviceName = reader.ReadString();
    }

    public override void Serialize(NetworkWriter writer) {
        writer.Write(instrumentDeviceName);
    }
}
