﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using WorldspaceController;

namespace WorldspaceController {
    public class NetworkPlayerController : NetworkBehaviour {
        #region Singleton
        private static NetworkPlayerController _localInstance;
        public static NetworkPlayerController LocalInstance {
            get {
                if (_localInstance == null) {
                    Debug.LogError("NetworkPlayerController.LocalInstance: not yet initialized, awake must have run");
                    return null;
                }
                return _localInstance;
            }
            set {
                _localInstance = value;
            }
        }

        public static NetworkPlayerController GetLocalNetworkPlayer() {
            for (int i = 0; i < ClientScene.localPlayers.Count; i++) {
                NetworkPlayerController netPlCt = ClientScene.localPlayers[i].gameObject.GetComponent<NetworkPlayerController>();
                if (netPlCt != null) {
                    return netPlCt;
                }
            }
            return null;
        }
        #endregion

        protected GameObject PlayerRootGameObject;
         
        protected List<NetworkInputDevice> hands = new List<NetworkInputDevice>();

        public GameObject ViveControllerPrefab;
        public GameObject MouseControllerPrefab;
        public GameObject LeapControllerPrefab;
        public GameObject AtomPrefab;

        void Awake() {
            //Debug.LogError("NetworkPlayerController.Awake");
            if (isLocalPlayer) { // do not use it here, not initialized!
            }

            PlayerRootGameObject = gameObject;
        }

        /// <summary>
        /// This happens after OnStartClient(), as it is triggered by an ownership message from the server.
        /// This is an appropriate place to activate components or functionality that should only be active for the local player, such as cameras and input.
        /// </summary>
        public override void OnStartLocalPlayer() {
            //Debug.Log("NetworkPlayerController.OnStartLocalPlayer " + enabled);
            base.OnStartLocalPlayer();

            if (isLocalPlayer) {
                _localInstance = this;
            }

            if (PlayerSettingsController.Instance.GetHeadTransform() == null) {
                Debug.LogError("NetworkPlayerController.OnStartLocalPlayer: headTransform not found!");
            }

            PlayerSettingsController.Instance.cameraInstance.transform.position = transform.position;
            // reparent, keep 0 pos and 0 rot
            transform.SetParent(PlayerSettingsController.Instance.GetHeadTransform(), false);
            //Debug.Log("NetworkPlayerController.OnStartLocalPlayer: reparenting to " + PlayerSettingsController.Instance.GetHeadTransform() +" " + isLocalPlayer+" @ position "+transform.position);

            //FindObjectOfType<OVRInputModule>().rayTransform = transform;

            if (isLocalPlayer) {
                Renderer myRenderer = GetComponentInChildren<Renderer>();
                if (myRenderer != null) {
                    myRenderer.material.color = Color.blue;
                }

                if (PlayerSettingsController.Instance.mouseControllerEnabled) {
                    ClientCreateMouseNetworkController(MouseController.Instance.inputDevice);
                }
                /*if (PlayerSettingsController.Instance.viveControllerEnabled) {
                    //CmdCreateViveNetworkControllers (netId);
                }*/
                /*if (PlayerSettingsController.Instance.leapControllerEnabled) {
                    CmdCreateLeapNetworkControllers (netId);
                }*/
            } else {
                Debug.LogWarning("NetworkPlayerController.OnStartLocalPlayer: this should not occur!, othervise i dont understand what functions get called on client/server");
                PlayerRootGameObject.name = "remotePlayerInstance";
            }

            if (isLocalPlayer) {
                // required to create callback for other listeners
                NetworkManagerModuleManager.Instance.OnStartLocalPlayerCompleted(this);
            }

            UpdatePlayerName();
        }

        public void Update() {
            if (isLocalPlayer) {
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
            }

            // keep body vertical
            Quaternion rot = Quaternion.Euler(-90, 0, transform.rotation.eulerAngles.y);
            //Debug.Log("apply rotation " + rot.eulerAngles + " " + transform.rotation.eulerAngles);
            transform.Find("body-head/body").transform.rotation = rot;

            // 
            foreach (NetworkInputDevice netInputDev in hands) {
                if (netInputDev != null) {
                    if (netInputDev.deviceHand == InputDeviceHand.Left) {
                        Transform lHand = transform.Find("body-head/body/l-hand-target");
                        Vector3 pos = netInputDev.transform.position + netInputDev.transform.forward * -0.1f;
                        if (float.IsNaN(pos.x) || float.IsNaN(pos.y) || float.IsNaN(pos.z)) {
                            Debug.LogWarning("error in hand pos calculation " + pos);
                        }
                        else {
                            lHand.position = pos;
                        }
                        lHand.rotation = netInputDev.transform.rotation;
                    }
                    if (netInputDev.deviceHand == InputDeviceHand.Right) {
                        Transform rHand = transform.Find("body-head/body/r-hand-target");
                        Vector3 pos = netInputDev.transform.position + netInputDev.transform.forward * -0.1f;
                        if (float.IsNaN(pos.x) || float.IsNaN(pos.y) || float.IsNaN(pos.z)) {
                            Debug.LogWarning("error in hand pos calculation " + pos);
                        }
                        else {
                            rHand.position = pos;
                        }
                        rHand.rotation = netInputDev.transform.rotation;
                    }

                    //if (netInputDev.deviceHand == InputDeviceHand.Undefined) {
                    //    Transform rHand = transform.Find("body-head/body/r-hand-target");
                    //    Vector3 pos = netInputDev.transform.position + netInputDev.transform.forward * -0.1f; //netInputDev.transform.forward * 0.4f + netInputDev.transform.up * -0.8f + netInputDev.transform.right * 0.2f;
                    //    if (float.IsNaN(pos.x) || float.IsNaN(pos.y) || float.IsNaN(pos.z)) {
                    //        Debug.LogWarning("error in hand pos calculation " + pos);
                    //    } else { 
                    //        rHand.position = pos;
                    //    }
                    //    //rHand.rotation = Quaternion.identity; // Quaternion.LookRotation(transform.forward); // netInputDev.transform.rotation;
                    //    rHand.localRotation = Quaternion.identity;
                    //}
                }

                //if (wsInputDev != null) { 
                //    if (wsInputDev.deviceHand == InputDeviceHand.Left) {
                //        Transform lHand = transform.Find("body-head/body/l-hand-target");
                //        lHand.position = wsInputDev.inputController.transform.position + wsInputDev.inputController.transform.forward * -0.1f; // + netInputDev.transform.up * -0.10f;
                //        lHand.rotation = wsInputDev.inputController.transform.rotation;
                //    }
                //    if (wsInputDev.deviceHand == InputDeviceHand.Right) {
                //        Transform rHand = transform.Find("body-head/body/r-hand-target");
                //        rHand.position = wsInputDev.inputController.transform.position + wsInputDev.inputController.transform.forward * -0.1f; // + netInputDev.transform.up * -0.10f;
                //        rHand.rotation = wsInputDev.inputController.transform.rotation;
                //    }
                //    if (wsInputDev.deviceHand == InputDeviceHand.Undefined) {
                //        Transform rHand = transform.Find("body-head/body/r-hand-target");
                //        rHand.position = wsInputDev.inputController.transform.position + wsInputDev.inputController.transform.forward * 0.6f;
                //        rHand.rotation = wsInputDev.inputController.transform.rotation;
                //    }
                //}
            }
        }

        #region player name sync
        public override void OnStartClient() {
            base.OnStartClient();
            ApplyPlayerName();
        }

        [SyncVar(hook = "PlayerName")]
        string playerName = "NotInitialized";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        [Command]
        public void CmdPlayerName(string value) {
            playerName = value;
            ApplyPlayerName();
        }
        /// <summary>
        /// callback der auf allen client ausgeführt wird, wenn der wert verändert wird
        /// </summary>
        [Client]
        public void PlayerName(string value) {
            playerName = value;
            // führe visuelle anpassungen aus (SeaboardDataHandler)
            ApplyPlayerName();
        }
        /// <summary>
        /// 
        /// </summary>
        public void UpdatePlayerName() {
            if (NetworkPlayerController.LocalInstance == this) {
                // TODO: Reto set player name, nothing else setup
                string title = "";
                switch (GiCPlaybackManager.Instance.ThisPlayerInterface) {
                    case GiC_Player.GJ:
                        title = "GJ";
                        break;
                    case GiC_Player.Leap:
                        title = "Trees";
                        break;
                    case GiC_Player.Paint:
                        title = "Paint";
                        break;
                    case GiC_Player.Seaboard:
                        title = "Keys";
                        break;
                    case GiC_Player.Undefined:
                        title = "Wrong";
                        break;
                    default:
                        title = "Undefined";
                        break;
                }
                //Debug.Log("NetworkPlayerController.UpdatePlayerName: " + title);
                CmdPlayerName(title);
            }
        }
        /// <summary>
        /// apply the value from syncvar
        /// </summary>
        public void ApplyPlayerName() {
            Transform t = transform.Find("NameLabel");
            if (t != null) {
                TextMesh tm = t.GetComponent<TextMesh>();
                if (tm != null) {
                    tm.text = playerName;
                    //Debug.Log("NetworkPlayerController.ApplyPlayerName: name applied " + playerName);
                }
            }

            Color c = Color.white;
            switch (playerName) {
                case "GJ":
                    c = Color.gray;
                    break;
                case "Trees":
                    c = Color.yellow;
                    break;
                case "Paint":
                    c = Color.green;
                    break;
                case "Keys":
                    c = Color.magenta;
                    break;
                default:
                    break;
            }

            Transform r = transform.Find("GodRay");
            if (r != null) {
                LineRenderer lr = r.GetComponent<LineRenderer>();
                if (lr != null) {
                    lr.startColor = c;
                    lr.endColor = c;
                    //Debug.Log("NetworkPlayerController.ApplyPlayerName: color applied " + c);
                }
            }

            //Debug.Log("NetworkPlayerController.ApplyPlayerName: label not found to apply name " + playerName);
        }
        #endregion

        #region Create Network Controllers (Mouse, Vive, Leap)
        [Client]
        public void ClientCreateMouseNetworkController(WorldspaceInputDevice inputDevice) {
            CmdCreateMouseNetworkControllers(netId, inputDevice.deviceId);
            //hands.Add(inputDevice);
        }
        [Command]
        void CmdCreateMouseNetworkControllers(NetworkInstanceId playerId, int inputDeviceId) {
            GameObject mouseControllerInstance = Instantiate(MouseControllerPrefab);
            NetworkMouseHand mouseHand = mouseControllerInstance.GetComponent<NetworkMouseHand>();

            mouseHand.ownerId = playerId;
            mouseHand.inputDeviceId = inputDeviceId;
            //mouseHand.deviceType = InputDeviceType.Mouse;
            //mouseHand.deviceHand = InputDeviceHand.Undefined;

            mouseControllerInstance.SetActive(true);

            NetworkServer.SpawnWithClientAuthority(mouseControllerInstance, connectionToClient);
        }

        [Client]
        public void ClientCreateViveNetworkController(WorldspaceInputDevice inputDevice) {
            CmdCreateViveNetworkController(netId, inputDevice.deviceId, inputDevice.deviceHand);
            //hands.Add(inputDevice);
        }
        [Command]
        void CmdCreateViveNetworkController(NetworkInstanceId playerId, int inputDeviceId, InputDeviceHand deviceHand) {
            GameObject viveInstance = Instantiate(ViveControllerPrefab);
            NetworkViveHand viveHand = viveInstance.GetComponent<NetworkViveHand>();
            viveHand.ownerId = playerId;
            viveHand.inputDeviceId = inputDeviceId;
            viveHand.gameObject.SetActive(true);

            //viveHand.deviceType = InputDeviceType.Vive;
            //viveHand.deviceHand = deviceHand;

            NetworkServer.SpawnWithClientAuthority(viveInstance, connectionToClient);
        }

        //[Command]
        //void CmdCreateViveNetworkControllers (NetworkInstanceId playerId) {
        //    GameObject leftHandInstance = Instantiate (viveControllerPrefab);
        //    GameObject rightHandInstance = Instantiate (viveControllerPrefab);

        //    NetworkViveHand leftVRHand = leftHandInstance.GetComponent<NetworkViveHand> ();
        //    NetworkViveHand rightVRHand = rightHandInstance.GetComponent<NetworkViveHand> ();

        //    //leftVRHand.side = InputDeviceHand.Left;
        //    //rightVRHand.side = InputDeviceHand.Right;
        //    leftVRHand.ownerId = playerId;
        //    rightVRHand.ownerId = playerId;

        //    leftVRHand.gameObject.SetActive (true);
        //    rightVRHand.gameObject.SetActive (true);

        //    NetworkServer.SpawnWithClientAuthority (leftHandInstance, base.connectionToClient);
        //    NetworkServer.SpawnWithClientAuthority (rightHandInstance, base.connectionToClient);
        //}

        [Client]
        public void ClientCreateLeapNetworkController(WorldspaceInputDevice inputDevice) {
            CmdCreateLeapNetworkController(netId, inputDevice.deviceId, inputDevice.deviceHand);
            //hands.Add(inputDevice);
        }
        [Command]
        void CmdCreateLeapNetworkController(NetworkInstanceId playerId, int inputDeviceId, InputDeviceHand deviceHand) {
            GameObject leapInstance = Instantiate(LeapControllerPrefab);
            NetworkLeapHand leapVrHand = leapInstance.GetComponent<NetworkLeapHand>();
            leapVrHand.ownerId = playerId;
            leapVrHand.inputDeviceId = inputDeviceId;
            leapVrHand.gameObject.SetActive(true);

            //leapVrHand.deviceType = InputDeviceType.Leap;
            //leapVrHand.deviceHand = deviceHand;

            NetworkServer.SpawnWithClientAuthority(leapInstance, connectionToClient);
        }

        //[Command]
        //void CmdCreateLeapNetworkControllers (NetworkInstanceId playerId) {
        //    GameObject leftHandInstance = Instantiate (leapControllerPrefab);
        //    GameObject rightHandInstance = Instantiate (leapControllerPrefab);

        //    NetworkLeapHand leftVRHand = leftHandInstance.GetComponent<NetworkLeapHand> ();
        //    NetworkLeapHand rightVRHand = rightHandInstance.GetComponent<NetworkLeapHand> ();

        //    leftVRHand.ownerId = playerId;
        //    rightVRHand.ownerId = playerId;
        //    leftVRHand.side = LeapHandSide.Left;
        //    rightVRHand.side = LeapHandSide.Right;

        //    leftVRHand.gameObject.SetActive (true);
        //    rightVRHand.gameObject.SetActive (true);

        //    NetworkServer.SpawnWithClientAuthority (leftHandInstance, base.connectionToClient);
        //    NetworkServer.SpawnWithClientAuthority (rightHandInstance, base.connectionToClient);
        //}
        #endregion


        public void AddedDevice (NetworkInputDevice addedDevice) {
            Debug.Log("NetworkPlayerController.AddedDevice: " + addedDevice);
            //NetworkInputDevice
            hands.Add(addedDevice);
        }

        #region Handle Atoms
        [Command]
        public void CmdCreateNetworkObject(Vector3 createPosition, Quaternion createRotation, string networkObjectPrefabName) {
            GameObject prefab = null;
            for (int i = 0; i < NetworkManagerModuleManager.Instance.spawnPrefabs.Count; i++) {
                if (NetworkManagerModuleManager.Instance.spawnPrefabs[i].name == networkObjectPrefabName) {
                    prefab = NetworkManagerModuleManager.Instance.spawnPrefabs[i];
                }
            }

            if (prefab != null) {
                GameObject networkObjectInstance = Instantiate(prefab, createPosition, createRotation);
                networkObjectInstance.SetActive(true);
                NetworkServer.Spawn(networkObjectInstance);
            }
        }

        /// <summary>
        /// Create an Atom
        /// </summary>
        /// <param name="createPosition">The Position where to create the Atom</param>
        /// <param name="createRotation">The Rotation to Initialize the object with</param>
        /// <param name="soundPresetId">The Sound Preset ID used to create the Atom</param>
        /// <param name="playerWhoCreatedTheAtom">the Player Who created the atom</param>
        [Command]
        public void CmdCreateAtom(Vector3 createPosition, Quaternion createRotation, int soundPresetId, GiC_Player playerWhoCreatedTheAtom) {
            Debug.Log("NetworkPlayerController.CmdCreateAtom: " + soundPresetId);

            GameObject atomInstance = Instantiate(AtomPrefab, createPosition, createRotation);
            NetworkInformationCore atomNetCore = atomInstance.GetComponent<NetworkInformationCore>();
            if (atomNetCore == null) {
                Destroy(atomInstance);
                Debug.LogError("NetworkPlayerController.CmdCreateAtom: failed to create valid Atom");
                return;
            }

            // TODO: Reto: active for now, but cannot work or does not make sense
            //GiC_ProceduralAtom procAtom = atomNetCore.GetComponent<GiC_ProceduralAtom>();
            //procAtom.Create(soundPresetId);
            //procAtom.Initialize();

            //NetworkInformationCore netCore = atomNetCore.GetComponent<NetworkInformationCore>();
            // hier kann irgendwas mit dem netCore gemacht werden, parameter setzen etc...
            atomNetCore.Initialize(soundPresetId);
            atomNetCore.gameObject.SetActive(true);

            //Debug.Log("NetworkPlayerController.CmdCreateAtom: " + createPosition + " " + atomInstance);
            if (atomInstance) {
                Debug.Log("NetworkPlayerController.Create AtomInstance " + atomInstance.name);
                NetworkServer.Spawn(atomInstance);
            } else {
                Debug.Log("Failed creating atom");
            }

            // To Max: New Atom has been created
            // GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/n/" + playerWhoCreatedTheAtom.ToString(), (int)atomNetCore.netId.Value, soundPresetId));
            GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/n", (int)atomNetCore.netId.Value, soundPresetId));

            atomNetCore.theAtom.Core.AtomCoreInteractable.SetPosition(createPosition);

            //And Where
            GicOscManager.Instance.SendAtomPosition((int)atomNetCore.netId.Value, createPosition);


            // Open Edit Room on local atom
            CmdGetAuthority(atomNetCore.netId);
        }

        /// <summary>
        /// Get Authority over an Atom
        /// </summary>
        /// <param name="objectId">Object identifier.</param>
        /// http://answers.unity3d.com/questions/1245341/unet-how-do-i-properly-handle-client-authority-wit.html ???
        [Command]
        public void CmdGetAuthority(NetworkInstanceId objectId) {
            GameObject iObject = NetworkServer.FindLocalObject(objectId);
            NetworkIdentity objectNetworkIdentity = iObject.GetComponent<NetworkIdentity>();
            //GameObject controllerObject = NetworkServer.FindLocalObject (controllerId);
            //NetworkIdentity playerNetworkIdentity = iObject.GetComponent<NetworkIdentity> ();
            //NetworkIdentity playerNetworkIdentity = connectionToClient;
            //objectNetworkIdentity.localPlayerAuthority = true;

            AssignAuthority(objectNetworkIdentity, connectionToClient);

            /*if (objectNetworkIdentity.localPlayerAuthority) {
                objectNetworkIdentity.AssignClientAuthority (connectionToClient);
                Debug.Log ("NetworkPlayerController.CmdGetAuthority: set client authority to " + connectionToClient);
            } else {
                Debug.LogWarning ("NetworkPlayerController.CmdGetAuthority: no localPlayerAuthority (server " + Network.isServer + " client " + Network.isClient + ")");
            }*/
        }
        [Server]
        private void AssignAuthority(NetworkIdentity objectNetworkIdentity, NetworkConnection theConnectionToClient) {
            NetworkConnection currentOwner = objectNetworkIdentity.clientAuthorityOwner;
            if (currentOwner == theConnectionToClient) {
            } else {
                if (currentOwner != null) {
                    Debug.Log("NetworkPlayerController.AssignAuthority: remove client authority from " + currentOwner);
                    objectNetworkIdentity.RemoveClientAuthority(currentOwner);
                }
                //Debug.Log("NetworkPlayerController.AssignAuthority: set client authority to " + _connectionToClient);
                objectNetworkIdentity.AssignClientAuthority(theConnectionToClient);
            }
        }

        /// <summary>
        /// Remove Authority over and Atom
        /// </summary>
        /// <param name="objectId">Object identifier.</param>
        [Command]
        public void CmdDropAuthority(NetworkInstanceId objectId) {
            //Debug.Log("NetworkPlayerController.CmdDropAuthority: remove client authority");
            GameObject iObject = NetworkServer.FindLocalObject(objectId);
            NetworkIdentity objectNetworkIdentity = iObject.GetComponent<NetworkIdentity>();
            //objectNetworkIdentity.localPlayerAuthority = false;
            //if (objectNetworkIdentity.localPlayerAuthority) {
            NetworkConnection currentOwner = objectNetworkIdentity.clientAuthorityOwner;
            if (currentOwner == connectionToClient) {
                objectNetworkIdentity.RemoveClientAuthority(connectionToClient);
            } else {
                string owner = (currentOwner != null) ? ("(" + currentOwner.address + " " + currentOwner.hostId + " " + currentOwner.connectionId + ")") : "null";
                Debug.LogWarning("NetworkPlayerController.CmdDropAuthority: tryed to remove authority from foreign object (currentOwner " + owner + " connectionToClient " + connectionToClient + ")");
            }

            /*    Debug.Log ("NetworkPlayerController.CmdGetAuthority: remove client authority from " + connectionToClient);
            } else {
                Debug.LogWarning ("NetworkPlayerController.CmdGrab: no localPlayerAuthority (server " + Network.isServer + " client " + Network.isClient + ")");
            }*/
        }

        [Command]
        public void CmdToggleAuthority(NetworkInstanceId objectId) {
            GameObject iObject = NetworkServer.FindLocalObject(objectId);
            NetworkIdentity objectNetworkIdentity = iObject.GetComponent<NetworkIdentity>();
            NetworkConnection currentOwner = objectNetworkIdentity.clientAuthorityOwner;
            if (currentOwner == connectionToClient) {
                objectNetworkIdentity.RemoveClientAuthority(connectionToClient);
            } else {
                AssignAuthority(objectNetworkIdentity, connectionToClient);
            }
        }


        //[Command]
        //public void CmdGrab (NetworkInstanceId objectId, NetworkInstanceId controllerId) {
        //    GameObject iObject = NetworkServer.FindLocalObject (objectId);
        //    NetworkIdentity objectNetworkIdentity = iObject.GetComponent<NetworkIdentity> ();
        //    //networkIdentity.localPlayerAuthority = true;
        //    //if (networkIdentity.localPlayerAuthority) {
        //    //objectNetworkIdentity.AssignClientAuthority (connectionToClient);

        //    //} else {
        //    //    Debug.LogWarning ("NetworkPlayerController.CmdGrab: no localPlayerAuthority (server " + Network.isServer + " client " + Network.isClient + ")");
        //    //}

        //    AssignAuthority (objectNetworkIdentity, connectionToClient);

        //    InteractableObject interactableObject = iObject.GetComponent<InteractableObject> ();
        //    if (isServer) //(!isClient)
        //        interactableObject.RpcAttachCall (controllerId); // client-side

        //    //NetworkInputDevice inputDevice = NetworkServer.FindLocalObject (controllerId).GetComponent<NetworkInputDevice> (); //.GetGrabPoint();
        //    interactableObject.AttachCall(controllerId); // inputDevice); // server-side
        //}

        //[Command] // called from the client, run on the server
        //public void CmdDrop (NetworkInstanceId objectId, Vector3 currentHolderVelocity) {
        //    GameObject iObject = NetworkServer.FindLocalObject (objectId);
        //    NetworkIdentity objectNetworkIdentity = iObject.GetComponent<NetworkIdentity> ();
        //    //if (objectNetworkIdentity.localPlayerAuthority) {
        //    //objectNetworkIdentity.RemoveClientAuthority (connectionToClient);
        //    //}

        //    // get object we drop
        //    InteractableObject interactableObject = iObject.GetComponent<InteractableObject> ();
        //    if (isServer) //(!isClient)
        //        interactableObject.RpcDetach (currentHolderVelocity); // client-side
        //    interactableObject.Detach (currentHolderVelocity); // server-side
        //}


        /*[Command]
    public void CmdUse (NetworkInstanceId objectId) {
        Debug.Log ("NetworkPlayerController.CmdUse: " + objectId);
        GameObject iObject = NetworkServer.FindLocalObject (objectId);
    //        NetworkIdentity networkIdentity = iObject.GetComponent<NetworkIdentity>();

        InteractableObject interactableObject = iObject.GetComponent<InteractableObject> ();
        if (isServer) //(!isClient)
            interactableObject.RpcUse (); // client-side
        interactableObject.Use (); // server-side
    }*/


        /*[Client]
    public bool PlayerHasAuthority (NetworkIdentity objectNetworkIdentity) {
        NetworkConnection objectOwner = objectNetworkIdentity.clientAuthorityOwner;
        return (connectionToClient == objectOwner);
    }*/


        #endregion
    }

}