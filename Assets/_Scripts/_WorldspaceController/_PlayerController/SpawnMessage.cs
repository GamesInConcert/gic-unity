﻿using System;
using UnityEngine.Networking;

public class SpawnMessage : MessageBase
{
	//public bool isVrPlayer;
    //public string vrDeviceFamily;
    //public string vrDeviceModel;
    public string vrDeviceName;
    //public string instrumentDeviceName;

    public override void Deserialize (NetworkReader reader)
	{
        //isVrPlayer = reader.ReadBoolean ();
        vrDeviceName = reader.ReadString();
        //instrumentDeviceName = reader.ReadString();
    }

    public override void Serialize (NetworkWriter writer)
	{
        //writer.Write (isVrPlayer);
        writer.Write(vrDeviceName);
        //writer.Write(instrumentDeviceName);
    }
}