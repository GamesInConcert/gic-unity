﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using System.Diagnostics;

public class BuildScript {
    static string windowsBuildPath = "/Users/rspoerri/Virtual Machines Shared Folder/Builds";

    static string GetProjectName() {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }

    static string[] GetScenePaths() {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++) {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }
    static string [] GetScenePathsFixed () {
        return new string[] { "Assets/Scenes/Test.unity", "Assets/Scenes/Finale.unity" };
    }

    static string GetProjectPath () {
        string codeBase = Assembly.GetExecutingAssembly().CodeBase;
        UriBuilder uri = new UriBuilder(codeBase);
        string path = Uri.UnescapeDataString(uri.Path);
        string resourcePath = Path.GetFullPath(new Uri(Path.Combine(Path.GetDirectoryName(path), "../../Builds")).AbsolutePath);
        return resourcePath;
    }
    static string GetProjectPathFiledialogue () {
        return EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
    }

    static string GetBuildFilename (string projectName, string projectPath, BuildTarget buildTarget) {
        string date = DateTime.Now.ToString("/yyyy-MM-dd");

        switch (buildTarget) {
        case BuildTarget.StandaloneWindows:
            goto case BuildTarget.StandaloneWindows64;
        case BuildTarget.StandaloneWindows64:
            return projectPath + date + "/" + projectName + ".exe";
            break;
        case BuildTarget.StandaloneOSXIntel:
            goto case BuildTarget.StandaloneOSX;
        case BuildTarget.StandaloneOSXIntel64:
            goto case BuildTarget.StandaloneOSX;
        case BuildTarget.StandaloneOSX:
            return projectPath + date + "/" + projectName;
            break;
        }

        return "";
    }

    static string GetExecutableFilename (string buildName, string projectName, BuildTarget buildTarget) {
        switch (buildTarget) {
        case BuildTarget.StandaloneWindows:
            goto case BuildTarget.StandaloneWindows64;
        case BuildTarget.StandaloneWindows64:
            return buildName;
            break;
        case BuildTarget.StandaloneOSXIntel:
            goto case BuildTarget.StandaloneOSX;
        case BuildTarget.StandaloneOSXIntel64:
            goto case BuildTarget.StandaloneOSX;
        case BuildTarget.StandaloneOSX:
            return buildName + ".app/Contents/MacOS/" + projectName;
            break;
        }

        return "";
    }

    static void StartExecutable (string executableName) {
        // Run the game (Process class from System.Diagnostics).
        var proc = new Process();
        UnityEngine.Debug.Log ("BuildScript.BuildGame: executeName " + executableName);
        proc.StartInfo.FileName = executableName;
        proc.Start();
    }

    static bool MkDir (string folderpath) {
        try {
            if (!Directory.Exists(folderpath)) {
                Directory.CreateDirectory(folderpath);
                return true;
            }
        }
        catch (IOException ex) {
            Console.WriteLine (ex.Message);
            return false;
        }
        return false;
    }

    [MenuItem("Build/AutoBuildOSX")]
    public static void BuildGame() {
        string[] levels = GetScenePaths();
        BuildTarget buildTarget = BuildTarget.StandaloneOSXIntel64; //EditorUserBuildSettings.activeBuildTarget;

        string projectPath = GetProjectPath();
        if (MkDir (projectPath)) { UnityEngine.Debug.Log ("BuildScript.BuildGame: Created Build Directory: " + projectPath); }
        string projectName = PlayerSettings.productName;
        string buildName = GetBuildFilename (projectName, projectPath, buildTarget);
        string executableName = GetExecutableFilename (buildName, projectName, buildTarget);

        // Build player.
        UnityEngine.Debug.Log ("BuildScript.BuildGame: buildName " + buildName);
        BuildOptions buildOptions = BuildOptions.AutoRunPlayer | BuildOptions.Development | BuildOptions.ShowBuiltPlayer;

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSX);
        BuildPipeline.BuildPlayer (levels, buildName, buildTarget, buildOptions);

        StartExecutable (executableName);
	}

    [MenuItem("Build/AutoBuildWindows")]
    public static void BuildGameWindows() {
        string[] levels = GetScenePaths();
        BuildTarget buildTarget = BuildTarget.StandaloneWindows64;

        string projectPath = windowsBuildPath; //GetProjectPath();
        if (MkDir (projectPath)) { UnityEngine.Debug.Log ("BuildScript.BuildGame: Created Build Directory: " + projectPath); }
        string projectName = PlayerSettings.productName;
        string buildName = GetBuildFilename (projectName, projectPath, buildTarget);
        string executableName = GetExecutableFilename (buildName, projectName, buildTarget);

        // Build player.
        UnityEngine.Debug.Log ("BuildScript.BuildGame: buildName " + buildName);
        BuildOptions buildOptions = BuildOptions.AutoRunPlayer | BuildOptions.Development | BuildOptions.ShowBuiltPlayer;

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows);
        BuildPipeline.BuildPlayer (levels, buildName, buildTarget, buildOptions);

        StartExecutable (executableName);
    }
}


/*
    static string GetProjectName() {
		string[] s = Application.dataPath.Split('/');
		return s[s.Length - 2];
	}
 
	static string[] GetScenePaths() {
		string[] scenes = new string[EditorBuildSettings.scenes.Length];
 
		for(int i = 0; i < scenes.Length; i++)
		{
			scenes[i] = EditorBuildSettings.scenes[i].path;
		}
 
		return scenes;
	}
 
	[MenuItem("File/AutoBuilder/Windows/32-bit")]
	static void PerformWinBuild () {
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/Win/" + GetProjectName() + ".exe",BuildTarget.StandaloneWindows,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/Windows/64-bit")]
	static void PerformWin64Build ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/Win64/" + GetProjectName() + ".exe",BuildTarget.StandaloneWindows64,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/Mac OSX/Universal")]
	static void PerformOSXUniversalBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSXUniversal);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/OSX-Universal/" + GetProjectName() + ".app",BuildTarget.StandaloneOSXUniversal,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/Mac OSX/Intel")]
	static void PerformOSXIntelBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSXIntel);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/OSX-Intel/" + GetProjectName() + ".app",BuildTarget.StandaloneOSXIntel,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/Mac OSX/PPC")]
	static void PerformOSXPPCBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSXPPC);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/OSX-PPC/" + GetProjectName() + ".app",BuildTarget.StandaloneOSXPPC,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/Mac OSX/Dashboard")]
	static void PerformOSXDashboardBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.DashboardWidget);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/OSX-Dashboard/" + GetProjectName() + ".wdgt",BuildTarget.DashboardWidget,BuildOptions.None);
	}
 
	[MenuItem("File/AutoBuilder/iOS")]
	static void PerformiOSBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iPhone);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/iOS",BuildTarget.iPhone,BuildOptions.None);
	}
	[MenuItem("File/AutoBuilder/Android")]
	static void PerformAndroidBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/Android",BuildTarget.Android,BuildOptions.None);
	}
	[MenuItem("File/AutoBuilder/Web/Standard")]
	static void PerformWebBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayer);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/Web",BuildTarget.WebPlayer,BuildOptions.None);
	}
	[MenuItem("File/AutoBuilder/Web/Streamed")]
	static void PerformWebStreamedBuild ()
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayerStreamed);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/Web-Streamed",BuildTarget.WebPlayerStreamed,BuildOptions.None);
	}
*/