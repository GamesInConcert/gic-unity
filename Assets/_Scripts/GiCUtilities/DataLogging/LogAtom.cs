﻿using UnityEngine;

public class LogAtom : MonoBehaviour
{

    private GiC_ProceduralAtom theAtom;
    private int atomID = 0;

    public void Initialize(GiC_ProceduralAtom atom, string WhoCreated)
    {
        theAtom = atom;
        atomID = theAtom.Core.AtomIdentifier;
    }

#if USING_SQUIGGLE
    private void FixedUpdate()
    {
        DebugGraph.Log("Atom_"+atomID+"_pos", this.transform.position);
    }
#endif

}
