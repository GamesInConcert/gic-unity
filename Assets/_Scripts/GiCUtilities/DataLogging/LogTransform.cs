﻿using UnityEngine;

public class LogTransform : MonoBehaviour
{
    public string LogItemName = "PlayerPos";
    private Vector2 prevPos;
    private Vector2 currentPos;
    private float playerDistance;
    private float TimeDeltaBetweenOscMessages = 10f;
    private float acumulatedTimeDelta;
#if USING_SQUIGGLE
    private void FixedUpdate()
    {
        if (!DebugGraph.LoggingEnabled) return;

        currentPos = new Vector2(transform.position.x, transform.position.z);
        DebugGraph.Log(LogItemName, new Vector2(transform.position.x, transform.position.z));
        playerDistance += Vector2.Distance(currentPos, prevPos);
        prevPos = currentPos;

        acumulatedTimeDelta += Time.unscaledDeltaTime;

        if (acumulatedTimeDelta > TimeDeltaBetweenOscMessages)
        {
            DebugGraph.Log("PlayerDistance", playerDistance);
            acumulatedTimeDelta = 0f;
        }
    }
#endif
}
