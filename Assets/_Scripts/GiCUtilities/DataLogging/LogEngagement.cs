﻿using System;
using UnityEngine;
using WorldspaceController;

[Serializable]
public enum GicEngagementState
{
    Idle,
    Ui,
    Moving,
    MovingObject,
    MakingMusic
}

public class LogEngagement : MonoBehaviour
{

#if USING_SQUIGGLE

    private Vector3 userPosLerp;
    private WorldspaceInputDevice myInputDevice;
    private bool Musicing = false;
    private bool UiClick = false;
    private bool initialized = false;

    private GicEngagementState currentState = GicEngagementState.Idle;

	// Use this for initialization
	void Start () {
	    if (GiCPlaybackManager.Instance != null && !initialized)
	    {
	        GiCPlaybackManager.Instance.OnChangeInterface += Initialize;
	        initialized = true;
	    }
    }

    private void Initialize()
    {
        foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance)
        {
            Debug.Log("InitCallbacks");
            InitControllerCallback(inputDevice.deviceId);
        }
    }

	// Update is called once per frame
	private void FixedUpdate () {
        if (DebugGraph.LoggingEnabled)
		    UpdateEngagementState();
	}

    private void UpdateEngagementState()
    {
        // Case: Idle
        // No Atom or Atom but not Moving, UI or Making Music
        // If moving user cannot make music or use UI
        // If Using UI user might move (but should not) and cannot make music
        // If user makes music Cannot use UI but move
        currentState = GicEngagementState.Idle;
        userPosLerp = Vector3.Lerp(userPosLerp, PlayerSettingsController.Instance.cameraInstance.transform.position, Time.unscaledDeltaTime * 5f);
        float movementFactor = Vector3.SqrMagnitude(userPosLerp - PlayerSettingsController.Instance.cameraInstance.transform.position);
        movementFactor = (float)Math.Round(movementFactor, 2);

        if (myInputDevice != null)
        {
            if (myInputDevice.deviceData.uiHit)
            {
                currentState = GicEngagementState.Ui;
            }

            if (movementFactor > 0f)
            {
                currentState = GicEngagementState.Moving;
            }

            if (myInputDevice.deviceData.grabbedInteractableObject != null)
            {
                if (myInputDevice.deviceData.grabbedInteractableObject.GetComponent<GiC_ProceduralAtom>() != null)
                {
                    currentState = GicEngagementState.MovingObject;
                }
                else
                {
                    currentState = GicEngagementState.MakingMusic;
                }
            }

            if (GiC_SoundingObjectsManager.Instance.currentEditableAtom != null && Musicing)
            {
                currentState = GicEngagementState.MakingMusic;
            }

        }
        DebugGraph.Log("PES", currentState);
    }

    private void InitControllerCallback(int controllerId)
    {
        WorldspaceInputDevice inputDevice = WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);

        // if this is a right vive controller
        if ((inputDevice.deviceHand == InputDeviceHand.Right) && (inputDevice.deviceType == InputDeviceType.Vive))
        {
            Debug.Log("LogEngagement.InitControllerCallback: add right vive " + controllerId);
            AddListenersR(inputDevice);
        }
        //if ((inputDevice.deviceHand == InputDeviceHand.Left) && (inputDevice.deviceType == InputDeviceType.Vive))
        //{
        //    Debug.Log("GiC_DrawStrokes.InitControllerCallback: add left vive " + controllerId);
        //    AddListenersL(inputDevice);
        //}
        if (inputDevice.deviceType == InputDeviceType.Mouse && PlayerSettingsController.Instance.mouseControllerEnabled)
        {
            Debug.Log("LogEngagement.InitControllerCallback: add mouse " + controllerId);
            AddListenersMouse(inputDevice);
        }
        if (inputDevice.deviceType == InputDeviceType.Camera && PlayerSettingsController.Instance.gazeControllerEnabled)
        {
            Debug.Log("LogEngagement.InitControllerCallback: add camera " + controllerId);
            AddListenersMouse(inputDevice);
        }
    }

    private void AddListenersR(WorldspaceInputDevice inputDevice)
    {
        if (!inputDevice.controller)
        {
            Debug.LogError("GiC_DRawStrokes.InitControllerCallback: controller undefined.. returning ");
            return;
        }

        inputDevice.inputController.viveTriggerTouched.AddListener(OnControllerTriggerTouched);
        inputDevice.inputController.viveTriggerTouchUp.AddListener(OnControllerTriggerTouchUp);

        myInputDevice = inputDevice;
    }

    private void OnControllerTriggerTouchUp(InputDeviceData arg0)
    {
        Musicing = false;
        UiClick = false;
    }

    private void OnControllerTriggerTouched(InputDeviceData deviceData)
    {
        if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Paint)
        {
            if (deviceData.hoveredInteractableObjects.Count == 0 && deviceData.grabbedInteractableObject == null)
            {
                if (deviceData.uiHit)
                {
                    UiClick = true;
                }
                else if (GiC_SoundingObjectsManager.Instance.currentEditableAtom != null)
                {
                    Musicing = true;
                }
            }
        }

        if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Leap)
        {
            if (deviceData.hoveredInteractableObjects.Count == 0 && deviceData.grabbedInteractableObject == null)
            {
                if (deviceData.uiHit)
                {
                    UiClick = true;
                }
                else if (GiC_SoundingObjectsManager.Instance.currentEditableAtom != null)
                {
                    Musicing = true;
                }
            }
        }
    }

    private void AddListenersMouse(WorldspaceInputDevice inputDevice)
    {
        if (!inputDevice.controller)
        {
            Debug.LogError("GiC_DRawStrokes.InitControllerCallback: controller undefined.. returning ");
            return;
        }

        inputDevice.inputController.mouseButton0Down.AddListener(OnControllerTriggerTouched);
        inputDevice.inputController.mouseButton0Up.AddListener(OnControllerTriggerTouchUp);

        myInputDevice = inputDevice;

    }
#endif
}
