﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class DataLoggingManager : MonoBehaviour
{

    public bool LoggingEnabled = false;
    public bool StackTraceEnabled = false;
    public string OutputFileName = "GiC_Log";
    public string OutputPath = "/";
    public bool FixedPlayTime = false;
    public float PlayTimeInMinutes = 15f;

    private bool dataHadBeenLogged = false;
    private float StartTime = 0f;
#if USING_SQUIGGLE
    // Use this for initialization
    private void Start ()
    {
        DebugGraph.ClearDeveloperConsole();
        DebugGraph.LoggingEnabled = LoggingEnabled;
	    DebugGraph.StackTraceEnabled = StackTraceEnabled;
	    dataHadBeenLogged = DebugGraph.LoggingEnabled;
	}
	
	// Update is called once per frame
    private void Update ()
	{
	    if (!FixedPlayTime) return;
	    if (Time.time > PlayTimeInMinutes * 60f)
	    {
	        QuitGame();
	    }
	}

    public void EnableLogging()
    {
        DebugGraph.LoggingEnabled = true;
        dataHadBeenLogged = DebugGraph.LoggingEnabled;
    }

    public void DisableLogging()
    {
        DebugGraph.LoggingEnabled = false;
    }

    public void SaveDataToFile(string path, string filename)
    {
        DebugGraph.Write("TotalPlayTimeInSeconds", Time.time);
        DebugGraph.SaveAllToFile(path+filename+"_"+ DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")+".csv");
    }

    private void OnApplicationQuit()
    {
        if (dataHadBeenLogged)
            SaveDataToFile(OutputPath, OutputFileName);
    }

    public void QuitGame()
    {
        // save any game data here
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
#endif
}
