﻿using UnityEngine;

public class LogEvent : MonoBehaviour
{
    public string LogItemName = "LogItem";
#if USING_SQUIGGLE
    
    public void LogTrigger()
    {
        if (DebugGraph.LoggingEnabled)
            DebugGraph.Write(LogItemName, 1);
    }

    public void LogTriggerInt(int input)
    {
        if (DebugGraph.LoggingEnabled)       
            DebugGraph.Write(LogItemName, input);
    }

    public void LogTriggerFloat(float input)
    {
        if (DebugGraph.LoggingEnabled)
            DebugGraph.Write(LogItemName, input);
    }

    public void LogTriggerBool(bool input)
    {
        if (DebugGraph.LoggingEnabled)
            DebugGraph.Write(LogItemName, input);
    }

    public void LogTriggerObj(object input)
    {
        if (DebugGraph.LoggingEnabled)
            DebugGraph.Write(LogItemName, "Click");
    }
#endif

}
