﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogAtomDistribution : MonoBehaviour
{

    public float TimeDeltaBetweenOscMessages = 1f;
    public float closeRange = 20f;
    public float MediumRange = 50f;

    private float acumulatedTimeDelta;
    private float editingTime = 0f;
    private float lastEditedAtom = 0;
    private float lastEditedAtomPreset = 0;
    private bool editing = false;

#if USING_SQUIGGLE
    
    private void Start()
    {
        GiC_SoundingObjectsManager.Instance.OnEditableAtomchange += OnGetAtom;
    }

    // Update is called once per frame
	void Update () {
        if (!DebugGraph.LoggingEnabled)
            return;

	    acumulatedTimeDelta += Time.unscaledDeltaTime;
	    bool sendNow = false;
	    if (acumulatedTimeDelta > TimeDeltaBetweenOscMessages)
	    {
	        sendNow = true;
	        acumulatedTimeDelta = 0f;
	    }

	    if (sendNow)
	    {
	        GetAndLogAtoms();
	    }

	    if (editing)
	        editingTime += Time.unscaledDeltaTime;
	}

    private void GetAndLogAtoms()
    {
        int veryClose = 0;
        int mediumRange = 0;
        int farAway = 0;

        foreach (var go in GiC_SoundingObjectsManager.Instance.GetAllAtomsAsGameObjects())
        {
            float distance = Vector3.Distance(Camera.main.transform.position, go.transform.position);
            if (distance <= closeRange)
            {
                veryClose++;
            } else if (distance > closeRange && distance <= mediumRange)
            {
                mediumRange++;
            } else if (distance > mediumRange)
            {
                farAway++;
            }
        }

        DebugGraph.Log("Atom_Count_Close", veryClose);
        DebugGraph.Log("Atom_Count_Medium", mediumRange);
        DebugGraph.Log("AtomsFar", farAway);

    }

    private void OnGetAtom(GiC_ProceduralAtom atom)
    {
        if (!DebugGraph.LoggingEnabled)
            return;

        if (atom != null)
        {
            lastEditedAtom = atom.Core.AtomIdentifier;
            lastEditedAtomPreset = atom.Core.InitialSoundPreset;
            editingTime = 0f;
            editing = true;
        }

        if (atom == null)
        {
            DebugGraph.Log("Atom_LastID", lastEditedAtom);
            DebugGraph.Log("Atom_LastPreset", lastEditedAtomPreset);
            DebugGraph.Log("Atom_EditingTime", editingTime);
        }
    }
#endif
}
