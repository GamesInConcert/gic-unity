﻿using System;
using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "GiC/ColorScheme", order = 1)]
[Serializable]
public class GicColorScheme : ScriptableObject
{
    public Color[] colors;
}
