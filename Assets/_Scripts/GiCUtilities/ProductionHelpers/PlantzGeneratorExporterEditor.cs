﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;


[CustomEditor(typeof(PlantGeneratorExporter))]
[CanEditMultipleObjects]

public class PlantzGeneratorExporterEditor : Editor
{

    public override void OnInspectorGUI()
    {
        PlantGeneratorExporter targetScript = (PlantGeneratorExporter) target;
        DrawDefaultInspector();

        if (GUILayout.Button("GeneratePlant"))
        {
            targetScript.GeneratePlant();
        }
    }
}
#endif