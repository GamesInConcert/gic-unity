﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ProceduralAtomPresetCreator))]
[CanEditMultipleObjects]
public class AtomGeometryCreatorEditorScript : Editor {

    public override void OnInspectorGUI()
    {
        ProceduralAtomPresetCreator targetScript = (ProceduralAtomPresetCreator)target;
        DrawDefaultInspector();
        //targetScript.speed = EditorGUILayout.Slider("Speed", targetPlayer.speed, 0, 100);

        if (GUILayout.Button("UpdateMaterial"))
        {
            targetScript.UpdateMaterial();
        }

        if (GUILayout.Button("CopyFromTarget"))
        {
            targetScript.CopyParamsFromTarget();
            
        }

        if (GUILayout.Button("CopyToTarget"))
        {
            targetScript.CopyParamsToTarget();
        }

        if (GUILayout.Button("GetTarget"))
        {
            targetScript.GetTarget();
        }

        if (GUILayout.Button("WriteDefineFile"))
        {
            targetScript.ExportGeometry();
        }

        if (GUI.changed)
        {
            targetScript.UpdateGeometry();
        }
        // Show default inspector property editor

    }

}
