﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProceduralToolkit;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
//using UnityEngine.VR.WSA.Persistence;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Material))]

public class ProceduralAtomPresetCreator : MonoBehaviour {

    [Range(0f, 1f)]
    public float Xparam = 0f;
    [Range(0f, 1f)]
    public float Yparam = 0f;

    public int targetSet = 0;

    public SoundPreset[] target;
    public GameObject targetGo;
    public Gic_ProceduralAtomParameters parametersA = new Gic_ProceduralAtomParameters();
    public Gic_ProceduralAtomParameters parametersB = new Gic_ProceduralAtomParameters();
    public Gic_ProceduralAtomParameters parametersC = new Gic_ProceduralAtomParameters();

    public string filename = "newFile";
    public bool OptimizeMesh = false;
    public bool ContinuousUpdate = false;



    private Gic_ProceduralAtomParameters parameters
    {
        get
        {
            return Gic_ProceduralAtomParameters.QLerp(parametersA, parametersB, parametersC, new Vector2(Xparam, Yparam));
        } 
    }
    
    // Use this for initialization
	void Start () {
        GetComponent<MeshRenderer>().material = parameters.myMaterial;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateMaterial()
    {
        GetComponent<MeshRenderer>().material = parameters.myMaterial;
    }

    public void UpdateGeometry()
    {
        if (!ContinuousUpdate)
            return;
        var mesh = GiC_ProceduralObjects.GenerativeSphericalObjectTriStrip(parameters).ToMesh();
        if (OptimizeMesh)
        {
#if UNITY_EDITOR
            MeshUtility.Optimize(mesh);
#endif
        }
        mesh.RecalculateTangents();
        mesh.RecalculateNormals();
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public void ExportGeometry()
    {
        ExportGeometryToList(GetComponent<MeshFilter>().sharedMesh, filename);
        //MeshDraft exp = MeshDraft.Tetrahedron(1f);
        //exp.Paint(Color.white);
        //ExportGeometryToList(exp.ToMesh(), filename);
    }

    public void ExportGeometryToList(Mesh mesh, string filename)
    {
        mesh.Paint(Color.white);
        string path = "Assets/Resources/"+ filename+".txt";
        if (OptimizeMesh)
        {
#if UNITY_EDITOR
            MeshUtility.Optimize(mesh);
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
#endif
        }
        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(filename);

        List<Vector3> verts = new List<Vector3>();    
        mesh.GetVertices(verts);
        Vector3[] vertices = verts.ToArray();

        List<Vector3> norms = new List<Vector3>();
        mesh.GetNormals(norms);
        Vector3[] normals = norms.ToArray();

        List<Vector4> tangs = new List<Vector4>();
        mesh.GetTangents(tangs);
        Vector4[] tangents = tangs.ToArray();

        List<Vector2> uv = new List<Vector2>();
        mesh.GetUVs(0, uv);
        Vector2[] uvs = uv.ToArray();

        List<Color> col = new List<Color>();
        mesh.GetColors(col);
        Color[] colors = col.ToArray();

        Debug.Log("indexcount: "+mesh.GetIndexCount(0));

        string line = "void " + filename + "(out vData o["+verts.Count+"]) {";
        writer.WriteLine(line);

        for (int i = 0; i < vertices.Length; i++)
        {
            writer.WriteLine("o[" + i + "].vertex = float4(" + vertices[i].x + ", " + vertices[i].y + ", " + vertices[i].z + ", 0.0);");
            writer.WriteLine("o[" + i + "].normal = float3(" + normals[i].x + ", " + normals[i].y + ", " + normals[i].z + ");");
            writer.WriteLine("o[" + i + "].tangent = float4(" + tangents[i].x + ", " + tangents[i].y + ", " + tangents[i].z + ", "+ +tangents[i].w + ");");
            writer.WriteLine("o[" + i + "].uv = float2(" + uvs[i].x + ", " + uvs[i].y + ");");
            writer.WriteLine("o[" + i + "].diffuse = float4(" + colors[i].r + ", " + colors[i].g + ", " + colors[i].b + ", " + +colors[i].a + ");");
            writer.WriteLine();

        }

        writer.WriteLine("}");
        writer.Close();

        //Re-import the file to update the reference in the editor
#if UNITY_EDITOR
        AssetDatabase.ImportAsset(path);
#endif
        TextAsset asset = (TextAsset)Resources.Load(filename);

        //Print the text from the file
        Debug.Log(asset.text);
    }

    //static void WriteString(string stringToWrite)
    //{
    //    string path = "Assets/Resources/test.txt";

    //    //Write some text to the test.txt file
    //    StreamWriter writer = new StreamWriter(path, true);
    //    writer.WriteLine("Test");
    //    writer.Close();

    //    //Re-import the file to update the reference in the editor
    //    AssetDatabase.ImportAsset(path);
    //    TextAsset asset = Resources.Load("test");

    //    //Print the text from the file
    //    Debug.Log(asset.text);
    //}

    public void GetTarget()
    {
        target = targetGo.GetComponent<GiC_BagPackController>().soundPresets;
    }

    public void CopyParamsFromTarget()
    {
        if ((target.Length -1) < targetSet) return;
        parametersA.SetAllParameters(target[targetSet].visualParameters);
        parametersB.SetAllParameters(target[targetSet].visualParametersModX);
        parametersC.SetAllParameters(target[targetSet].visualParametersModY);
    }

    public void CopyParamsToTarget()
    {
        target[targetSet].visualParameters.SetAllParameters(parametersA);
        target[targetSet].visualParametersModX.SetAllParameters(parametersB);
        target[targetSet].visualParametersModY.SetAllParameters(parametersC);
    }

}
