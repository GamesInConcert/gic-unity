﻿using System.Collections;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

public class PlantGeneratorExporter : MonoBehaviour
{
    
    public Mesh PlantPartInner;
    public Color ColorInner;
    public float DistanceToMidInner = 0.2f;
    public float ProbabilityInner = 0.9f;
    public float FalloffInner = 0.4f;
    public float ScaleMultInner = 1f;
    public Mesh PlantPartOuter;
    public Color ColorMid;
    public float DistanceToMidMid = 0.7f;
    public float probabilityMid = 0.6f;
    public float FalloffMid = 0.3f;
    public Vector3 ScaleMultMid = Vector3.one;
    public Mesh PlantPartFlower;
    public Color Colorflower;
    public float DistanceToMidFlower = 0.8f;
    public float ProbabilityFlower = 0.4f;
    public float FalloffFlower = 0.2f;
    public float ScaleMultFlower = 1f;
    public float FlowerelEvation = 0.5f;
    public float FlowerElevationVariationMult = 1f;

    public float PlantMinRadius = 0.1f;
    public float PlantMaxRadius = 1f;
    public float PlantMinSize = 0.1f;
    public float PlantMaxSize = 2f;
    public float PlantSizeToHeightRatio = 2f;
    public int PlantParts = 9;
    public GameObject PlantHotspot;
    public Material PlantMaterial;

    public float PerlinCoordMult = 1f;
    public float PerlinCoordOffset = 0f;
    public float PerlinAngleMult = 1f;
    public float OffsetMult = 1f;

    private GameObject plant;

	// Use this for initialization
	void Start ()
	{
	    plant = CreateParentGo();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.O))
	    {
	        DestroyImmediate(plant);
	        plant = CreateFlowerPatchFromPrefab(PlantParts, PlantMinRadius, PlantMaxRadius, PlantMinSize, PlantMaxSize, PlantSizeToHeightRatio);
	    }
	}

    public void GeneratePlant()
    {
        DestroyImmediate(plant);
        plant = CreateFlowerPatchFromPrefab(PlantParts, PlantMinRadius, PlantMaxRadius, PlantMinSize, PlantMaxSize, PlantSizeToHeightRatio);
    }

    private GameObject CreateParentGo()
    {
        var returnObject = new GameObject();
        returnObject.name = "Flower";
        returnObject.transform.parent = PlantHotspot.transform;
        returnObject.transform.localPosition = Vector3.zero;
        returnObject.transform.localScale = Vector3.one;
        returnObject.transform.localRotation = Quaternion.identity;
        return returnObject;
    }

    // Nach aussen lehnen der objekte .. pflanzenteile evtl doch nicht so gut. nutze primitives?
    private GameObject CreateFlowerPatchFromPrefab(int instances, float minRdius, float maxRadius, float minSize, float maxSize, float sizeToHeightRatio, float goldenAngle = 137.5f)
    {
        GameObject returnObject = new GameObject("FlowerPatch");
        returnObject.transform.position = PlantHotspot.transform.position;
        //returnObject.transform.rotation = Quaternion.identity;
        //returnObject.transform.localScale = Vector3.one;
        //var myParams = new Gic_ProceduralAtomParameters(parameters);
        float angle = 0;
        goldenAngle = (goldenAngle / 360f) * (Mathf.PI * 2f);
        float radiusSteps = (maxRadius - minRdius) / instances;
        float sizeSteps = (maxSize - minSize) / instances;
        float colorSteps = 1f / (float) instances;
        Vector3 pos = Vector3.zero;
        Vector3 scale = Vector3.one*maxSize;
        Quaternion rot = Quaternion.identity;


        for (int i = 0; i < instances; i++)
        {
            float angleMinRandom = angle - (Mathf.PerlinNoise((i * PerlinCoordMult) + PerlinCoordOffset,(i * PerlinCoordMult) + PerlinCoordOffset) * PerlinAngleMult);       
            scale = new Vector3(minSize + (sizeSteps * i), (minSize * sizeToHeightRatio) + (sizeSteps * sizeToHeightRatio) * i, minSize + (sizeSteps * i));
            pos = new Vector3(Mathf.Sin(angleMinRandom), 0f, Mathf.Cos(angleMinRandom)) * (i * radiusSteps);
            rot = Quaternion.LookRotation(Vector3.zero - pos);

            bool innerPlantSet = CalculateProbability((float)i / instances, DistanceToMidInner, ProbabilityInner, FalloffInner);
            bool midPlantSet = CalculateProbability((float) i / instances, DistanceToMidMid, probabilityMid, FalloffMid);

            if (!midPlantSet)
            {
                scale = new Vector3(maxSize - (sizeSteps * i), (maxSize * sizeToHeightRatio) - (sizeSteps * sizeToHeightRatio) * i, maxSize - (sizeSteps * i));
                CreateVariant(PlantPartInner, pos, rot, scale*ScaleMultInner, Color.Lerp(ColorInner, ColorMid, i*colorSteps), returnObject.transform);
            }

            if (midPlantSet)
            {
                scale = new Vector3(minSize + (sizeSteps * i), (minSize * sizeToHeightRatio) + (sizeSteps * sizeToHeightRatio) * i, minSize + (sizeSteps * i));
                CreateVariant(PlantPartOuter, pos, rot, Vector3.Scale(scale, ScaleMultMid), ColorMid, returnObject.transform);
            }

            if (midPlantSet && RandomE.Chance(ProbabilityFlower))
            {
                scale = new Vector3(minSize + (sizeSteps * i), (minSize) + (sizeSteps) * i, minSize + (sizeSteps * i));
                pos += new Vector3(0f, FlowerelEvation + Mathf.PerlinNoise((i * PerlinCoordMult) + PerlinCoordOffset, (i * PerlinCoordMult) + PerlinCoordOffset) * FlowerElevationVariationMult, 0f);
                CreateVariant(PlantPartFlower, pos, rot, scale * ScaleMultFlower, Colorflower, returnObject.transform);
            }

            angle += goldenAngle;
        }
        return returnObject;
    }

    private GameObject CreateVariant(Mesh mesh, Vector3 pos, Quaternion rot, Vector3 scale, Color color, Transform parent)
    {
        Debug.Log(color);
        var newObj = new GameObject("PlantPart");
        newObj.transform.parent = parent;
        newObj.AddComponent<MeshFilter>().mesh = mesh;
        Mesh toColorize = newObj.GetComponent<MeshFilter>().mesh;
        newObj.GetComponent<MeshFilter>().mesh = ColorizeMesh(toColorize, color);
        newObj.AddComponent<MeshRenderer>().sharedMaterial = PlantMaterial;

        newObj.transform.localScale = scale;
        newObj.transform.localRotation = rot;
        newObj.transform.localPosition = pos + new Vector3(0f, scale.y*0.5f, 0f);

        return newObj;
    }

    bool CalculateProbability(float distanceTocenter, float hotspotDistancetoCenter, float hotspotProbability, float falloff)
    {
        falloff = 1f / (falloff*0.5f);
        float currentDistancetoHotspot = 1 - Mathf.Abs(hotspotDistancetoCenter - distanceTocenter);
        currentDistancetoHotspot *= falloff;
        currentDistancetoHotspot -= (falloff -1f);

        float probability = hotspotProbability * currentDistancetoHotspot;       

        return RandomE.Chance(probability);

    }

    Mesh ColorizeMesh(Mesh mesh, Color newColor)
    {
        Color[] cols = new Color[mesh.vertexCount];
        for (int i = 0; i < mesh.colors.Length; i++)
        {
            cols[i] = newColor;
        }
        mesh.colors = cols;
        return mesh;
    }

}
