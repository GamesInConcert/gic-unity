﻿using UnityEngine;
using System.Collections;
using ProceduralToolkit;
using System.Collections.Generic;

public static class PlantGenerator
{

    public static MeshDraft Plant(Color colorA, Color colorB, bool hasFlower = false, int stemSegments = 20, float height = 5f, float roundness = 0.3f, float stemRadius = 1f)
    {
        //int hasFlowerRandom = 0;
        int flowerpoint = 0;
        int internalRoundness = (int)Mathf.Ceil(20 * roundness);
        float internalRoundnessNormalized = 1 / internalRoundness;
        float internalRoundnessRadians = (Mathf.PI + Mathf.PI) / internalRoundness;
        float internalSegmentsRadians = (Mathf.PI + Mathf.PI) / stemSegments;

        var plant = new MeshDraft { name = "plant" };

        //        plant.vertices.Add(Vector3.zero);
        //        plant.normals.Add(Vector3.zero.normalized);

        List<Vector3> stem = new List<Vector3>();

        for (int i = 0; i < stemSegments; i++)
        {
            float random = i > 0f ? Random.Range(0.1f, 0.4f) - 0.2f : 0f;
            float z = (height / stemSegments) * i;
            float x = Mathf.Sin(i * internalSegmentsRadians)+ random;
            float y = Mathf.Cos(i * internalSegmentsRadians)* random;
            stem.Add(new Vector3(x, z, y));
        }

        if (hasFlower)
        {
            int hasFlowerRandom = (int)Mathf.Floor(Random.value*200);
            hasFlower = hasFlowerRandom >= 100;
            flowerpoint = Mathf.Clamp(Mathf.CeilToInt((hasFlowerRandom-100)/stem.Count), (stem.Count+2)%stem.Count, stem.Count-1);
        }


        float colorLerpFraction = (float)1f/stem.Count;
        for (int i = 0; i < stem.Count; i += 1)
        {
            int j = i >= stem.Count - 1 ? stem.Count - 1 : i + 1;

            float radiusI = stemRadius - (stemRadius / stemSegments) * i;
            radiusI = Mathf.Sin(radiusI);
            
            float radiusJ = stemRadius - (stemRadius / stemSegments) * j;
            radiusJ = Mathf.Sin(radiusJ);

            List<Vector3> lB = PlantGenerator.PointsOnCircle3(radiusI, internalRoundness, stem[i]);
            List<Vector3> uB = PlantGenerator.PointsOnCircle3(radiusJ, internalRoundness, stem[j]);
            var bandPiece = MeshDraft.Band(lB, uB);
            var actualColor = Color.Lerp(colorA, colorB, colorLerpFraction*i);
            bandPiece.Paint(actualColor);
            plant.Add(bandPiece);

            if (hasFlower && i == flowerpoint)
            {
                var pcoll = PlantCollection(Mathf.CeilToInt(Random.value*10), actualColor, colorB, stemSegments/2,
                    height*0.3f, roundness, stemRadius*0.5f);
                pcoll.Scale(0.7f);
                pcoll.Move(stem[i]);
                //pcoll.Rotate(Quaternion.LookRotation(stem[j] - stem[i]));
                plant.Add(pcoll);
            }

        }
        return plant;
    }

    public static MeshDraft PlantCollection(int amount, Color colorA, Color colorB, int stemSegments = 20, float height = 5f, float roundness = 0.3f, float stemRadius = 1f)
    {
        float goldenAngle = 137.5077f;
        float goldenAngleRad = 2.39996f;
        MeshDraft draft = new MeshDraft { name = "plantCollection" };
        var stem = Plant(colorA, colorB, false, stemSegments, height, roundness, stemRadius);
        for (int i = 0; i < amount; i++)
        {           
            stem.Rotate(Quaternion.Euler(0, goldenAngle *i, 0));
            float randomScale = Random.Range(0.4f, 0.6f);
            stem.Scale(new Vector3(randomScale, 1f, randomScale));
            draft.Add(stem);
        }
        return draft;
    }

    /// <summary>
    /// Returns point on circle in the XZ plane
    /// </summary>
    /// <param name="radius">Circle radius</param>
    /// <param name="angle">Angle in radians</param>
    private static Vector3 PointOnCircle3(float radius, float angle)
    {
        return new Vector3(radius * Mathf.Sin(angle), 0, radius * Mathf.Cos(angle));
    }

    /// <summary>
    /// Returns list of points on circle in the XZ plane
    /// </summary>
    /// <param name="radius">Circle radius</param>
    /// <param name="segments">Number of circle segments</param>
    private static List<Vector3> PointsOnCircle3(float radius, int segments, Vector3 origin)
    {
        float segmentAngle = Mathf.PI * 2 / segments;
        float currentAngle = 0f;
        var ring = new List<Vector3>(segments);
        for (var i = 0; i < segments; i++)
        {

            ring.Add(PointOnCircle3(radius, currentAngle) + origin);
            currentAngle -= segmentAngle;
        }
        return ring;
    }

}