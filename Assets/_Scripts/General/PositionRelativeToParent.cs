﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRelativeToParent : MonoBehaviour {
    public Vector3 offset = new Vector3(0, -0.3f, 0.3f);
    public float baseRotation = 0;
    public float rotationOffset = 0;
    public float mouseScrollRotationSpeed = 40;

    public float pressTimer;
    void Update () {
        //rotationOffset += Input.GetAxis("Mouse ScrollWheel") * mouseScrollRotationSpeed;
        if (Input.GetMouseButton(0) && Input.GetMouseButton(1)) {
            pressTimer += Time.deltaTime;
        } else {
            pressTimer = 0;
        }

        if (pressTimer > 0.5f) {
            pressTimer = 0;
            CenterOnView();
        }
    }

    void CenterOnView () {
        //Vector3 myForwardVector = Vector3.ProjectOnPlane(transform.forward, Vector3.up);
        //Vector3 parentForwardVector = Vector3.ProjectOnPlane(transform.parent.forward, Vector3.up);
        float parentRotation = Quaternion.LookRotation(transform.parent.forward).eulerAngles.y;
        float myRotation = Quaternion.LookRotation(transform.forward).eulerAngles.y;
        //rotationOffset = (Quaternion.LookRotation(myForwardVector) * Quaternion.Inverse(Quaternion.LookRotation(parentForwardVector))).eulerAngles.y;
        rotationOffset = parentRotation; // parentRotation - myRotation;
        //Debug.Log("myForwardVector " + myForwardVector + " parentForwardVector " + parentForwardVector + " rotOffset " + rotationOffset);
        Debug.Log("myRotation " + myRotation + " parentRotation " + parentRotation + " rotOffset " + rotationOffset);
    }

    void LateUpdate () {
        baseRotation = transform.parent.parent.rotation.eulerAngles.y;
        transform.position = transform.parent.position + Quaternion.Euler(0, baseRotation + rotationOffset, 0) * offset;
        transform.rotation = transform.parent.parent.rotation * Quaternion.Euler(0, rotationOffset, 0);
        transform.localScale = Vector3.one;
	}
}
