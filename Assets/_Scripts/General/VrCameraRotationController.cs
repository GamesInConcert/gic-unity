﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrCameraRotationController : MonoBehaviour {

    [Header("alternative rotation keys")]
    public KeyCode leftKey = KeyCode.Q;
    public KeyCode rightKey = KeyCode.E;
    private float mouseScrollSum = 0;
    [Header("mouse scroll speed (bigger is slower interval)")]
    public float mouseScrollInterval = 3;
    [Header("amount to rotate")]
    public float rotation = 15;

    // Update is called once per frame
    void Update () {
		if (Input.GetKeyDown(leftKey)) {
            TurnCamera(-rotation);
        }
        if (Input.GetKeyDown(rightKey)) {
            TurnCamera(rotation);
        }
        mouseScrollSum += Input.mouseScrollDelta.y;
        if (mouseScrollSum > mouseScrollInterval) {
            mouseScrollSum -= mouseScrollInterval;
            TurnCamera(-rotation);
        } else if (mouseScrollSum < -mouseScrollInterval) {
            mouseScrollSum += mouseScrollInterval;
            TurnCamera(rotation);
        }
	}

    public void TurnCamera (float angle) {
        //PlayerSettingsController.Instance.cameraInstance.GetComponent<SteamVR_Camera>()
        PlayerSettingsController.Instance.cameraInstance.transform.localRotation *= Quaternion.Euler(0, angle, 0);
    }
}
