﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodRayLineRenderer : MonoBehaviour {

    LineRenderer lr;

	// Use this for initialization
	void Start () {
        lr = GetComponent<LineRenderer>();

        lr.SetVertexCount(5);
	}
	
	// Update is called once per frame
	void Update () {
        lr.SetPosition(0, transform.position + Vector3.up * 0.7f + Vector3.forward * 0.01f);
        lr.SetPosition(1, transform.position + Vector3.up * 5f + Vector3.forward * 0.01f);
        lr.SetPosition(2, transform.position + Vector3.up * 10f + Vector3.forward * 0.01f);
        lr.SetPosition(3, transform.position + Vector3.up * 25f + Vector3.forward * 0.01f);
        lr.SetPosition(4, transform.position + Vector3.up * 50f + Vector3.forward * 0.01f);
	}
}
