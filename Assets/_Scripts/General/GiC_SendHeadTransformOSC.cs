﻿using UnityEngine;
using UnityEngine.Networking;
using WorldspaceController;

public class GiC_SendHeadTransformOSC : MonoBehaviour {

    public float TimeDeltaBetweenMessages = 0.15f;
    private float accumulatedTimeDelta = 0f;
    //private List<float> oscMessage = new List<float>();
    private OscMessage oscPos;
    private OscMessage oscRot;

    private Vector3 prevPos = Vector3.zero;

    private Transform playerTransform;

	// Use this for initialization
	void Start () {
        // TODO: Simon, so wuerde ich den player initialisieren, siehe OnStartLocalPlayer
        NetworkManagerModuleManager.Instance.onStartLocalPlayerEvent.AddListener(OnStartLocalPlayer);
    }

    private void OnStartLocalPlayer(NetworkBehaviour arg0) {
        //string playerName = GiC_PlaybackManager.Instance.thisPlayerInterface.ToString();
        oscPos = new OscMessage("/g/pt/pos", 0, 0f, 0f, 0f);
        oscRot = new OscMessage("/g/pt/rot", 0, 0f);
        playerTransform = NetworkPlayerController.LocalInstance.transform;
        Debug.Log(name + "OnStartLocalPlayer" + " " + playerTransform.position);
    }

    // Update is called once per frame
    void Update () {
        // wait for player initialization (OnStartLocalPlayer)
        if (oscPos == null || playerTransform == null)
            return;

		if (accumulatedTimeDelta > TimeDeltaBetweenMessages)
        {
            if (playerTransform != null && ApproximatelyEqual(prevPos, playerTransform.position, 0.05f))
            {
                //oscMessage.Clear();
                oscPos.args[0] = (int)GiCPlaybackManager.Instance.ThisPlayerInterface;
                oscPos.args[1] = playerTransform.position.x;
                oscPos.args[2] = playerTransform.position.z;
                oscPos.args[3] = playerTransform.position.y;
                GicOscManager.Instance.AddDataToMetaInformationBundle(oscPos, GicOscManager.Instance.UseMulticastBroadcast);
                prevPos = playerTransform.position;
            }
            oscRot.args[0] = (int)GiCPlaybackManager.Instance.ThisPlayerInterface;
            oscRot.args[1] = playerTransform.rotation.eulerAngles.y;
            GicOscManager.Instance.AddDataToMetaInformationBundle(oscRot, GicOscManager.Instance.UseMulticastBroadcast);
            accumulatedTimeDelta = 0f;
            //Debug.Log("HEAd Transform");
        }
        accumulatedTimeDelta += Time.unscaledDeltaTime;
	}

    private bool ApproximatelyEqual(Vector3 from, Vector3 to, float maxDifference, bool precisionMode = false)
    {
        Vector3 delta = to - from;
        if (precisionMode)
        {
            return delta.sqrMagnitude > (maxDifference * maxDifference);
        }

        if (Mathf.Abs(delta.x) > maxDifference || Mathf.Abs(delta.y) > maxDifference || Mathf.Abs(delta.z) > maxDifference)
        {
            return true;
        }
        return false;
    }
}
