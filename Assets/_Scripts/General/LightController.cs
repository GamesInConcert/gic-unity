﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{

    public bool EnablePlayerLights = false;
    private Light[] sceneLights;

    private void OnEnable()
    {
        SetPlayerLights();
    }

    public void SetPlayerLights()
    {
        TogglePlayerLights(EnablePlayerLights);
    }

    public void TogglePlayerLights(bool enabled = false)
    {
        sceneLights = FindObjectsOfType<Light>();

        foreach (var light in sceneLights)
        {
            if (light.gameObject.CompareTag("PlayerLight"))
                light.enabled = EnablePlayerLights;
        }
    }

}
