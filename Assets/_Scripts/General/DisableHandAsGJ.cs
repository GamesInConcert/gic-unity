﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableHandAsGJ : MonoBehaviour
{
    public Renderer myRenderer;

	// Use this for initialization
	void Start () {
	    if (myRenderer != null && GiCPlaybackManager.Instance != null)
	    {
	        myRenderer.enabled = GiCPlaybackManager.Instance.ThisPlayerInterface != GiC_Player.GJ;
	    }	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
