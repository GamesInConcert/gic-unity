﻿using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneToLoad
{
    DarkTerrain = 0,
    BrightTerrain = 1,
}

public class SceneLoader : MonoBehaviour
{
    public GiC_Player WhoIsPlaying = GiC_Player.Paint;
    public SceneToLoad WhichSceneToLoad = SceneToLoad.DarkTerrain;

    public bool EnableMouseMode = false;

    public PlayerSettingsController CurrentPlayerSettingsController;

    //public bool remoteMaxMspForGj = true;
    //public bool remoteMaxMspForOthers = false;
    //public string remoteMaxMspIp =  "10.128.41.205";

    public GiCPlaybackManager PlaybackManager;
    //public Scene[] AvailableScenes;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SwitchSettings(WhoIsPlaying, EnableMouseMode);
        SceneManager.LoadScene(WhichSceneToLoad.ToString(), LoadSceneMode.Additive);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        SceneManager.SetActiveScene(scene);
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void SwitchSettings(GiC_Player playerRole, bool mouseMode = false)
    {
        if (mouseMode)
        {
            CurrentPlayerSettingsController.vrEnabled = false;
            CurrentPlayerSettingsController.mouseControllerEnabled = true;
            CurrentPlayerSettingsController.gazeControllerEnabled = false;
            CurrentPlayerSettingsController.leapControllerEnabled = false;
            CurrentPlayerSettingsController.viveControllerEnabled = false;
            PlaybackManager.ThisPlayerInterface = playerRole;
            return;
        }


        switch (playerRole)
        {
            case GiC_Player.Seaboard:
                CurrentPlayerSettingsController.vrEnabled = true;
                CurrentPlayerSettingsController.mouseControllerEnabled = false;
                CurrentPlayerSettingsController.gazeControllerEnabled = true;
                CurrentPlayerSettingsController.leapControllerEnabled = false;
                CurrentPlayerSettingsController.viveControllerEnabled = false;
                //GicOscManager.Instance.SetOscOut(remoteMaxMspForOthers, remoteMaxMspIp);
                PlaybackManager.ThisPlayerInterface = playerRole;
                break;
            case GiC_Player.Leap:
                CurrentPlayerSettingsController.vrEnabled = true;
                CurrentPlayerSettingsController.mouseControllerEnabled = false;
                CurrentPlayerSettingsController.gazeControllerEnabled = false;
                CurrentPlayerSettingsController.leapControllerEnabled = false;
                CurrentPlayerSettingsController.viveControllerEnabled = true;
                //GicOscManager.Instance.SetOscOut(remoteMaxMspForOthers, remoteMaxMspIp);
                PlaybackManager.ThisPlayerInterface = playerRole;
                break;
            case GiC_Player.Paint:
                CurrentPlayerSettingsController.vrEnabled = true;
                CurrentPlayerSettingsController.mouseControllerEnabled = false;
                CurrentPlayerSettingsController.gazeControllerEnabled = false;
                CurrentPlayerSettingsController.leapControllerEnabled = false;
                CurrentPlayerSettingsController.viveControllerEnabled = true;
                //GicOscManager.Instance.SetOscOut(remoteMaxMspForOthers, remoteMaxMspIp);
                PlaybackManager.ThisPlayerInterface = playerRole;
                break;
            case GiC_Player.GJ:
                CurrentPlayerSettingsController.vrEnabled = false;
                CurrentPlayerSettingsController.mouseControllerEnabled = true;
                CurrentPlayerSettingsController.gazeControllerEnabled = false;
                CurrentPlayerSettingsController.leapControllerEnabled = false;
                CurrentPlayerSettingsController.viveControllerEnabled = false;
                //GicOscManager.Instance.SetOscOut(remoteMaxMspForGj, remoteMaxMspIp);
                PlaybackManager.ThisPlayerInterface = playerRole;
                break;
            case GiC_Player.Undefined:
                CurrentPlayerSettingsController.vrEnabled = false;
                CurrentPlayerSettingsController.mouseControllerEnabled = true;
                CurrentPlayerSettingsController.gazeControllerEnabled = false;
                CurrentPlayerSettingsController.leapControllerEnabled = false;
                CurrentPlayerSettingsController.viveControllerEnabled = false;
                //GicOscManager.Instance.SetOscOut(remoteMaxMspForOthers, remoteMaxMspIp);
                PlaybackManager.ThisPlayerInterface = playerRole;
                break;
        }
    }

}
