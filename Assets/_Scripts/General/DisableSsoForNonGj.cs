﻿using UnityEngine;

public class DisableSsoForNonGj : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerSettingsController.Instance != null)
            this.gameObject.SetActive(PlayerSettingsController.Instance.mouseControllerEnabled);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
