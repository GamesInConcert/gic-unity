﻿using System;
using System.Collections.Generic;
using UnityEngine;
using WorldspaceController;

public class GiCPlaybackManager : MonoBehaviour
{

    public static float GlobalTime { get; private set; }

    public GiC_Player ThisPlayerInterface;
    private GiC_Player previousPlayerInterface = GiC_Player.Undefined;
    public GameObject PlayheadPrefab;
    public GameObject UiHost;

    private GameObject leapUi;
    private GameObject paintUi;
    private GameObject seaboardUi;
    private GameObject gjUi;
    private List<GameObject> EditUis = new List<GameObject>();

    private GameObject viveCamView;

    public int Bpm { get; private set; }
    public int NumBeats { get; private set; }

    public Action OnChangeInterface;

    public static GiCPlaybackManager Instance { get; private set; }

    #region Unity Internals

    private void OnEnable()
    {
        Bpm = 120;
        NumBeats = 8;
        if (Instance != null && Instance != this)
        {
            Debug.LogError("Only One Instance of GIC_PlaybackManager allowed.. Deleting");
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        //previousPlayerInterface = ThisPlayerInterface;

        // run for existing and comping input devices
        WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.AddListener(InitControllerCallback);
        foreach (WorldspaceInputDevice inputDevice in WorldspaceInputDeviceManager.Instance)
        {
            InitControllerCallback(inputDevice.deviceId);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            TogglePlayback();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ThisPlayerInterface = GiC_Player.Seaboard;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ThisPlayerInterface = GiC_Player.Leap;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ThisPlayerInterface = GiC_Player.Paint;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ThisPlayerInterface = GiC_Player.GJ;
        }
        ChangeUi();
    }

    #endregion

    #region Callbacks

    private void InitControllerCallback(int controllerId)
    {
        if (!LoadInterfaces())
            return;

        WorldspaceInputDevice inputDevice = WorldspaceInputDeviceManager.Instance.GetInputDeviceFromId(controllerId);
        Debug.Log("GiC_PlaybackManager.InitControllerCallback: devices " + inputDevice);
        // if this is a right vive controller
        if (PlayerSettingsController.Instance.viveControllerEnabled)
        {
            if ((inputDevice.deviceHand == InputDeviceHand.Left) && (inputDevice.deviceType == InputDeviceType.Vive))
            {
                WorldspaceInputDeviceManager.Instance.newInputDeviceInitializedEvent.RemoveListener(InitControllerCallback);
                UiHost.transform.SetParent(inputDevice.controller);
                UiHost.transform.localPosition = Vector3.zero;
                UiHost.transform.localRotation = Quaternion.identity;
                UiHost.transform.localScale = Vector3.one;
            }
        }
        //other controllers (mouse, leap and gaze) attach to camera
        else
        {
            if ((inputDevice.deviceHand == InputDeviceHand.Undefined) && ((inputDevice.deviceType == InputDeviceType.Camera) || (inputDevice.deviceType == InputDeviceType.Mouse)))
            {
                if (UiHost.GetComponent<PositionRelativeToParent>() == null)
                {
                    Debug.Log("GiC_PlaybackManager.InitControllerCallback: attach to player");
                    UiHost.transform.SetParent(inputDevice.controller);
                    PositionRelativeToParent positioning = UiHost.AddComponent<PositionRelativeToParent>();
                    VrCameraRotationController rotationControl = PlayerSettingsController.Instance.cameraInstance.AddComponent<VrCameraRotationController>();
                    if (PlayerSettingsController.Instance.mouseControllerEnabled)
                    {
                        // leave default
                    }
                    else if (PlayerSettingsController.Instance.leapControllerEnabled)
                    {
                        positioning.offset = new Vector3(0, -0.5f, 0.3f);
                    }
                }
            }
        }
        Debug.Log("Init Controller Callback");
        ChangeUi(true);
        EditUiSetActive(false);
    }


    #endregion

    #region UI Handling

    private void ChangeUi(bool forceUpdate = false)
    {
        if ((ThisPlayerInterface != previousPlayerInterface) && (seaboardUi != null) && (paintUi != null) && (leapUi != null) && (gjUi != null)) {
            previousPlayerInterface = ThisPlayerInterface;

            if (viveCamView == null) {
                viveCamView = GameObject.Find("ViveCam");
            }

            if (seaboardUi != null)
                seaboardUi.SetActive(ThisPlayerInterface == GiC_Player.Seaboard);

            if (paintUi != null)
                paintUi.SetActive(ThisPlayerInterface == GiC_Player.Paint);

            if (leapUi != null)
                leapUi.SetActive(ThisPlayerInterface == GiC_Player.Leap);

            if (gjUi != null)
                gjUi.SetActive(ThisPlayerInterface == GiC_Player.GJ);

            if (viveCamView != null) {
                viveCamView.SetActive(ThisPlayerInterface == GiC_Player.Seaboard);
            }

            if (OnChangeInterface != null) {
                Debug.Log("change interface");
                OnChangeInterface();
            }

            forceUpdate = true;
#if USING_SQUIGGLE
            DebugGraph.Write("PlayerInterface", ThisPlayerInterface.ToString());
#endif
        }

        if (forceUpdate) {
            try {
                if (((NetworkSettingsController.Instance.networkState == NetworkSettingsController.NETWORK_STATE.CLIENT) ||
                    (NetworkSettingsController.Instance.networkState == NetworkSettingsController.NETWORK_STATE.HOST)) &&
                    (NetworkPlayerController.LocalInstance != null)) {
                    NetworkPlayerController.LocalInstance.UpdatePlayerName();
                }
            }
            catch (Exception e) {
                Debug.Log("GiCPlaybackManager.ChangeUI: error updating player name " + e.ToString());
            }
        }
    }

    private bool LoadInterfaces()
    {
        if (UiHost == null)
            return false;
        seaboardUi = UiHost.transform.Find("SeaboardUI").gameObject;
        EditUis.Add(seaboardUi.transform.Find("Canvas/EditUi").gameObject);

        paintUi = UiHost.transform.Find("PaintUI").gameObject;
        EditUis.Add(paintUi.transform.Find("Canvas/EditUi").gameObject);

        leapUi = UiHost.transform.Find("LeapUI").gameObject;
        EditUis.Add(leapUi.transform.Find("Canvas/EditUi").gameObject);

        gjUi = UiHost.transform.Find("GjUI").gameObject;

        Debug.Log("Found: "+EditUis.Count+" Edit Uis");

        return (seaboardUi != null && paintUi != null && leapUi != null && gjUi != null);
    }

    public void EditUiSetActive(bool active)
    {
        foreach (var ui in EditUis)
        {
            ui.SetActive(active);
        }
    }

#endregion

#region Atom Playback Related

    public void TogglePlayback(bool playback)
    {
        GiC_SoundingObjectsManager.Instance.ToggleCurrentAtomPlayback(playback);
    }

    public void TogglePlayback()
    {
        GiC_SoundingObjectsManager.Instance.ToggleCurrentAtomPlayback();
    }

    public void ResyncAtomPlayback()
    {
        GiC_SoundingObjectsManager.Instance.ResyncAtomPlayback();
    }

    public void SetBpm(int newBpm)
    {
        Bpm = newBpm == Bpm ? Bpm : newBpm;
        if (GiC_SoundingObjectsManager.Instance.currentEditableAtom == null) return;
        GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.SetBpm(Bpm);
    }

    public void SetNumBeats(int newNumBeats)
    {
        NumBeats = newNumBeats == NumBeats ? NumBeats : newNumBeats;
        if (GiC_SoundingObjectsManager.Instance.currentEditableAtom == null) return;
        GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.SetNumBeats(NumBeats);
    }

#endregion

}
