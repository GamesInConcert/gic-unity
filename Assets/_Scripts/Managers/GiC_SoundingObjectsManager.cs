﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WorldspaceController;

public class GiC_SoundingObjectsManager : MonoBehaviour {
    private List<AtomInformationCore> existingAtoms = new List<AtomInformationCore>();
    private Dictionary<int, AtomInformationCore> atomsByIdentifier = new Dictionary<int, AtomInformationCore>();
    //private Dictionary<int, AtomInformationCore> deletedAtomsByIdentifier = new Dictionary<int, AtomInformationCore>();
    private int currentHighestIdentifier = 1;
    //private List<int> deletedObjects = new List<int>();
    //private GameObject visibleAtomsParentObject;
    //private GameObject deletedAtomsParentObject;

    // inspextor hack .. revert
    //private GiC_ProceduralAtom currentlyEditedAtom { get; set; }
    private GiC_ProceduralAtom currentlyEditedAtom;
    public GiC_ProceduralAtom currentEditableAtom { get { return currentlyEditedAtom; } }

    public static GiC_SoundingObjectsManager Instance;

    //private bool gettingAuthorityforAtom = false;

    public Action<GiC_ProceduralAtom> OnEditableAtomchange;

    public bool HasJustCreatedAnAtom;

    #region Unity Internals

    void OnDestroy()
    {
        Destroy(this);
    }

    // Use this for initialization
    void Start ()
	{
	    //visibleAtomsParentObject = this.gameObject.transform.Find("VisibleAtoms").gameObject;
	    if (Instance != null && Instance != this)
	    {
	        Debug.LogError("Only one Object of type: " + name + "is allowed per scene. Deleting Object");
	        OnDestroy();
	    }
	    else
	    {
	        Instance = this;
	    }
	
	}

    #endregion

    #region Atom Functions

    public void DeleteAtom()
    {
        // Stop Playback
        // Remove any control and links to playback manager etc...
        // Move Atom to "deadObjects" parent
        // "Kill Atom"
    }

    // TODO: Netzwerkkompabilität
    public void RegisterNewAtom(AtomInformationCore atomCore)
    {       
        // Register it with this Manager
        existingAtoms.Add(atomCore);
        AtomInformationCore test;
        atomsByIdentifier.TryGetValue(atomCore.AtomIdentifier, out test);
        int identifier = test == null ? atomCore.AtomIdentifier : currentHighestIdentifier;
        Debug.Log("New Atom Identifier =" + identifier);
        atomsByIdentifier.Add(identifier, atomCore);

        //Debug.Log(atomCore.AtomIdentifier + " " + currentHighestIdentifier);
        // Increase Identifier by 1
        currentHighestIdentifier++;
    }

    public AtomInformationCore GetAtomByIdentifier(int atomIdentifier)
    {
        if (atomsByIdentifier.ContainsKey(atomIdentifier)) { 
            return atomsByIdentifier[atomIdentifier];
        } else {
            Debug.LogError("GiC_SoundingObjectManager.GetAtomByIdentifier. Error Atom with id"+ atomIdentifier+ " not Found.");
            return null;
        }
    }

    public List<AtomInformationCore> GetAllAtoms()
    {
        return existingAtoms;
    }

    public void ResendAllAtoms ()
    {
        for (int i= existingAtoms.Count-1; i>=0; i--)
        {
            if (existingAtoms[i].NetworkedData != null)
            {
                existingAtoms[i].NetworkedData.CmdResendData();
            }
            else
            {
                Debug.Log("GiC_SoundingObjectsManager.ResendAttAtoms: removing atom "+i+" its null");
                existingAtoms.RemoveAt(i);
            }
        }
    }

    public void SanitizeAtoms()
    {
        for (int i = existingAtoms.Count - 1; i >= 0; i--)
        {
            if (existingAtoms[i].NetworkedData == null)
            {
                Debug.Log("GiC_SoundingObjectsManager.ResendAttAtoms: removing atom " + i + " its null");
                existingAtoms.RemoveAt(i);
            }
        }

        int[] allKEys = atomsByIdentifier.Keys.ToArray();

        for (int i = allKEys.Length - 1; i >= 0; i--)
        {
            if (atomsByIdentifier[allKEys[i]] == null)
            {
                Debug.Log("GiC_SoundingObjectsManager.ResendAttAtoms: removing atom "+ allKEys[i]+ " at atomsByIdentifier its null");
                atomsByIdentifier.Remove(allKEys[i]);
            }
        }

    }

    //private bool switching = false;


    public void ManageAuthority(GiC_ProceduralAtom atom)
    {
        StartCoroutine(ManageAuthorityCoroutine(atom));
    }

    public IEnumerator ManageAuthorityCoroutine(GiC_ProceduralAtom atom)
    {
        // Do nothing
        if (currentEditableAtom == atom && atom != null)
        {
            Debug.LogWarning("CurrentEditableAtom == Atom. This is weird and should not happen.");
            yield break;
        }

        // Drop authority
        if (atom == null && currentEditableAtom != null)
        {
            Debug.Log("SetAtomToEdit: Drop Authority: " + currentEditableAtom.Core.AtomIdentifier);
            currentEditableAtom.networkCore.DropAuthority();
            yield break;
        }

        // Get Authority
        if (currentEditableAtom == null && atom != null)
        {
            Debug.Log("SetAtomToEdit: Get Authority: " + atom.Core.AtomIdentifier);
            atom.networkCore.GetAuthority();
            yield break;
        }

        //Switch Authority
        if (currentEditableAtom != atom && currentEditableAtom != null && atom != null)
        {
            //switching = true;
            Debug.Log("SetAtomToEdit: Switch Authority from: " + currentEditableAtom.Core.AtomIdentifier + " to: " + atom.Core.AtomIdentifier);
            currentEditableAtom.networkCore.DropAuthority();
            
            while (currentEditableAtom != null && NetworkInteractableObject.HasLocalPlayerAuthority(currentEditableAtom.gameObject))
            {
                yield return null;
            }

            atom.networkCore.GetAuthority();

            //switching = false;
        }
    }

    public void RegisterAtom(GiC_ProceduralAtom atom, bool requestEditing)
    {
        currentlyEditedAtom = atom;
        if (GicOscManager.Instance.OscOut.isOpen && requestEditing)
        {
            GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/we", currentlyEditedAtom.Core.AtomIdentifier, (int)GiCPlaybackManager.Instance.ThisPlayerInterface, 1));
        }
        if (OnEditableAtomchange != null)
        {
            OnEditableAtomchange(currentEditableAtom);
        }
    }

    public void UnregisterAtom(GiC_ProceduralAtom atom)
    {
        if (currentlyEditedAtom == null) return;
        Debug.Log("SetAtomToEdit: Stop Editing: " + currentlyEditedAtom.Core.AtomIdentifier);
        if (GicOscManager.Instance.OscOut.isOpen && GicEditRoomManager.Instance.editRoomActive)
        {
            GicOscManager.Instance.AddDataToMetaInformationBundle(new OscMessage("/g/sm/we", currentlyEditedAtom.Core.AtomIdentifier, (int)GiCPlaybackManager.Instance.ThisPlayerInterface, 0));
        }

        SetAtomRecord();
        currentlyEditedAtom = null;
        if (OnEditableAtomchange != null)
        {
            OnEditableAtomchange(currentEditableAtom);
        }

    }

    private void SetAtomRecord()
    {
        if (currentEditableAtom == null)
        {
            Debug.LogWarning("No atom detected: Nothing to record");
            return;
        }
        if (GiCPlaybackManager.Instance.ThisPlayerInterface == GiC_Player.Paint && GicEditRoomManager.Instance.editRoomActive && !currentEditableAtom.Core.SendToMaxPlaybackNow)
        {
            currentEditableAtom.Core.ArmRecordPlayback();
        }
    }

    public List<GameObject> GetAllAtomsAsGameObjects()
    {
        bool doSanitize = false;
        List<GameObject> goArray = new List<GameObject>();
        foreach (var atom in existingAtoms)
        {
            if (atom != null) { 
                goArray.Add(atom.gameObject);
            } else
            {
                Debug.LogError("atom is null");
                doSanitize = true;
            }
        }
        if (doSanitize)
        {
            SanitizeAtoms();
        }
        return goArray;
    }

    #endregion

    #region Playback Related

    public void ToggleCurrentAtomPlayback(bool playback)
    {
        if (currentEditableAtom == null) return;

        if (currentEditableAtom.Core.CurrentPlaybackState == GiC_PlaybackStates.Play && !playback)
        {
            currentEditableAtom.StopPlayback();
        }
        else
        {
            currentEditableAtom.StartPlayback();
        }
    }

    public void ToggleCurrentAtomPlayback()
    {
        if (currentEditableAtom == null) return;

        if (currentEditableAtom.Core.CurrentPlaybackState == GiC_PlaybackStates.Play)
        {
            currentEditableAtom.StopPlayback();
        }
        else
        {
            currentEditableAtom.StartPlayback();
        }
    }

    public void ResyncAtomPlayback()
    {
        currentEditableAtom.Resync();
    }

    #endregion
}
