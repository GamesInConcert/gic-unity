﻿using UnityEngine;
using System.Collections;
using VR= UnityEngine.VR;

public class VRManager : MonoBehaviour {

    public bool vrEnabled = false;
    public GameObject SteamVRprefab = null;
    public GameObject SteamCameraPrefab = null;


    void OnEnable()
    {

        vrEnabled = UnityEngine.XR.XRSettings.enabled;
        if (vrEnabled || SteamVR.instance.compositor != null)
        {
            Camera[] cams = GameObject.FindObjectsOfType<Camera>();
            foreach (var cam in cams)
            {
                Destroy(cam.gameObject);
            }
            Debug.Log(true);
            if (GameObject.Find("[SteamVR]") != true)
            {
                Instantiate(SteamVRprefab);
            }

            if (GameObject.Find("[CameraRig]") !=true)
            {
                Instantiate(SteamCameraPrefab);
            }
        } else
        {
            if (GameObject.FindObjectsOfType<Camera>().Length < 1){
                GameObject Camera = new GameObject("Camera");
                Camera.AddComponent<Camera>();
                Camera.tag = "MainCamera";
            }
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
