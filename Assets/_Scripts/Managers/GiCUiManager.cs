﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class GiCUiManager : MonoBehaviour
{

    private static GiCUiManager _instance;
    public bool DisableMenu = false;
    public GiCMenuItemSettings[] atomMenuItems;
    public GameObject atomMenuItemPrefab;
    public GameObject ButtonPrefab;
    public GameObject TogglePrefab;
    public GameObject SliderPrefab;

    public static GiCUiManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject("UiManager").AddComponent<GiCUiManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (GameObject.FindObjectOfType<GiCUiManager>() != this)
        {
            Destroy(this);
            return;
        }
        _instance = this;
    }

    // Use this for initialization
        void Start () {
	}

    // Update is called once per frame
    void Update () {
		
	}
}
