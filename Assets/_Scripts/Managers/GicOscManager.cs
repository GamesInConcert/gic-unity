﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

//using UnityEditor;

public class GicOscManager : MonoBehaviour
{

    // General Variables
    public bool SendGicPointsAsBundle { get; set; }
    public bool UseFixedUpdate = true;
    public bool UseMulticastReceiveForSeaboard;
    public bool UseMulticastBroadcast;

    //[HideInInspector]
    public bool remoteMaxMsp = false;
    //[HideInInspector]
    public string remoteMaxMspIp;

    public bool IpReceived;

    private IPAddress clientIp;

    // OSC Related
    public OscIn OscIn;
    public OscIn SeaboardBroadcastReceive;
    public OscIn LoopSyncReceive;
    public OscOut OscOut;
    public OscOut OscOutLocal;

    // Musical Information (Strokes, Leap)
    public OscBundle OscBundleMusical  { get; private set; }
    public OscBundle OscBundleMusicalLocal  { get; private set; }

    // Atom Meta Information (Position, Storage,) 
    public OscBundle OscBundleAtomMetaInformation { get; private set; }
    public OscBundle OscBundleAtomMetaInformationLocal { get; private set; }
    //private OscMessage gicPoint;
    private OscMessage atomPosition;

    private static GicOscManager _instance;

    #region Singleton Constructors

    private GicOscManager()
    {
    }

    public static GicOscManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject("OscManager").AddComponent<GicOscManager>();
            }

            return _instance;
        }
    }
    #endregion

    private void Awake()
    {
        if (FindObjectOfType<GicOscManager>() != this)
        {
            Destroy(this);
            return;
        }
        _instance = this;
    }


    private void OnEnable()
    {
        if (GiCPlaybackManager.Instance != null)
        {
            GiCPlaybackManager.Instance.OnChangeInterface += TriggerRenegotiateIp;
        }
    }

    private void OnDisable()
    {
        if (GiCPlaybackManager.Instance != null && GiCPlaybackManager.Instance.OnChangeInterface != null)
        {
            // ReSharper disable once DelegateSubtraction
            GiCPlaybackManager.Instance.OnChangeInterface -= TriggerRenegotiateIp;
        }
        ShutdownOsc();
    }

    // Use this for initialization
    private void Start ()
    {
        StartCoroutine(InitOsc());
    }

    private void TriggerRenegotiateIp()
    {
        StopAllCoroutines();
        StartCoroutine(RenegotiateIp());
    }

    private IEnumerator RenegotiateIp()
    {
        ShutdownOsc();
        while (OscIn.isOpen || OscOut.isOpen)
        {
            yield return null;
        }
        yield return StartCoroutine(InitOsc());
    }

    private void ShutdownOsc()
    {
        IpReceived = false;
        OscOut.Close();
        OscIn.Close();

        OscIn.Unmap(SeaboardPitVel);
        OscIn.Unmap(SeaboardAfterTouch);
        OscIn.Unmap(SeaboardGlide);
        OscIn.Unmap(SeaboardSlide);
        OscIn.Unmap(SeaboardXyPad);
        OscIn.Unmap(SeaboardRec);
    }


    // Update is called once per frame
    private void Update () {
        if (!UseFixedUpdate && IpReceived)
        {
            SendOscBundles();
        }
	}

    private void FixedUpdate()
    {
        if (UseFixedUpdate && IpReceived)
        {
            SendOscBundles();
        }
    }

    private void SendOscBundles()
    {
        //Debug.Log("SendOscBundles");
        if (OscOut.isOpen && IpReceived && (OscBundleMusical != null) && (OscBundleAtomMetaInformation != null))
        {
            if (OscBundleMusical.packets.Count > 0)
            {
                OscOut.Send(OscBundleMusical);
                OscBundleMusical.Clear();
            }
            if (OscBundleAtomMetaInformation.packets.Count > 0)
            {
                OscOut.Send(OscBundleAtomMetaInformation);
                OscBundleAtomMetaInformation.Clear();
            }

            if (!UseMulticastBroadcast) return;
            if (OscOut.isOpen && IpReceived && (OscBundleMusical != null) && (OscBundleAtomMetaInformation != null))
            {
                if (OscBundleMusicalLocal.packets.Count > 0)
                {
                    OscOutLocal.Send(OscBundleMusicalLocal);
                    OscBundleMusicalLocal.Clear();
                }
                if (OscBundleAtomMetaInformationLocal.packets.Count > 0)
                {
                    OscOutLocal.Send(OscBundleAtomMetaInformationLocal);
                    OscBundleAtomMetaInformationLocal.Clear();
                }
            }
        }
        else
        {
            Debug.LogWarning("GicOscManager.SendOscBundles: not sending oscBundleMusical!");
        }
    }


    private void OpenInitialOsc(bool local)
    {
        if (!local)
        {
            OscOut.Open(6006, "255.255.255.255");
            OscIn.Open(GetOscIncomingPortBasedOnPlayerRole());
            OscIn.Map("/gic/special/ipFromMax", ReceiveIpFromClient);
        }
        else
        {
            OscOut.Open(6006, "127.0.0.1");
            OscIn.Open(6007);
            OscIn.Map("/gic/special/ipFromMax", ReceiveIpFromClient);
        }
    }



    //public void SetOscOut (bool remote, string ip)
    //{
    //    remoteMaxMsp = remote;
    //    remoteMaxMspIp = ip;

    //    OscOut.Open(6066, "255.255.255.255");
    //    if (remote)
    //    {
    //        OscOutLocal.Open(6006, ip);
    //        //OscOut.Open(6006, ip);
    //    }
    //    else
    //    {
    //        OscOutLocal.Open(6006, "127.0.0.1");
    //    }

    //}

    private IEnumerator InitOsc()
    {
        // enable Multicast Broadcast to send my IP and receive the others IP to handndle changing IPs
        if (!(OscIn = GetComponent<OscIn>())) OscIn = gameObject.AddComponent<OscIn>();
        if (!(OscOut = GetComponent<OscOut>())) OscOut = gameObject.AddComponent<OscOut>();       

        OpenInitialOsc(UseMulticastBroadcast && !remoteMaxMsp);

        // Wait until the Ip is negotiated before continuing
        yield return StartCoroutine(SendReceiveIpAdress());

        IpReceived = true;

        if (IpReceived)
        {
            Debug.Log("[=======> OSC <=======] IpNegotiation success, received ip "+ clientIp);

            if (UseMulticastReceiveForSeaboard)
            {
                //Debug.Log("[=======> OSC <=======] Using Multicast receive for Seaboard on: 224.1.1.1:19666");
                //SeaboardBroadcastReceive.Open(19666, "224.1.1.1");
                SeaboardBroadcastReceive.Open(6066);
            }

            if (UseMulticastBroadcast)
            {
                OscOutLocal = gameObject.AddComponent<OscOut>();
                OscOutLocal.bundleMessagesOnEndOfFrame = true;
                OscOutLocal.Open(6006, remoteMaxMsp?remoteMaxMspIp:"127.0.0.1");
                //OscOutLocal.Open(6006, "127.0.0.1");

                // replace last part of ip with 255 and assign as ip broadcast address
                //string[] splitIpAddress = OscIn.ipAddress.Split('.');
                //splitIpAddress[3] = "255";
                //string newIpAdress = String.Join(".", splitIpAddress);
                string newIpAdress = "255.255.255.255";
                OscOut.multicastLoopback = false;
                OscOut.bundleMessagesOnEndOfFrame = true;
                OscOut.Open(6066, newIpAdress);

                OscBundle sendBundle = new OscBundle();
                sendBundle.Add(new OscMessage("/gic/alive", "MulticastMode Multicast Bundle"));
                OscOut.Send(sendBundle);

                sendBundle.Clear();
                sendBundle.Add(new OscMessage("/gic/alive", "MulticastMode Local Bundle"));
                OscOutLocal.Send(sendBundle);
            }
            else
            {
                //OscOut.Open(6006, clientIp.ToString());
                OscOut.Open(6006, "255.255.255.255");
                //OscOut.Open(6006, remoteMaxMspIp);
                //SetOscOut(remoteMaxMsp, remoteMaxMspIp);

                OscBundle sendBundle = new OscBundle();
                sendBundle.Add(new OscMessage("/gic/alive", "Single Instance Mode: Bundle"));
                OscOut.Send(sendBundle);
            }

            OscBundleMusical = new OscBundle();
            OscBundleAtomMetaInformation = new OscBundle();
            OscBundleMusicalLocal = new OscBundle();
            OscBundleAtomMetaInformationLocal = new OscBundle();

            // Cache Messages
            atomPosition = new OscMessage("/g/sm/ap", 0, 0f, 0f, 0f);

            if (UseMulticastReceiveForSeaboard)
            {
                SeaboardBroadcastReceive.Map("/g/s/pv", SeaboardPitVel);
                SeaboardBroadcastReceive.Map("/g/s/at", SeaboardAfterTouch);
                SeaboardBroadcastReceive.Map("/g/s/g", SeaboardGlide);
                SeaboardBroadcastReceive.Map("/g/s/s", SeaboardSlide);
                SeaboardBroadcastReceive.Map("/g/s/xy", SeaboardXyPad);
                SeaboardBroadcastReceive.Map("/g/ls", AtomLoopSync);
            }
            else
            {
                // Add Callbacks
                OscIn.Map("/g/s/pv", SeaboardPitVel);
                OscIn.Map("/g/s/at", SeaboardAfterTouch);
                OscIn.Map("/g/s/g", SeaboardGlide);
                OscIn.Map("/g/s/s", SeaboardSlide);
                OscIn.Map("/g/s/xyPad", SeaboardXyPad);
            }
        } else
        {
            Debug.LogError("[=======> OSC <=======] IpNegotiation has Failed. No Osc Connction available");
        }
    }

    private IEnumerator SendReceiveIpAdress()
    {
        float timeoutCounter = 0;
        while (timeoutCounter < 5f && !IpReceived)
        {
            if (timeoutCounter >= 5f)
            {
                Debug.LogWarning("[=======> OSC <=======] Osc Ip Negotiation Failed, timeout reached");
                OscOut.Close();
            }

            if (OscOut.isOpen)
            {
                OscBundle sendBundle = new OscBundle();
                string myIP = UseMulticastBroadcast ? "127.0.0.1" : GetLocalIpAddress();
                if (remoteMaxMsp)
                    myIP = GetLocalIpAddress();
                sendBundle.Add(new OscMessage("/gic/special/ipFromUnity", myIP, UseMulticastBroadcast && !remoteMaxMsp ? (int)GiC_Player.Seaboard : (int)GiCPlaybackManager.Instance.ThisPlayerInterface));
                OscOut.Send(sendBundle);
            }

            timeoutCounter += Time.deltaTime;
            yield return null;
        }

        if (IpReceived)
        {
            Debug.Log("[=======> OSC <=======] Client IP received: " + clientIp);
            OscOut.Close();
            yield return StartCoroutine(WaitUntilOscOutIsClosed());
        }
        yield return true;
    }

    private IEnumerator WaitUntilOscOutIsClosed()
    {
        while (OscOut.isOpen)
        {
            yield return null;
        }
    }

    private int GetOscIncomingPortBasedOnPlayerRole()
    {
        switch (GiCPlaybackManager.Instance.ThisPlayerInterface)
        {
            case GiC_Player.Seaboard:
                return 6007;
            case GiC_Player.Paint:
                return 6008;
            case GiC_Player.Leap:
                return 6009;
            case GiC_Player.GJ:
                return 6010;
            case GiC_Player.Undefined:
                return 6007;
            default:
                return 6007;
        }
    }

    // Send Methods
    //public void AddGicPointOsc(float[] floatArrayToSend)
    //{
    //    if (!OscOut.isOpen)
    //        return;
    //    var byteArray = new byte[floatArrayToSend.Length * 4];
    //    Buffer.BlockCopy(floatArrayToSend, 0, byteArray, 0, byteArray.Length);
    //    gicPoint.args[0] = byteArray;
    //    OscBundleMusical.Add(gicPoint);
    //}

    public void SendAtomPosition(int atomIdentifier, Vector3 newPosition)
    {
        if (!OscOut.isOpen || !IpReceived) {
            //Debug.LogError("GicOscManager.SendAtomPosition: oscOut not open");
            return;
        }
        if (atomPosition == null) {
            Debug.LogError("[=======> OSC <=======] GicOscManager.SendAtomPosition: atomPosition is null");
            //StartCoroutine(InitOsc());
            return;
        }

        atomPosition.args[0] = atomIdentifier;
        atomPosition.args[1] = newPosition.x;
        atomPosition.args[2] = newPosition.z;
        atomPosition.args[3] = newPosition.y;

        AddDataToMetaInformationBundle(atomPosition);

        //OscBundleAtomMetaInformation.Add();
    }

    public void AddDataToMetaInformationBundle(OscMessage message, bool local = false)
    {
        if (OscBundleAtomMetaInformation == null)
        {
            return;
        }

        if (local)
        {
            OscBundleAtomMetaInformationLocal.Add(message);
        }
        else
        {
            OscBundleAtomMetaInformation.Add(message);
        }
    }

    private DateTime oscErrorLogged = DateTime.MinValue;
    public void AddDataToMusicalInformationBundle(OscMessage message, bool local = false)
    {
        if (OscBundleMusical == null)
        {
            return;
        }

        if (local)
        {
            OscBundleMusicalLocal.Add(message);
        }
        else
        {
            OscBundleMusical.Add(message);
        }      
    }

    // Receive Methods

    private void ReceiveIpFromClient(OscMessage message)
    {
        string ipAdress = remoteMaxMspIp; // "10.128.69.246";
        message.TryGet(0, out ipAdress);
        IPAddress.TryParse(ipAdress, out clientIp);
        if (clientIp.AddressFamily == AddressFamily.InterNetwork)
        {
            IpReceived = true;
            if (remoteMaxMsp)
                remoteMaxMspIp = ipAdress;
            Debug.Log("[=======> OSC <=======] Client IP received Method: " + clientIp);
        }
        else
        {
            ipAdress = remoteMaxMspIp;
            IPAddress.TryParse(ipAdress, out clientIp);
            Debug.LogWarning("[=======> OSC <=======] Invalid Client IP received: " + clientIp);
        }
    }


    private int atomID;
    private void AtomLoopSync(OscMessage message)
    {
        if (GiC_SoundingObjectsManager.Instance.currentEditableAtom == null)
            return;

            int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbPitV);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);

        if (myAtom != null && myAtom == GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core)
            myAtom.ResyncPlayback();
    }

    private int sbPitV, sbVelV;
    private void SeaboardPitVel(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbPitV);
        message.TryGet(2, out sbVelV);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardDataPiVeRelVe(sbPitV, sbVelV, 0);
    }

    private int sbPitAt, sbVelAt;
    private void SeaboardAfterTouch(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbPitAt);
        message.TryGet(2, out sbVelAt);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardDataAfterTouch(sbPitAt, sbVelAt);
    }

    private int sbGlide;
    private void SeaboardGlide(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbGlide);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardGlideValue(sbGlide);
    }

    private int sbSlide;
    private void SeaboardSlide(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbSlide);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardSlideValue(sbSlide);
    }

    private float sbPadX, sbPadY;
    private void SeaboardXyPad(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbPadX);
        message.TryGet(2, out sbPadY);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardDataXyPad(sbPadX, sbPadY);
    }

    private int sbRec;
    private void SeaboardRec(OscMessage message)
    {
        int sbAtom;
        message.TryGet(0, out sbAtom);
        message.TryGet(1, out sbRec);
        AtomInformationCore myAtom = GiC_SoundingObjectsManager.Instance.GetAtomByIdentifier(sbAtom);
        if (myAtom != null)
            myAtom.SeaboardDataHandler.SetSeaboardDataOnOff(sbRec);
    }

    // Helpers

    public static string GetLocalIpAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                //Debug.Log("My Ip is: "+ip.ToString());
                return ip.ToString();
            }
        }
        throw new Exception("[=======> OSC <=======] Local IP Address Not Found!");
    }

}
