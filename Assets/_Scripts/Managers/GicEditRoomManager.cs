﻿using UnityEngine;
using WorldspaceController;

public class GicEditRoomManager : MonoBehaviour
{

    private GameObject theEditRoom;
    public GameObject EditRoomMesh;
    public GameObject Radar;
    public bool editRoomActive;

    private static GicEditRoomManager _instance;

    #region Singleton Constructors

    public static GicEditRoomManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject.Find("EditRoom").AddComponent<GicEditRoomManager>();                
            }

            return _instance;
        }
    }
    #endregion

    private void Awake()
    {
        if (FindObjectOfType<GicEditRoomManager>() != this)
        {
            Destroy(this);
            return;
        }
        _instance = this;
        Initialize();
    }


    private void Update () {
        if (Radar == null) return;
	    if (editRoomActive && GiC_SoundingObjectsManager.Instance.currentEditableAtom != null && GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.CurrentPlaybackState == GiC_PlaybackStates.Play)
	    {
	        Radar.transform.localRotation = Quaternion.Euler(90f, (GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.NormalizedAtomPlaybackTime * 360f)+180f, 0f);
	    }
    }

    private void Initialize()
    {
        theEditRoom = gameObject;
        CloseEditRoom();
    }

    public void OpenEditRoom(Vector3 newPos)
    {
        // Local Position Player == etwas zu hoch.. verlässliche methode finden den edit soom auf augenhöhe zu behalten aber nicht zu hoch.
        theEditRoom.transform.position = new Vector3(newPos.x, NetworkPlayerController.LocalInstance.transform.position.y-1f, newPos.z);
        SetEditRoomMesh();
        theEditRoom.SetActive(true);
        GiCPlaybackManager.Instance.EditUiSetActive(true);
        editRoomActive = true;
    }

    public void CloseEditRoom()
    {
        //Debug.Log("Close Edit Room");
        GiCPlaybackManager.Instance.EditUiSetActive(false);
        theEditRoom.SetActive(false);
        editRoomActive = false;
    }

/*
    private void SetEditRoomCollider()
    {
        //Mesh colliderMesh = GiC_SoundingObjectsManager.instance.currentEditableAtom.SeaboardGeometry.GetComponent<MeshFilter>().sharedMesh;
        EditRoomMesh.GetComponent<MeshCollider>().sharedMesh = GiC_SoundingObjectsManager.instance.currentEditableAtom.EditRoomColliderMesh;
        //EditRoomMesh.GetComponent<MeshCollider>().convex = true;
        //EditRoomMesh.GetComponent<MeshCollider>().inflateMesh = true;
    }
*/

    private void SetEditRoomMesh()
    {
        EditRoomMesh.GetComponent<MeshFilter>().sharedMesh = GiC_SoundingObjectsManager.Instance.currentEditableAtom.EditRoomColliderMesh;
        EditRoomMesh.GetComponent<MeshCollider>().sharedMesh = EditRoomMesh.GetComponent<MeshFilter>().sharedMesh;
    }
}
