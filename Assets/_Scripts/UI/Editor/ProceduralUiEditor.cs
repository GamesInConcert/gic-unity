﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



[CustomEditor(typeof(GiC_RadialSliderProcedural))]
[CanEditMultipleObjects]
public class ProceduralUiEditor : Editor
{
    public override void OnInspectorGUI()
    {

        GiC_RadialSliderProcedural targetScript = (GiC_RadialSliderProcedural) target;
        DrawDefaultInspector();

        if (GUI.changed)
        {
            targetScript.Initialize();
        }
    }

}
