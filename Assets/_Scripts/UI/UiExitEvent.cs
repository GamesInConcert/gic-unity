﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiExitEvent : MonoBehaviour, IPointerExitHandler {
    public CanvasGroup canvasGroup;

    public void OnPointerExit(PointerEventData eventData) {
        Close();
    }

    public void Close() {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }
}
