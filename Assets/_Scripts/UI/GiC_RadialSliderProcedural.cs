﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GiC_RadialSliderProcedural : MonoBehaviour,  IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IDragHandler
{
    public enum Direction { Cw, Ccw }
    [Tooltip("Direction of rotation CW - clockwise, CCW - counterClockwise")]
    public GiC_RadialSlider.Direction RotationDirection = GiC_RadialSlider.Direction.CW;
    [Tooltip("The Initial Value of the Knob")]
    public float InitialValue;
    private float initalValue { get { return InitialValue < MinOutput ? MinOutput : InitialValue; } }
    [Tooltip("Min value of the knob, minimum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
    public float MinOutput;
    [Tooltip("Max value of the knob, maximum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
    public float MaxOutput;

    public int PieCutoutInDeg = 60;
    public int ZeroPositionInDeg;

    [Tooltip("Clamp output value between 0 and 1, usefull with loops > 1")]
    public bool WholeNumbers;
    [Tooltip("snap to position?")]
    public bool SnapToPosition;
    [Tooltip("Number of positions to snap")]
    public int SnapStepsPerLoop = 10;
    [Range(0f,1f)]
    public float DebugFill = 1f;
    [Space(30)]
    public KnobFloatValueEvent OnValueChanged;
    public KnobFloatValueEvent OnNormalizedValueChanged;

    public Image Background;
    public Image Handle;
    public GiC_ProceduralUi Fill;
    public GameObject ImagesParent;
    private bool canDrag;
    private float internalKnobValue;
    private float currentAngle;
    //private float previousValue = 0.5f;

    private bool internalSnapToPosition { get { return WholeNumbers || SnapToPosition; } }

    private float knobRange { get { return Mathf.Abs(MaxOutput - MinOutput); } }
    private int internalSnapStepsPerLoop { get { return WholeNumbers ? Mathf.CeilToInt(knobRange) : SnapStepsPerLoop; } }
    private RectTransform rectTransform;

    // springdamper vars
    private float prevRot;
    private float dampedRot;
    private float currentRotVel;

    private float targetFillVal;
    private float dampedFillVal;
    private float currentValVel;

    private float targetRotationZ;

    private void Awake()
    {
        RecreateKnob();
    }
    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        rectTransform = GetComponent<RectTransform>();
        RecreateKnob();
        SetKnobValue(initalValue);

        GiC_SoundingObjectsManager.Instance.OnEditableAtomchange += OnAtomChanged;
    }

    private void RecreateKnob()
    {
        Fill.knobParams.OpeningAngle = PieCutoutInDeg;
        Fill.RecreateKnob();
    }

    private void Update()
    {
        CalculateChanges();
    }

    private void CalculateChanges()
    {
        dampedRot = Mathf.SmoothDampAngle(dampedRot, targetRotationZ, ref currentRotVel, Time.unscaledDeltaTime * 0.5f);
        dampedFillVal = Mathf.SmoothDamp(dampedFillVal, targetFillVal, ref currentValVel, Time.unscaledDeltaTime * 0.5f);
        Handle.transform.localRotation = Quaternion.Euler(0, 0, dampedRot);
        Fill.knobParams.Fill = dampedFillVal;
        Fill.UpdateFill();
    }

    //ONLY ALLOW ROTATION WITH POINTER OVER THE CONTROL
    public void OnPointerDown(PointerEventData eventData)
    {
        canDrag = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        canDrag = false;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        canDrag = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        // Set to false if you want to disable rotation out of bounds
        canDrag = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //CHECK IF CAN DRAG
        if (!canDrag)
        {
            return;
        }
        UpdateKnobValues(eventData);
    }

    private void OnAtomChanged(GiC_ProceduralAtom newAtom)
    {
        if (newAtom == null || MaxOutput < 1.5f)
            return;
        float newVal = MaxOutput > 50f ? newAtom.Core.Bpm : newAtom.Core.NumBeats;
        //Debug.Log("Slider CHange: " +newVal + " max " + MaxOutput);
        SetKnobValue(newVal);
    }

    public void SetKnobValue(float value)
    {
        prevRot = GiC_MathUtils.Scale(value, MinOutput, MaxOutput, 0f, 360f - PieCutoutInDeg);
        CalcVals(prevRot+0.5f);
        CalculateChanges();
    }

    private void UpdateKnobValues(PointerEventData eventData)
    {
        Vector3 myUp = Quaternion.Euler(0f, 0f, -ZeroPositionInDeg) * new Vector3(0, 1, 0);

        Vector2 localCursor;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position,
            eventData.pressEventCamera, out localCursor);
        Vector2 v1 = localCursor - Vector2.zero;

        currentAngle = GiC_MathUtils.AngleSigned(v1, myUp, Vector3.forward);
        if (currentAngle < 0)
        {
            currentAngle = 360f + currentAngle;
        }

        CalcVals(currentAngle);
    }

    private void CalcVals(float angle)
    {
        float newtargetRotationZ = angle;
        if (internalSnapToPosition)
        {
            float snapStep = (((360f - PieCutoutInDeg)) / internalSnapStepsPerLoop);
            newtargetRotationZ = (Mathf.Round(newtargetRotationZ / snapStep) * snapStep);
        }
        newtargetRotationZ = Mathf.Clamp(-newtargetRotationZ, -(360f - PieCutoutInDeg), 0);

        //PREVENT OVERROTATION
        // To Improve
        //if (Mathf.Abs(prevRot - newtargetRotationZ) > 225f)
        //{
        //    return;
        //}

        targetRotationZ = newtargetRotationZ;

        internalKnobValue = GiC_MathUtils.Scale(targetRotationZ, -(360f - PieCutoutInDeg), 0f, 0f, 1f);
        internalKnobValue = RotationDirection == GiC_RadialSlider.Direction.CW ? 1 - internalKnobValue : internalKnobValue;

        targetFillVal = GiC_MathUtils.Scale(targetRotationZ, -(360f - PieCutoutInDeg), 0f, 1f, 0f);
        InvokeEvents(internalKnobValue);
        prevRot = newtargetRotationZ;
    }

    private void InvokeEvents(float value)
    {
        OnNormalizedValueChanged.Invoke(value);
        value = Mathf.Clamp(MinOutput + (value * knobRange), MinOutput, MaxOutput);
        if (WholeNumbers)
        {
            value = Mathf.Floor(value);
        }
        OnValueChanged.Invoke(value);
    }
}