﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gic_Ui_ValueToLabel : MonoBehaviour
{

    public string prepend = "";
    public string append = "";
    public Text textobject;

    private void Start()
    {
        textobject = GetComponentInChildren<Text>();
    }

    public void SetLabel(float value)
    {
        if (textobject == null)
            return;
        textobject.text = prepend + value.ToString() + append;
    }

    public void SetLabel(int value)
    {
        if (textobject == null)
            return;
        textobject.text = prepend + value.ToString() + append;
    }

    //public void SetLabel(int value)
    //{

    //}

}
