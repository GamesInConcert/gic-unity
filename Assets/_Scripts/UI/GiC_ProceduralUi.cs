﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProceduralToolkit;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public enum GiC_KnobType
{
    TriangleStrip,
    QuadStrip,
    Ring
}

[Serializable]
public struct Gic_KnobParameters
{
    public GiC_KnobType KnobType;
    public float OpeningAngle;
    [Range(0f, 1f)]
    public float Fill;
    public float width;
    public int segments;
    public float Variation;
    public Color ColorA;
    public Color ColorB;
    public Color InactiveColor;
    public int widthSegments;

    //public Gic_KnobParameters()
    //{
    //    KnobType = GiC_KnobType.TriangleStrip;
    //    OpeningAngle = 0f;
    //    Fill = 1f;
    //    width = 0.1f;
    //    segments = 20;
    //    Variation = 0f;
    //    ColorA = Color.blue;
    //    ColorB = Color.red;
    //    widthSegments = 2;
    //}
}

public class GiC_ProceduralUi : MonoBehaviour
{
    public Gic_KnobParameters knobParams;
    private Mesh myUiMesh;
    private MeshFilter thisMeshFilter;
    private Color allZeroColor = new Color(0,0,0,0);
    //public GiC_KnobType KnobType;
    //public float StartingAngle = 0f;
    //public float OpeningAngle = 0f;
    //[Range(0f, 1f)]
    //public float Fill = 0f;
    //public float width = 0.1f;
    //public int segments = 10;
    //public float Variation = 0f;
    //public Color ColorA;
    //public Color ColorB;
    //public int widthSegments = 3;

    private Color32[] oColors;

	// Use this for initialization
	void Start ()
	{
        //RecreateKnob();
        //CreateLinearSlider();
        //CreateFlatSphere();
	}
	
	// Update is called once per frame
	void Update () {

	    //thisMeshFilter.sharedMesh = CreatePolygonalRing2(OpeningAngle, Fill, width, segments, Variation);
	}

    public void UpdateFill()
    {
        if (thisMeshFilter == null)
            return;
        thisMeshFilter.sharedMesh = AlphaFadeMeshColors(thisMeshFilter.sharedMesh, oColors, knobParams.Fill, knobParams.InactiveColor);
    }

    public static Mesh CreateKnobMesh(Gic_KnobParameters kParam)
    {
        Mesh returnVal;
        switch (kParam.KnobType)
        {
                case GiC_KnobType.TriangleStrip:
                returnVal = CreateTriangleStripRing(kParam.OpeningAngle, 1, kParam.width, kParam.segments, kParam.Variation);
                break;
                case GiC_KnobType.QuadStrip:
                returnVal = CreateQuadStripRing(kParam.OpeningAngle, 1, kParam.width, kParam.segments, kParam.widthSegments, kParam.Variation);
                break;
                case GiC_KnobType.Ring:
                returnVal = CreateRing(kParam.OpeningAngle, 1, kParam.width, kParam.segments, kParam.Variation);
                break;
                default:
                returnVal = new Mesh();
                break;
        }
        return returnVal;
    }

    public void RecreateKnob()
    {
        thisMeshFilter = GetComponent<MeshFilter>();
        if (thisMeshFilter == null)
            return;

        var newUiElement = CreateKnobMesh(knobParams);
        newUiElement = ColorizePolygonStyle(newUiElement, knobParams.ColorA, knobParams.ColorB);
        thisMeshFilter.sharedMesh = newUiElement;
        oColors = newUiElement.colors32;
    }

    public void CreateLinearSlider()
    {
        thisMeshFilter = GetComponent<MeshFilter>();
        if (thisMeshFilter == null)
            return;

        var newUiElement = CreateTriangleStripLinearSlider(1, 0.1f, knobParams.segments, 2f);
        newUiElement = ColorizePolygonStyle(newUiElement, knobParams.ColorA, knobParams.ColorB);
        thisMeshFilter.sharedMesh = newUiElement;
        oColors = newUiElement.colors32;
    }

    public void CreateFlatSphere()
    {
        thisMeshFilter = GetComponent<MeshFilter>();
        if (thisMeshFilter == null)
            return;

        var newUiElement = CreateTriangleStripFilledCircle(1, 1, knobParams.segments, 2f);
        newUiElement = ColorizePolygonStyle(newUiElement, knobParams.ColorA, knobParams.ColorB);
        thisMeshFilter.sharedMesh = newUiElement;
        oColors = newUiElement.colors32;
    }

    public static Mesh CreateRing(float openingAngle, float fill, float width, int segments, float variation)
    {
        List<Vector3> outerRing = new List<Vector3>();
        List<Vector3> innerRing = new List<Vector3>();

        float angleToFill = 360f - openingAngle;
        angleToFill = Mathf.Deg2Rad*angleToFill;
        


        float pisegment = (angleToFill*fill) / (segments-1);
        outerRing.Add(PointOnCircle3(1f + width, 0f));
        innerRing.Add(PointOnCircle3(1f - width, 0f));


        for (int i = 0; i < segments; i++)
        {
            outerRing.Add(PointOnCircle3(1f + width, pisegment * i));
            innerRing.Add(PointOnCircle3(1f - width, pisegment * i));
        }

        var draft = MeshDraft.FlatBand(innerRing, outerRing, false);

        var returnMesh = draft.ToMesh();
        //returnMesh.RecalculateNormals();

        return returnMesh;
    }

    public static Mesh CreateTriangleStripRing(float openingAngle, float fill, float width, int segments, float variation)
    {
        int fillsegments = Mathf.FloorToInt(segments * fill);

        if (fillsegments < 2)
            return new Mesh();

        List<Vector3> Vertecies = new List<Vector3>();

        float angleToFill = 360f - openingAngle;
        angleToFill = Mathf.Deg2Rad * angleToFill;

        float pisegment = (angleToFill) / (segments - 1);
        Vertecies.Add(PointOnCircle3(1f + width, 0f));
        
        for (int i = 0; i <= fillsegments; i++)
        {
            if (i > 1 && (pisegment * i > (angleToFill - pisegment)) && angleToFill == 360)
            {
                Vertecies.Add(i < fillsegments ? Vertecies[1] : Vertecies[0]);
            }
            else
            {
                float piSeg = i == fillsegments ? angleToFill : pisegment * i;
                Vertecies.Add(i % 2 == 1
                    ? PointOnCircle3(1f + width, piSeg, variation, true, i)
                    : PointOnCircle3(1f - width, piSeg, variation, true, i));
            }
        }

        var draft = MeshDraft.TriangleStrip(Vertecies);

        var returnMesh = draft.ToMesh();
        returnMesh.RecalculateNormals();

        return returnMesh;
    }

    public static Mesh CreateTriangleStripLinearSlider(float fill, float width, int segments, float height)
    {
        int fillsegments = Mathf.FloorToInt(segments-1 * fill);

        if (fillsegments < 2)
            return new Mesh();

        List<Vector3> Vertecies = new List<Vector3>();

        float heightsegment = height / segments;

        Vertecies.Add(new Vector3(1f + width, 0f, 0f));

        for (int i = 0; i <= fillsegments; i++)
        {
                Vertecies.Add(i % 2 == 1
                    ? new Vector3(1f + width, 0, heightsegment*i)
                    : new Vector3(1f - width, 0, heightsegment*i));
            if (i == fillsegments)
            {
                Vertecies.Add(i % 2 == 1
                    ? new Vector3(1f - width, 0, heightsegment * i)
                    : new Vector3(1f + width, 0, heightsegment * i));
            }
            
        }

        var draft = MeshDraft.TriangleStrip(Vertecies);

        var returnMesh = draft.ToMesh();
        returnMesh.RecalculateNormals();

        return returnMesh;
    }

    public static Mesh CreateTriangleStripFilledCircle(float fill, float width, int segments, float radius)
    {
        var returnMesh = MeshDraft.FlatSphere(radius, segments, segments).ToMesh();
        //var returnMesh = MeshDraft.Sphere(radius, segments, segments).ToMesh();
        returnMesh.RecalculateNormals();

        return returnMesh;
    }

    public static Mesh CreateQuadStripRing(float openingAngle, float fill, float width, int segments, int widthSegments, float variation)
    {
        List<Vector3> Vertecies = new List<Vector3>();
        MeshDraft draft = new MeshDraft { name = "ui" };

        float angleToFill = 360f - openingAngle;
        angleToFill = Mathf.Deg2Rad * angleToFill;

        float pisegment = (angleToFill) / (segments);
        int fillsegments = Mathf.CeilToInt(segments * fill);
        float widthParts = width / widthSegments; 

        for (int v = 0; v <= widthSegments; v++)
        {
            Vertecies.Add(PointOnCircle3(widthParts * (float)(v+1), 0, variation, true, 1000));
        }

        int j = 0;
        int k = (widthSegments * 2) - 2;
        int l = (widthSegments * 2) - 1;
        int m = 1;

        for (int i = 0; i < fillsegments; i++)
        {
            for (int v = 0; v <= widthSegments; v++)
            {
                Vertecies.Add(PointOnCircle3(widthParts* (float)(v+1), pisegment * i, variation, true, i));
            }
            while (l < Vertecies.Count-1)
            {
                k = j + (widthSegments+1);
                l = j + (widthSegments+2);
                m = j + 1;

                draft.Add(MeshDraft.Quad(Vertecies[j], Vertecies[k], Vertecies[l], Vertecies[m]));
                j++;
            }
            j++;
        }

        var returnMesh = draft.ToMesh();
        returnMesh.RecalculateNormals();

        return returnMesh;
    }

    public static Mesh ColorizePolygonStyle(Mesh meshToColorize, Color colorA, Color colorB)
    {
        Color32[] returnColors = new Color32[meshToColorize.vertexCount];
        for (int i = 0; i < meshToColorize.vertexCount; i++)
        {
            float j = Mathf.Floor(i / 3) / (meshToColorize.vertexCount / 3);
            returnColors[i] = (Color.Lerp(colorA, colorB, j));
        }
        meshToColorize.colors32 = returnColors;
        return meshToColorize;
    }

    public static Mesh AlphaFadeMeshColors(Mesh meshtoFade, Color32[] original, float fill, Color disabledcolor)
    {
        Color32[] meshColors = meshtoFade.colors32;
        int vCount = meshtoFade.colors32.Length;
        int filled = Mathf.CeilToInt(vCount * fill);

        for (int i = 0; i < vCount; i++)
        {
            if (i < filled)
            {
                meshColors[i] = original[i];
            }
            else
            {
                meshColors[i] = original[i]*disabledcolor;
            }
        }

        meshtoFade.colors32 = meshColors;
        return meshtoFade;
    }

    public static Vector3 PredictableRandomVector(float scale = 1f, bool signed = false, int seed = 100)
    {
        Random.InitState(seed);
        Vector3 returnValue;
        if (signed)
        {
            returnValue = new Vector3(2*Random.value-1f, 2 * Random.value - 1f, 2 * Random.value - 1f);
        }
        else
        {
            returnValue = new Vector3(Random.value, Random.value, Random.value);
        }
        return returnValue * scale;
    }

    /// <summary>
    /// Returns point on circle in the XZ plane
    /// </summary>
    /// <param name="radius">Circle radius</param>
    /// <param name="angle">Angle in radians</param>
    public static Vector3 PointOnCircle3(float radius, float angle, float variation = 0f, bool signed = false, int seed = 100)
    {
        Vector3 random = Vector3.zero;
        if (variation > 0f)
        {
            random = PredictableRandomVector(variation, signed, seed);
        }
        return new Vector3((radius) * Mathf.Sin(angle + random.z), random.y, (radius) * Mathf.Cos(angle + random.x));
    }
}
