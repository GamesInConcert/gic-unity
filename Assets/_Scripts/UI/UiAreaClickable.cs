﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiAreaClickable : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {
    public void OnPointerDown(PointerEventData eventData) {
        Debug.Log("UiAreaClickable.OnPointerDown: '" + eventData.selectedObject + "' was pressed at " + eventData.position);
    }

    public void OnPointerUp(PointerEventData eventData) {
        Debug.Log("UiAreaClickable.OnPointerUp: '" + eventData.selectedObject + "' was released at " + eventData.position);
    }

    public void OnPointerClick (PointerEventData eventData) {
        Debug.Log("UiAreaClickable.OnPointerClick: '" + eventData.selectedObject + "' was clicked at " + eventData.position);
	}
}
