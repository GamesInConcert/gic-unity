﻿

using UnityEngine.Events;
using UnityEngine.EventSystems;



namespace UnityEngine.UI.Extensions
{
    /// Credit Tomasz Schelenz, Updated for World Space UI by Simon Pfaff
    /// Sourced from - https://bitbucket.org/ddreaper/unity-ui-extensions/issues/46/feature-uiknob#comment-29243988
    /// <summary>
    /// KNOB controller
    /// 
    /// Fields
    /// - direction - direction of rotation CW - clockwise CCW - counter clock wise
    /// - InitialValue - Initial Value of the Knob
    /// - minOutput - The minimum value of the knob
    /// - maxOutput - The maximum value of the knob
    /// - loops - how any turns around knob can do
    /// - wholeNumbers - Only output whole numbers (Enables snap to position)
    /// - snapToPosition - Snaps to "1/snap Steps per loop"
    /// - snapStepsPerLoop - Sets the division for snapping
    /// - OnValueChanged - event that is called every frame while rotationg knob, sends <float> argument of knobValue (between min and max output)
    /// - OnNormalizedValueChanged - event that is called every frame while rotationg knob, sends normalized Value of the knob
    /// NOTES
    /// - script works only in images rotation on Z axis;
    /// 
    /// </summary>
    //[RequireComponent(typeof(Image))]
    [AddComponentMenu("UI/Extensions/GiC_RadialSlider")]
    public class GiC_RadialSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler
    {
        public enum Direction { CW, CCW };
        [Tooltip("Direction of rotation CW - clockwise, CCW - counterClockwise")]
        public Direction direction = Direction.CW;
        [Tooltip("The Initial Value of the Knob")]
        public float InitialValue;
        private float initalValue { get { return InitialValue < minOutput ? minOutput : InitialValue; } }
        [Tooltip("Min value of the knob, minimum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
        public float minOutput = 0;
        [Tooltip("Max value of the knob, maximum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
        public float maxOutput = 0;

        public int pieCutoutInDeg = 60;
        public int zeroPositionInDeg = 0;
        private bool partial { get { return pieCutoutInDeg < 0; } }
        private int halfSliceInt { get { return pieCutoutInDeg / 2; } }
        private float halfSliceNormalized { get { return halfSliceInt/360f; } }
        [Tooltip("Clamp output value between 0 and 1, usefull with loops > 1")]
        public bool wholeNumbers = false;
        [Tooltip("snap to position?")]
        public bool snapToPosition = false;
        [Tooltip("Number of positions to snap")]
        public int snapStepsPerLoop = 10;
        [Space(30)]
        public KnobFloatValueEvent OnValueChanged;
        public KnobFloatValueEvent OnNormalizedValueChanged;
       // public KnobFloatValueEvent OnNormalizedValueDampedChanged;

        public Image background;
        public Image handle;
        private GameObject handleGo { get { return handle.gameObject; } }
        public Image fill;
        public GameObject ImagesParent;
        //public RectTransform targetTransform;
        //private float _initAngle;
        //private Vector2 _currentVector;
        //private Vector3 _initPosition;
        //private Quaternion _initRotation;
        //private Vector3 _initRotEuler;
        private bool canDrag = false;
        private float internalKnobValue = 0f;
        private float currentAngle;
        private float previousValue = 0.5f;

        private bool internalSnapToPosition { get { return wholeNumbers || snapToPosition; } }
        //private float maxValue = 0;

        private float knobRange { get { return Mathf.Abs(maxOutput - minOutput); } }
        private int internalSnapStepsPerLoop { get { return wholeNumbers ? Mathf.CeilToInt(knobRange) : snapStepsPerLoop; } }
        private RectTransform rectTransform;
        private Vector2 prevVector2 = Vector2.zero;

        // springdamper vars
        private float prevRot = 0f;
        private float dampedRot = 0f;
        private float currentRotVel = 0f;

        private float targetFillVal = 0f;
        private float dampedFillVal = 0f;
        private float currentValVel = 0f;

        private float targetRotationZ;

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();

            ImagesParent.transform.localRotation = Quaternion.Euler(0, 0, -zeroPositionInDeg);
            //background.transform.localRotation 
            //fill.transform.localRotation = Quaternion.Euler(0, 0, -zeroPositionInDeg);
            background.fillAmount = 1f - halfSliceNormalized * 2f;
            //InvokeEvents((initalValue - minOutput) / knobRange);
            //handle.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Clamp((initalValue - minOutput) / knobRange * 360f, pieCutoutInDeg, 360-pieCutoutInDeg)));
        }

        private void Update()
        {
            if (Mathf.Approximately(dampedFillVal, targetFillVal))
                return;

            dampedRot = Mathf.SmoothDampAngle(dampedRot, targetRotationZ, ref currentRotVel, Time.unscaledDeltaTime * 0.5f);
            dampedFillVal = Mathf.SmoothDamp(dampedFillVal, targetFillVal, ref currentValVel, Time.unscaledDeltaTime * 0.5f);
            handle.transform.localRotation = Quaternion.Euler(0, 0, dampedRot);
            fill.fillAmount = dampedFillVal;
        }

        //ONLY ALLOW ROTATION WITH POINTER OVER THE CONTROL
        public void OnPointerDown(PointerEventData eventData)
        {
            canDrag = true;
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            canDrag = false;
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            canDrag = true;
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            // Set to false if you want to disable rotation out of bounds
            canDrag = true;
        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            SetInitPointerData(eventData);
        }
        void SetInitPointerData(PointerEventData eventData)
        {

        }
        public void OnDrag(PointerEventData eventData)
        {
            //CHECK IF CAN DRAG
            if (!canDrag)
            {
                SetInitPointerData(eventData);
                return;
            }

            Vector3 myUp = Quaternion.Euler(0f, 0f, -zeroPositionInDeg) * new Vector3(0, 1, 0);

            // Own Stuff
            Vector2 localCursor;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(this.rectTransform, eventData.position,
                eventData.pressEventCamera, out localCursor);
            Vector2 V1 = localCursor - Vector2.zero;

            currentAngle = AngleSigned(V1, myUp, Vector3.forward);

            if (currentAngle < 0)
            {
                currentAngle = 360f + currentAngle;
            }

            float newtargetRotationZ = currentAngle;
            //Debug.Log();
            if (internalSnapToPosition)
            {
                float snapStep = (((360f-pieCutoutInDeg)) / (float)internalSnapStepsPerLoop);
                newtargetRotationZ = (Mathf.Round(newtargetRotationZ / snapStep) * snapStep);
            }
            //Debug.Log(newtargetRotationZ);
            internalKnobValue = Scale(targetRotationZ, -(360f - pieCutoutInDeg), 0f, 0f, 1f);
            newtargetRotationZ = Mathf.Clamp(-newtargetRotationZ, -(360f - pieCutoutInDeg), 0);

            internalKnobValue = direction == Direction.CW ? 1 - internalKnobValue : internalKnobValue;

            //PREVENT OVERROTATION
            // To Improve

            if (Mathf.Abs(prevRot - newtargetRotationZ) > 270f)
            {
                return;
            }

            targetFillVal = Scale(targetRotationZ, -(360f - pieCutoutInDeg), 0f, 1 - (halfSliceNormalized * 2f), 0f);
            targetRotationZ = newtargetRotationZ;
            InvokeEvents(internalKnobValue);
            prevRot = newtargetRotationZ;
        }
        //private void SnapToPosition(ref float internalKnobValue)
        //{
        //    float snapStep = 1 / (float)internalSnapStepsPerLoop;
        //    float newValue = Mathf.Round(internalKnobValue / snapStep) * snapStep;
        //    internalKnobValue = newValue;
        //}

        private void SetHandleAndFill(float targetRotation)
        {
        }

        private void InvokeEvents(float value)
        {
            OnNormalizedValueChanged.Invoke(value);
            //targetFillVal = value;
            //value = Scale(value, pieCutoutInDeg/360f, (360 - pieCutoutInDeg)/360f, 0f, 1f);
            value = Mathf.Clamp(minOutput + (value * knobRange), minOutput, maxOutput);
            if (wholeNumbers)
            {
                value = Mathf.Floor(value);
            }
            OnValueChanged.Invoke(value);
        }

        private float Scale(float valueIn, float newMin, float newMax)
        {
            float m = 1 / (newMin - newMax);
            float c = -newMin * m;
            return valueIn * m + c;
        }

        private float Scale(float value, float min, float max, float minScale, float maxScale)
        {
            float scaled = minScale + (value - min) / (max - min) * (maxScale - minScale);
            return scaled;
        }

        /// <summary>
        /// Determine the signed angle between two vectors, with normal 'n'
        /// as the rotation axis.
        /// </summary>
        private float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(
                Vector3.Dot(n, Vector3.Cross(v1, v2)),
                Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }
    }

    //[System.Serializable]
    //public class KnobFloatValueEvent : UnityEvent<float> { }

}