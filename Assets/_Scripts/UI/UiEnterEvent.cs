﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiEnterEvent : MonoBehaviour, IPointerEnterHandler {
    public CanvasGroup canvasGroup;

    public void OnPointerEnter(PointerEventData eventData) {
        Open();
    }

    public void Open() {
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }
}
