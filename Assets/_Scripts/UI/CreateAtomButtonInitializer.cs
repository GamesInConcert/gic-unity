﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using WorldspaceController;

public class CreateAtomButtonInitializer : MonoBehaviour {

    public int id = -1;

    public void Awake () {
        GetComponent<Button>().onClick.AddListener(CreateAtom);
    }

    NetworkPlayerController localPlayer = null;
	// Update is called once per frame

    void CreateAtom () {
        if (localPlayer == null) {
            localPlayer = NetworkPlayerController.GetLocalNetworkPlayer();
        }
        if (localPlayer != null) { 
            Vector3 pos = transform.position - transform.forward * 0.1f;
            //Debug.Log("CreateAtomButtonInitializer.CreateAtom: " + pos);
            localPlayer.CmdCreateAtom(pos, Quaternion.identity, id, GiCPlaybackManager.Instance.ThisPlayerInterface);
        } else {
            Debug.LogWarning("CreateAtomButtonInitializer.CreateAtom: network not yet initialized!");
        }
    }
}
