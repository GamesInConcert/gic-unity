﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UiEnterExitHandler : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler {
    public void Start() {}

    public void Update() {}

    public void OnPointerExit(PointerEventData eventData) {
        Open();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        Close();
    }
    public void Open() {
    }
    public void Close() {
    }
}