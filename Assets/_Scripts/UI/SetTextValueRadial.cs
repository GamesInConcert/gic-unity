﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetTextValueRadial : MonoBehaviour
{
    public GameObject TextGo;
    private TextMeshPro textMesh;
    public string ItemText;

	// Use this for initialization
	void Awake ()
	{
	    textMesh = TextGo.GetComponent<TextMeshPro>();
	    textMesh.richText = true;
	}

    public void SetText(float value)
    {
        textMesh.text = ItemText + "<br>" + value.ToString("0.##");
    }
}
