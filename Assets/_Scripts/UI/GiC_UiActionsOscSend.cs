﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldspaceController;

public class GiC_UiActionsOscSend : MonoBehaviour
{

    private AtomInformationCore GetCore()
    {
        return GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core;
    }

    private bool CurrentAtomIsNull()
    {
        return GiC_SoundingObjectsManager.Instance.currentEditableAtom == null;
    }

    #region Seaboard UI

    //public void SetSeaboardLoopDisabled(bool newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (newValue && targetScript != null)
    //        targetScript.SetLoopMode(0);
    //}

    //public void SetSeaboardLoopEnabled(bool newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (newValue && targetScript != null)
    //        targetScript.SetLoopMode(1);
    //}

    //public void SetSeaboardClick(bool newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    bool clickEnabled = Convert.ToBoolean(newValue);
    //    if (targetScript != null)
    //        targetScript.ToggleClick(clickEnabled);
    //}

    public void ToggleSeaboardRecordMode(bool newValue)
    {
        if (CurrentAtomIsNull())
            return;
        AtomInformationCore targetScript = GetCore();
        targetScript.ArmRecordPlayback();
        //bool recordEnabled = Convert.ToBoolean(newValue);
        if (targetScript != null) {
            targetScript.ArmRecordPlayback(newValue);
        }
    }

    //public void SetSeaboardBpm(float newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (targetScript != null)
    //        targetScript.SetBpm(Convert.ToInt32(newValue));
    //}

    //public void SetSeaboardNumBeats(float newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (targetScript != null)
    //        targetScript.SetNumBeats(Convert.ToInt32(newValue));
    //}

    #endregion

    #region Drawing UI

    public void DeleteCurrentEditedDrawing()
    {
        if (CurrentAtomIsNull())
            return;
        GiC_DrawingDataHandler targetScript = GiC_SoundingObjectsManager.Instance.currentEditableAtom.Core.DrawingDataHandler;
        if (targetScript != null)
            targetScript.DeleteDrawing();
    }

    //public void SetDrawingClick(bool newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    //bool clickEnabled = Convert.ToBoolean(newValue);
    //    if (targetScript != null)
    //    {
    //        targetScript.ToggleClick(newValue);
    //    }
    //}

    //public void ToggleDrawingRecordMode(bool newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    GiC_DrawingDataHandler targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core.DrawingDataHandler;
    //    bool recordEnabled = Convert.ToBoolean(newValue);
    //    if (targetScript != null)
    //    {
    //        if (recordEnabled)
    //        {
    //            // targetScript.StartRecording();
    //        }
    //        else
    //        {
    //            // targetScript.StopRecording();
    //        }
    //    }
    //}

    //public void SetDrawingBpm(float newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (targetScript != null)
    //    targetScript.SetBpm(Convert.ToInt32(newValue));
    //}

    //public void SetDrawingNumBeats(float newValue)
    //{
    //    if (CurrentAtomIsNull())
    //        return;
    //    AtomInformationCore targetScript = GiC_SoundingObjectsManager.instance.currentEditableAtom.Core;
    //    if (targetScript != null)
    //        targetScript.SetNumBeats(Convert.ToInt32(newValue));
    //}

    #endregion

    #region General UI

    // General
    public void StartAtomPlayback(bool newValue)
    {
        if (newValue)
            GiCPlaybackManager.Instance.TogglePlayback(true);
    }

    public void StopAtomPlayback(bool newValue)
    {
        if (newValue)
            GiCPlaybackManager.Instance.TogglePlayback(false);
    }

    public void ResyncAtomPlayback()
    {
        GiCPlaybackManager.Instance.ResyncAtomPlayback();
    }

    public void ToggleEditMode(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        if ((bool) newValue)
        {
            //objectBeingUsed.GetComponent<AtomCoreInteractable>().StartEditMode();
        }
        else
        {
            //objectBeingUsed.GetComponent<AtomCoreInteractable>().StopEditMode();
        }       
    }

    public void SetClick(bool newValue)
    {
        if (CurrentAtomIsNull())
            return;
        AtomInformationCore targetScript = GetCore();
        //bool clickEnabled = Convert.ToBoolean(newValue);
        if (targetScript != null)
        {
            targetScript.ToggleClick(newValue);
        }
    }

    public void SetBpm(float newValue)
    {
        if (GiCPlaybackManager.Instance == null)
            return;
        GiCPlaybackManager.Instance.SetBpm(Convert.ToInt32(newValue));
        //if (CurrentAtomIsNull())
        //    return;
        //AtomInformationCore targetScript = GetCore();
        //if (targetScript != null)
        //    targetScript.SetBpm(Convert.ToInt32(newValue));
    }

    public void SetNumBeats(float newValue)
    {
        if (GiCPlaybackManager.Instance == null)
            return;
        GiCPlaybackManager.Instance.SetNumBeats(Convert.ToInt32(newValue));
        //if (CurrentAtomIsNull())
        //    return;
        //AtomInformationCore targetScript = GetCore();
        //if (targetScript != null)
        //    targetScript.SetNumBeats(Convert.ToInt32(newValue));
    }

    public void SetLoopDisabled(bool newValue)
    {
        if (CurrentAtomIsNull())
            return;
        AtomInformationCore targetScript = GetCore();
        if (newValue && targetScript != null)
            targetScript.SetLoopMode(0);
    }

    public void SetLoopEnabled(bool newValue)
    {
        if (CurrentAtomIsNull())
            return;
        AtomInformationCore targetScript = GetCore();
        if (newValue && targetScript != null)
            targetScript.SetLoopMode(1);
    }

    public void OpenEditMode(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        //objectBeingUsed.GetComponent<AtomCoreInteractable>().StartEditMode();
    }

    public void CloseEditMode(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        //objectBeingUsed.GetComponent<AtomCoreInteractable>().StopEditMode();
    }

    #endregion


    // Paint
    public void TogglePaintLoopMode(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        // TODO
    }

    public void SetPaintLoopLength(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        int timeInMs = (int) newValue * 1000;
        // TODO
    }

    public void SetPaintNumBeats(GameObject objectBeingUsed, WorldspaceInputDevice usingObject, object newValue)
    {
        // TODO
    }

}