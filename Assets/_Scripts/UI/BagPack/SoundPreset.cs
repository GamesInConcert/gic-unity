﻿using System;
using UnityEngine;

[Serializable]
public class SoundPreset
{
    public GameObject backpackSlot;
    public GameObject presetGo;
    public Gic_ProceduralAtomParameters visualParameters;
    public Gic_ProceduralAtomParameters visualParametersModX;
    public Gic_ProceduralAtomParameters visualParametersModY;
    public int presetIndex;

    public SoundPreset(GameObject backPackSlot, GameObject newPresetGo, int PresetIndex, Formen assignedForm)
    {
        backpackSlot = backPackSlot;
        presetGo = newPresetGo;
        presetIndex = PresetIndex;
        visualParameters = new Gic_ProceduralAtomParameters();
    }

    public SoundPreset()
    {
        backpackSlot = null;
        presetGo = null;
        presetIndex = 0;
        visualParameters = new Gic_ProceduralAtomParameters();
        visualParametersModX = new Gic_ProceduralAtomParameters();
        visualParametersModY = new Gic_ProceduralAtomParameters();
    }

    public SoundPreset (int _presetIndex) {
        // TODO: Simon ich benoetige eine version wo ich daten per index abfragen kann
        backpackSlot = null;
        presetGo = null;
        presetIndex = _presetIndex;
        visualParameters = new Gic_ProceduralAtomParameters();
        visualParametersModX = new Gic_ProceduralAtomParameters();
        visualParametersModY = new Gic_ProceduralAtomParameters();
    }

    public SoundPreset(SoundPreset paramIn)
    {
        backpackSlot = paramIn.backpackSlot;
        presetGo = paramIn.presetGo;
        presetIndex = paramIn.presetIndex;
        visualParameters = paramIn.visualParameters;
        visualParametersModX = paramIn.visualParametersModX;
        visualParametersModY = paramIn.visualParametersModY;
    }
}
