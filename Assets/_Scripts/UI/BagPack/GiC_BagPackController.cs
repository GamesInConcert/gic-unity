﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class GiC_BagPackController : MonoBehaviour
{
    private bool bagPackOpen = false;

    public bool bagPackIsOpen { get { return bagPackOpen; } }
    public bool UseGlobalColorSchemeColors = false;
    public BagpackStorageSlot[] StorageSlots;
    public SoundPreset[] soundPresets = new SoundPreset[3];
    private GameObject containers;
    //private GameObject mainCamera;

    private GameObject PresetContainer;

    private string localPath = "Assets/Resources/StoredObjects/";

    void OnEnable ()
	{
	    containers = transform.Find("Containers").gameObject;
        //mainCamera = GameObject.FindWithTag("MainCamera");
        containers.SetActive(bagPackOpen);
	}

    private void ExchangeSoundPresetColors()
    {
        int i = 0;
        foreach (var preset in soundPresets)
        {
            preset.visualParameters.colorA = GiC_LeapMakeHotspots.Instance.PlantColors[i * 4];
            preset.visualParameters.colorB = GiC_LeapMakeHotspots.Instance.PlantColors[i * 4 + 1];
            i++;
        }
    }

    void Start()
    {
    }

	void Update () {
        if (Input.GetKeyDown(KeyCode.B))
	    {
            Debug.Log("press B");
            if (!bagPackOpen)
	        {
                OpenBagPack();
             //   containers.SetActive(true);
             //   containers.transform.position = mainCamera.transform.position;
	            //containers.transform.rotation = Quaternion.AngleAxis(mainCamera.transform.eulerAngles.y, Vector3.up);

             //   PresetContainer = new GameObject();
	            //PresetContainer.transform.SetParent(this.transform);

             //   for (int i = 0; i<soundPresets.Length; i++)
	            //{
	            //        soundPresets[i].presetGo = CreatePresetObject(i);
             //           soundPresets[i].presetGo.transform.SetParent(PresetContainer.transform);
             //   }

	            //bagPackOpen = true;
	        }
	        else
	        {
                CloseBagPack();
             //   for (int i = 0; i < soundPresets.Length; i++)
             //   {                     
             //       Destroy(PresetContainer);
             //   }
             //   containers.SetActive(false);
	            //bagPackOpen = false;
	        }
	    }
	}

    public void SetBagPackOpen(bool state) {
        if (state != bagPackIsOpen) {
            if (state)
                OpenBagPack();
            else
                CloseBagPack();
        }
    }

    public void ToggleBagPack()
    {
        if (!bagPackIsOpen)
            OpenBagPack();
        else
            CloseBagPack();
    }

    public void OpenBagPack()
    {
        if (UseGlobalColorSchemeColors)
            ExchangeSoundPresetColors();

        containers.SetActive(true);
        containers.transform.position = Camera.main.transform.position;
        containers.transform.rotation = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up);

        PresetContainer = new GameObject();
        PresetContainer.transform.SetParent(this.transform);

        for (int i = 0; i < soundPresets.Length; i++)
        {
            soundPresets[i].presetGo = CreatePresetObject(i);
            soundPresets[i].presetGo.transform.SetParent(PresetContainer.transform);
            //soundPresets[i].presetGo.transform.rotation = Quaternion.identity;
        }

        bagPackOpen = true;
    }

    public void CloseBagPack()
    {
        for (int i = 0; i < soundPresets.Length; i++)
        {
            Destroy(PresetContainer);
        }
        containers.SetActive(false);
        bagPackOpen = false;
    }

    /// <summary>
    /// Creates and initializes a Blank Atom with a preset from presetindex
    /// </summary>
    /// <param name="presetIndex">The Preset to be used to create the new Atom</param>
    /// <returns>new Atom as GameObject</returns>
    GameObject CreatePresetObject(int presetIndex)
    {
        //SoundingObject_PickAndPlace pnp;
       // int newSoundingObjectNumber = GiC_SoundingObjectsManager.instance.existingSoundingObjects.Count + 1;

        string goName = "SoundPreset_" + presetIndex;
        var newAtom = new GameObject(goName);
        newAtom.transform.position = soundPresets[presetIndex].backpackSlot.transform.position;
        newAtom.transform.rotation = Quaternion.identity; // soundPresets[presetIndex].backpackSlot.transform.rotation;

        // Add CoreScript and initialize Atom.
        newAtom.AddComponent<GiC_ProceduralAtom>().Create(soundPresets[presetIndex], soundPresets[presetIndex].presetIndex, this, true);

        //pnp = returnGO.AddComponent<SoundingObject_PickAndPlace>();
        //pnp.bpController = this;
        //pnp.SetObjectNumber(newSoundingObjectNumber);
        //pnp.initialSoundPreset = soundPresets[presetIndex].presetIndex;

        return newAtom;
    }
}
