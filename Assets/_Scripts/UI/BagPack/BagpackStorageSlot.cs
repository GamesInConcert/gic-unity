﻿using System;
using UnityEngine;

[Serializable]
public class BagpackStorageSlot
{
    public GameObject bagPackSlot;
    public GameObject storedObject;
    public BoxCollider bagPackSlotCollider { get { return bagPackSlot.GetComponent<BoxCollider>(); } }

    public BagpackStorageSlot()
    {
        bagPackSlot = null;
        storedObject = null;
    }
}
