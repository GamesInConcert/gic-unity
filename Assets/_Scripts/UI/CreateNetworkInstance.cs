﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using WorldspaceController;

public class CreateNetworkInstance : MonoBehaviour {

    public GameObject networkObjectPrefab;

    public void Awake () {
        GetComponent<Button>().onClick.AddListener(CreateObject);
    }

    NetworkPlayerController localPlayer = null;
	// Update is called once per frame

    void CreateObject () {
        if (localPlayer == null) {
            localPlayer = NetworkPlayerController.GetLocalNetworkPlayer();
        }
        if (localPlayer != null) { 
            Vector3 pos = transform.position - transform.forward * 0.1f;
            localPlayer.CmdCreateNetworkObject(pos, Quaternion.identity, networkObjectPrefab.name);
        } else {
            Debug.LogWarning("CreateNetworkInstance.CreateObject: network not yet initialized!");
        }
    }
}
