// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GiC/AnimatedBrush Distance Double Fade"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Metallic("Metallic", Range( 0 , 1)) = 0.2
		_Smoothness("Smoothness", Range( 0 , 1)) = 0.2
		_Albedo("Albedo", 2D) = "white" {}
		_Sparkle("Sparkle", 2D) = "white" {}
		_gradient("gradient", 2D) = "white" {}
		_Pow("Pow", Range( 0 , 5)) = 0.2
		_Sparkle_Multiplier("Sparkle_Multiplier", Range( 0 , 5)) = 0.2
		_MKGlowTex("MKGlowTex", 2D) = "black" {}
		_FadeClamp("FadeClamp", Range( 0 , 1)) = 0
		_DistanceMultiplier("DistanceMultiplier", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Off
		ZTest LEqual
		Blend One One , SrcAlpha OneMinusSrcAlpha
		BlendOp Add , Add
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 4.6
		#pragma multi_compile_instancing
		#pragma only_renderers d3d11 
		#pragma surface surf Standard keepalpha noshadow exclude_path:deferred nolightmap  nodirlightmap 
		struct Input
		{
			float4 vertexColor : COLOR;
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform float _DistanceMultiplier;
		uniform float _FadeClamp;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _Pow;
		uniform sampler2D _MKGlowTex;
		uniform float _Sparkle_Multiplier;
		uniform sampler2D _Sparkle;
		uniform float4 _Sparkle_ST;
		uniform sampler2D _gradient;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 temp_output_113_0 = (i.vertexColor).rgb;
			float grayscale109 = Luminance(temp_output_113_0);
			float3 temp_cast_0 = (grayscale109).xxx;
			float4 ase_vertex4Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 temp_output_100_0 = mul( unity_ObjectToWorld, ase_vertex4Pos );
			float clampResult105 = clamp( ( distance( temp_output_100_0 , float4( _WorldSpaceCameraPos , 0.0 ) ) * _DistanceMultiplier ) , 0.0 , _FadeClamp );
			float3 lerpResult108 = lerp( temp_output_113_0 , temp_cast_0 , clampResult105);
			float4 appendResult116 = (float4(lerpResult108 , i.vertexColor.a));
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = ( appendResult116 * tex2D( _Albedo, uv_Albedo ) ).xyz;
			float2 uv_TexCoord78 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 panner120 = ( uv_TexCoord78 + _Time.x * float2( 1.2,0 ));
			float2 uv_Sparkle = i.uv_texcoord * _Sparkle_ST.xy + _Sparkle_ST.zw;
			float2 panner77 = ( uv_TexCoord78 + _Time.x * float2( 1,0 ));
			float blendOpSrc96 = tex2D( _Sparkle, uv_Sparkle ).a;
			float blendOpDest96 = tex2D( _gradient, panner77 ).a;
			float4 blendOpSrc80 = ( _Pow * ( appendResult116 * tex2D( _MKGlowTex, panner120 ).a ) );
			float4 blendOpDest80 = ( _Sparkle_Multiplier * ( appendResult116 * ( saturate( ( 1.0 - ( ( 1.0 - blendOpDest96) / blendOpSrc96) ) )) ) );
			o.Emission = ( blendOpSrc80 + blendOpDest80 ).xyz;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			float4 tex2DNode117 = tex2D( _Albedo, uv_Albedo );
			o.Alpha = tex2DNode117.a;
			clip( tex2DNode117.a - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14401
202;92;1353;775;1344.609;535.9968;1.6;True;False
Node;AmplifyShaderEditor.ObjectToWorldMatrixNode;99;-953.2199,-1346.153;Float;False;0;1;FLOAT4x4;0
Node;AmplifyShaderEditor.PosVertexDataNode;98;-942.2288,-1191.316;Float;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;-726.4431,-1213.284;Float;False;2;2;0;FLOAT4x4;0.0;False;1;FLOAT4;0.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;97;-887.0247,-943.1313;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.VertexColorNode;12;-1008.163,-684.592;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;103;-390.397,-1084.832;Float;False;Property;_DistanceMultiplier;DistanceMultiplier;10;0;Create;True;0;0.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;112;-540.7369,-1476.285;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;91;-1030.601,18.20052;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;78;-1014.099,431.5999;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;104;-180.3756,-1231.023;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;113;-825.3239,-687.8001;Float;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;106;-180.9109,-1539.982;Float;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-210.7158,-1455.704;Float;False;Property;_FadeClamp;FadeClamp;9;0;Create;True;0;0.856;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;105;1.655829,-1251.143;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;77;-737.0989,431.4;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,0;False;1;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCGrayscale;109;-609.8862,-626.2934;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;74;-332.6989,301.7997;Float;True;Property;_Sparkle;Sparkle;4;0;Create;True;None;27d4725c90c51bb4ba35ea4cf5b4a9b7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;108;-390.1352,-754.5131;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0.0,0,0,0;False;2;FLOAT;0.0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;75;-350.2989,496.9999;Float;True;Property;_gradient;gradient;5;0;Create;True;None;27d4725c90c51bb4ba35ea4cf5b4a9b7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;120;-743.8074,265.6032;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1.2,0;False;1;FLOAT;0.0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;72;-499.0989,20.1999;Float;True;Property;_MKGlowTex;MKGlowTex;8;0;Create;True;None;27d4725c90c51bb4ba35ea4cf5b4a9b7;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;96;-24.40107,332.4004;Float;False;ColorBurn;True;2;0;FLOAT;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;116;-204.4937,-619.4774;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;26;483.3104,-483.7589;Float;False;Property;_Pow;Pow;6;0;Create;True;0.2;0.8;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;16.10104,144.9999;Float;False;2;2;0;FLOAT4;0.0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;82;476.8199,-399.1181;Float;False;Property;_Sparkle_Multiplier;Sparkle_Multiplier;7;0;Create;True;0.2;2.13;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;246.9012,314.515;Float;False;2;2;0;FLOAT4;0.0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;70;790.2199,-147.1103;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT4;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;83;790.9562,-47.29453;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT4;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;4;-495.9551,-189.8637;Float;True;Property;_Albedo;Albedo;3;0;Create;True;None;b883dac42542ac440b23667224be172f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;73;947.8356,489.8087;Float;False;Property;_Smoothness;Smoothness;2;0;Create;True;0.2;0.339;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;102;-345.7183,-1204.448;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-26.68513,-219.3591;Float;False;2;2;0;FLOAT4;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;50;951.9649,409.6045;Float;False;Property;_Metallic;Metallic;1;0;Create;True;0.2;0.452;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;101;-535.3478,-1213.945;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;117;933.5194,202.349;Float;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;None;b883dac42542ac440b23667224be172f;True;0;False;white;Auto;False;Instance;4;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;80;1003.273,-153.9694;Float;False;LinearDodge;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1268.325,-193.9743;Float;False;True;6;Float;ASEMaterialInspector;0;0;Standard;GiC/AnimatedBrush Distance Double Fade;False;False;False;False;False;False;True;False;True;False;False;False;False;False;False;False;True;Off;0;3;False;0;0;False;0;Custom;0.5;True;False;0;True;TransparentCutout;;Transparent;ForwardOnly;False;True;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;0;4;10;25;False;0.5;False;4;One;One;2;SrcAlpha;OneMinusSrcAlpha;Add;Add;0;False;0.48;1,1,1,1;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0.0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;89;-2665.899,-35.80005;Float;False;100;100;Comment;0;;1,1,1,1;0;0
WireConnection;100;0;99;0
WireConnection;100;1;98;0
WireConnection;112;0;100;0
WireConnection;112;1;97;0
WireConnection;104;0;112;0
WireConnection;104;1;103;0
WireConnection;113;0;12;0
WireConnection;105;0;104;0
WireConnection;105;1;106;0
WireConnection;105;2;107;0
WireConnection;77;0;78;0
WireConnection;77;1;91;1
WireConnection;109;0;113;0
WireConnection;108;0;113;0
WireConnection;108;1;109;0
WireConnection;108;2;105;0
WireConnection;75;1;77;0
WireConnection;120;0;78;0
WireConnection;120;1;91;1
WireConnection;72;1;120;0
WireConnection;96;0;74;4
WireConnection;96;1;75;4
WireConnection;116;0;108;0
WireConnection;116;3;12;4
WireConnection;69;0;116;0
WireConnection;69;1;72;4
WireConnection;81;0;116;0
WireConnection;81;1;96;0
WireConnection;70;0;26;0
WireConnection;70;1;69;0
WireConnection;83;0;82;0
WireConnection;83;1;81;0
WireConnection;102;0;101;0
WireConnection;102;1;101;0
WireConnection;24;0;116;0
WireConnection;24;1;4;0
WireConnection;101;0;100;0
WireConnection;101;1;97;0
WireConnection;80;0;70;0
WireConnection;80;1;83;0
WireConnection;0;0;24;0
WireConnection;0;2;80;0
WireConnection;0;3;50;0
WireConnection;0;4;73;0
WireConnection;0;9;117;4
WireConnection;0;10;117;4
ASEEND*/
//CHKSM=EEE4DDE5DAA2050095C7CD98F4C8D5A798DBFB3A