#if !defined(GIC_GS_INCLUDED)
#define GIC_GS_INCLUDED

#include "GiC Base Geometry Defines.cginc"
#include "UnityCG.cginc"

sampler2D _TerrainHeights;
sampler2D _NoiseTexture2;
float4 _PlantDistributionModifier;
float _MinRadius, _MaxRadius, _MinScale, _MaxScale;
float _MinVoronoi, _MaxVoronoi;
float4 _TerrainScale;
float _TerrainOffset;
float3 _WorldPos;
half4 _FlowerColors[13];
float _fi;

// A struct defining a triangle
//struct triData {
//	vData t[3];
//};
    
// Calculates the normal from three verteces
float3 CalculateNormal(float4 v0, float4 v1, float4 v2) {
	return normalize(cross(v1.xyz - v0.xyz, v2.xyz - v0.xyz));
}

//// Returns a Triangle (triData)) given three vertexes. Also Calculates normals and maps uvs
//void MakeTriangle(float4 v1, float4 v2, float4 v3, fixed4 color, out vData o[3]) {				
//	o[0].vertex = v1;
//	o[1].vertex = v2;
//	o[2].vertex = v3;

//	float3 normal = CalculateNormal(v3, v2, v1);

//	o[0].normal = normal;
//	o[1].normal = normal;
//	o[2].normal = normal;

//	o[0].uv = float2(0.0f, 0.0f);
//	o[1].uv = float2(0.0f, 1.0f);
//	o[2].uv = float2(1.0f, 1.0f);

//	o[0].diffuse = color;
//	o[1].diffuse = color;
//	o[2].diffuse = color;
//}

// Returns a Triangle given three INITIALIZED vData Structs
//triData TriFromVert(vData data[3]) {
//	triData o;
//	o.t[0] = data[0];
//	o.t[1] = data[1];
//	o.t[2] = data[2];
//	return o;
//}

// Returns a point on a circle given radius of the circle and angle in degrees
float4 PointOnCircle3XZ(float radius, float angle) {
	return float4(radius*sin(angle*0.01745329f), 0.0f, radius*cos(angle*0.01745329f), 0.0f);
}

float random (in float2 st) { 
    return frac(sin(dot(st.xy,float2(12.9898,78.233))) * 43758.5453123);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation
    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return lerp(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

[instance(FlowerInstances)]
[maxvertexcount(16)]
void geom(point v2g p[1], in uint InstanceID : SV_GSInstanceID, inout TriangleStream<g2f> triStream)
{


    float voronoi = p[0].voronoi;
    if (voronoi < _MaxVoronoi && voronoi > _MinVoronoi+(p[0].pos.y*0.01))
    {
        //voronoi *= 0.2;
        vData v = { float4(0.0f, 0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f), float2(0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

        
        float terrainHeight = tex2Dlod(_TerrainHeights, float4((_WorldPos.xz + p[0].pos.xz) / _TerrainScale.xy, 0, 0)).r;
        float2 noise = tex2Dlod(_NoiseTexture2, float4(p[0].uv.xy, 0, 0)).rg;
        //int plantChance = ceil(noise.x - _PlantDistributionModifier.x) + ceil(noise.y - _PlantDistributionModifier.y) + ceil(noise.x - _PlantDistributionModifier.z);
        int plantChance =  _PlantDistributionModifier.x + _PlantDistributionModifier.y + _PlantDistributionModifier.z;
        //float4 newColor = _FlowerColors[0];

        float radiusSteps = (_MaxRadius - _MinRadius) / FlowerInstances;
        float sizeSteps = (_MaxScale - _MinScale) / FlowerInstances;
        //float colorSteps = 1.0 / FlowerInstances;

        //float4 posi = p[0].pos + float4(sin(InstanceID * GOLDEN_ANGLE), 0.0, cos(InstanceID * GOLDEN_ANGLE), 0.0) * (InstanceID * radiusSteps);
        float4 posi = p[0].pos;
        posi.y = (terrainHeight * _TerrainScale.z)-_TerrainOffset;
        float doublenoise = noise.x * noise.y;
        float3 rot = float3(cos(doublenoise) * InstanceID * 13.5f, (GOLDEN_ANGLE * InstanceID) + (noise.x * noise.y * 30.0), sin(doublenoise) * InstanceID * 11.324f);
        float xyScale = (_MaxScale - (sizeSteps * InstanceID));

        int forLoopCount = 16;
        
        vData plantData[16];

        if (plantChance == 0)
        {
            // Crystals
            xyScale *= 300.0;
            BlenderShapeA3(plantData);
        }
        else if (plantChance == 1)
        {
            xyScale *= 1.0;
            forLoopCount = 12;
            Tetrahedron(plantData);
        }
        else
        {
            xyScale *= 550.0;
            BlenderShapeC1(plantData);
            rot.x += 90;
            rot.z += 45;
        }

        float myFi = _fi - (atan2(posi.x, posi.z) + PI);
        myFi += PI;
        myFi = abs(myFi % (2.0 * PI));
        myFi -= PI;
        myFi = abs(myFi);
        myFi *= 0.05;

        xyScale *= (1.0 + myFi);

        float dist = clamp(dot(_WorldPos - _WorldSpaceCameraPos, _WorldPos - _WorldSpaceCameraPos)*0.01, 0.0, 1.0);
        float4 newColor = lerp(_FlowerColors[plantChance], _FlowerColors[plantChance + 3], voronoi * (1.0 - p[0].mod.y));
        newColor = lerp(newColor, dot(newColor, float3(0.299, 0.587, 0.114)), dist);
			
        g2f i;
			
        float voronoiSizeMod = -(cos((-PI * voronoi)) * p[0].pos.y * PI) + sin((0 - p[0].mod.x) * PI + (PI * 0.5));
        float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(1, voronoiSizeMod, voronoi) + _MinScale;
        //float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(voronoiSizeMod, 0.1, voronoi) + _MinScale;
    
        // Matrices

        float4x4 rotateY =
        {
        cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 scaleMat =
        {
        scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 rotScale = mul(rotateY, scaleMat);

        UNITY_INITIALIZE_OUTPUT(g2f, i);
        for (int vert = 0; vert < 16; vert++)
        {
            if (vert > forLoopCount)
            {
                continue;
            }
            v = plantData[vert];
            float4 vertex = mul(rotScale, v.vertex);
            float3 normal = mul(rotScale, v.normal);
            vertex = vertex + posi;
            i.pos = UnityObjectToClipPos(vertex);
            i.coords = vertex.xyz;
            i.worldPos = mul(unity_ObjectToWorld, vertex) + _WorldPos;
            i.normal = UnityObjectToWorldNormal(normal);
                        
#if defined(BINORMAL_PER_FRAGMENT)
		    i.tangent =     float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
#else
            i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
            i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
#endif

            i.tint = newColor;

            i.uv.xy = v.uv;
            i.uv.zw = v.uv;

            ComputeVertexLightColor(i);
            TRANSFER_VERTEX_TO_FRAGMENT(i);
		    UNITY_TRANSFER_FOG(i, i.pos);
            TRANSFER_SHADOW(i);

            triStream.Append(i);
        }
        triStream.RestartStrip();
    }

}
#endif