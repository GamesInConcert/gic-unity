﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

// Thread Group size
#define thread_group_size_x 8
#define thread_group_size_y 8
#define thread_group_size_z 1

#define group_size_x 4
#define group_size_y 4
#define group_size_z 1

struct voronoiData
{
	float3 pos;
    float2 mod;
	float voronoi;
};

struct HotPos{
	float3 pos;
    float2 mod;
    int active;
};

Texture2D _NoiseTex;
SamplerState sampler_NoiseTex;

float gridDistance;
float minowskyMix; 
float noiseMultiplier;
RWStructuredBuffer<voronoiData> outputBuffer;
RWStructuredBuffer<HotPos> HotspotPositions;

int GetIDX (uint3 id) 
{
int returnVal = (id.x + (id.y * thread_group_size_x * group_size_x) + (id.z * thread_group_size_x * group_size_y * thread_group_size_y * group_size_z));
return returnVal;
}

float Distance_minkowski(float2 a, float2 b, float mix)
{
    return pow(pow(abs(a.x - b.x), mix) + pow(abs(a.y - b.y), mix), 1.0f / mix);
}

float random(in float2 st)
{
    return frac(sin(dot(st.xy, float2(12.9898, 78.233))) * 43758.5453123);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float simplexNoise(in float2 st)
{
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f * f * (3.0 - 2.0 * f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

[numthreads(thread_group_size_x, thread_group_size_y, thread_group_size_z)]
void CSMain (uint3 grpID : SV_GroupID, uint3 id : SV_DispatchThreadID, uint3 grpThreadID : SV_GroupThreadID, uint groupIndex : SV_GroupIndex)
{
	//int idx = id.x + (id.y * thread_group_size_x * group_size_x) + (id.z * thread_group_size_x * group_size_y * thread_group_size_y * group_size_z);
	int idx = GetIDX(id);
	//uint hotspotsLength;
	//uint hotspotsStride;

	half minDist = 100.0f;
    //half2 ns = ((_NoiseTex.SampleLevel(sampler_NoiseTex, float2(0.15,0.15)+(id.xy/float2(256.0, 256.0)), 0).rg + half2(simplexNoise(id.xy), simplexNoise(id.xz)))* noiseMultiplier);
    float2 ns = (normalize((_NoiseTex.SampleLevel(sampler_NoiseTex, float2((0.05 + id.x) / 128.0, (0.05 + id.y) / 128.0), 0).rg) + half2(simplexNoise(id.xy), simplexNoise(id.xz))) * 2 - float2(1, 1));
    //ns -= float2(0.5 * noiseMultiplier, 0.5 * noiseMultiplier);
    ns = lerp(float2(1, 1), ns, noiseMultiplier);
    //half2 ns = half2(1, 1);
    half3 pos = half3((id.x * (gridDistance * ns.x)) - (16.0 * gridDistance * ns.x), id.z, (id.y * (gridDistance * ns.y)) - (16.0 * gridDistance * ns.y));
    //pos += ns*noiseMultiplier;
    //pos += half2(simplexNoise(id.xy), simplexNoise(id.xz))* noiseMultiplier;
	//HotspotPositions.GetDimensions(hotspotsLength, hotspotsStride);

    int myHotspot = 0;    

    [allow_uav_condition]
    for (int i = 0; i < 21; i++)
    {
        if (HotspotPositions[i].active == 0)
        {
            continue;
        }
        
        half dist = Distance_minkowski(pos.xz, HotspotPositions[i].pos.xy, minowskyMix);
        
        if (dist < minDist) {
            minDist = dist;
            myHotspot = i;
        }
        //minDist = min(dist, minDist);
    }
    pos.y = HotspotPositions[myHotspot].pos.z;
    
    float newVor = minDist * 0.15f;
    newVor = 1.0 - (newVor);
	//outputBuffer[idx].voronoi = minDist * 0.1f * HotspotPositions[myHotspot].pos.z;#
    outputBuffer[idx].voronoi = newVor;
	outputBuffer[idx].pos = pos;
    outputBuffer[idx].mod = HotspotPositions[myHotspot].mod;

}
