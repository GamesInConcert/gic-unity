﻿Shader "Custom/GiC_Plants Advanced Single Instance" {
	Properties {
		_Tint ("Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Albedo", 2D) = "white" {}

		[NoScaleOffset] _NormalMap ("Normals", 2D) = "bump" {}
		_BumpScale ("Bump Scale", Float) = 1

		[NoScaleOffset] _MetallicMap ("Metallic", 2D) = "white" {}
		[Gamma] _Metallic ("Metallic", Range(0, 1)) = 0
		_Smoothness ("Smoothness", Range(0, 1)) = 0.1

		[NoScaleOffset] _OcclusionMap ("Occlusion", 2D) = "white" {}
		_OcclusionStrength("Occlusion Strength", Range(0, 1)) = 1

		[NoScaleOffset] _EmissionMap ("Emission", 2D) = "black" {}
		_Emission ("Emission", Color) = (0, 0, 0)

		[NoScaleOffset] _DetailMask ("Detail Mask", 2D) = "white" {}
		_DetailTex ("Detail Albedo", 2D) = "gray" {}
		[NoScaleOffset] _DetailNormalMap ("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale ("Detail Bump Scale", Float) = 1

		[NoScaleOffset] _TerrainHeights ("Terrain Heightmap", 2D) = "black" {}
		_TerrainScale("Terrain Scale", Vector) = (512, 512, 100, 0)
		_TerrainOffset("Terrain Offset", Float) = -18.0

        [NoScaleOffset] _NoiseTexture2 ("Noise Texture2", 2D) = "black" {}
        _PlantDistributionModifier ("Plant Distribution Modifier", Vector) = (0.6, 0.15, 0.25, 0)

		_fi("Fi", Float) = 0.0

		_Cutoff("Alpha Cutoff", Range(0,1)) = 0.1
		_Size ("Size", Range(0,10)) = 1.0
		_Height("Height", Range(0,10)) = 2.0

		_MinScale("MinScale", Float) = 0.2
		_MaxScale("MaxScale", Float) = 1.0

		_MinVoronoi("MinVor", Float) = 0.2
		_MaxVoronoi("MaxVor", Float) = 0.2
		
	}

	CGINCLUDE

	#define GOLDEN_ANGLE 2.39982768056
	#define PI 3.1415926535

	ENDCG

	SubShader {

	Pass {
		Tags{ "LightMode" = "ForwardBase" }
		
		Cull Off
		//Lighting On
        //Cull Off

		CGPROGRAM

			#pragma target 5.0

			#pragma shader_feature _METALLIC_MAP
			#pragma shader_feature _ _SMOOTHNESS_ALBEDO _SMOOTHNESS_METALLIC
			#pragma shader_feature _NORMAL_MAP
			#pragma shader_feature _OCCLUSION_MAP
			#pragma shader_feature _EMISSION_MAP
			#pragma shader_feature _DETAIL_MASK
			#pragma shader_feature _DETAIL_ALBEDO_MAP
			#pragma shader_feature _DETAIL_NORMAL_MAP

			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile_fwdbase

            #define FORWARD_BASE_PASS

			#pragma vertex vertPos
			#pragma geometry geom
			#pragma fragment frag

            #include "GiC Lighting Include.cginc"
            #include "GiC Geometry Include Single.cginc"

		ENDCG
	}
    Pass {
		    Tags{ "LightMode" = "ForwardAdd" }
		
			Cull Off
			Blend One One
			ZWrite Off
	
		    CGPROGRAM

			    #pragma target 5.0

			    #pragma shader_feature _METALLIC_MAP
			    #pragma shader_feature _ _SMOOTHNESS_ALBEDO _SMOOTHNESS_METALLIC
			    #pragma shader_feature _NORMAL_MAP
		        #pragma shader_feature _DETAIL_MASK
			    #pragma shader_feature _DETAIL_ALBEDO_MAP
			    #pragma shader_feature _DETAIL_NORMAL_MAP
			
                #pragma multi_compile_fwdadd_fullshadows
			  
			    #pragma vertex vertPos
			    #pragma geometry geom
			    #pragma fragment frag


                #include "GiC Lighting Include.cginc"
                #include "GiC Geometry Include Single.cginc"
			   
		    ENDCG
	    }
		Pass {
			Tags {
				"LightMode" = "ShadowCaster"
			}
			Cull Off
			CGPROGRAM

			#pragma target 5.0

			#pragma vertex shadowVertPos
            #pragma geometry shadowGeom
			#pragma fragment shadowFrag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_shadowcaster

			#include "GiC Shadows Single.cginc"

			ENDCG
		}
    }
    CustomEditor "GiCPlantsAdvancedSingleGUI"
}
