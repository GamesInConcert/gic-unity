﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

#if !defined(GIC_LIGHTING_INCLUDED)
#define GIC_LIGHTING_INCLUDED

#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

// Uniforms
float4 _Tint;
sampler2D _MainTex, _DetailTex, _DetailMask;
float4 _MainTex_ST, _DetailTex_ST;

sampler2D _NormalMap, _DetailNormalMap;
float _BumpScale, _DetailBumpScale;

sampler2D _MetallicMap;
float _Metallic;
float _Smoothness;

sampler2D _OcclusionMap;
float _OcclusionStrength;

sampler2D _EmissionMap;
float3 _Emission;

half _Size;
half _Height;
uniform fixed _Cutoff;

// Structures
// Vertex to Geometry 
struct v2g {
	float4 pos : SV_POSITION;
	float4 col : COLOR;
    float4 uv : TEXCOORD0;
	float voronoi : PSIZE;
    float4 mod : TEXCOORD1;
};

// Geometry to fragment
struct g2f {
	float4 pos : SV_POSITION;
	float4 uv : TEXCOORD0;
	float3 normal : TEXCOORD1;
    float4 tint: COLOR;

    float3 coords : TEXCOORD9;

	#if defined(BINORMAL_PER_FRAGMENT)
	    float4 tangent : TEXCOORD2;
	#else
	    float3 tangent : TEXCOORD2;
	    float3 binormal : TEXCOORD3;
	#endif

	float3 worldPos : TEXCOORD4;
    LIGHTING_COORDS(7,8)
	//SHADOW_COORDS(5)

	#if defined(VERTEXLIGHT_ON)
	    float3 vertexLightColor : TEXCOORD6;
	#endif
};

// ComputeBuffer Struct for VertexShader
struct data {
	float3 pos;	
    float2 mod;
	float voronoi;
};

StructuredBuffer<data> buf_Points;

// Getters for the various textures

float GetDetailMask (g2f i) {
	#if defined (_DETAIL_MASK)
		return tex2D(_DetailMask, i.uv.xy).a;
	#else
		return 1;
	#endif
}

float3 GetAlbedo (g2f i) {
	float3 albedo = tex2D(_MainTex, i.uv.xy).rgb * i.tint.rgb;
	#if defined (_DETAIL_ALBEDO_MAP)
		float3 details = tex2D(_DetailTex, i.uv.zw) * unity_ColorSpaceDouble;
		albedo = lerp(albedo, albedo * details, GetDetailMask(i));
	#endif
	return albedo;
}

float3 GetAlbedoTriplanar(g2f i){
    half3 blend = abs(i.normal);
    // make sure the weights sum up to 1 (divide by sum of x+y+z)
    blend /= dot(blend,1.0);
    // read the three texture projections, for x,y,z axes
    fixed3 cx = tex2D(_MainTex, i.coords.yz);
    fixed3 cy = tex2D(_MainTex, i.coords.xz);
    fixed3 cz = tex2D(_MainTex, i.coords.xy);
    // blend the textures based on weights
    return cx * blend.x + cy * blend.y + cz * blend.z;
}

float GetMetallic (g2f i) {
	#if defined(_METALLIC_MAP)
		return tex2D(_MetallicMap, i.uv.xy).a;
	#else
		return _Metallic;
	#endif
}

float GetSmoothness (g2f i) {
	float smoothness = 1;
	#if defined(_SMOOTHNESS_ALBEDO)
		smoothness = tex2D(_MainTex, i.uv.xy).a;
	#elif defined(_SMOOTHNESS_METALLIC) && defined(_METALLIC_MAP)
		smoothness = tex2D(_MetallicMap, i.uv.xy).a;
	#endif
	return smoothness * _Smoothness;
}

float GetOcclusion (g2f i) {
	#if defined(_OCCLUSION_MAP)
		return lerp(1, tex2D(_OcclusionMap, i.uv.xy).g, _OcclusionStrength);
	#else
		return 1;
	#endif
}

float3 GetEmission (g2f i) {
	#if defined(FORWARD_BASE_PASS)
		#if defined(_EMISSION_MAP)
			return tex2D(_EmissionMap, i.uv.xy) * _Emission * (i.tint*0.5);
		#else
			return _Emission;
		#endif
	#else
		return 0;
	#endif
}

float3 BoxProjection (
	float3 direction, float3 position,
	float4 cubemapPosition, float3 boxMin, float3 boxMax
) {
	#if UNITY_SPECCUBE_BOX_PROJECTION
		UNITY_BRANCH
		if (cubemapPosition.w > 0) {
			float3 factors =
				((direction > 0 ? boxMax : boxMin) - position) / direction;
			float scalar = min(min(factors.x, factors.y), factors.z);
			direction = direction * scalar + (position - cubemapPosition);
		}
	#endif
	return direction;
}

UnityLight CreateLight (g2f i) {
	UnityLight light;

	#if defined(POINT) || defined(POINT_COOKIE) || defined(SPOT)
		light.dir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos);
	#else
		light.dir = _WorldSpaceLightPos0.xyz;
	#endif

	UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
	
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

void ComputeVertexLightColor (inout g2f i) {
	#if defined(VERTEXLIGHT_ON)
		i.vertexLightColor = Shade4PointLights(
			unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
			unity_LightColor[0].rgb, unity_LightColor[1].rgb,
			unity_LightColor[2].rgb, unity_LightColor[3].rgb,
			unity_4LightAtten0, i.worldPos, i.normal
		);
	#endif
}

float3 GetTangentSpaceNormal (g2f i) {
	float3 normal = float3(0, 0, 1);
	#if defined(_NORMAL_MAP)
		normal = UnpackScaleNormal(tex2D(_NormalMap, i.uv.xy), _BumpScale);
	#endif
	#if defined(_DETAIL_NORMAL_MAP)
		float3 detailNormal =
			UnpackScaleNormal(
				tex2D(_DetailNormalMap, i.uv.zw), _DetailBumpScale
			);
		detailNormal = lerp(float3(0, 0, 1), detailNormal, GetDetailMask(i));
		normal = BlendNormals(normal, detailNormal);
	#endif
	return normal;
}

float3 CreateBinormal (float3 normal, float3 tangent, float binormalSign) {
	return cross(normal, tangent.xyz) *
		(binormalSign * unity_WorldTransformParams.w);
}

void InitializeFragmentNormal(inout g2f i) {
	float3 tangentSpaceNormal = GetTangentSpaceNormal(i);
	#if defined(BINORMAL_PER_FRAGMENT)
		float3 binormal = CreateBinormal(i.normal, i.tangent.xyz, i.tangent.w);
	#else
		float3 binormal = i.binormal;
	#endif
	
	i.normal = normalize(tangentSpaceNormal.x * i.tangent + tangentSpaceNormal.y * binormal + tangentSpaceNormal.z * i.normal );
}

UnityIndirect CreateIndirectLight (g2f i, float3 viewDir) {
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;

	#if defined(VERTEXLIGHT_ON)
		indirectLight.diffuse = i.vertexLightColor;
	#endif

	#if defined(FORWARD_BASE_PASS)
		indirectLight.diffuse += max(0, ShadeSH9(float4(i.normal, 1)));
		float3 reflectionDir = reflect(-viewDir, i.normal);
		Unity_GlossyEnvironmentData envData;
		envData.roughness = 1 - GetSmoothness(i);
		envData.reflUVW = BoxProjection(
			reflectionDir, i.worldPos,
			unity_SpecCube0_ProbePosition,
			unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax
		);
		float3 probe0 = Unity_GlossyEnvironment(
			UNITY_PASS_TEXCUBE(unity_SpecCube0), unity_SpecCube0_HDR, envData
		);
		envData.reflUVW = BoxProjection(
			reflectionDir, i.worldPos,
			unity_SpecCube1_ProbePosition,
			unity_SpecCube1_BoxMin, unity_SpecCube1_BoxMax
		);
		#if UNITY_SPECCUBE_BLENDING
			float interpolator = unity_SpecCube0_BoxMin.w;
			UNITY_BRANCH
			if (interpolator < 0.99999) {
				float3 probe1 = Unity_GlossyEnvironment(
					UNITY_PASS_TEXCUBE_SAMPLER(unity_SpecCube1, unity_SpecCube0),
					unity_SpecCube0_HDR, envData
				);
				indirectLight.specular = lerp(probe1, probe0, interpolator);
			}
			else {
				indirectLight.specular = probe0;
			}
		#else
			indirectLight.specular = probe0;
		#endif

		float occlusion = GetOcclusion(i);
		indirectLight.diffuse *= occlusion;
		indirectLight.specular *= occlusion;
	#endif

	return indirectLight;
}

v2g vert(uint id : SV_VertexID) {
	v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    o.pos = mul (unity_ObjectToWorld,buf_Points[id].pos);
	o.voronoi = buf_Points[id].voronoi;
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
	o.col = float4(1.0f, 1.0f, 1.0f, 1.0f);
	return o;
}

v2g vertPos(uint id : SV_VertexID, float4 pos : POSITION, float4 uv0 : TEXCOORD0)
{
    v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    //o.pos = mul(unity_ObjectToWorld, pos + buf_Points[id].pos);
    o.pos = (float4(buf_Points[id].pos,0.0));
    o.voronoi = buf_Points[id].voronoi;
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
    o.col = float4(1.0f, 1.0f, 1.0f, 1.0f);
    o.uv = uv0;
    return o;
}

float4 frag(g2f i) : COLOR {
    InitializeFragmentNormal(i);
    float3 normal = i.normal;
    //float3 lightDir = _WorldSpaceLightPos0.xyz;
    float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);

	float3 specularTint;
	float oneMinusReflectivity;
	float3 albedo = DiffuseAndSpecularFromMetallic(GetAlbedo(i), GetMetallic(i), specularTint, oneMinusReflectivity);

	float4 color = UNITY_BRDF_PBS(albedo, specularTint,	oneMinusReflectivity, GetSmoothness(i),	i.normal, viewDir,CreateLight(i), CreateIndirectLight(i, viewDir));
    //float4 color = LIGHT_ATTENUATION(i);
	color.rgb += GetEmission(i);

    return color;

}
#endif