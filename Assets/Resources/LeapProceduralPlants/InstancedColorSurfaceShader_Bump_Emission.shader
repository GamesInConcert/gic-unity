﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "GiC/InstancedColorSurfaceShader_Bump_Emission" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_EmissionColor("EmissionColor", Color) = (1,1,1,1)
		_EmissionMap("Emission (RGB)", 2D) = "white" {}
		_EmissionIntensity("EmissionIntensity", Float) = 0.0
		_myPos("ObjectPosition", Vector) = (0, 0, 0, 0)
		_fiIn("FiIn", Float) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard noforwardadd vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _EmissionMap;

		struct v2f {
			fixed4 color;
		};

		struct Input {
			float2 uv_MainTex;
			float2 uv_EmissionMap;
			float3 viewDir;
			float dist;
			float fi;
		};

		half _Glossiness;
		half _Metallic;
		half _EmissionIntensity;
		fixed4 _EmissionColor;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
			UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
			UNITY_DEFINE_INSTANCED_PROP(float4, _myPos)
			UNITY_DEFINE_INSTANCED_PROP(float, _fiIn)
//#define _Color_arr Props
		UNITY_INSTANCING_BUFFER_END(Props)

		float GetFi(float fiIn, float3 objPos)
		{
			return abs(fmod(abs(fiIn - atan2(objPos.x, objPos.z)), 6.283185308) - 3.1415926535);
			//return abs(fmod(abs(fiIn + atan2(objPos.x, objPos.z)), 6.283185308) - 3.1415926535);
		}

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o)
			float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
			o.dist = clamp(dot(worldPos - _WorldSpaceCameraPos, worldPos - _WorldSpaceCameraPos)*0.001, 0.0, 0.5);
			o.fi = GetFi(UNITY_ACCESS_INSTANCED_PROP(Props, _fiIn), UNITY_ACCESS_INSTANCED_PROP(Props, _myPos.xyz));
			//o.color.rgb = lerp(v.color.rgb, dot(v.color.rgb, float3(0.299, 0.587, 0.114)), dist);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
			o.Albedo = lerp(c.rgb, dot(c.rgb, float3(0.299, 0.587, 0.114)), IN.dist);
			// Metallic and smoothness come from slider variables
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = tex2D(_EmissionMap, IN.uv_EmissionMap) * _EmissionColor * _EmissionIntensity * o.Albedo * (1.0 + IN.fi);
			//o.Emission = tex2D(_EmissionMap, IN.uv_EmissionMap) * pow(rim, _EmissionIntensity) *  _EmissionColor * o.Albedo;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
