

#if !defined(GiC_SHADOWS_INCLUDED)
#define GiC_SHADOWS_INCLUDED

#include "UnityCG.cginc"
#include "GiC Base Geometry Defines.cginc"

struct v2g {
	float4 pos : SV_POSITION;
    float4 uv : TEXCOORD0;
	float voronoi : PSIZE;
    float4 mod : TEXCOORD1;

};

struct SHADOW_VERTEX
{
	float4 vertex : POSITION; // has to be called this way because of unity macro
};

struct g2f {
    //float4 pos : SV_POSITION;
    V2F_SHADOW_CASTER;
};

struct data {
	float3 pos;	
    float2 mod;
	float voronoi;
};

float3 _WorldPos;
sampler2D _TerrainHeights;
sampler2D _NoiseTexture2;
float4 _PlantDistributionModifier;
float _MinRadius, _MaxRadius, _MinScale, _MaxScale;
float _MinVoronoi, _MaxVoronoi;
float4 _TerrainScale;
float _TerrainOffset;

StructuredBuffer<data> buf_Points;

v2g shadowVert(uint id : SV_VertexID) {
	v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    o.pos = mul (unity_ObjectToWorld,buf_Points[id].pos);
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
	o.voronoi = buf_Points[id].voronoi;
	return o;
}

v2g shadowVertPos(uint id : SV_VertexID, float4 pos : POSITION, float4 uv : TEXCOORD0)
{
    v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    o.pos = float4(buf_Points[id].pos, 0.0);
    //o.pos = mul(unity_ObjectToWorld, pos + buf_Points[id].pos);
    o.voronoi = buf_Points[id].voronoi;
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
    o.uv = uv;
    return o;
}

[instance(FlowerInstances)]
[maxvertexcount(16)]
void shadowGeom(point v2g p[1], in uint InstanceID : SV_GSInstanceID, inout TriangleStream<g2f> triStream) {
    //float voronoi = clamp(p[0].voronoi, 0.0, 1.0);
    float voronoi = p[0].voronoi;
    if (voronoi < _MaxVoronoi && voronoi > _MinVoronoi + (p[0].pos.y * 0.01))
    {
        //voronoi *= 0.2;
        vData vD = { float4(0.0f, 0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f), float2(0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

        float radiusSteps = (_MaxRadius - _MinRadius) / FlowerInstances;
        float sizeSteps = (_MaxScale - _MinScale) / FlowerInstances;

        float terrainHeight = tex2Dlod(_TerrainHeights, float4((_WorldPos.xz + p[0].pos.xz) / _TerrainScale.xy, 0, 0)).r;
        float xyScale = (_MaxScale - (sizeSteps * InstanceID));

        float2 noise = tex2Dlod(_NoiseTexture2, float4(p[0].uv.xy, 0, 0)).rg;
        int plantChance = ceil(noise.x - _PlantDistributionModifier.x) + ceil(noise.y - _PlantDistributionModifier.y) + ceil(noise.x - _PlantDistributionModifier.z);

        int forLoopCount = 16;
        
        vData plantData[16];

        //xyScale *= 500.0;
        
        
        if (plantChance == 0)
        {
            xyScale *= 350.0;
            BlenderShapeA3(plantData);
        }
        else if (plantChance == 1)
        {
            xyScale *= 1.0;
            forLoopCount = 12;
            Tetrahedron(plantData);
        }
        else
        {
            xyScale *= 650.0;
            BlenderShapeC1(plantData);
        }
			
        float4 posi = p[0].pos + float4(sin(InstanceID * GOLDEN_ANGLE), 0.0, cos(InstanceID * GOLDEN_ANGLE), 0.0) * (InstanceID * radiusSteps);
        //float4 posi = p[0].pos;
        posi.y = (terrainHeight * _TerrainScale.z)-_TerrainOffset;
        float doublenoise = noise.x * noise.y;
        float3 rot = float3(cos(doublenoise) * InstanceID * 13.5f, (GOLDEN_ANGLE * InstanceID) + (noise.x * noise.y * 30.0), sin(doublenoise) * InstanceID * 11.324f);
        
        //float voronoiSizeMod = (lerp(pow(p[0].pos.y * voronoi, (p[0].mod.y * 10) + 2.0), 1.0, voronoi) * 0.25);
        float voronoiSizeMod = -(cos((-PI * voronoi)) * p[0].pos.y * PI) + sin((0 - p[0].mod.x) * PI + (PI * 0.5));
        //float voronoiSizeMod = -(cos((-PI * voronoi)) * p[0].pos.y * PI) + sin((0 - p[0].mod.y) * PI + (PI * 0.5));
        float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(1, voronoiSizeMod, voronoi) + _MinScale;
    
        float4x4 rotateY =
        {
            cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 scaleMat =
        {
            scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 rotScale = mul(rotateY, scaleMat);
			
        g2f i;
        SHADOW_VERTEX v;
	//g2f tri[3];

        for (int vert = 0; vert < 16; vert++)
        {
            if (vert > forLoopCount)
            {
                continue;
            }
            vD = plantData[vert];
            float4 vertex = mul(rotScale, vD.vertex);
            vertex += posi;
            //v.vertex = mul(UNITY_MATRIX_MV, vertex);
            v.vertex = vertex;
            TRANSFER_SHADOW_CASTER(i);
            //TRANSFER_SHADOW(i);

            triStream.Append(i);
        }
        triStream.RestartStrip();
    }
}

float4 shadowFrag (g2f i) : COLOR {
    float4 color = fixed4(1,1,1,1);
    SHADOW_CASTER_FRAGMENT(i);
}

#endif