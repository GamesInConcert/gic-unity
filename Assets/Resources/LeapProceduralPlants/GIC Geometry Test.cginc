#if !defined(GIC_GS_INCLUDED)
#define GIC_GS_INCLUDED

#include "GiC Base Geometry Defines.cginc"
#include "UnityCG.cginc"

sampler2D _TerrainHeights;
sampler2D _NoiseTexture2;
float4 _PlantDistributionModifier;
float _MinRadius, _MaxRadius, _MinScale, _MaxScale;
float4 _FlowerColor1, _FlowerColor2, _FlowerColor3, _FlowerColor4;
float _MinVoronoi, _MaxVoronoi;
float4 _TerrainScale;
float _TerrainOffset;
float3 _WorldPos;
half4 _FlowerColors[13];

// A struct defining a triangle
struct triData {
	vData t[3];
};
    
// Calculates the normal from three verteces
float3 CalculateNormal(float4 v0, float4 v1, float4 v2) {
	return normalize(cross(v1.xyz - v0.xyz, v2.xyz - v0.xyz));
}

// Returns a Triangle (triData)) given three vertexes. Also Calculates normals and maps uvs
void MakeTriangle(float4 v1, float4 v2, float4 v3, fixed4 color, out vData o[3]) {				
	o[0].vertex = v1;
	o[1].vertex = v2;
	o[2].vertex = v3;

	float3 normal = CalculateNormal(v3, v2, v1);

	o[0].normal = normal;
	o[1].normal = normal;
	o[2].normal = normal;

	o[0].uv = float2(0.0f, 0.0f);
	o[1].uv = float2(0.0f, 1.0f);
	o[2].uv = float2(1.0f, 1.0f);

	o[0].diffuse = color;
	o[1].diffuse = color;
	o[2].diffuse = color;
}

// Returns a Triangle given three INITIALIZED vData Structs
triData TriFromVert(vData data[3]) {
	triData o;
	o.t[0] = data[0];
	o.t[1] = data[1];
	o.t[2] = data[2];
	return o;
}

// Returns a point on a circle given radius of the circle and angle in degrees
float4 PointOnCircle3XZ(float radius, float angle) {
	return float4(radius*sin(angle*0.01745329f), 0.0f, radius*cos(angle*0.01745329f), 0.0f);
}

float random (in float2 st) { 
    return frac(sin(dot(st.xy,float2(12.9898,78.233))) * 43758.5453123);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return lerp(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

float4x4 CreateWorldMatrix(float3 pos, float3 rot, float3 scl)
{
    float4x4 rotate;

    float4x4 translate =
    {
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        pos.x, pos.y, pos.z, 1.0
    };

    float4x4 rotateX =
    {
        1.0, 0.0, 0.0, 0.0,
        0.0, cos(rot.x), -sin(rot.x), 0.0,
        0.0, sin(rot.x), cos(rot.x), 0.0,
        0.0, 0.0, 0.0, 1.0
    };

    float4x4 rotateY =
    {
        cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
    };

    float4x4 rotateZ =
    {
        cos(rot.z), -sin(rot.z), 0.0, 0.0,
        sin(rot.z), cos(rot.z), 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    };

    float4x4 scale =
    {
        scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
    };

    rotate = mul(rotateY, mul(rotateX, rotateZ));
    return mul(scale, mul(rotate, translate));
}

[instance(FlowerInstances)]
[maxvertexcount(16)]
void geomT(point v2g p[1], in uint InstanceID : SV_GSInstanceID, inout TriangleStream<g2f> triStream)
{
    if (p[0].voronoi < _MaxVoronoi && p[0].voronoi > _MinVoronoi)
    {
        vData v = { float4(0.0f, 0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f), float2(0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

        
        float terrainHeight = tex2Dlod(_TerrainHeights, float4((_WorldPos.xz + p[0].pos.xz) / _TerrainScale.xy, 0, 0)).r;
        float noise = tex2Dlod(_NoiseTexture2, float4(p[0].uv.xy, 0,0)).r;
        int plantChance = ceil(noise-_PlantDistributionModifier.x)+ceil(noise-_PlantDistributionModifier.y)+ceil(noise-_PlantDistributionModifier.z);
        float4 newColor = float4(1,1,1,1);

        float radiusSteps = (_MaxRadius - _MinRadius) / FlowerInstances;
        float sizeSteps = (_MaxScale - _MinScale) / FlowerInstances;
        float colorSteps = 1.0 / FlowerInstances;

        float4 posi = p[0].pos + float4(sin(InstanceID * GOLDEN_ANGLE), 0.0, cos(InstanceID * GOLDEN_ANGLE), 0.0) * (InstanceID * radiusSteps);
        posi.y = (terrainHeight * _TerrainScale.z)-_TerrainOffset;
        float3 rot = float3(0.0, GOLDEN_ANGLE * InstanceID, 0.0);
        float xyScale = (_MaxScale - (sizeSteps * InstanceID)) * p[0].voronoi + _MinScale;

        int forLoopCount = 12;
        
        vData plantData[16];

        OptimizeTryout(plantData);

        forLoopCount = 15;
		
        g2f i;
			

        float3 scl = float3(xyScale, xyScale*1.4, xyScale) * p[0].voronoi;
    
        float4x4 rotateY =
        {
        cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 scaleMat =
        {
        scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 rotScale = mul(rotateY, scaleMat);
        UNITY_INITIALIZE_OUTPUT(g2f, i);
        for (int vert = 0; vert < 16; vert++)
        {
            if (vert > forLoopCount)
            {
                continue;
            }
            v = plantData[vert];
            float4 vertex = mul(rotScale, v.vertex);
            vertex = vertex + posi;
            //i.pos = UnityObjectToClipPos(vertex);
            i.pos = UnityObjectToClipPos(vertex);
            i.worldPos = mul(UNITY_MATRIX_VP, vertex);
            i.coords = vertex.xyz;
            //i.worldPos = vertex;
            i.normal = UnityObjectToWorldNormal(v.normal);
            //i.normal = mul(UNITY_MATRIX_MVP, v.normal);
                        
#if defined(BINORMAL_PER_FRAGMENT)
		    i.tangent =     float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
#else
            i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
            i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
#endif

            i.tint = newColor;

            i.uv.xy = v.uv;
            i.uv.zw = v.uv;

            ComputeVertexLightColor(i);
            TRANSFER_VERTEX_TO_FRAGMENT(i);
		    UNITY_TRANSFER_FOG(i, i.pos);
            TRANSFER_SHADOW(i);

            triStream.Append(i);
        }
        triStream.RestartStrip();
    }

}
#endif