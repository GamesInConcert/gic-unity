﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "Custom/GiC_Plants" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NormalTex("Normal", 2D) = "white" {}
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5
		[Gamma] _Metallic ("Metallic", Range(0,1)) = 0.5
		_Cutoff("Alpha Cutoff", Range(0,1)) = 0.1
		_Size ("Size", Range(0,10)) = 1.0
		_Height("Height", Range(0,10)) = 2.0
	}
	SubShader {
		Tags{ "Queue" = "Geometry+1" "RenderType" = "Opaque" }
		//LOD 200

	Pass {
		Tags{ "LightMode" = "ForwardBase" }
		
		Cull OFF
		Lighting On

		CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#include "AutoLight.cginc"
            //#include "UnityStandardBRDF.cginc"
            //#include "UnityStandardUtils.cginc"
            #include "UnityPBSLighting.cginc"

			#pragma target 5.0
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag

			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog

			#pragma fragmentoption ARB_precision_hint_fastest

			#define PyramidSegments 3

			sampler2D _MainTex, _NormalTex;

			struct v2g {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
				float voronoi : PSIZE;
			};

			struct g2f {
	            float4 pos : SV_POSITION;
	            float2 uv : TEXCOORD0;
	            float3 normal : TEXCOORD1;
                float4 tint: COLOR;
	            //#if defined(BINORMAL_PER_FRAGMENT)
		        //    float4 tangent : TEXCOORD2;
	            //#else
		        //    float3 tangent : TEXCOORD2;
		        //    float3 binormal : TEXCOORD3;
	            //#endif

	            float3 worldPos : TEXCOORD4;

	            //SHADOW_COORDS(5)

	            //#if defined(VERTEXLIGHT_ON)
		        //    float3 vertexLightColor : TEXCOORD6;
	            //#endif
			};

			struct data {
				float3 pos;	
				float voronoi;
			};

			struct vData {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				fixed4 diffuse : COLOR0;
			};

			struct triData {
				vData t[3];
			};

			float _Smoothness;
			half _Metallic;
			half _Size;
			half _Height;
			uniform fixed _Cutoff;
			//uniform float4 _LightColor0;
			float3 _WorldPos;
			fixed4 _Color;
			StructuredBuffer<data> buf_Points;

			float3 CalculateNormal(float4 v0, float4 v1, float4 v2) {
				return normalize(cross(v1.xyz - v0.xyz, v2.xyz - v0.xyz));
			}

			void MakeTriangle(float4 v1, float4 v2, float4 v3, fixed4 color, out vData o[3]) {				
				o[0].vertex = v1;
				o[1].vertex = v2;
				o[2].vertex = v3;

				float3 normal = CalculateNormal(v1, v2, v3);

				o[0].normal = normal;
				o[1].normal = normal;
				o[2].normal = normal;

				o[0].uv = float2(0.0f, 0.0f);
				o[1].uv = float2(0.0f, 1.0f);
				o[2].uv = float2(1.0f, 1.0f);

				o[0].diffuse = color;
				o[1].diffuse = color;
				o[2].diffuse = color;
			}

			triData TriFromVert(vData data[3]) {
				triData o;
				o.t[0] = data[0];
				o.t[1] = data[1];
				o.t[2] = data[2];
				return o;
			}

			float4 PointOnCircle3XZ(float radius, float angle) {
				return float4(radius*sin(angle*0.01745329f), 0.0f, radius*cos(angle*0.01745329f), 0.0f);
			}

			void DefinedPyramid(out vData o[12]) {

				float4 v1 = float4(0.0, 0.0, 1.0, 0.0);
				float4 v2 = float4(0.866025403, 0.0, -0.5, 0.0);
				float4 v3 = float4(-0.86602540, 0.0, -0.5, 0.0);
				float4 v4 = float4(0.0, 1.0, 0.0, 0.0);
				float2 uv1 = float2(0.0, 0.0);
				float2 uv2 = float2(0.0, 0.5);
				float2 uv3 = float2(0.0, 1.0);
				float2 uv4 = float2(1.0, 1.0);
				fixed4 color = fixed4(1.0, 1.0, 1.0, 1.0);
				
				// Tri 1
				o[0].vertex = v1;
				o[0].uv = uv1;
				o[0].diffuse = color;
				
				o[1].vertex = v4;
				o[1].uv = uv4;
				o[1].diffuse = color;

				o[2].vertex = v2;
				o[2].uv = uv2;
				o[2].diffuse = color;

				float3 normal = CalculateNormal(v2, v4, v1);
				o[0].normal = normal;
				o[1].normal = normal;
				o[2].normal = normal;

				// Tri 2
				o[3].vertex = v2;
				o[3].uv = uv3;
				o[3].diffuse = color;

				o[4].vertex = v4;
				o[4].uv = uv4;
				o[4].diffuse = color;

				o[5].vertex = v3;
				o[5].uv = uv1;
				o[5].diffuse = color;

				normal = CalculateNormal(v3, v4, v2);
				o[3].normal = normal;
				o[4].normal = normal;
				o[5].normal = normal;


                //Tri 3
				o[6].vertex = v3;
				o[6].uv = uv3;
				o[6].diffuse = color;

				o[7].vertex = v4;
				o[7].uv = uv4;
				o[7].diffuse = color;

				o[8].vertex = v1;
				o[8].uv = uv1;
				o[8].diffuse = color;

				normal = CalculateNormal(v1, v4, v3);
				o[6].normal = normal;
				o[7].normal = normal;
				o[8].normal = normal;

                //Tri 4
				o[9].vertex = v1;
				o[9].uv = uv1;
				o[9].diffuse = color;
				o[10].vertex = v2;
				o[10].uv = uv2;
				o[10].diffuse = color;
				o[11].vertex = v3;
				o[11].uv = uv3;
				o[11].diffuse = color;

                normal = CalculateNormal(v3, v2, v1);

				o[9].normal = normal;
				o[10].normal = normal;
				o[11].normal = normal;
								
			}

			vData InitvData() {
				vData o = { float4(0.0f, 0.0f, 0.0f, 0.0f) , float3(0.0f, 0.0f, 0.0f),  float2(0.0f, 0.0f) ,  fixed4(0.0f, 0.0f, 0.0f, 0.0f)  };
				return o;
			}

			triData InitTriData() {
				vData d[3] = { {InitvData()}, {InitvData()}, {InitvData()} };
				return TriFromVert(d);
			}

			v2g vert(uint id : SV_VertexID) {
				v2g o;
				o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
				o.voronoi = buf_Points[id].voronoi;
				o.col = float4(1.0f, 1.0f, 0.0f, 0.0f);
				return o;
			}

			[maxvertexcount((PyramidSegments+1)*3)]
			void geom(point v2g p[1], inout TriangleStream<g2f> triStream) {

				vData v = { float4(0.0f, 0.0f, 0.0f, 0.0f) , float3(0.0f, 0.0f, 0.0f),  float2(0.0f, 0.0f) ,  fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

				vData pyramid[12];
				
				DefinedPyramid(pyramid);
			
				g2f i;
				
				//g2f tri[3];

				for (int vert = 0; vert < 12; vert++) {
						v = pyramid[vert];
                        float4 vertex = v.vertex + p[0].pos;
						i.pos = UnityObjectToClipPos(vertex);
                        i.worldPos = UnityObjectToClipPos(vertex).xyz;
						i.uv = v.uv;
						i.normal = UnityObjectToWorldNormal(v.normal);
                        //i.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
						i.tint = p[0].col;
						//TRANSFER_VERTEX_TO_FRAGMENT(fIn);
						//UNITY_TRANSFER_FOG(fIn, fIn.pos);
                        //TRANSFER_SHADOW(fIn);

						triStream.Append(i);
				}
			}

			float4 frag(g2f i) : COLOR {
                float3 normal = normalize(i.normal);
                float3 lightDir = _WorldSpaceLightPos0.xyz;
                float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
                //float3 halfVector = normalize(lightDir + viewDir);

                float3 lightColor = _LightColor0.rgb;
                
                // Create unity light structure to be passed to PBS function
                UnityLight light;
				light.color = lightColor;
				light.dir = lightDir;
				light.ndotl = DotClamped(i.normal, lightDir);

                // Create unity indirectLight structure to be passed to PBS function
				UnityIndirect indirectLight;
				indirectLight.diffuse = 0;
				indirectLight.specular = 0;

                float3 albedo = tex2D(_MainTex, i.uv).rgb * i.tint.rgb;
                float3 specularTint;
                float oneMinusReflectivity;
				albedo = DiffuseAndSpecularFromMetallic(albedo, _Metallic, specularTint, oneMinusReflectivity);
                //float3 specular = specularTint * lightColor * pow(	DotClamped(halfVector, normal),_Smoothness * 100);
                // Lambert Diffuse
				//float3 diffuse = albedo * lightColor * DotClamped(lightDir, normal);
                return UNITY_BRDF_PBS(albedo, specularTint,	oneMinusReflectivity, _Smoothness, normal, viewDir, light, indirectLight);

			}
		ENDCG
		}
		//			// shadow caster
		//			Pass
		//			{
		//				//Name "ShadowCaster"
		//				Tags{ "LightMode" = "ShadowCaster" }
		//
		//				Fog{ Mode Off }
		//				ZWrite On ZTest LEqual
		//
		//				CGPROGRAM
		//// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
		//#pragma exclude_renderers d3d11 gles
		//
		//				#pragma target 5.0
		//				#pragma vertex vert
		//				#pragma geometry geom
		//				#pragma fragment frag
		//
		//				#include "UnityCG.cginc"
		//				#include "HLSLSupport.cginc"
		//
		//				#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
		//				#include "AutoLight.cginc"
		//				#define _PyramidSegments 3
		//
		//
		//				struct v2g {
		//					float4 pos : SV_POSITION;
		//					float voronoi : PSIZE;
		//				};
		//
		//				struct SHADOW_VERTEX
		//				{
		//					float4 vertex : POSITION; // has to be called this way because of unity macro
		//				};
		//
		//				struct g2f {
		//					float2 uv_MainTexture : TEXCOORD0;
		//					V2F_SHADOW_CASTER;
		//				};
		//
		//				struct data {
		//					float3 pos;
		//					float voronoi;
		//				};
		//
		//				struct vData {
		//					float4 vertex : SV_POSITION;
		//					float3 normal : NORMAL;
		//					float2 uv : TEXCOORD0;
		//					fixed4 diffuse : COLOR0;
		//				};
		//
		//				struct triData {
		//					vData t[3];
		//				};
		//
		//				sampler2D _MainTex;
		//				half _Size;
		//				half _Height;
		//				half4 _Color;
		//				uniform fixed _Cutoff;
		//				float3 _WorldPos;
		//				StructuredBuffer<data> buf_Points;
		//
		//				float3 CalculateNormal(vData[] tri) {
		//					return float3 normal = normalize(cross(tri[1].vertex.xyz - tri[0].vertex.xyz, tri[2].vertex.xyz - tri[0].vertex.xyz));
		//				}
		//
		//				vData[] MakeTriangle(float4 v1, float4 v2, float4 v3, fixed4 color) {
		//					vData o[3];
		//					o[0].vertex = v1;
		//					o[1].vertex = v2;
		//					o[2].vertex = v3;
		//
		//					float3 normal = CalculateNormal(o);
		//
		//					o[0].normal = normal;
		//					o[1].normal = normal;
		//					o[2].normal = normal;
		//
		//					o[0].uv = float2(0.0f, 0.0f);
		//					o[1].uv = float2(0.0f, 1.0f);
		//					o[2].uv = float2(1.0f, 1.0f);
		//
		//					o[0].diffuse = color;
		//					o[1].diffuse = color;
		//					o[2].diffuse = color;
		//
		//					return o;
		//				}
		//
		//				triData TriFromVert(vData[] data) {
		//					triData o;
		//					o.t[0] = data[0];
		//					o.t[1] = data[1];
		//					o.t[2] = data[3];
		//				}
		//
		//				triData[] BaselessPyramid(float4 baseCenter, float4 color, float height, float radius) {
		//					int segmants = 3;
		//					triData o[segments + 1];
		//
		//					float4 verts[segments + 1];
		//
		//					float segmentAngle = 360.0f / segments;
		//					float currentAngle = 0.0f;
		//
		//					verts[0] = float4(baseCenter.x, baseCenter.y*height, baseCenter.z, baseCenter.w);
		//
		//					for (int i = 1; i <= segments; i++) {
		//						verts[i] = PointOnCircle3XZ(radius, currentangle) + baseCenter;
		//						currentAngle += segmentAngle;
		//					}
		//
		//					for (int i = i; i <= segments; i++) {
		//						o[i] = TriFromVert(MakeTriangle(verts[0], verts[i], verts[i + 1]));
		//					}
		//					o[segments] = TriFromVert(MakeTriangle(verts[0], verts[segments], verts[1]));
		//					return o;
		//				}
		//
		//				float4 PointOnCircle3XZ(float radius, float angle) {
		//					return float4(radius*sin(angle*0.01745329f), 0.0f, radius*cos(angle*0.01745329f), 0.0f);
		//				}
		//
		//				v2g vert(uint id : SV_VertexID) {
		//					v2g o;
		//					o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
		//					o.voronoi = buf_Points[id].voronoi;
		//					return o;
		//				}
		//
		//				[maxvertexcount(12)]
		//				void geom(point v2g p[1], inout TriangleStream<g2f> triStream) {
		//
		//					float2 size = float2(p[0].voronoi*_Size, p[0].voronoi*_Size);
		//					//float4 v[4];
		//
		//					triData tris[_PyramidSegments + 1];
		//
		//					tris = BaselessPyramid(p[0].pos, _Color, _Height, p[0].voronoi*_Size, _PyramidSegments);
		//
		//					//v[0] = p[0].pos.xyzw + float4(-size.x,0, -size.y, 0);
		//					//v[1] = p[0].pos.xyzw + float4(-size.x, 0, size.y, 0);
		//					//v[2] = p[0].pos.xyzw + float4(size.x, 0, -size.y, 0);
		//					//v[3] = p[0].pos.xyzw + float4(0, size.x, 0, 0);
		//
		//					SHADOW_VERTEX v;
		//
		//					g2f fIn;
		//					//g2f tri[3];
		//					vData vDat;
		//					for (int i = 0; i < _PyramidSegments + 1; i++) {
		//						for (int j = 0; j < 3; j++) {
		//							vDat = tris[i].t[j];
		//							v.vertex = mul(unity_WorldToObject, vDat.vertex);
		//							fIn.uv_MainTexture = vDat.uv;
		//							fIn.normal = vDat.normal;
		//							fIn.diff = vDat.diffuse;
		//							TRANSFER_SHADOW_CASTER(fIn);
		//
		//							triStream.Append(fIn);
		//						}
		//					}
		//
		//					outStream.RestartStrip();
		//
		//					//fIn.pos = UnityObjectToClipPos(v[0]);
		//					//fIn.uv_MainTexture = float2(0.0f, 0.0f);
		//					//fIn.diff = float4(1.0f, p[0].voronoi, 0.0f, 0.0f);
		//					//TRANSFER_VERTEX_TO_FRAGMENT(fIn);
		//					//UNITY_TRANSFER_FOG(fIn, fIn.pos);
		//
		//					//triStream.Append(fIn);
		//
		//					//fIn.pos = UnityObjectToClipPos(v[1]);
		//					//fIn.uv_MainTexture = float2(0.0f, 1.0f);
		//					//fIn.diff = float4(1.0f, p[0].voronoi, 0.0f, 0.0f);
		//					//TRANSFER_VERTEX_TO_FRAGMENT(fIn);
		//					//UNITY_TRANSFER_FOG(fIn, fIn.pos);
		//
		//					//triStream.Append(fIn);
		//
		//					//fIn.pos = UnityObjectToClipPos(v[2]);
		//					//fIn.uv_MainTexture = float2(1.0f, 0.0f);
		//					//fIn.diff = float4(1.0f, p[0].voronoi, 0.0f, 0.0f);
		//					//TRANSFER_VERTEX_TO_FRAGMENT(fIn);
		//					//UNITY_TRANSFER_FOG(fIn, fIn.pos);
		//
		//					//triStream.Append(fIn);
		//
		//					//fIn.pos = UnityObjectToClipPos(v[3]);
		//					//fIn.uv_MainTexture = float2(1.0f, 1.0f);
		//					//fIn.diff = float4(1.0f, p[0].voronoi, 0.0f, 0.0f);
		//					//TRANSFER_VERTEX_TO_FRAGMENT(fIn);
		//					//UNITY_TRANSFER_FOG(fIn, fIn.pos);
		//
		//					//triStream.Append(fIn);
		//
		//				}
		//
		//				//[maxvertexcount(4)]
		//				//void geom(point v2g p[1], inout TriangleStream<g2f> triStream) {
		//
		//				//	float2 size = float2(p[0].voronoi*_Size, p[0].voronoi*_Size);
		//				//	float4 vert[4];
		//
		//				//	//vert[0] = p[0].pos.xyzw + float4(-size.x, 0, -size.y, 0);
		//				//	//vert[1] = p[0].pos.xyzw + float4(-size.x, 0, size.y, 0);
		//				//	//vert[2] = p[0].pos.xyzw + float4(size.x, 0, -size.y, 0);
		//				//	//vert[3] = p[0].pos.xyzw + float4(0, size.x, 0, 0);
		//
		//				//	g2f fIn;
		//
		//				//	SHADOW_VERTEX v;
		//
		//				//	v.vertex = mul(unity_WorldToObject, vert[0]);
		//				//	fIn.uv_MainTexture = float2(0.0f, 0.0f);
		//				//	TRANSFER_SHADOW_CASTER(fIn)
		//
		//				//	triStream.Append(fIn);
		//
		//				//	v.vertex = mul(unity_WorldToObject, vert[1]);
		//				//	fIn.uv_MainTexture = float2(0.0f, 1.0f);
		//				//	TRANSFER_SHADOW_CASTER(fIn)
		//
		//				//	triStream.Append(fIn);
		//
		//				//	v.vertex = mul(unity_WorldToObject, vert[2]);
		//				//	fIn.uv_MainTexture = float2(1.0f, 0.0f);
		//				//	TRANSFER_SHADOW_CASTER(fIn)
		//
		//				//	triStream.Append(fIn);
		//
		//				//	v.vertex = mul(unity_WorldToObject, vert[3]);
		//				//	fIn.uv_MainTexture = float2(1.0f, 1.0f);
		//				//	TRANSFER_SHADOW_CASTER(fIn)
		//
		//				//	triStream.Append(fIn);
		//				//}
		//
		//				fixed4 frag(g2f fIn) : COLOR
		//				{
		//					fixed4 color = tex2D(_MainTex, fIn.uv_MainTexture);
		//					if (color.a < _Cutoff)
		//						discard;
		//
		//					SHADOW_CASTER_FRAGMENT(fIn)
		//				}
		//
		//			ENDCG
		//		}
	}
	FallBack "Diffuse"
}
