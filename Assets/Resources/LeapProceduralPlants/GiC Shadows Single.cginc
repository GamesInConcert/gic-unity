#if !defined(GiC_SHADOWS_INCLUDED)
#define GiC_SHADOWS_INCLUDED

#include "UnityCG.cginc"
#include "GiC Base Geometry Defines.cginc"

struct v2g {
	float4 pos : SV_POSITION;
    float4 uv : TEXCOORD0;
	float voronoi : PSIZE;
    float4 mod : TEXCOORD1;

};

struct SHADOW_VERTEX
{
	float4 vertex : POSITION; // has to be called this way because of unity macro
};

struct g2f {
    //float4 pos : SV_POSITION;
    V2F_SHADOW_CASTER;
};

struct data {
	float3 pos;	
    float2 mod;
	float voronoi;
};

float3 _WorldPos;
sampler2D _TerrainHeights;
sampler2D _NoiseTexture2;
float4 _PlantDistributionModifier;
float _MinScale, _MaxScale;
float _MinVoronoi, _MaxVoronoi;
float4 _TerrainScale;
float _TerrainOffset;
float _fi;
vData vD = { float4(0.0f, 0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f), float2(0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

StructuredBuffer<data> buf_Points;

float GetFi(float fiIn, float3 objPos)
{
    return abs(abs((fiIn - atan2(objPos.x, objPos.z)) % 6.283185308) - PI);
}

v2g shadowVert(uint id : SV_VertexID) {
	v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    o.pos = mul (unity_ObjectToWorld,buf_Points[id].pos);
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
	o.voronoi = buf_Points[id].voronoi;
	return o;
}

v2g shadowVertPos(uint id : SV_VertexID, float4 pos : POSITION, float4 uv : TEXCOORD0)
{
    v2g o;
	//o.pos = float4(buf_Points[id].pos + _WorldPos, 1.0f);
    //o.pos = mul(unity_ObjectToWorld, float4(buf_Points[id].pos, 0.0)) + float4(_WorldPos, 0.0);
    o.pos = float4(buf_Points[id].pos, 0.0);
    //o.pos = mul(unity_ObjectToWorld, pos + buf_Points[id].pos);
    o.voronoi = buf_Points[id].voronoi;
    o.mod = float4(buf_Points[id].mod.xy, 0, 0);
    o.uv = uv;
    return o;
}

[maxvertexcount(16)]
void shadowGeom(point v2g p[1], inout TriangleStream<g2f> triStream) {
    float voronoi = p[0].voronoi;
    if (voronoi < _MaxVoronoi && voronoi > _MinVoronoi + (p[0].pos.y * 0.01))
    {
        int forLoopCount = 16;
        int plantChance = _PlantDistributionModifier.x + _PlantDistributionModifier.y + _PlantDistributionModifier.z;
        plantChance = floor(plantChance - 1.0);
        float2 noise = tex2Dlod(_NoiseTexture2, float4(p[0].uv.xy, 0, 0)).rg;
        float doublenoise = noise.x * noise.y;
        float terrainHeight = tex2Dlod(_TerrainHeights, float4((_WorldPos.xz + p[0].pos.xz) / _TerrainScale.xy, 0, 0)).r;
               
        vData plantData[16];

        float4 posi = p[0].pos;
        posi.y = (terrainHeight * _TerrainScale.z) - _TerrainOffset;

        float3 rot = float3(cos(doublenoise) * 13.5, noise.x * noise.y * 30.0, sin(doublenoise) * 11.324f);
        float xyScale = _MinScale;
        
        if (plantChance == 0)
        {
            // Crystals
            xyScale *= 300.0;
            BlenderShapeA3(plantData);
        }
        else if (plantChance == 1)
        {
            xyScale *= 0.75;
            forLoopCount = 12;
            Tetrahedron(plantData);
        }
        else
        {
            xyScale *= 550.0;
            BlenderShapeC1(plantData);
            rot.x += 90;
            rot.z += 45;
        }
			
        xyScale *= (1.0 + GetFi(_fi, posi));	
        
        float dist = clamp(dot(_WorldPos - _WorldSpaceCameraPos, _WorldPos - _WorldSpaceCameraPos) * 0.01, 0, 1.0);

        xyScale *= (_MinScale + clamp(dist, 0.25, 1.0) * (_MaxScale - _MinScale));
        
        float voronoiSizeMod = cos(PI * p[0].mod.x) - PI * p[0].pos.y * cos(PI * voronoi);
        float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(1, voronoiSizeMod, voronoi) + _MinScale;
    
        // Matrices
        float4x4 rotateY =
        {
            cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 scaleMat =
        {
            scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 rotScale = mul(rotateY, scaleMat);
			
        g2f i;
        SHADOW_VERTEX v;

        for (int vert = 0; vert < 16; vert++)
        {
            if (vert > forLoopCount)
            {
                continue;
            }
            vD = plantData[vert];
            float4 vertex = mul(rotScale, vD.vertex);
            vertex += posi;
            //v.vertex = mul(UNITY_MATRIX_MV, vertex);
            v.vertex = vertex;
            TRANSFER_SHADOW_CASTER(i);
            //TRANSFER_SHADOW(i);

            triStream.Append(i);
        }
        triStream.RestartStrip();
    }
}

float4 shadowFrag (g2f i) : COLOR {
    float4 color = fixed4(1,1,1,1);
    SHADOW_CASTER_FRAGMENT(i);
}

#endif