#if !defined(GIC_GS_INCLUDED)
#define GIC_GS_INCLUDED

#include "GiC Base Geometry Defines.cginc"
#include "UnityCG.cginc"

sampler2D _TerrainHeights;
sampler2D _NoiseTexture2;
float4 _PlantDistributionModifier;
float _MinScale, _MaxScale;
float _MinVoronoi, _MaxVoronoi;
float4 _TerrainScale;
float _TerrainOffset;
float3 _WorldPos;
half4 _FlowerColors[13];
float _fi;

vData v = { float4(0.0f, 0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f), float2(0.0f, 0.0f), fixed4(0.0f, 0.0f, 0.0f, 0.0f) };

float GetFi(float fiIn, float3 objPos)
{
    return abs(abs((fiIn - atan2(objPos.x, objPos.z)) % 6.283185308) - PI);
}

[maxvertexcount(16)]
void geom(point v2g p[1], inout TriangleStream<g2f> triStream)
{
    float voronoi = p[0].voronoi;
    if (voronoi < _MaxVoronoi && voronoi > _MinVoronoi+(p[0].pos.y*0.01))
    {
        g2f i;

        int forLoopCount = 16;
        int plantChance = _PlantDistributionModifier.x + _PlantDistributionModifier.y + _PlantDistributionModifier.z;
        plantChance = floor(plantChance - 1.0);
        float2 noise = tex2Dlod(_NoiseTexture2, float4(p[0].uv.xy, 0, 0)).rg;
        float doublenoise = noise.x * noise.y;
        float terrainHeight = tex2Dlod(_TerrainHeights, float4((_WorldPos.xz + p[0].pos.xz) / _TerrainScale.xy, 0, 0)).r;
               
        vData plantData[16];

        float4 posi = p[0].pos;
        posi.y = (terrainHeight * _TerrainScale.z)-_TerrainOffset;

        float3 rot = float3(cos(doublenoise) * 13.5, noise.x * noise.y * 30.0, sin(doublenoise) * 11.324f);
        float xyScale = _MinScale;
        
        if (plantChance == 0)
        {
            // Crystals
            xyScale *= 300.0;
            BlenderShapeA3(plantData);
        }
        else if (plantChance == 1)
        {
            xyScale *= 0.75;
            forLoopCount = 12;
            Tetrahedron(plantData);
        }
        else
        {
            xyScale *= 550.0;
            BlenderShapeC1(plantData);
            rot.x += 90;
            rot.z += 45;
        }

        xyScale *= (1.0 + GetFi(_fi, posi));

        float dist = clamp(dot(_WorldPos - _WorldSpaceCameraPos, _WorldPos - _WorldSpaceCameraPos) * 0.01, 0, 1.0);

        xyScale *= (_MinScale + clamp(dist, 0.25, 1.0) * (_MaxScale - _MinScale));

        float voronoiSizeMod = cos(PI * p[0].mod.x) - PI * p[0].pos.y * cos(PI * voronoi);
        float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(1, voronoiSizeMod, voronoi) + _MinScale;

        //float4 newColor = lerp(_FlowerColors[(plantChance * 4) + 2], _FlowerColors[(plantChance * 4) + 1], voronoi * (1.0 - p[0].mod.y));
        float4 newColor = lerp(_FlowerColors[(plantChance * 4) + 2], _FlowerColors[(plantChance * 4) + 1], dot(p[0].pos, p[0].pos) * 0.025);
        newColor *= ((p[0].pos.y * 0.5)+0.5);
        newColor = lerp(newColor, dot(newColor, float3(0.299, 0.587, 0.114)), clamp(dist, 0, 0.6));
			
        //float voronoiSizeMod = -(cos((-PI * voronoi)) * p[0].pos.y * PI) + sin((0 - p[0].mod.x) * PI + (PI * 0.5));
        //float3 scl = float3(xyScale, xyScale * 1.4, xyScale) * lerp(voronoiSizeMod, 0.1, voronoi) + _MinScale;
    
        // Matrices
        float4x4 rotateY =
        {
        cos(rot.y), 0.0, sin(rot.y), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(rot.y), 0.0, cos(rot.y), 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 scaleMat =
        {
        scl.x, 0.0, 0.0, 0.0,
        0.0, scl.y, 0.0, 0.0,
        0.0, 0.0, scl.z, 0.0,
        0.0, 0.0, 0.0, 1.0
        };

        float4x4 rotScale = mul(rotateY, scaleMat);

        UNITY_INITIALIZE_OUTPUT(g2f, i);
        for (int vert = 0; vert < 16; vert++)
        {
            if (vert > forLoopCount)
            {
                continue;
            }
            v = plantData[vert];
            float4 vertex = mul(rotScale, v.vertex);
            float3 normal = mul(rotScale, v.normal);
            vertex = vertex + posi;
            i.pos = UnityObjectToClipPos(vertex);
            i.coords = vertex.xyz;
            i.worldPos = mul(unity_ObjectToWorld, vertex) + _WorldPos;
            i.normal = UnityObjectToWorldNormal(normal);
                        
#if defined(BINORMAL_PER_FRAGMENT)
		    i.tangent =     float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
#else
            i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
            i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
#endif

            i.tint = newColor;

            i.uv.xy = v.uv;
            i.uv.zw = v.uv;

            ComputeVertexLightColor(i);
            TRANSFER_VERTEX_TO_FRAGMENT(i);
		    UNITY_TRANSFER_FOG(i, i.pos);
            TRANSFER_SHADOW(i);

            triStream.Append(i);
        }
        triStream.RestartStrip();
    }

}

float random(in float2 st)
{
    return frac(sin(dot(st.xy, float2(12.9898, 78.233))) * 43758.5453123);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise(in float2 st)
{
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation
    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f * f * (3.0 - 2.0 * f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}
#endif