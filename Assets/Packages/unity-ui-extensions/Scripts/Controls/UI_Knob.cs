﻿/// Credit Tomasz Schelenz 
/// Sourced from - https://bitbucket.org/ddreaper/unity-ui-extensions/issues/46/feature-uiknob#comment-29243988

using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// KNOB controller
/// 
/// Fields
/// - direction - direction of rotation CW - clockwise CCW - counter clock wise
/// - knobValue - Output value of the control
/// - maxValue - max value knob can rotate to, if higher than loops value or set to 0 - it will be ignored, and max value will be based on loops
/// - loops - how any turns around knob can do
/// - clampOutput01 - if true the output knobValue will be clamped between 0 and 1 regardless of number of loops.
/// - snapToPosition - snap to step. NOTE: max value will override the step.
/// - snapStepsPerLoop - how many snap positions are in one knob loop;
/// - OnValueChanged - event that is called every frame while rotationg knob, sends <float> argument of knobValue
/// NOTES
/// - script works only in images rotation on Z axis;
/// - while dragging outside of control, the rotation will be cancelled
/// </summary>
/// 
namespace UnityEngine.UI.Extensions
{
    //[RequireComponent(typeof(Image))]
    [AddComponentMenu("UI/Extensions/UI_Knob")]
    public class UI_Knob : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler
    {
        public enum Direction { CW, CCW };
        [Tooltip("Direction of rotation CW - clockwise, CCW - counterClockwise")]
        public Direction direction = Direction.CW;

        public float InitialValue;
        private float initalValue { get { return InitialValue < minOutput ? minOutput : InitialValue; } }
        [Tooltip("Min value of the knob, minimum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
        public float minOutput = 0;
        [Tooltip("Max value of the knob, maximum RAW output value knob can reach, overrides snap step, IF set to 0 or higher than loops, max value will be set by loops")]
        public float maxOutput = 0;
        [Tooltip("How many rotations knob can do, if higher than max value, the latter will limit max value")]
        [Range(1,10)]
        public int loops = 1;
        [Tooltip("Clamp output value between 0 and 1, usefull with loops > 1")]
        public bool wholeNumbers = false;
        [Tooltip("snap to position?")]
        public bool snapToPosition = false;

        [Tooltip("Number of positions to snap")]
        public int snapStepsPerLoop = 10;
        [Space(30)]
        public KnobFloatValueEvent OnValueChanged;
        public KnobFloatValueEvent OnNormalizedValueChanged;

        public RectTransform targetTransform;
        private float _currentLoops = 0;
        private float _previousValue = 0;
        private float _initAngle;
        private float _currentAngle;
        private Vector2 _currentVector;
        private Vector3 _initPosition;
        private Quaternion _initRotation;
        private Vector3 _initRotEuler;
        private bool _canDrag = false;
        private float internalKnobValue = 0f;
        private bool internalSnapToPosition {get { return wholeNumbers || snapToPosition; } }
        private float maxValue = 0;

        private float knobRange { get { return Mathf.Abs(maxOutput - minOutput); } }
        private int internalSnapStepsPerLoop { get { return wholeNumbers ? Mathf.CeilToInt(knobRange) : snapStepsPerLoop; } }
        private RectTransform rectTransform;
        private Vector2 prevVector2 = Vector2.zero;

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            InvokeEvents((initalValue - minOutput) / knobRange);
            targetTransform.rotation = Quaternion.Euler(new Vector3(0,0,((initalValue - minOutput) / knobRange)*360f));
        }

        //ONLY ALLOW ROTATION WITH POINTER OVER THE CONTROL
        public void OnPointerDown(PointerEventData eventData)
        {
            _canDrag = true;
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            _canDrag = false;
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            _canDrag = true;
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            _canDrag = true;
        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            SetInitPointerData(eventData);
        }
        void SetInitPointerData(PointerEventData eventData)
        {
            _initRotation = targetTransform.rotation;
            _initRotEuler = targetTransform.rotation.eulerAngles;
            _currentVector = eventData.position - (Vector2)transform.position;
            _initAngle = Mathf.Atan2(_currentVector.y, _currentVector.x) * Mathf.Rad2Deg;
            Vector2 localCursor;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out localCursor))
                _initPosition = localCursor - Vector2.zero;

            _initAngle = Mathf.Atan2(_initPosition.y, _initPosition.x) * Mathf.Rad2Deg;
        }
        public void OnDrag(PointerEventData eventData)
        {
            if (internalSnapToPosition)
            {
                
            }
            //CHECK IF CAN DRAG
            if (!_canDrag)
            {
                SetInitPointerData(eventData);
                
                return;
            }

            // Own Stuff
            Vector2 localCursor;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(this.rectTransform, eventData.position,
                eventData.pressEventCamera, out localCursor);

            //Vector2 myVector2 = eventData.position - (Vector2)rectTransform.position;
            Vector2 myLocalVector2 = localCursor - Vector2.zero;
            //float _angle = Vector2.Angle(_initPosition, myLocalVector2);
            //Debug.Log(myLocalVector2 + " " + localCursor);


           // _currentVector = eventData.position - (Vector2)transform.position;
           // _currentAngle = Mathf.Atan2(_currentVector.y, _currentVector.x) * Mathf.Rad2Deg;

            //Simon Add
            _currentAngle = Mathf.Atan2(myLocalVector2.y, myLocalVector2.x) * Mathf.Rad2Deg;
            Debug.Log(_currentAngle);

            //Quaternion addRotation = Quaternion.AngleAxis(_currentAngle - _initAngle, Vector3.forward);
            //addRotation.eulerAngles = new Vector3(0, 0, addRotation.eulerAngles.z);
            //float rotationZ = (_initRotation * addRotation).eulerAngles.z;
            float rotationZ = _initRotEuler.z + (_currentAngle - _initAngle);
            if (internalSnapToPosition)
            {
                float snapStep = 360f / (float) internalSnapStepsPerLoop;
                rotationZ = Mathf.Round(rotationZ / snapStep) * snapStep;
            }
            Quaternion finalRotation = Quaternion.Euler(0f, 0f, rotationZ);

            //Quaternion finalRotation = (_initRotation * addRotation);

            if (direction == Direction.CW)
            {
                internalKnobValue = 1 - (finalRotation.eulerAngles.z / 360f);

                //if (internalSnapToPosition)
                //{
                //    SnapToPosition(ref internalKnobValue);
                //    finalRotation.eulerAngles = new Vector3(0, 0, 360 * internalKnobValue);
                //}
            }
            else
            {
                internalKnobValue = (finalRotation.eulerAngles.z / 360f);

                //if (internalSnapToPosition)
                //{
                //    SnapToPosition(ref internalKnobValue);
                //    finalRotation.eulerAngles = new Vector3(0, 0, 360 - 360 * internalKnobValue);
                //}
            }

            //PREVENT OVERROTATION
            if (Mathf.Abs(internalKnobValue - _previousValue) > 0.5f)
            {
                if (internalKnobValue < 0.5f && loops > 1 && _currentLoops < loops - 1)
                {
                    _currentLoops++;
                }
                else if (internalKnobValue > 0.5f && _currentLoops >= 1)
                {
                    _currentLoops--;
                }
                else
                {
                    if (internalKnobValue > 0.5f && _currentLoops == 0)
                    {
                        internalKnobValue = 0;
                        targetTransform.localEulerAngles = Vector3.zero;
                        SetInitPointerData(eventData);
                        InvokeEvents(internalKnobValue + _currentLoops);
                        return;
                    }
                    else if (internalKnobValue < 0.5f && _currentLoops == loops - 1)
                    {
                        internalKnobValue = 1;
                        targetTransform.localEulerAngles = Vector3.zero;
                        SetInitPointerData(eventData);
                        InvokeEvents(internalKnobValue + _currentLoops);
                        return;
                    }
                }
            }

            //CHECK MAX VALUE
            if (maxValue > 0)
            {
                if (internalKnobValue + _currentLoops > maxValue)
                {
                    internalKnobValue = maxValue;
                    float maxAngle = direction == Direction.CW ? 360f - 360f * maxValue : 360f * maxValue;
                    targetTransform.localEulerAngles = new Vector3(0, 0, maxAngle);
                    SetInitPointerData(eventData);
                    InvokeEvents(internalKnobValue);
                    return;
                }
            }

            targetTransform.localRotation = finalRotation;
            InvokeEvents((internalKnobValue + _currentLoops)/loops);

            _previousValue = internalKnobValue;
        }
        private void SnapToPosition(ref float internalKnobValue)
        {
            float snapStep = 1 / (float)internalSnapStepsPerLoop;
            float newValue = Mathf.Round(internalKnobValue / snapStep) * snapStep;
            internalKnobValue = newValue;
        }

        private void InvokeEvents(float value)
        {
            OnNormalizedValueChanged.Invoke(value);
            value = Mathf.Clamp(minOutput + (value * knobRange), minOutput, maxOutput);
            if (wholeNumbers)
            {
                value = Mathf.Ceil(value);
            }
            OnValueChanged.Invoke(value);           
        }
    }

    [System.Serializable]
    public class KnobFloatValueEvent : UnityEvent<float> { }

}