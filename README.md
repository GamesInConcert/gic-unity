# Games In Concert (Unity Project)

Games in concert is a SNF funded research project exploring the possibilities and implications of collaborative creativity in virtual environments.
This is the Main Repository for the Unity3D part of the project. 

For the Max/MSP part please visit: (https://gitlab.zhdk.ch/GamesInConcert/gic-max)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To sucessfully run the Unity project the following dependencies have to be installed.
VR Mode is based on using a HTC Vive Headset and controllers.

* [Leap Motion Unity Library (Core Assets)](https://developer.leapmotion.com/unity/#116)
* + Hands Module
* + Leap Motion Interaction Engine Module

From the Asset Store
* [Steam VR](https://assetstore.unity.com/packages/templates/systems/steamvr-plugin-32647) - The SteamVR Extention
* [TextMeshPro](https://assetstore.unity.com/packages/essentials/beta-projects/textmesh-pro-84126) - To display UI texts propperly
* [OSC simpl](https://assetstore.unity.com/packages/tools/input-management/osc-simpl-53710) - For OSC communication with Max/MSP
* [Squiggle](https://assetstore.unity.com/packages/tools/utilities/squiggle-21970) - If you wish to use the provided data collection scripts (works without)

If you do not wish to use OSCSimpl you can use one of the libraries below, However you`ll have to rewrite the OSCManager.cs script accordingly

* [UnityOSC](https://github.com/thomasfredericks/UnityOSC) - By ThomasFredericks
* [UnityOSC](https://github.com/jorgegarcia/UnityOSC) - By Jorge Garcia

### Installing

To Get the Project running: 
* Clone this repo (or download it).
* Install All Dependencies.
* Open the "GicNetworkedGamesInConcert_SeparateTerrain" scene found in _Scenes.
* Add that scene and the Bright and Dark terrain Scenes to your build settings.

You should now have a working scene wothout any scripting errors.

## Running the Scene

### Setup
* Find the _Managers GameObject
* Select which role you want to play and which terrain you want to use.
* If you have no VR headset check the Mouse Mode checkbox as well.
* Hit the play button.

### How to play

* Use nubers 1-4 to change your role ingame
* Navigation can be done using standard wsad and mouse or by using the Vive Controllers (Touchpad klick).
* First Create an atom by opening the bagpack (Click on the ui or hit B on your keyboard).
* Grab one and drag it into the scene by using the "grab" buttons. (Mouse: Right-Click, ViveController: Grab).
* Now you can create music depending on your role.
* **Trees:** Point to the floor and use the "action" button (Mouse: LeftClick, ViveController: Trigger)
* You can move the trees by grabbing the lower part of its cage.
* You can modify the trees by grabbing the upper part of its cage.
* **Paint:** Point towards the wall arround you and use the action button to paint.
* Use the UI to change the parameters of your stroke and paint modes.

## Compatibility:

The Project has been extensively tested on Windows 10 x64 but should also run on MaxOS (With minor modifications)

## Authors

* **Simon Pfaff** - *Concept, Framework and Shaders* - [Bleep-O-Matic](http://www.bleep-o-matic.com)
* **Olav Lervik** - *Concept & Audio Programming* - [Homepage](http://olavlervik.com/)
* **Reto Spoerri** - *Concept & Input & Networking* - [Ludic GmbH](http://www.ludic.ch/)

## License

This project is licensed under the CC BY-SA 4.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Libraries Created dirung the project

* [OpusDotNet]https://github.com/ludos1978/OpusDotNet - Implementation of the Opus Codec in Unity3D
* NetXR Library - A comprehensive framework for VR networked interactions and data transfer in Unity3D - Coming Soon

## Acknowledgments

* [ProceduralToolkit](https://github.com/Syomus/ProceduralToolkit) - An awesome framework for procedural content generation in Unity by Daniil Basmanov. Thanks!
* [InverseKinematics](https://assetstore.unity.com/packages/tools/animation/inverse-kinematics-1829) - An very nice free IK solver by Jose (Dogzerx) Díaz. Thanks!
* [Unity UI Extensions](https://bitbucket.org/UnityUIExtensions/unity-ui-extensions) - This should be standard in Unity. Thanks guys!

